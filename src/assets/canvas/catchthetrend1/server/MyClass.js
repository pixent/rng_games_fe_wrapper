import {CoreLib} from "../../../../../../pslib/src/core/CoreLib";
import {appModel} from "../../../../twlib/models/server/AppModel";









validatePlayButton (btnStr = undefined) {
    this.currentOverText.visible = false;
    let betflag = true;
    if (this.currentOver <= this.runningOver) {
        this.playBtn.visible = false;
        this.updateBtn.visible = false;
        this.navigateNextBtn.visible = false;
        this.currentOverText.visible = true;
        if (this.currentOver == this.runningOver) {
            this.currentOverText.text = CoreLib.Util.getContent("currentOverText");
            CoreLib.UIUtil.updateTextColor(this.currentOverText, "textOrange");
        } else {
            CoreLib.UIUtil.updateTextColor(this.currentOverText, "textLight");
            let betdata = this.checkBetPlacedOver(this.currentOver);
            if (betdata) {
                let newdata = this.getPlayResponseData(this.playResponse, this.currentOver);
                CoreLib.UIUtil.updateTextColor(this.currentOverText, "textLight");
                if (newdata) {
                    this.currentOverText.text = "Points won " + appModel.getPointFinalValue(newdata.points);
                }
            } else {
                this.currentOverText.text = CoreLib.Util.getContent("notPlayed");
            }
        }
    } else {
        let total = this.currentComp.getTotalSelected();
        if (this.checkBetPlacedOver(this.currentOver)) {
            this.playBtn.visible = false;
            this.updateBtn.visible = true;
            this.navigateNextBtn.visible = true;
            this.updateBtn.setEnabled(true);
            this.navigateNextBtn.setEnabled(true);
        } else {
            this.playBtn.setEnabled(true);
            this.playBtn.visible = true;
            this.updateBtn.visible = false;
            this.navigateNextBtn.visible = false;
        }
    }
    if (this.currentOver <= CoreLib.Model.GameConfig.currentOver + 1) {
        this.playBtn.visible = false;
        this.updateBtn.visible = false;
        this.navigateNextBtn.visible = false;
        this.currentComp.disableBetChange();
        betflag = false;
    }
    if(this.currentOver >= 20){
        this.navigateNextBtn.visible = false;
    }
    return betflag;

}









onOverSelection (over, isNavigated = true) {
    if (this.currentOver == over) {
        this.hideOverSelection();
        return;
    }
    if (this.isAnimating) {
        this.onAnimEnd();
    }
    if (isNavigated) {
        this.isUserNavigated = true;
        this.startTimeCheck();
    }
    this.hideOverSelection();
    this.onOverChangeClicked(over - this.currentOver);
}





hideOverSelection () {

}





// ---------------------------------------------

getPlayResponseData (result, over) {
    let data = null;
    if (result) {
        let len = result.length;
        if (len > 0) {
            for (let k = 0; k < len; k++) {
                if (k == over) {
                    data = result[k];
                }
            }
        }
    }
    return data;
}











// ------------------------------------------------------------------
adjustHeight (ht) {
    //this.guideRect.height = ht;
    //this.resizeViewComponents();
}