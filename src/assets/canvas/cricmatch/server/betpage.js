import {LibContainer} from "../../../../../../../../../../pslib/src/pixiwrapper/LibContainer";
import {CoreLib} from "../../../../../../../../../../pslib/src/core/CoreLib";
import {appModel} from "../../../../../../../../twlib/models/server/AppModel";

export class CTTBetViewCompDi extends LibContainer {
    constructor(config) {
        super(config);

        this.guideRect = this.elementsList["guideRect"];
        this.bg = this.elementsList["bg"];

        // let rnd = CoreLib.Util.getRandomRange(1, 5);
        // this.bg.texture = CoreLib.UIUtil.getTexture("betpanelbg" + rnd);

        this.overSelectionNavComp = this.elementsList["overSelectionNavComp"];

        this.currentComp = this.elementsList["page1Comp"];
        this.instructionComp = this.elementsList["instructionComp"];
        this.otherComp = this.elementsList["page2Comp"];
        this.otherComp.visible = false;
        this.overSelectionComp = this.elementsList["overSelectionComp"];
        this.overSelectionComp.on("OVER_SELECTED", this.onOverSelected.bind(this));

        this.currentComp.on("BetSelectionUpdate", this.onSelectionUpdate.bind(this));
        this.otherComp.on("BetSelectionUpdate", this.onSelectionUpdate.bind(this));

        this.mySelectionComp = this.elementsList["mySelectionComp"];
        this.mySelectionComp.visible = false;
        this.mySelectionComp.on("HIDE_MY_SELECTION", this.hideMySelectionComp.bind(this));
        this.mySelectionComp.on("SHOW_MY_SELECTION", this.showMySelectionCompInternal.bind(this));


        this.isUserNavigated = false;
        this.playBtn = this.elementsList["playBtn"];
        this.playBtn.addInteraction(this.onCTTPlayClicked.bind(this));
        this.updateBtn = this.elementsList["updateBtn"];
        this.updateBtn.addInteraction(this.onCTTUpdateClicked.bind(this));
        this.currentOverText = this.elementsList["currentOverText"];
        this.currentOverText.visible = false;
        this.updateBtn.visible = false;

        this.nextBtn = this.elementsList["nextBtn"];
        this.nextBtn.addInteraction(this.onNextClick.bind(this));
        this.prevBtn = this.elementsList["prevBtn"];
        this.prevBtn.addInteraction(this.onPrevClick.bind(this));

        this.firstUpdateDone = false;
        // this.validateBetState(this.betData.bets[this.currentInnings - 1][this.currentOver - 1]);
        this.nextBtn.setEnabled(false);
        this.prevBtn.setEnabled(false);
        this.resetInnings();
        this.updateGameState(0);
        //CoreLib.EventHandler.addEventListener("CTT_UPDATE_SCORE", this.cttUpdateScore.bind(this));
        //CoreLib.EventHandler.addEventListener("Inning_Changed", this.resetInnings.bind(this));



    }

    cttUpdateScore (data) {
        this.gameResultData = data;
        let disable2ndInningBet = false;
        if(this.gameResultData && this.gameResultData.runningScore.inning == 2){
            if(this.gameResultData.runningScore.secondInning.wicketCount >= 9){
                disable2ndInningBet = true;
            }else if((this.gameResultData.runningScore.firstInning.overStat.totalScore - this.gameResultData.runningScore.secondInning.overStat.totalScore) <= 10){
                disable2ndInningBet = true;
            }
        }
        if (disable2ndInningBet) {
            this.playBtn.visible = false;
            this.playBtn.setEnabled(false);
        }
    }
    resetInnings() {
        this.currentSelection = [0,0,0,0];
        this.minOver = 1;
        this.maxOver = 20;
        CoreLib.Model.GameConfig.matchStarted = false;

        this.betData = {};
        this.betData.bets = [];
        const totalOvers = 20;
        for (let i = 0; i < 2; i++) {
            this.betData.bets[i] = [];
            for (let k = 0; k < totalOvers; k++) {
                this.betData.bets[i].push([0,0,0,0]);
            }
        }
        this.pointsArray = [];
        for (let k = 0; k < 20; k++) {
            this.pointsArray.push(-1);
        }
        //this.overSelectionNavComp.updatePageNumber(this.currentOver, this.currentInnings, this.runningOver, this.checkBetPlacedOver(this.currentOver));
        this.currentInnings = 1;
        this.runningOver = 1;
        this.currentOver = 1;


        if (this.instructionComp) {
            this.instructionComp.showInstruction(CoreLib.Model.GameConfig.isCashGame);
        }

        this.hideMySelectionComp();
    }

    onInitialScoreUpdate (data) {
        let result = appModel.getGameInitData();
        let history = result.gameHistory;

        this.playResponse = [];
        this.placedBetData = [];
        if (history) {
            let len = history.length;
            if (len > 0) {
                if (CoreLib.Model.GameConfig.currentInning == history[0].inningNumber) {
                    for (let k = 0; k < len; k++) {
                        let flag = false;
                        if (CoreLib.Model.GameConfig.isCashGame) {
                            if (history[k].betAmount > 0) {
                                flag = true;
                            }
                        } else {
                            if (history[k].betAmount == 0) {
                                flag = true;
                            }
                        }
                        if (flag) {
                            history[k].overNumber++;
                            let newresult = JSON.parse(history[k].result);
                            if (newresult) {
                                if (newresult.runningInnings == 1) {
                                    this.playResponse[history[k].overNumber] = newresult.inning1;
                                } else {
                                    this.playResponse[history[k].overNumber] = newresult.inning2;
                                }
                            }

                        }
                    }
                    this.placedBetData = history;
                    this.placedBetData.sort(this.compareBetData.bind(this));
                }

            }





            if (CoreLib.Model.GameConfig.isCashGame) {
                this.overSelectionComp.showBetPlacedHighlight(this.placedBetData);
                if (this.placedBetData.length > 0) {
                    this.showMySelectionComp(false);

                }
            }
        }
        if (data.overStat.overCount > 0) {
            CoreLib.Model.GameConfig.matchStarted = true;
        }
        this.currentInnings = data.innings;
        if (CoreLib.Model.GameConfig.matchStarted) {
            this.runningOver = CoreLib.Model.GameConfig.currentOver + 1;
            this.currentOver = CoreLib.Model.GameConfig.currentOver + 2;
        } else {
            this.runningOver = 1;
            this.currentOver = 1;
        }
        this.mySelectionComp.updateState(this.runningOver);
        this.updateGameState(1);

    }

    compareBetData(a, b) {
        if (a.overNumber < b.overNumber) {
            return -1;
        }
        if (a.overNumber > b.overNumber) {
            return 1;
        }
        return 0;
    }

    resetAll (data) {
        this.currentInnings = data.innings;
        this.runningOver = CoreLib.Model.GameConfig.currentOver + 1;
        this.currentOver = CoreLib.Model.GameConfig.currentOver + 2;
        //this.overSelectionComp.clearAllBetPlacedData();
        // this.updateGameState(0);
    }

    populatePageData(comp = this.currentComp) {
        comp.populateBetPage(this.currentOver, this.playResponse, this.placedBetData, CoreLib.Model.GameConfig.currentOver);
    }




    updatePlayerBetData (data, isHistory) {
        this.isBetPlaceRequestSent = false;
        this.placedBetData = appModel.getCTTBetSlips();
        this.overSelectionComp.removeTempBetHighlight();
        this.overSelectionComp.showBetPlacedHighlight(this.placedBetData);
        this.currentComp.updateBetPlacedData(this.placedBetData);
        this.onOverChangeUpdate();
        this.setInteractions();
        //
        if (CoreLib.Model.GameConfig.isCashGame) {
            this.mySelectionComp.visible = true;
            this.mySelectionComp.updateState(this.runningOver);
            this.resizeViewComponents();
        }
        // this.updateGameState(1);
    }
    hideMySelectionComp (isPlayDone = false) {
        this.mySelectionComp.visible = false;
        this.resizeViewComponents();

    }
    showMySelectionComp (isHistory = false) {

        if (this.placedBetData) {
            if (!isHistory) {
                this.mySelectionComp.updateState(this.runningOver);
            }
            this.forcedUpdateSent = false;
            this.resizeViewComponents();
        }

    }
    showMySelectionCompInternal() {
        this.mySelectionComp.visible = true;
        this.resizeViewComponents();
    }











    onOverEnd () {
        this.overEnded = true;
        if (this.runningOver + 1 == 20) {
            CoreLib.WrapperService.closeBetPopup();
        }
        if (CoreLib.Model.GameConfig.isCashGame) {
            let flag = this.isBlankBetPlaced(this.runningOver + 1);
            if (flag) {
                let selections = this.getPreviousOverSelection(this.runningOver);
                this.isUserNavigated = true;
                this.startTimeCheck();
                let rect = {};
                rect.y = this.y;
                rect.height = this.guideRect.height - this.overSelectionComp.height - this.overSelectionNavComp.height;
                this.forcedUpdateSent = true;
                CoreLib.WrapperService.sendCTTPlayRequest(selections, this.runningOver + 1, this.currentInnings, true, CoreLib.Model.GameConfig.isCashGame, rect, true);
            }

        } else {
            // let len = this.getPlacedBetDataLength();
            // if (len > 0) {
            //     let flag = this.isBlankBetPlacedForFree(this.runningOver + 1);
            //     if (!flag) {
            //         let selections = this.getPreviousOverSelection(this.runningOver);
            //         this.isUserNavigated = true;
            //         this.startTimeCheck();
            //         let rect = {};
            //         rect.y = this.y;
            //         rect.height = this.guideRect.height - this.overSelectionComp.height - this.overSelectionNavComp.height;
            //         this.forcedUpdateSent = true;
            //         CoreLib.WrapperService.sendCTTPlayRequest(selections, this.runningOver + 1, this.currentInnings, true, CoreLib.Model.GameConfig.isCashGame, rect, true);
            //
            //     }
            //
            // }
        }
        if (this.isBetPlaceRequestSent) {
            if (this.runningOver <= 18) {
                CoreLib.WrapperService.updateOverEnd(this.runningOver + 2);
                if (CoreLib.Model.GameConfig.isCashGame) {
                    //this.overSelectionComp.increamentOverNumberSelection();
                }
            } else {
                CoreLib.WrapperService.closeBetPopup(this.runningOver + 2);
            }

        }
        this.mySelectionComp.updateState(this.runningOver + 1);
        if (CoreLib.Model.GameConfig.isCashGame) {
            this.checkToShowAnyWin();
        }
        this.setInteractions();
        setTimeout(this.onOverStart.bind(this), 500);

    }
    getPlacedBetDataLength () {
        let len = 0;
        if (this.placedBetData) {
            for (let p in this.placedBetData) {
                len++;
            }
        }
        return len;
    }
    checkToShowAnyWin () {
        let group = appModel.getBetOversGroup(this.currentOver);
        let flag = false;
        if (group && group.length > 0) {
            let checkobj = this.getPlayResponseForOver(group[group.length - 1].overNumber);
            if (checkobj) {
                let newarr = checkobj.overs.toString().split(".");
                if (newarr[1] == 6 || newarr[1] == "6") {
                    flag = true;
                }
            }
        }

        if (flag) {
            let len = group.length;
            let reqObj = {};
            reqObj.totalOversPlayed = len;
            reqObj.totalWin = 0;
            reqObj.oversWon = 0;
            reqObj.correctSelections = 0;
            if (len > 0) {
                for (let k = 0; k < len; k++) {
                    let obj = this.getPlayResponseForOver(group[k].overNumber);
                    reqObj.correctSelections = obj.winningScores.length;
                    if (obj) {
                        if (obj.winAmount > 0) {
                            reqObj.oversWon++;
                            reqObj.totalWin += obj.winAmount;
                        }
                    }
                }
            }
            if (reqObj.totalOversPlayed > 0) {
                CoreLib.WrapperService.showMatchEndPopup(reqObj);
            }
        }

    }


    getBetSlipText () {
        let obj = this.getPlayResponseData();
        if (obj && this.currentOver < this.runningOver) {
            let arr = [];
            arr.push(obj.winningScores.length);
            return CoreLib.Util.parseMessage(CoreLib.Util.getContent("selctionCorrect"), arr);

        } else {
            if (CoreLib.Model.GameConfig.isCashGame) {
                return CoreLib.Util.getContent("viewBetslip")
            } else {
                return CoreLib.Util.getContent("viewBetslipFree")
            }

        }
    }


    updateCurrentOverText () {
        if (this.isBetPlacedForThisOver() && ((this.currentOver > this.runningOver) || this.currentOver == 1)) {
            if (CoreLib.Model.GameConfig.isCTT) {
                this.currentOverText.visible = false;
                return;
            } else {
                this.currentOverText.text = CoreLib.Util.getContent("SelectionsEntered");
                CoreLib.UIUtil.updateTextColor(this.currentOverText, "secondaryLight");
                this.currentOverText.visible = true;
                return;
            }

        }
        if (this.currentOver == this.runningOver && CoreLib.Model.GameConfig.currentBall < 6) {
            this.currentOverText.text = CoreLib.Util.getContent("currentOverText");
            CoreLib.UIUtil.updateTextColor(this.currentOverText, "secondaryLight");
        } else {
            CoreLib.UIUtil.updateTextColor(this.currentOverText, "ColorWhite");
            let newdata = this.getPlayResponseData();

            CoreLib.UIUtil.updateTextColor(this.currentOverText, "ColorWhite");
            if (newdata) {
                if (CoreLib.Model.GameConfig.isCashGame) {
                    this.currentOverText.text = "Cash won " + CoreLib.WrapperService.formatCurrency(newdata.winAmount != undefined ? newdata.winAmount : 0, 0);
                } else {
                    this.currentOverText.text = "Points won " + (newdata.points != undefined ? newdata.points : 0);
                }

            } else {
                this.currentOverText.text = CoreLib.Util.getContent("notPlayed");
            }
        }
        if (this.currentOver <= this.runningOver && CoreLib.Model.GameConfig.matchStarted) {
            this.currentOverText.visible = true;
        } else {
            this.currentOverText.visible = false;
        }
    }




    // ---------------------------------------
    onCTTPlayClicked () {
        this.isUserNavigated = true;
        this.startTimeCheck();
        let isUpdate = this.checkAlreadyBetSent(this.currentOver);
        this.isBetPlaceRequestSent = true;
        let rect = {};
        rect.y = this.y + this.parent.y;
        rect.height = this.guideRect.height - this.overSelectionComp.height - this.overSelectionNavComp.height;
        CoreLib.WrapperService.sendCTTPlayRequest(this.currentSelection, this.currentOver, this.currentInnings, isUpdate, CoreLib.Model.GameConfig.isCashGame, rect);
    }
    onCTTUpdateClicked () {
        let group = appModel.getBetOversGroup(this.currentOver);
        let overs = [];
        let len = group.length;
        let selections = [];
        let winAmount = null;
        let winningScores = null;
        let nonWinningScores = null;
        let obj = this.getPlayResponseForOver(group[group.length - 1].overNumber);
        let obj1 = this.getPlayResponseForOver(this.currentOver);
        if (obj1) {
            let newarr = obj1.overs.toString().split(".");
            if (newarr[1] == 6 || newarr[1] == "6") {
                winningScores = obj1.winningScores;
                nonWinningScores = obj1.nonWinningScores;
            }

        }
        if (obj) {
            let newarr = obj.overs.toString().split(".");
            if (newarr[1] == 6 || newarr[1] == "6") {
                if (CoreLib.Model.GameConfig.isCashGame) {
                    winAmount = obj.winAmount;
                } else {
                    winAmount = obj.points;
                }
                winningScores = obj.winningScores;
                nonWinningScores = obj.nonWinningScores;
            }
        }
        for (let k = 0; k< len; k++) {
            //if (group[k].overNumber == this.currentOver) {

            //}
            selections.push(group[k].selections);
            overs.push(group[k].overNumber);
        }

        CoreLib.WrapperService.showVCTTBetSlip(overs, this.currentOver, selections, group[0].betAmount, winAmount, winningScores, nonWinningScores);
    }

    onSelectionUpdate (arr) {
        this.currentSelection = arr;

    }
    onOverSelected (over, isNavigated = false) {
        if (this.currentOver == over) {
            return;
        }
        if (this.isAnimating) {
            //this.onAnimEnd();
        }
        if (isNavigated) {
            this.startTimeCheck();
        }
        this.onOverChangeClicked(over - this.currentOver);
    }
    onNavigateNextClicked () {
        this.onNextClick();
    }
    onNextClick () {
        if (this.isAnimating) {
            this.onAnimEnd();
        }
        this.isUserNavigated = true;
        this.startTimeCheck();
        this.onOverChangeClicked(1);
    }

    onPrevClick () {
        if (this.isAnimating) {
            this.onAnimEnd();
        }
        this.isUserNavigated = true;
        this.startTimeCheck();
        this.onOverChangeClicked(-1);
    }

    onOverChangeClicked (increament) {
        this.currentOver += increament;
        if (this.currentOver > 20) {
            this.currentOver = 20;
        }

        this.otherComp.visible = true;
        this.isAnimating = true;

        this.populatePageData(this.otherComp);
        if (increament > 0) {
            this.otherComp.x = this.guideRect.width;
            CoreLib.AnimationManager.animateTween(this.currentComp, 0.5, {x : -this.guideRect.width * 1.25, ease : Power2.easeOut});
            CoreLib.AnimationManager.animateTween(this.otherComp, 0.5, {x : this.finalX, ease : Power2.easeOut, onComplete : this.onAnimEnd.bind(this)});
        } else {
            this.otherComp.x = -this.guideRect.width;
            CoreLib.AnimationManager.animateTween(this.currentComp, 0.5, {x : this.guideRect.width * 1.25, ease : Power2.easeOut});
            CoreLib.AnimationManager.animateTween(this.otherComp, 0.5, {x : this.finalX, ease : Power2.easeOut, onComplete : this.onAnimEnd.bind(this)});
        }
        this.onOverChangeUpdate();
        this.setInteractions();

    }
    onAnimEnd () {
        CoreLib.AnimationManager.killTweensOf(this.currentComp);
        CoreLib.AnimationManager.killTweensOf(this.otherComp);
        this.otherComp.x = this.finalX;
        this.currentComp.x = -this.guideRect.width;
        let tempcomp = this.currentComp;
        this.currentComp = this.otherComp;
        this.otherComp = tempcomp;
        this.isAnimating = false;
    }


    startTimeCheck () {
        this.isUserNavigated = true;
        clearTimeout(this.timerid);
        this.timerid = setTimeout(this.enableNavigation.bind(this), 30000);
    }
    enableNavigation () {
        this.isUserNavigated = false;
    }




    betPlacingSelectedOvers (data) {
        if (CoreLib.Model.GameConfig.isCashGame) {
            this.overSelectionComp.showTempBetHighlight(this.currentOver, data.overs);
        }
    }
    betPlacingSelectedOversClosed () {
        this.overSelectionComp.removeTempBetHighlight();

    }

    getPreviousOverSelection (over) {
        let selections = [0,0,0,0];
        let obj = appModel.getBetDataForOver(over);
        if (obj) {
            selections = obj.selections;
        }
        return selections;
    }

    getCurrentInningBetData () {
        return appModel.getCurrentInningBetData();

    }
    isBlankBetPlaced (over) {
        let flag = false;
        let obj = appModel.getBetDataForOver(over);
        if (obj) {
            let total = 0;
            let len2 = obj.selections.length;
            for (let i = 0; i < len2; i++) {
                total += Number(obj.selections[i]);
            }
            if (total == 0) {
                flag = true;
            }
        }
        return flag;
    }
    isBlankBetPlacedForFree (over) {
        let flag = false;
        let obj = appModel.getBetDataForOver(over);
        if (obj) {
            let total = 0;
            let len2 = obj.selections.length;
            for (let i = 0; i < len2; i++) {
                total += Number(obj.selections[i]);
            }
            if (total == 0) {
                flag = true;
            }
        }
        return flag;
    }



    // getter methods
    getPlayResponseData () {
        let result = this.playResponse;
        let data = null;
        for (let p in result) {
            if (this.currentOver == p) {
                data = result[p];
                break;
            }
        }
        return data;
    }
    checkAlreadyBetSent(over) {
        let flag = false;
        let obj = appModel.getBetDataForOver(over);
        if (obj) {
            flag = true;
        }
        return flag;
    }

    checkBetPlacedOver (over) {
        let flag = false;
        let obj = appModel.getBetDataForOver(over);
        if (obj) {
            flag = true;
        }
        return flag;
    }
    getPointsArray () {
        this.pointsArray = [];
        const totalOvers = 20;
        for (let k = 1; k <= totalOvers; k++) {
            if (this.playResponse) {

                if (this.playResponse[k]) {
                    if (CoreLib.Model.GameConfig.isCashGame) {
                        this.pointsArray[k - 1] = this.playResponse[k].winAmount;
                    } else {
                        this.pointsArray[k - 1] = this.playResponse[k].points;
                    }

                } else {
                    if (k < this.runningOver) {
                        this.pointsArray[k - 1] = 0;
                    } else {
                        this.pointsArray[k - 1] = -1;
                    }
                }
            } else {
                if (k < this.runningOver) {
                    this.pointsArray[k - 1] = 0;
                } else {
                    this.pointsArray[k - 1] = -1;
                }
            }
        }
        return this.pointsArray;
    }
    getPlayResponseForOver(over) {
        let obj = null;
        if (this.playResponse) {
            for (let p in this.playResponse) {
                if (p == over) {
                    obj = this.playResponse[p];
                }
            }
        }
        return obj;

    }



    resizeViewComponents(layoutData = null) {
        super.resizeViewComponents(layoutData);
        if (this.currentComp) {
            this.guideRect.width = CoreLib.UIUtil.getGameWidth();

            CoreLib.UIUtil.setPositionX(this.overSelectionComp, 0);
            CoreLib.UIUtil.setPositionY(this.overSelectionComp, 0);

            CoreLib.UIUtil.alignCompHScreen(this.mySelectionComp, "left", this.guideRect);
            CoreLib.UIUtil.setPositionY(this.mySelectionComp, this.overSelectionComp.height);

            CoreLib.UIUtil.setPositionX(this.overSelectionNavComp, 0);

            if (this.mySelectionComp.visible) {
                CoreLib.UIUtil.setPositionY(this.overSelectionNavComp, this.mySelectionComp.y + this.mySelectionComp.height);
            } else {
                CoreLib.UIUtil.setPositionY(this.overSelectionNavComp, this.overSelectionComp.y + this.overSelectionComp.height);
            }


            if (this.instructionComp) {
                CoreLib.UIUtil.scaleObjectWithRef(this.instructionComp, this.guideRect, 0.8);
                CoreLib.UIUtil.setPositionX(this.instructionComp, (this.guideRect.width - this.instructionComp.width) / 2);
                CoreLib.UIUtil.setPositionY(this.instructionComp, this.overSelectionNavComp.y + this.overSelectionNavComp.height * 1.25);
            }

            this.currentComp.resizeCustom(this.guideRect.width * 0.9);
            this.otherComp.resizeCustom(this.guideRect.width * 0.9);

            // let sc = CoreLib.UIUtil.getGameWidth() * 0.9 / this.currentComp.width;
            // this.currentComp.scale.set(sc);

            CoreLib.UIUtil.setPositionX(this.currentComp, (this.guideRect.width - this.currentComp.width) / 2);
            if (this.instructionComp && this.instructionComp.visible) {
                CoreLib.UIUtil.setPositionY(this.currentComp, this.instructionComp.y + this.instructionComp.height * 1.1);
            } else {
                CoreLib.UIUtil.setPositionY(this.currentComp, this.overSelectionNavComp.y + this.overSelectionNavComp.height * 1.5);
            }

            this.otherComp.scale.set(this.currentComp.scale.x);
            this.otherComp.y = this.currentComp.y;
            this.finalX = this.currentComp.x;

            CoreLib.UIUtil.alignCompHScreen(this.nextBtn, "right", this.guideRect);
            CoreLib.UIUtil.alignCompHScreen(this.prevBtn, "left", this.guideRect);
            CoreLib.UIUtil.setPositionY(this.nextBtn, this.currentComp.y + (this.currentComp.height - this.nextBtn.height) / 2);
            CoreLib.UIUtil.setPositionY(this.prevBtn, this.nextBtn.y);




            this.playBtn.scale.set(1);
            let sc = (this.guideRect.width * 0.8) / this.playBtn.width;
            this.playBtn.scale.set(sc);
            this.updateBtn.scale.set(sc);
            CoreLib.UIUtil.alignCompHScreen(this.playBtn, "center", this.guideRect);
            this.playBtn.y = this.currentComp.y + this.currentComp.height + 40;
            CoreLib.UIUtil.setPositionX(this.updateBtn, this.playBtn.x);
            CoreLib.UIUtil.setPositionY(this.updateBtn, this.playBtn.y);

            CoreLib.UIUtil.setPositionX(this.currentOverText, this.guideRect.width / 2);
            CoreLib.UIUtil.setPositionY(this.currentOverText, this.playBtn.y + this.playBtn.height / 2);


            this.guideRect.height = this.playBtn.y + this.playBtn.height + 40;

            this.bg.height = Math.abs(this.guideRect.height);
            this.bg.scale.x = this.bg.scale.y;
            if (this.bg.width < this.guideRect.width) {
                this.bg.width = this.guideRect.width;
                this.bg.scale.y = this.bg.scale.x;
            }
            CoreLib.UIUtil.alignCompHScreen(this.bg, "center", this.guideRect);

            CoreLib.EventHandler.dispatchEvent("FORCE_RESIZE_COMPONENT");

        }

    }

}
