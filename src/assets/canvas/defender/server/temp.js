import {LibContainer} from "../../../../../../../../../pslib/src/pixiwrapper/LibContainer";
import {CoreLib} from "../../../../../../../../../pslib/src/core/CoreLib";
import {TWPreloaderComp} from "../../../../../../../twlib/views/comp/TWPreloaderComp";
import {FWPOversMarker} from "./FWPOversMarker";
import { FWPRunMarker } from './FWPRunMarker'
import { Point } from 'pixi'


let totalMarkers = 30;
export class FWPRunsComp extends LibContainer {
    constructor(config) {
        super(config);
        this.guideRect = this.elementsList["guideRect"];
        this.bgRect = this.elementsList["bgRect"];
        this.maskRect = this.elementsList["maskRect"];
        this.multiLine = this.elementsList["multiLine"];
        this.indicator = this.elementsList["indicator"];
        //this.guideRect.alpha = 0;
        //this.bgRect.alpha = 0;
        this.markersArray = [];
        this.totalWidth = this.guideRect.width;
        this.eContainer = CoreLib.UIUtil.getContainer();
        this.addChild(this.eContainer);
        this.eContainer.mask = this.maskRect;

        this.markerGaps = [];
        for (let k = 0; k < totalMarkers; k++) {
            let marker = new FWPRunMarker(k);
            this.eContainer.addChild(marker);
            marker.x = 24;
            marker.y = this.bgRect.height - marker.height * (k + 1);
            marker.setData(k);
            this.markersArray.push(marker);
        }
        this.compHeight = this.markersArray[0].tText.height;
        //this.eContainer.mask = this.maskRect;
        this.maskRect.visible = false;

        this.ball = CoreLib.UIUtil.getRectangle(40,40, 0xff0000);
        this.addChild(this.ball);
    }

    checkMousePoisition () {
        var interactionManager = CoreLib.View.getPixiRenderer().plugins.interaction.mouse.global.x;
        console.log(interactionManager)
    }

    onMouseMove (e) {
        console.log("mouse move ")
    }

    setHeight (ht) {
        this.guideRect.height = ht;
        this.bgRect.height = ht;
        this.bgRect.y = 0;
        this.totalHeight = this.bgRect.height;
        this.maskRect.width = this.bgRect.width;
        this.maskRect.height = this.guideRect.height;
        this.maskRect.x = this.bgRect.x;
        this.maskRect.y = this.guideRect.y;

    }
    startGame (val) {

        this.elapsedFactor = 1;
        this.initialToShow = 6;
        this.runsGap = 10;
        this.currentValue = 1;
        this.currentRun = 1;


        this.markerGap = this.totalHeight / this.currentRun;
        this.positionMarkers(true);

    }

    positionMarkers() {
        for (let k = 0; k < totalMarkers; k++) {
            let val = k * this.runsGap;
            this.markersArray[k].setData(val);
            let yyyy = val * this.markerGap + this.compHeight;
            this.markersArray[k].setIndividualHeight(this.markerGap * this.runsGap);
            this.markersArray[k].y = this.bgRect.height - yyyy;

        }
        let yy = this.currentRun * this.markerGap + this.compHeight;
        this.multiLine.y = this.bgRect.height - this.multiLine.height - yy;
        this.indicator.y = this.multiLine.y + (this.multiLine.height - this.indicator.height) / 2;

    }

    updateComp (multi, ballPos) {
        this.currentRun = multi;
        this.ball.x = ballPos.x;
        this.ball.y = ballPos.y;
        return;

        this.currentValue++;
        //this.markerGap -= 0.02;
        this.markerGap = (this.totalHeight * 0.75) / this.currentRun;
        if (this.currentRun > 0 && this.currentRun <= 5) {
            this.runsGap = 1;
        } else if (this.currentRun > 5 && this.currentRun <= 10) {
            this.runsGap = 2;
        } else if (this.currentRun > 10  && this.currentRun < 20) {
            this.runsGap = 5;
        } else if (this.currentRun > 20 && this.currentRun < 30) {
            this.runsGap = 10;
        } else if (this.currentRun > 30 && this.currentRun < 50) {
            this.runsGap = 15;
        } else {
            this.runsGap = 20;
        }
        this.positionMarkers();


    }

    endGame () {

    }
    cleanUpOldRound () {

    }




    onAllPositionChanged () {
        this.isAnimationUpdate = false;
    }



    getTimeCheckIndex (time) {
        let index = -1;
        let len = this.timeChecksIntervals.length;
        for (let k = 0; k < len; k++) {
            if (this.timeChecksIntervals[k].time == time) {
                index = k;
                break;
            }
        }
        return index;
    }


    resizeViewComponents(layoutData = null) {
        super.resizeViewComponents(layoutData);
        this.guideRect.width = CoreLib.UIUtil.getGameWidth();
        this.bgRect.x = CoreLib.UIUtil.getGameWidth() - this.bgRect.width;
        this.maskRect.x = this.bgRect.x;
        this.multiLine.scale.set(1);
        let sc = (this.guideRect.width) / this.multiLine.width;
        this.indicator.scale.set(0.5);

        this.multiLine.scale.set(sc);
        this.multiLine.x = 0;
        this.indicator.x = this.guideRect.width - this.indicator.width;
        this.eContainer.x = this.bgRect.x;
        //this.guideRect.width = CoreLib.UIUtil.getGameWidth();
    }

}
