import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ConfigService } from './config.service';
@Injectable({
  providedIn: 'root'
})
export class PenaltyShootoutService {

  playerDetails = new BehaviorSubject<PlayerDetails>({playerId:''});
  playerDetails$ = this.playerDetails.asObservable();

  socketConnected = new  BehaviorSubject<boolean>(false);
  socketConnected$ = this.socketConnected.asObservable();

  constructor(private http:HttpClient, private configService: ConfigService) { }

  setPlayerDetails(details:PlayerDetails)
  {
    this.playerDetails.next(details);
    console.warn(details)
  }

  setSocketconnected(val:boolean)
  {
    this.socketConnected.next(val)
  }

  setUsername(username:string)
  {
    let body = new FormData();
    body.append('playerId',this.playerDetails.value.playerId);
    body.append('playerName',username)
    return this.http.post(this.configService.configData.url.socket+'updatePlayer',body)
  }
}

interface PlayerDetails
{
  playerId:string;
}