import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SocketService } from './socket.service';

@Injectable()
export class FwpService {

    messageSubscription: any;
    messageSubject = new Subject();

    constructor(public socketService: SocketService) { 
        this.messageSubscription = this.socketService.messageSubject.subscribe((message) => {
        this.emitMessage(message);
      });
    }
    
    emitMessage(message: any) {
        this.messageSubject.next(message);
    }
}
