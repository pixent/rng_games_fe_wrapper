import { Injectable, APP_INITIALIZER } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class ConfigService {
    configData: any;
    textConfigData: any = {};

    constructor(private http: HttpClient) { }

    loadConfig() {
        return new Promise((resolve, reject) => {
            this.http.get('./assets/config/config.json')
                .subscribe((data: any) => {
                    this.configData = data['config'];
                    this.configData.isRGSSetup = false;
                    let url = window.location.href.split('/');
                    let urlStr = window.location.href.split('?')[1];
                    let urlParams: any = {};
                    if (urlStr) {
                        let params = urlStr.split("&");
                        for (let p in params) {
                            let param = params[p].split('=');
                            if (params[params.length - 1]) {
                                urlParams[param[0]] = param[1].split('#')[0];
                            } else {
                                urlParams[param[0]] = param[1];
                            }
                        }
                        if (urlParams['languagecode'] || urlParams['languageCode']) {
                            this.configData.languageCode = urlParams['languagecode'] || urlParams['languageCode'];
                        }
                        if (urlParams['playerName']) {
                            this.configData.playerName = urlParams['playerName'];
                        }
                        if (urlParams.platformId == "2") {
                            if (urlParams.languageCode) {
                                this.configData.languageCode = urlParams.userCulture ? urlParams.userCulture.split('-')[0] : urlParams.userCulture;
                                this.configData.token = urlParams.token;
                                this.configData.playerId = urlParams.playerId;
                            } else {
                                this.configData.languageCode = urlParams.languageCode.split("-")[0];
                            }
                        }
                        if (urlParams.playerId) {
                            this.configData.playerId = urlParams.playerId;
                        }
                        if (urlParams['currencycode']) {
                            if (this.configData.url.currencies[urlParams['currencycode']]) {
                                this.configData.currencyCode = this.configData.url.currencies[urlParams['currencycode']];
                            } else {
                                this.configData.currencyCode = urlParams['currencycode'].toUpperCase();
                            }
                        }
                        if (urlParams['currencyCode']) {
                            this.configData.currencyCode = urlParams['currencyCode'];
                        }
                        if (urlParams['standalone']) {
                            this.configData.standalone = urlParams['standalone'];
                        }
                        console.log('isStandalone ', urlParams['standalone'])
                        this.configData.platformId = urlParams.platformId || "0";
                        this.configData.operatorId = urlParams.operatorId || "demo";
                        this.configData.brand = urlParams.brand || "demo";
                        this.configData.token = urlParams.token;
                        if (this.configData.platformId == "2" && this.configData.currencyCode == "undefined") {
                            this.configData.currencyCode = "KSH";
                        }
                        this.configData.gameType = urlParams.gameType;
                        this.configData.isFreeGame = urlParams.isFreeGame || false;

                    } else {
                        this.configData.platformId = "0";
                        this.configData.operatorId = "demo";
                        this.configData.brand = "demo";
                    }
                    // this.configData.platformId = "2";
                    if (window.location.href.indexOf("localhost") >= 0) {
                        // this.configData.url.rgs = this.configData.url.devDefenderSocketUrl;
                        this.configData.url.apiUrl = this.configData.url.rgs + "truewave";
                        this.configData.url.canvas = this.configData.url.localcanvas;
                        this.configData.url.socket = this.configData.url.rgs + this.configData.url.socket + this.configData.url.socket;
                        this.configData.isRGSSetup = true;
                        console.log({urlParams, urlStr})
                    } else if (window.location.href.indexOf("https://dev-games-rgs.sportsit-tech.net/lobby") >= 0) {
                        this.configData.url.apiUrl = this.configData.url.dev;
                        this.configData.url.canvas = window.location.origin + '/games/games/' + this.configData.url.canvas;
                        this.configData.url.socket = this.configData.url.dev + this.configData.url.socket;
                    } else if (window.location.href.indexOf("https://dev-games-rgs.sportsit-tech.net/") >= 0) {
                        // this.configData.url.rgs = this.configData.url.devDefenderSocketUrl;
                        this.configData.url.apiUrl = this.configData.url.rgs + "truewave";
                        this.configData.url.canvas = window.location.origin + '/games/games/' + this.configData.url.canvas;
                        this.configData.url.socket = this.configData.url.rgs + this.configData.url.socket + this.configData.url.socket;
                        console.log('--------------------------------------------------')
                        console.log({urlParams: urlParams})
                        console.log('--------------------------------------------------')
                        // if(urlParams["gameName"] === "defender"){
                        //     this.configData.url.socket = this.configData.url.devDefenderSocketUrl;
                        // }
                        this.configData.isRGSSetup = true;
                    } else if (window.location.href.indexOf("https://games-rgs.sportsit-tech.net/") >= 0) {
                        this.configData.url.apiUrl = this.configData.url.prodrgs + "truewave";
                        this.configData.url.canvas = window.location.origin + '/games/games/' + this.configData.url.canvas;
                        this.configData.url.socket = this.configData.url.prodrgs + this.configData.url.socket + this.configData.url.socket;
                        this.configData.isRGSSetup = true;
                    } else if (window.location.href.indexOf("https://stg-games-rgs.sportsit-tech.net/") >= 0) {
                        this.configData.url.apiUrl = this.configData.url.stgrgs + "truewave";
                        this.configData.url.canvas = window.location.origin + '/games/games/' + this.configData.url.canvas;
                        this.configData.url.socket = this.configData.url.stgrgs + this.configData.url.socket + this.configData.url.socket;
                        this.configData.isRGSSetup = true;
                    } else {
                        if (window.location.href.indexOf("sp-twgames-web.sportsit-tech.net") >= 0) {
                            this.configData.platformId = 1;
                            this.configData.operatorId = "starpunter";
                            this.configData.brand = "starpunter";
                        }
                        this.configData.url.canvas = window.location.origin + '/' + this.configData.url.canvas;
                        this.configData.url.socket = window.location.origin + '/' + this.configData.url.socket;
                        this.configData.url.apiUrl = window.location.origin;
                        if (window.location.href.indexOf("demo.games.sportsit-tech.net") >= 0) {
                            this.configData.url.socket = this.configData.url.dev + 'truewave/';
                            this.configData.url.lobbyUrl = this.configData.url.dev + 'truewave/';
                        }
                    }
                    // this.configData.platformId = "2";
                    // this.configData.operatorId = "paribet";
                    // this.configData.currencyCode = "XAF";
                    if (window.location.href.indexOf("10bet") >= 0) {
                        this.configData.psFreePlayNotAvailable = true;
                        this.configData.gameType = "CASH";
                        let params: any = document.cookie;
                        if (params) {
                            params = params.split("; ");
                            for (let i = 0; i < params.length; i++) {
                                let arr = params[i].split("=");
                                if (arr[0] == "UserCulture") {
                                    this.configData.languageCode = arr[1] ? arr[1].split('-')[0] : arr[1];
                                }
                            }
                        }
                    }

                    if(urlParams['userRegion']){
                        this.configData.url.userRegion = urlParams['userRegion'];
                    } else {
                        this.configData.url.userRegion = this.configData.url.regions[window.location.origin] || "IN";
                    }

                    console.log("configdata userRegion ", this.configData.url.userRegion);

                    if (this.configData.platformId == "4") {
                        this.configData.url.userRegion = "US";
                    }
                    if (this.configData.operatorId == "playbigapp") {
                        this.configData.psCashPlayNotAvailable = true;
                        this.configData.psFreePlayNotAvailable = true;
                    }
                    if (this.configData.platformId == "2") {                      

                        if(this.configData.operatorId == "betyetu"){
                            this.configData.url.userRegion = urlParams.userCulture ? urlParams.userCulture.split('-')[1] : "PT";
                        } else {
                            this.configData.url.userRegion = "KE";
                        }

                        if (this.configData.operatorId == "betlion") {
                            this.configData.url.userRegion = "KE";
                            this.configData.psFreePlayNotAvailable = true;
                        }
                        if (this.configData.operatorId == "betlionzm") {
                            this.configData.url.userRegion = "ZM";
                            this.configData.psFreePlayNotAvailable = true;
                        }

                        if (this.configData.operatorId == "paribet") {
                            this.configData.psFreePlayNotAvailable = true;
                        }
                    }
                    if (this.configData.platformId == "1" && (this.configData.operatorId == "pixwin")) {
                        this.configData.currencyCode = "BRL";
                    }
                    this.configData.url.parentRegion = this.configData.url.parentRegions[window.location.origin];

                    // console.warn("configData url ", this.configData.url);
                    // console.warn("configData operatorId ", this.configData.operatorId);
                    this.configData.betsData = this.configData.bets;
                    if (this.configData.url.userRegion == "IN" && this.configData.operatorId != "starbet" && this.configData.operatorId != "everymatrix"
                        && this.configData.operatorId != "yangasport" && this.configData.operatorId != "playbigapp" && this.configData.operatorId.indexOf("hub88") < 0) {
                        this.configData.bets = this.configData.bets["default"];
                    } else if(this.configData.operatorId == "paribet" || this.configData.operatorId == "paribetug" || this.configData.operatorId == "hub88" || this.configData.operatorId == "hub88eu" || this.configData.operatorId == "hub88as"){
                        this.configData.bets = this.configData.bets[this.configData.currencyCode];
                        console.warn("paribet bets ", this.configData.bets)
                    }
                    else {
                        this.configData.bets = this.configData.bets[this.configData.url.userRegion + "_" + this.configData.operatorId.toUpperCase() + "_" + this.configData.currencyCode] || this.configData.bets[this.configData.currencyCode] || this.configData.bets["default"];
                    }
                    if (this.configData.bets) {
                        for (let i = 0; i < this.configData.bets.betsArray.length; i++) {
                            this.configData.bets.betsArray[i] = this.configData.bets.betsArray[i] * this.configData.bets.multiplier;
                        }
                    }

                    // operator separation
                    if (this.configData.defender.operatorSeparation && this.configData.defender.operatorSeparation.indexOf(this.configData.operatorId) < 0) {
                        this.configData.defender.operatorSeparation.push(this.configData.operatorId);
                    }

                    // defender hide exit button

                    if (urlParams["hideBackButton"]) {
                        this.configData.defenderHideExitButton[this.configData.operatorId] = true;
                    }

                    // get text configuration
                    const language = this.configData.languageCode || "en";
                    const jurisdiction = this.configData.url.userRegion.toLowerCase();
                    let jsonFileName = 'textConfig';

                    console.warn("this.configData ", this.configData.operatorId)
                    if (this.configData.operatorId == "paribet" || this.configData.operatorId == "betyetu" || this.configData.operatorId == "everymatrix") {
                        jsonFileName = `${this.configData.operatorId}DefenderConfig`;
                    }

                    // if (this.configData.operatorId == "playbigapp") {
                    //     this.configData.currencyCode = "";
                    // }

                    this.http.get(`./assets/config/${jurisdiction}/${language}/${jsonFileName}.json`)
                        .subscribe((data: any) => {
                            // console.warn("data from config ", data);
                            this.textConfigData = data;
                            resolve(true);
                        }, (err: any) => {
                            this.http.get(`./assets/config/in/en/demoDefenderConfig.json`)
                                .subscribe((data: any) => {
                                    // console.warn("data from config ", data);
                                    this.textConfigData = data;
                                    resolve(true);
                                })
                        });
                }, (err: any) => {
                    console.log(err);
                });
        });
    }

    getConfig() {
        return this.loadConfig();
    }
}

export function ConfigFactory(config: ConfigService) {
    return () => config.loadConfig();
}

export function init() {
    return {
        provide: APP_INITIALIZER,
        useFactory: ConfigFactory,
        deps: [ConfigService],
        multi: true
    }
}

const ConfigModule = {
    init: init
}

export { ConfigModule };