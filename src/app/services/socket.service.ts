import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ConfigService } from './config.service';
declare var SockJS: any;
declare var Stomp: any;

@Injectable()
export class SocketService {


    public stompClient: any;
    clientId: any;
    socketEndPointForInit: any;
    socketStatus: any;
    gameType: any;
    matchId: any;
    connectionObj: any;
    playerBetSubscribed: boolean = false;

    messageSubject = new Subject();


    constructor(private configService: ConfigService) {
        // this.clientId = this.getClientId(1000, 9999);
    }


    connectToServer(url: string, gameType: string, playerId: string, eventName: string) {
        this.clientId = this.getClientId(1000, 9999);
        this.socketEndPointForInit = this.clientId;
        let socket = new SockJS(url);
        this.stompClient = Stomp.over(socket);
        this.stompClient.debug = true;
        this.stompClient.heartbeat.outgoing = 20000;
        // console.log("connect to socket == " + url, gameType, playerId, eventName)
        this.gameType = gameType;
        this.connectionObj = {
            url: url,
            gameType: gameType,
            playerId: playerId,
            eventName: eventName
        }
        this.stompClient.connect({}, (frame: any) => {
            // subscribe message handlers here

            if (this.gameType == 'firstWicketPartnership') {
                if (this.configService.configData.url.isCertification) {
                    this.stompClient.subscribe('/client/fwp/' + this.configService.configData.operatorId + "/gameEvent", (data: { body: string; }) => {
                        this.onGameEventResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chat', (data: { body: string; }) => {
                        this.onFwpChat(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chatHistory', (data: { body: string; }) => {
                        this.onLastTenChatHistory(JSON.parse(data.body));
                    });
                } else {

                    this.stompClient.subscribe('/client/bet/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpBetSessionResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/init/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onAuthenticateResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/balance/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onBalanceResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/cashOut/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpCashout(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/playerBets/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onGameInitResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/last20/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onLastTwentyGamesResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/fwpData/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpDataResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/firstWicket/score/' + eventName, (data: { body: string; }) => {
                        this.onFwpScore(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/firstWicket/result/' + eventName, (data: { body: string; }) => {
                        this.onFwpResult(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/fwp/chat', (data: { body: string; }) => {
                        this.onFwpChat(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/fwp/chatHistory', (data: { body: string; }) => {
                        this.onLastTenChatHistory(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/firstWicket/newMatch/' + ((this.configService.configData.defender.operatorSeparation.indexOf(this.configService.configData.operatorId)>=0) ? this.configService.configData.operatorId : "default"), (data: { body: string; }) => {
                        this.stompClient.subscribe('/client/firstWicket/score/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onFwpScore(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/firstWicket/result/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onFwpResult(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/fwpBet/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onAllFwpBet(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/fwpCashOut/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onAllFwpCashOut(JSON.parse(data.body));
                        });
                        this.onFwpNewMatch(JSON.parse(data.body));
                    });
                }
            } else if (this.gameType == 'defender') {
                if (this.configService.configData.url.isCertification) {
                    // let sessionId = this.getUUid();
                    // let newGameObj = {
                    //     "eventType": "NEW_GAME", 
                    //     "timestamp": new Date().getTime(),
                    //     "content": {
                    //         "id": sessionId,
                    //         "gameStartDelay": 10 
                    //     }
                    // };
                    this.stompClient.subscribe('/client/defender/' + this.configService.configData.operatorId + "/gameEvent", (data: { body: string; }) => {
                        this.onGameEventResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chat', (data: { body: string; }) => {
                        this.onFwpChat(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chatHistory', (data: { body: string; }) => {
                        this.onLastTenChatHistory(JSON.parse(data.body));
                    });
                    // if (window.location.href.indexOf("https://games-rgs.sportsit-tech.net/")>=0) {
                    //     this.stompClient.subscribe('/client/defender/' + this.configService.configData.operatorId + "/gameEvent", (data: { body: string; }) => {
                    //         this.onGameEventResponse(JSON.parse(data.body));
                    //     });
                    //     this.stompClient.subscribe('/client/chat', (data: { body: string; }) => {
                    //         this.onFwpChat(JSON.parse(data.body));
                    //     });
                    //     this.stompClient.subscribe('/client/chatHistory', (data: { body: string; }) => {
                    //         this.onLastTenChatHistory(JSON.parse(data.body));
                    //     });
                    // } else {
                    //     this.stompClient.subscribe('/topic/client.defender.' + this.configService.configData.operatorId + ".gameEvent", (data: { body: string; }) => {
                    //         this.onGameEventResponse(JSON.parse(data.body));
                    //     });
                    //     this.stompClient.subscribe('/topic/client.chat', (data: { body: string; }) => {
                    //         this.onFwpChat(JSON.parse(data.body));
                    //     });
                    //     this.stompClient.subscribe('/topic/client.chatHistory', (data: { body: string; }) => {
                    //         this.onLastTenChatHistory(JSON.parse(data.body));
                    //     });
                    // }
                } else {
                    this.stompClient.subscribe('/client/bet/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpBetSessionResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/init/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onAuthenticateResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/balance/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onBalanceResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/cashOut/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpCashout(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/playerBets/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onGameInitResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/last20/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onLastTwentyGamesResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/defenderData/' + this.socketEndPointForInit, (data: { body: string; }) => {
                        this.onFwpDataResponse(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/defender/score/' + eventName, (data: { body: string; }) => {
                        this.onFwpScore(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/defender/result/' + eventName, (data: { body: string; }) => {
                        this.onFwpResult(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chat', (data: { body: string; }) => {
                        this.onFwpChat(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/chatHistory', (data: { body: string; }) => {
                        this.onLastTenChatHistory(JSON.parse(data.body));
                    });
                    this.stompClient.subscribe('/client/defender/newMatch/' + ((this.configService.configData.defender.operatorSeparation.indexOf(this.configService.configData.operatorId)>=0) ? this.configService.configData.operatorId : "default"), (data: { body: string; }) => {
                        this.stompClient.subscribe('/client/defender/score/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onFwpScore(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/defender/result/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onFwpResult(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/defenderBet/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onAllFwpBet(JSON.parse(data.body));
                        });
                        this.stompClient.subscribe('/client/defenderCashOut/' + JSON.parse(data.body).id, (data: { body: string; }) => {
                            this.onAllFwpCashOut(JSON.parse(data.body));
                        });
                        this.onFwpNewMatch(JSON.parse(data.body));
                    });
                }
            } else if (this.gameType == 'home-run') {
                this.stompClient.subscribe('/client/home-run/' + this.configService.configData.operatorId + "/gameEvent", (data: { body: string; }) => {
                    this.onGameEventResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/chat', (data: { body: string; }) => {
                    this.onFwpChat(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/chatHistory', (data: { body: string; }) => {
                    this.onLastTenChatHistory(JSON.parse(data.body));
                });
            } else if (this.gameType == 'penaltyShootout' || this.gameType == 'freePenaltyShootout') {
                this.stompClient.subscribe('/client/' + gameType + '/' + eventName, (data: { body: string }) => {
                    this.onPsoEventMessage(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/init/' + this.socketEndPointForInit, (data: { body: string; }) => {
                    this.onAuthenticateResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/bet/' + playerId, (data: { body: string }) => {
                    this.onPsoBetSessionResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/playerBets/' + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoGameInitResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/bet/', (data: { body: string }) => {
                    this.onPsoBetResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/balance/' + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoBalanceResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/penaltyShootout/' + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoPSResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe("/client/freePenaltyShootout/" + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoFreePSResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe("/client/tournamentJoin/" + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoTourneyJoinResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe("/client/tournamentPenaltyShootout/" + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoTourneyPSResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe("/client/closeBet/" + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoCloseGame(JSON.parse(data.body));
                });
                this.stompClient.subscribe("/client/closeTournamentBets/" + this.socketEndPointForInit, (data: { body: string }) => {
                    this.onPsoCloseTourneyGame(JSON.parse(data.body));
                });
            }
            else {
                this.stompClient.subscribe('/client/init/' + this.socketEndPointForInit, (data: { body: string; }) => {
                    this.onAuthenticateResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/' + gameType + '/' + eventName, (data: { body: string; }) => {
                    this.onEventMessage(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/bet/' + playerId, (data: { body: string; }) => {
                    this.onBetSessionResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/playerBets/' + this.socketEndPointForInit, (data: { body: string; }) => {
                    this.onGameInitResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/bet/' + this.socketEndPointForInit, (data: { body: string; }) => {
                    this.onBetResponse(JSON.parse(data.body));
                });
                this.stompClient.subscribe('/client/balance/' + this.socketEndPointForInit, (data: { body: string; }) => {
                    this.onBalanceResponse(JSON.parse(data.body));
                });
            }


            this.setConnected(true);
        }, (message: any) => {
            this.onDisconnected();
        });
    }

    getBalance(data: any) {
        // console.log("send balance request == ", data)
        this.stompClient.send("/server/balance/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    authenticatePlayer(data: any) {
        // console.log("send init request == ", data)
        this.stompClient.send("/server/init/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    getGameInit(data: any) {
        this.matchId = data.matchId;
        // console.log("send gameinit  request == ", this.socketEndPointForInit, data);
        this.stompClient.send("/server/playerBets/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    sendBetRequest(data: any, overs: any) {
        // console.log("Data in Service", data)
        for (let i = 0; i < overs; i++) {
            if (i > 0) {
                ++data.overNumber;
                // data.selections = data.selections;
            }
            this.stompClient.send("/server/bet/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }

    sendFwpDataRequest(data: any) {
        if (this.stompClient) {
            this.stompClient.send("/server/fwpData/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }

    sendDefenderDataRequest(data: any) {
        if (this.stompClient) {
            this.stompClient.send("/server/defenderData/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }

    sendHomeRunDataRequest(data: any) {
        if (this.stompClient) {
            this.stompClient.send("/server/defenderData/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }

    onBalanceResponse(data: any) {
        let balanceResponse = {
            "message": "balance",
            "data": data
        }
        this.messageSubject.next(balanceResponse);
    }

    onAuthenticateResponse(data: any) {
        let authenticateResponse = {
            "message": "authenticate",
            "data": data
        }
        this.messageSubject.next(authenticateResponse);
    }

    onGameInitResponse(data: any) {
        let gameInitResponse = {
            "message": "init",
            "data": data
        }
        this.messageSubject.next(gameInitResponse);
    }
    onBetResponse(data: any) {
        let betResponse = {
            "message": "betresponse",
            "data": data
        }
        console.log('socket bet response', data);
        this.messageSubject.next(betResponse);
    }
    onBetSessionResponse(data: any) {
        let betSessionResponse = {
            "message": "betsessionresponse",
            "data": data
        }
        console.log('socket bet session response', data);
        this.messageSubject.next(betSessionResponse);
    }


    onEventMessage(data: any) {
        let eventResponse = {
            "message": "event",
            "data": data
        }
        this.messageSubject.next(eventResponse);
    }

    setConnected(flag: boolean) {
        // console.log("Socket connected")
        this.socketStatus = true;
        this.messageSubject.next('socket connected');

    }
    onDisconnected() {
        // if (this.socketStatus) {
        //     this.stompClient.disconnect();
        //     this.socketStatus = false;
        // }
        this.disconnectSocket();
        if (this.connectionObj) {
            let disconnectedData = {
                "message": "disconnected",
                "data": true
            }
            this.messageSubject.next(disconnectedData);
            this.connectToServer(this.connectionObj.url, this.connectionObj.gameType, this.connectionObj.playerId, this.connectionObj.eventName);
        }
    }

    disconnectSocket() {
        if (this.stompClient && this.connectionObj) {
            this.connectionObj = null;
            this.stompClient.disconnect();
        }
    }

    onLastTwentyGamesResponse(data: any) {
        let lastTenResponse = {
            "message": "last20",
            "data": data
        }
        this.messageSubject.next(lastTenResponse);
    }

    onLastTenChatHistory(data: any) {
        let lastTenChatResponse = {
            "message": "lasttenchat",
            "data": data
        }
        this.messageSubject.next(lastTenChatResponse);
    }

    onFwpDataResponse(data: any) {
        let fwpDataResponse = {
            "message": "fwpdata",
            "data": data
        }
        this.messageSubject.next(fwpDataResponse);
    }

    // fwp

    onFwpBetSessionResponse(data: any) {
        let betSessionResponse = {
            "message": "betsessionresponse",
            "data": data
        }
        this.messageSubject.next(betSessionResponse);
    }

    onFwpCashout(data: any) {
        let cashOutResponse = {
            "message": "cashout",
            "data": data
        }
        this.messageSubject.next(cashOutResponse);
    }

    onFwpNewMatch(data: any) {
        // console.log('socket response new match',data);
        let matchResponse = {
            "message": "newmatch",
            "data": data
        }
        this.matchId = data;
        this.messageSubject.next(matchResponse);
    }

    onFwpScore(data: any) {
        let scoreResponse = {
            "message": "score",
            "data": data
        }
        this.messageSubject.next(scoreResponse);
    }

    onFwpResult(data: any) {
        let resultResponse = {
            "message": "result",
            "data": data
        }
        this.messageSubject.next(resultResponse);
    }

    onFwpChat(data: any) {
        let chatResponse = {
            "message": "chat",
            "data": data
        }
        this.messageSubject.next(chatResponse);
    }

    onAllFwpBet(data: any) {
        let allFwpBetResponse = {
            "message": "fwpbet",
            "data": data
        }
        this.messageSubject.next(allFwpBetResponse);
    }

    onAllFwpCashOut(data: any) {
        let allFwpCashOutResponse = {
            "message": "fwpcashout",
            "data": data
        }
        this.messageSubject.next(allFwpCashOutResponse);
    }

    onGameEventResponse(data: any) {
        let gameEventResponse = {
            "message": "gameevent",
            "data": data
        }
        this.messageSubject.next(gameEventResponse);
    }

    sendFwpBetRequest(data: any) {
        this.stompClient.send("/server/bet/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendFwpCashOutRequest(data: any) {
        this.stompClient.send("/server/cashOut/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendFwpChatRequest(data: any) {
        this.stompClient.send("/server/chat", {}, JSON.stringify(data));
    }

    sendDefenderBetRequest(data: any) {
        console.warn("Bet request sent ", new Date().getTime())
        this.stompClient.send("/server/bet/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendDefenderCashOutRequest(data: any) {
        this.stompClient.send("/server/cashOut/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendDefenderChatRequest(data: any) {
        this.stompClient.send("/server/chat", {}, JSON.stringify(data));
    }

    sendHomeRunBetRequest(data: any) {
        this.stompClient.send("/server/bet/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendHomeRunCashOutRequest(data: any) {
        this.stompClient.send("/server/cashOut/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }

    sendHomeRunChatRequest(data: any) {
        this.stompClient.send("/server/chat", {}, JSON.stringify(data));
    }

    sendHomeRunLastTenChatRequest(operatorId: string) {
        if (this.stompClient) {
            this.stompClient.send(`/server/chatHistory`, {}, JSON.stringify({gameType: "DEFENDER", operatorId: operatorId}));
        }
    }
    
    sendLastTwentyGamesRequest(data: any) {
        if (this.stompClient) {
            this.stompClient.send("/server/last20/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }

    sendLastTenChatRequest(operatorId: string) {
        if (this.stompClient) {
            this.stompClient.send(`/server/chatHistory`, {}, JSON.stringify({gameType: "FWP", operatorId: operatorId}));
        }
    }

    sendDefenderLastTenChatRequest(operatorId: string) {
        if (this.stompClient) {
            this.stompClient.send(`/server/chatHistory`, {}, JSON.stringify({gameType: "DEFENDER", operatorId: operatorId}));
        }
    }

    getClientId(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    getSocketId() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    // penalty shootout

    getPsoBalance(data: any) {
        // console.log("send balance request == ", JSON.stringify(data))
        this.stompClient.send("/server/balance/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    getPsoGameInit(data: any) {
        // console.log("send gameinit  request == ", this.socketEndPointForInit, JSON.stringify(data));
        this.stompClient.send("/server/playerBets/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    sendPsoFreeBetRequest(data: any, playerId: any) {
        // console.log("send Free bet == ", playerId, " :: ", JSON.stringify(data));
        if (this.stompClient)
            this.stompClient.send("/server/bet/" + playerId, {}, JSON.stringify(data));
    }
    sendPsoBetRequest(data: any, playerId: any) {
        // console.log("send bet == ", playerId, " :: ", JSON.stringify(data));
        if (this.stompClient)
            this.stompClient.send("/server/bet/" + playerId, {}, JSON.stringify(data));
    }
    sendTourneyJoinRequest(data: any) {
        // console.log("send tourney join == ", JSON.stringify(data));
        this.stompClient.send("/server/tournamentJoin/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    sendGameExit(data: any) {
        // console.log("send Game exit == ", " :: ", JSON.stringify(data));
        this.stompClient.send("/server/closeBet/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    sendTourneyGameExit(data: any) {
        // console.log("send Game exit == ", " :: ", JSON.stringify(data));
        this.stompClient.send("/server/closeTournamentBets/" + this.socketEndPointForInit, {}, JSON.stringify(data));
    }
    sendBallKickRequest(data: any, isCash: any) {
        // console.log("send ballkick == ", JSON.stringify(data));
        if (isCash) {
            this.stompClient.send("/server/penaltyShootout/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        } else {
            this.stompClient.send("/server/freePenaltyShootout/" + this.socketEndPointForInit, {}, JSON.stringify(data));
        }
    }
    sendTourneyBallKickRequest(data: any) {
        //// console.log("send tourney ballkick == ", JSON.stringify(data));
        this.stompClient.send("/server/tournamentPenaltyShootout/" + this.socketEndPointForInit, {}, JSON.stringify(data));

    }

    onPsoPSResponse(data: any) {
        //// console.log("bll kick response == ", data);
        let psoPsResponse = {
            "message": "ballKickReponse",
            "data": data
        }
        this.messageSubject.next(psoPsResponse);
    }
    onPsoFreePSResponse(data: any) {
       // console.log("222 == ", data);
        let psoFreePSResponse = {
            "message": "freePSResponse",
            "data": data
        }
        this.messageSubject.next(psoFreePSResponse);
    }
    onPsoTourneyPSResponse(data: any) {
        let psoTourneyPSRespons = {
            "message": "tourneyPSResponse",
            "data": data
        }
        this.messageSubject.next(psoTourneyPSRespons);
    }
    onPsoTourneyJoinResponse(data: any) {
        let psoTourneyJoinResponse = {
            "message": "tourneyJoinResponse",
            "data": data
        }
        this.messageSubject.next(psoTourneyJoinResponse);
    }
    onPsoCloseGame(data: any) {
        let psoCloseGame = {
            "message": "closeGame",
            "data": data
        }
        this.messageSubject.next(psoCloseGame);
    }
    onPsoCloseTourneyGame(data: any) {
        let psoCloseTourneyGame = {
            "message": "closeTourneyGame",
            "data": data
        }
        this.messageSubject.next(psoCloseTourneyGame);
    }
    onPsoBalanceResponse(data: any) {
        let psoBalanceResponse = {
            "message": "balanceResponse",
            "data": data
        }
        // console.warn("loading...received balance call",data);
        this.messageSubject.next(psoBalanceResponse);
    }
    onPsoGameInitResponse(data: any) {
       // console.log("3333 == ", data);
        let psoGameInitResponse = {
            "message": "gameInitResponse",
            "data": data
        }
        this.messageSubject.next(psoGameInitResponse);
    }
    onPsoBetResponse(data: any) {
        let psoBalanceResponse = {
            "message": "betResponse",
            "data": data
        }
        this.messageSubject.next(psoBalanceResponse);
    }
    onPsoBetSessionResponse(data: any) {
        let psoBetSessionResponse = {
            "message": "betSessionResponse",
            "data": data
        }
        this.messageSubject.next(psoBetSessionResponse);
    }

    onPsoEventMessage(data: any) {
        let psoEventMessage = {
            "message": "eventMessage",
            "data": data
        }
        this.messageSubject.next(psoEventMessage);
    }

    subscribePlayerBet(playerId:  any) {
        setTimeout(()=> {
            this.playerBetSubscribed = true;
        }, 1000);
        this.stompClient.subscribe('/client/bet/' + playerId, (data: { body: string; }) => {
            this.onPsoBetSessionResponse(JSON.parse(data.body));
        });
    }

    subscribeGenericPlayerBet(playerId:  any) {
        setTimeout(()=> {
            this.playerBetSubscribed = true;
        }, 1000);
        this.stompClient.subscribe('/client/bet/' + playerId, (data: { body: string; }) => {
            this.onPsoBetSessionResponse(JSON.parse(data.body));
        });
    }


}
