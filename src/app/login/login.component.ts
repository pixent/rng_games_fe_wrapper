import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../services/config.service';
import { SocketService } from '../services/socket.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: any = {};
  showError: boolean = false;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, public configService: ConfigService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    if (sessionStorage.getItem("loginState")) {
      // this.router.navigate([""]);
      let gameName = sessionStorage.getItem("gameName");
      if (gameName == "Penalty Shootout") {
        this.router.navigate(["home/penaltyshootout"], { state: {gameName: gameName}});
      } else if (gameName == "First Wicket Partnership") {
        this.router.navigate(["home/firstwicketpartnership"], { state: {gameName: gameName}});
      } else if (gameName == "Defender") {
        this.router.navigate(["home/defender"], { state: {gameName: gameName}});
      } else if (gameName == "Match Predictor") {
        this.router.navigate(["home/iframe"], { state: {gameName: gameName}});
      } else {
        this.router.navigate([""], { state: {gameName: gameName}});
      }
    }
  }

  ngAfterViewInit(): void {

  }

  onSubmit() {
    let users = this.configService.configData.users;
    let authenticatedUser = false;
    let gameName = 'All';
    for (let i = 0; i < users.length; i++) {
      if (users[i].username == this.loginForm.value.username && users[i].password == this.loginForm.value.password) {
        authenticatedUser = true;
        gameName = users[i].games;
      }
    }
    if (authenticatedUser) {
      //@ts-ignore
      sessionStorage.setItem("loginState",true);
      sessionStorage.setItem("gameName",gameName);
      if (gameName == "Penalty Shootout") {
        this.router.navigate(["home/penaltyshootout"], { state: {gameName: gameName}});
      } else if (gameName == "First Wicket Partnership") {
        this.router.navigate(["home/firstwicketpartnership"], { state: {gameName: gameName}});
      } else if (gameName == "Defender") {
        this.router.navigate(["home/defender"], { state: {gameName: gameName}});
      } else if (gameName == "Match Predictor") {
        this.router.navigate(["home/iframe"], { state: {gameName: gameName}});
      } else {
        if(!gameName) {
          gameName = "All";
        }
        this.router.navigate([""], { state: {gameName: gameName}});
      }
    } else {
      this.showError = true;
    }
  }



}
