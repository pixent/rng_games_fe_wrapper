import { ViewportScroller, DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Config } from 'protractor';
import { ConfigService } from '../../services/config.service'

@Component({
  selector: 'app-Leaderboardpick10',
  templateUrl: './Leaderboardpick10.component.html',
  styleUrls: ['./Leaderboardpick10.component.css'],
  providers: [
    DatePipe
  ]
})
export class LeaderboardPick10Component implements OnInit {

  leaderBoardData: any = [];
  constructor(public configService: ConfigService, private changeDetectorRef: ChangeDetectorRef, private http: HttpClient, private viewportScroller: ViewportScroller, private datePipe: DatePipe) { }

  ngOnInit(): void {

  }
  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0,window.history.state.item]);
    this.leaderBoardData = this.configService.configData.leaderboard;
    for (let p in this.leaderBoardData) {
      this.leaderBoardData[p].random = Math.ceil(Math.random()*4);
    }
    this.changeDetectorRef.detectChanges();
    document.getElementById('iframeElement')!.style.display = "none";
    this.getLeaderBoardData();
  }

  getLeaderBoardData() {
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" +  currentMonth + "-01 00:00";
    let endDate = date.getFullYear() + "-" +  currentMonth + "-" + new Date(date.getFullYear(),date.getMonth()+1,0).getDate() + " 23:59";
    let leaderBoardUrl = this.configService.configData.url.socket + "leaderboard?gameName=pick10&startDate=" + startDate + "&endDate=" + endDate;
    this.http.get(leaderBoardUrl).subscribe((data: any) => {
      this.leaderBoardData = data;
    },(err: any) => {
        console.log(err);
    });
  }

}
