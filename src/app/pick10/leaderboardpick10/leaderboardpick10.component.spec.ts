import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardPick10Component } from './leaderboardpick10.component';

describe('LeaderboardPick10Component', () => {
  let component: LeaderboardPick10Component;
  let fixture: ComponentFixture<LeaderboardPick10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderboardPick10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardPick10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
