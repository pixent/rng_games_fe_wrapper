import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pick10Component } from './pick10.component';

describe('Pick10Component', () => {
  let component: Pick10Component;
  let fixture: ComponentFixture<Pick10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pick10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pick10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
