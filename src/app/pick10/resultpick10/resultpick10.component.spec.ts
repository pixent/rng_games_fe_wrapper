import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPick10Component } from './resultpick10.component';

describe('ResultPick10Component', () => {
  let component: ResultPick10Component;
  let fixture: ComponentFixture<ResultPick10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultPick10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPick10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
