import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resultpick10',
  templateUrl: './resultpick10.component.html',
  styleUrls: ['./resultpick10.component.css']
})
export class ResultPick10Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    document.getElementById('iframeElement')!.style.display = "none";
  }

}
