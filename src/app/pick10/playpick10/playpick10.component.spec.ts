import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Playpick10Component } from './playpick10.component';

describe('Playpick10Component', () => {
  let component: Playpick10Component;
  let fixture: ComponentFixture<Playpick10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Playpick10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Playpick10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
