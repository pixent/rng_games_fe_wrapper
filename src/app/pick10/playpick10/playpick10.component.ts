import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../../services/config.service';
import { ScoreBoardService } from '../../services/scoreboard.service';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-playpick10',
  templateUrl: './playpick10.component.html',
  styleUrls: ['./playpick10.component.css']
})
export class Playpick10Component implements OnInit {

  url: any;
  iframeUrl: any;

  constructor(public sanitizer: DomSanitizer, public configService: ConfigService, private scoreBoardService: ScoreBoardService, public viewportScroller: ViewportScroller ) { }

  ngOnInit(): void {
    this.url = this.configService.configData.url.canvas + "pick10/index.html";
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    document.getElementById('iframeElement')!.style.display = "block";
  }


  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0,window.history.state.item]);
  }

}
