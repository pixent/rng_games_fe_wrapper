import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-howtoplaypick10',
  templateUrl: './howtoplaypick10.component.html',
  styleUrls: ['./howtoplaypick10.component.css']
})
export class HowToPlayPick10Component implements OnInit {

  constructor(private viewportScroller: ViewportScroller) { }

  ngOnInit(): void {
    document.getElementById('iframeElement')!.style.display = "none";
  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0,window.history.state.item]);
  }

}
