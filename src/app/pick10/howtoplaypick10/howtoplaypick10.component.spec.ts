import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HowToPlayPick10Component } from './howtoplaypick10.component';

describe('HowToPlayPick10Component', () => {
  let component: HowToPlayPick10Component;
  let fixture: ComponentFixture<HowToPlayPick10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HowToPlayPick10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HowToPlayPick10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
