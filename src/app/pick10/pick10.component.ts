// @ts-nocheck
import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScoreBoardService } from '../services/scoreboard.service';
import { ConfigService } from '../services/config.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment-timezone';


@Component({
  selector: 'app-pick10',
  templateUrl: './pick10.component.html',
  styleUrls: ['./pick10.component.css']
})

export class Pick10Component implements OnInit {

  matchDetails: any;
  scoreCardDetails: any;
  url: any;
  iframeUrl: any;
  playButtonState: any = false;
  betValues: any;
  selectedBet: any = 100;
  loadedIframe: any;
  inputSelected: boolean = false;
  scrollYPosition: any;

  constructor(private route: ActivatedRoute, public sanitizer: DomSanitizer, private router: Router, public configService: ConfigService, private scoreBoardService: ScoreBoardService) {
    scoreBoardService.changeEmitted$.subscribe(
      text => {
        this.scoreCardDetails = text;
        // this.setScoreCard();
      });
  }

  ngOnInit(): void {
    this.url = this.configService.configData.url.canvas + "pick10/index.html";
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    // this.checkRoute();
    this.matchDetails = window.history.state.item;
    if (this.matchDetails) {
      localStorage.setItem('matchDetails', JSON.stringify(this.matchDetails));
    }
    if (localStorage.getItem('matchDetails')) {
      this.matchDetails = JSON.parse(localStorage.getItem('matchDetails')!);
    }
    // this.setScoreCard();
    this.setCountdown();
    if (this.matchDetails.date != "Completed") {
      setInterval(() => this.setCountdown(), 1000);
    }
  }

  ngAfterViewInit() {
    let iframeElement = document.getElementById('iframeElement');
    let topNavHeight = document.getElementById('top-nav').clientHeight;
    let pick10HeadHeight = document.getElementById('pick-10-head').clientHeight;
    let pick10CardHeight = document.getElementById('pick-10-card-2').clientHeight;
    // let pick10NavHeight = document.getElementById('pick-10-nav').clientHeight;
    // let pick10BetHeight = document.getElementById('pick-10-bet').clientHeight;
    if (iframeElement) {
      iframeElement.style.height = (window.innerHeight - pick10HeadHeight - pick10CardHeight - topNavHeight) + "px";
    }
  }

  @HostListener('window:scroll', ['$event'])
  
  onScroll(event) {
    this.scrollYPosition = document.documentElement.scrollTop;
  }

  @HostListener('window:resize', ['$event'])

  onResize(event) {
    event.target.innerWidth;
  }



  @HostListener('window:message', ['$event'])

  onMessage(res: any) {
    // console.log('received message from game',res.data);
    if (res.data) {
      let messageData = res.data;
      this.loadedIframe = document.getElementById('iframeElement');
      switch (messageData.messageName) {
        case 'init':
          this.initializeGame(messageData);
          break;
        case 'initiateBet':
          this.setBetValues(messageData);
          break;
        case 'Pick10PlayButtonState':
          this.setPlayButtonState(messageData);
          break;
        case 'hidePick10PlayControls':
          this.hidePick10PlayControls(messageData);
          break;
        case 'appHeight':
          this.setIframeHeight(messageData);
          break;
        case 'LobbyClicked':
          this.navigateToLobby(messageData);
          break;
        case 'CTTMenuButtonClicked':
          this.navigateCTTMenu(messageData);
          break;
          
        default:
        // code block
      }
    }

    if (res.data && res.messageName == "MatchScore") {
      this.scoreBoardService.setScoreBoard(res.data);
      this.scoreBoardService.emitChange(res.data);
    }

  }

  navigateToLobby() {
    this.router.navigate([''])
  }

  navigateCTTMenu(data) {
    if (data.buttonId == "howtoplay") {
      this.router.navigate(['howtoplaypick10'], { relativeTo: this.route, state: { item: this.scrollYPosition } });
    } else if (data.buttonId == "predictover") {
      this.router.navigate(['playpick10'], { relativeTo: this.route, state: { item: this.scrollYPosition } });
    } else if (data.buttonId == "result") {
      this.router.navigate(['resultpick10'], { relativeTo: this.route, state: { item: this.scrollYPosition } });
    } else if (data.buttonId == "leaderboard") {
      this.router.navigate(['leaderboardpick10'], { relativeTo: this.route, state: { item: this.scrollYPosition } });
    } else {

    }
  }

  checkRoute() {
    if (this.router.url != "/home") {
      $('#' + this.router.url.replace('/', '')).addClass('active').siblings('li').removeClass('active');
    }
  }

  onHowToPlayClicked(event: any) {
    this.addActive(event);
    this.router.navigate(['howtoplaypick10'], { relativeTo: this.route });
    document.getElementById('pick-10-bet')?.style.display = "none";
  }

  onPlayClicked(event: any) {
    this.addActive(event);
    this.router.navigate(['playpick10'], { relativeTo: this.route });
    document.getElementById('pick-10-bet')?.style.display = "flex";
  }

  onResultClicked(event: any) {
    this.addActive(event);
    this.router.navigate(['resultpick10'], { relativeTo: this.route });
    document.getElementById('pick-10-bet')?.style.display = "none";
  }

  onLeaderboardClicked(event: any) {
    this.addActive(event);
    this.router.navigate(['leaderboardpick10'], { relativeTo: this.route });
    document.getElementById('pick-10-bet')?.style.display = "none";
  }

  addActive(event: any) {
    $(event.target.closest('li')).addClass('active').siblings('li').removeClass('active');
  }

  setCountdown() {
    let serverDate = moment.tz(new Date(), "Asia/Kolkata").format();
    let currentDate = moment(serverDate.split('+')[0]);
    let resultDate = moment(new Date(this.matchDetails.startDate));
    let dateDiff = moment.duration(currentDate.diff(resultDate));
    if (-dateDiff._data.seconds < 0) {
      this.matchDetails.date = "Completed";
    } else {
      this.matchDetails.date = (-dateDiff._data.hours) + "hr : " + (-dateDiff._data.minutes) + "m : " + (-dateDiff._data.seconds) + "s";
    }
  }

  initializeGame(obj) {
    let matchDetails: any = {};
    matchDetails.message = "MATCH_DETAILS";
    matchDetails.data = this.matchDetails;
    this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
  }

  setBetValues(obj) {
    this.betValues = obj.betsArray;
    this.selectedBet = this.betValues[obj.selectedBetIndex];
    $('#button-' + this.selectedBet).addClass('button-selected');
  }

  setPlayButtonState(obj) {
    this.playButtonState = obj.enablePlay;
  }

  selectButton(item, event) {
    $('.bet-button').css("background", "white");
    document.getElementById('input-bet')?.style.setProperty('background', 'white', 'important');
    event.target.classList.add('button-selected');
    this.selectedBet = item;
    this.inputSelected = false;
    this.playButtonState = true;
  }

  selectInputButton(event) {
    $('.bet-button').css("background", "white");
    event.target.classList.add('button-selected');
    this.selectedBet = event.target.value;
    this.inputSelected = true;
    this.playButtonState = true;
  }

  changeInputValue(event) {
    this.selectedBet = event.target.value;
    this.playButtonState = true;
  }

  onPlayButtonClicked() {
    let betDetails: any = {};
    betDetails.message = "BET_DETAILS";
    betDetails.data = {
      "gameType": "pick10",
      "bet": this.selectedBet
    }
    this.loadedIframe.contentWindow.gameExtension(betDetails, "*");
  }

  hidePick10PlayControls(obj) {
    if (obj.enablePlay) {
      document.getElementById('pick-10-bet')?.style.display = "block";
    } else {
      document.getElementById('pick-10-bet')?.style.display = "none";
    }
  }

  setIframeHeight(obj) {
    if(obj.appHeight) {
      document.getElementById('iframeElement')?.style.height = obj.appHeight + "px";
    }
  }
}