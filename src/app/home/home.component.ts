import { Component, HostListener, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../services/config.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showHeader: boolean = true;

  constructor(private http: HttpClient, public configService: ConfigService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.getLobbyDetails();
    this.route.queryParams.subscribe(
      params => {
        if(params['gameName']) {
          this.showHeader = false;
        }
    });
  }


  getLobbyDetails() {

  }

}
