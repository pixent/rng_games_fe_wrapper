import { ChangeDetectorRef, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatchTrendComponent } from './catchtrendv3/catchtrend.component';
import { CatchTrendHistoryComponent } from './catchtrendv3/catchthetrendhistory/catchthetrendhistory.component';
import { HowToPlayCatchTrendComponent } from './catchtrendv3/howtoplaycatchtrend/howtoplaycatchtrend.component';
import { LeaderboardCatchTrendComponent } from './catchtrendv3/leaderboardcatchtrend/leaderboardcatchtrend.component';
import { PredictOverCatchTrendComponent } from './catchtrendv3/predictovercatchtrend/predictovercatchtrend.component';
import { ResultCatchTrendComponent } from './catchtrendv3/resultcatchtrend/resultcatchtrend.component';
import { ScorecardCatchTrendComponent } from './catchtrendv3/scorecardcatchtrend/scorecardcatchtrend.component';
import { HomeComponent } from './home/home.component';
import { LobbyComponent } from './lobby/lobby.component';
import { FaqPenaltyShootoutComponent } from './penaltyshootoutv3/faqpenaltyshootout/faqpenaltyshootout.component';
// import { LeaderboardPenaltyShootoutComponent } from './penaltyshootoutv3/leaderboardpenaltyshootout/leaderboardpenaltyshootout.component';
import { PenaltyShootoutComponent } from './penaltyshootoutv3/penaltyshootout.component';
import { PlayPenaltyShootoutComponent } from './penaltyshootoutv3/playpenaltyshootout/playpenaltyshootout.component';
import { ResultPenaltyShootoutComponent } from './penaltyshootoutv3/resultpenaltyshootout/resultpenaltyshootout.component';
import { HowToPlayPick10Component } from './pick10/howtoplaypick10/howtoplaypick10.component';
import { LeaderboardPick10Component } from './pick10/leaderboardpick10/leaderboardpick10.component';
import { Pick10Component } from './pick10/pick10.component';
import { Playpick10Component } from './pick10/playpick10/playpick10.component';
import { ResultPick10Component } from './pick10/resultpick10/resultpick10.component';
import { OddstableComponent } from './catchtrendv3/oddstable/oddstable.component';
import { FirstwicketpartnershipComponent } from './firstwicketpartnership/firstwicketpartnership.component';
import { LoginComponent } from './login/login.component';
import { CashplayscreenpenaltyshootoutComponent } from './penaltyshootoutv3/cashplayscreenpenaltyshootout/cashplayscreenpenaltyshootout.component';
import { HowToPlayPenaltyShootoutComponent } from './penaltyshootoutv3/howtoplaypenaltyshootout/howtoplaypenaltyshootout.component';
import { LeaderboardpenaltyshootoutComponent } from './penaltyshootoutv3/cashplayscreenpenaltyshootout/leaderboardpenaltyshootout/leaderboardpenaltyshootout.component';
import { DefenderChatsComponent } from './defender/defenderbottom/defenderchats/defenderchats.component';
import { DefenderComponent } from './defender/defender.component';
import { HomeRunComponent } from './homerun/homerun.component';
import { ErrorPageComponent } from './errorpage/errorpage.component';
import { IframeComponent } from './iframe/iframe.component';

const routes: Routes = [
  {
    path: '', component: LobbyComponent,
  },
  {
    path: 'home', data: {}, component: HomeComponent, children: [
      {
        path: '', component: Pick10Component, children: [
          { path: 'playpick10', redirectTo: '' },
          { path: '', component: Playpick10Component },
          { path: 'howtoplaypick10', component: HowToPlayPick10Component },
          { path: 'resultpick10', component: ResultPick10Component },
          { path: 'leaderboardpick10', component: LeaderboardPick10Component }
        ]
      },
      { path: 'pick10', redirectTo: '' },
      {
        path: 'catchthetrend', component: CatchTrendComponent, children: [
          { path: 'predictovercatchtrend', redirectTo: '' },
          { path: '', component: PredictOverCatchTrendComponent },
          { path: 'howtoplaycatchtrend', component: HowToPlayCatchTrendComponent },
          { path: 'resultcatchtrend', component: ResultCatchTrendComponent },
          { path: 'leaderboardcatchtrend', component: LeaderboardCatchTrendComponent },
          { path: 'historycatchtrend', component: CatchTrendHistoryComponent },
          { path: 'scorecardcatchtrend', component: ScorecardCatchTrendComponent },
          { path: 'oddstable', component: OddstableComponent }

        ]
      },
      {
        path: 'firstwicketpartnership', component: FirstwicketpartnershipComponent, children: [
          { path: 'playfirstwicket', redirectTo: '' },
          { path: '', component: PredictOverCatchTrendComponent }

        ]
      },
      {
        path: 'defender', component: DefenderComponent, children: [
          { path: 'playfirstwicket', redirectTo: '' },
          { path: '', component: PredictOverCatchTrendComponent }

        ]
      },
      {
        path: 'homerun', component: HomeRunComponent, children: [
          { path: 'playfirstwicket', redirectTo: '' },
          { path: '', component: PredictOverCatchTrendComponent }

        ]
      },
      {
        path: 'penaltyshootout', component: PenaltyShootoutComponent, children: [
          { path: 'playpenaltyshootout', redirectTo: '' },
          { path: '', component: PlayPenaltyShootoutComponent },
          { path: 'howtoplaypenaltyshootout', component: HowToPlayPenaltyShootoutComponent },
          {
            path: 'cashplayscreenpenaltyshootout', component: CashplayscreenpenaltyshootoutComponent, children: [
              { path: 'leaderboardpenaltyshootout', component: LeaderboardpenaltyshootoutComponent }
            ]
          },
          { path: 'faqpenaltyshootout', component: FaqPenaltyShootoutComponent },
          { path: 'resultpenaltyshootout', component: ResultPenaltyShootoutComponent },
          // { path: 'leaderboardpenaltyshootout', component: LeaderboardPenaltyShootoutComponent }
        ]
      },
      { path: 'iframe', component: IframeComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'errorpage', component: ErrorPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'disabled', anchorScrolling: 'enabled', useHash: true })],
  exports: [RouterModule],
  providers: [PenaltyShootoutComponent]
})
export class AppRoutingModule { }
