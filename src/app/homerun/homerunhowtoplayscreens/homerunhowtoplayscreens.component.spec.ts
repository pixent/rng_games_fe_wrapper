import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRunhowtoplayscreensComponent } from './homerunhowtoplayscreens.component';

describe('HomeRunhowtoplayscreensComponent', () => {
  let component: HomeRunhowtoplayscreensComponent;
  let fixture: ComponentFixture<HomeRunhowtoplayscreensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRunhowtoplayscreensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRunhowtoplayscreensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
