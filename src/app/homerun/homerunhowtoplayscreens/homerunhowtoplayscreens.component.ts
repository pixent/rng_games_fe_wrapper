import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { HomeRunComponent } from '../homerun.component';

@Component({
  selector: 'app-homerunhowtoplayscreens',
  templateUrl: './homerunhowtoplayscreens.component.html',
  styleUrls: ['./homerunhowtoplayscreens.component.css']
})
export class HomeRunhowtoplayscreensComponent implements OnInit {
  imagesConfig: any;
  showHTPScreens: boolean = false;
  showHTPScreensInDesktop: boolean = false;

  constructor(private homerunComponent: HomeRunComponent,public configService: ConfigService) { }

  ngOnInit(): void {

    this.imagesConfig = this.configService.textConfigData["htpScreensImagesPath"];
    // console.log("images config ", this.imagesConfig);
    if(!this.configService.configData.isMobile){
      this.showHTPScreensInDesktop = true;
    }

    // (<any>$(".carousel")).carousel();
  }

  onHowToPlaySkipClicked() {
    this.showHTPScreens = false;
    let skipCount = localStorage.getItem("howToPlaySkipCount");
    this.homerunComponent.showHowToPlayFullScreens = false;
    this.homerunComponent.setIframeHeight();
    localStorage.setItem("howToPlaySkipCount", (Number(skipCount)+1).toString());
  }

}
