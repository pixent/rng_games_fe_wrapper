import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRunHistoryComponent } from './homerunhistory.component';

describe('HomeRunHistoryComponent', () => {
  let component: HomeRunHistoryComponent;
  let fixture: ComponentFixture<HomeRunHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRunHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRunHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
