import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ConfigService } from 'src/app/services/config.service';
import { HomeRunComponent } from '../../homerun.component';

@Component({
  selector: 'app-homerunhistory',
  templateUrl: './homerunhistory.component.html',
  styleUrls: ['./homerunhistory.component.css']
})
export class HomeRunHistoryComponent implements OnInit {

  startDate: any;
  endDate: any;
  gameMode: any = 'Cashplay';
  matchDetails: any;
  resultData: any;
  resultDataRes: any;
  showResultData: any = {};
  textConfig: any = {};
  buttonConfig: any = {};
  currencyCode: any = '';

  @Input() resetHistory: Subject<any> = new Subject<any>();
  colorFlag: string = '';
  userDetails: any;
  stakeTax: any;
  withholdTax: any;
  isShabiki: boolean = false;
  showWithholdingTax: boolean = true;
  currencySymbol: string | undefined;
  isMobile: boolean = false;

  constructor(public configService: ConfigService, private http: HttpClient, public fwpComponent: HomeRunComponent) { }

  ngOnInit(): void {
    this.matchDetails = this.fwpComponent.matchDetails;
    this.userDetails = this.fwpComponent.userDetails;
    this.currencyCode = this.fwpComponent.currencyCode;
    this.currencySymbol = this.fwpComponent.currencySymbol;
    this.isShabiki = this.fwpComponent.isShabiki;

    this.isMobile = this.fwpComponent.isMobile;

    if(this.configService.configData.operatorId == "paribet"){
      this.showWithholdingTax = false;
    }
    

    this.stakeTax = this.configService.configData.taxData[this.userDetails.userRegion] ? this.configService.configData.taxData[this.userDetails.userRegion].stakeTax : 0;
    
    this.withholdTax = this.fwpComponent.userDetails.withholdTax;

    this.resetHistory.subscribe(response => {
      if (response) {
        this.getResultData();
      }
    })
    this.getResultData();

    this.textConfig = this.fwpComponent.textConfig;
    this.buttonConfig = this.fwpComponent.buttonConfig;
  }


  getResultData() {
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59:59";
    if (this.startDate) {
      startDate = this.startDate.year + "-" + (this.startDate.month < 10 ? "0" + this.startDate.month : this.startDate.month) + "-" + (this.startDate.day < 10 ? "0" + this.startDate.day : this.startDate.day) + " 00:00:00";
      endDate = this.endDate.year + "-" + (this.endDate.month < 10 ? "0" + this.endDate.month : this.endDate.month) + "-" + (this.endDate.day < 10 ? "0" + this.endDate.day : this.endDate.day) + " 00:00:00";
    }
    let gameName = "homerun";
    if (this.gameMode == "Freeplay") {
      gameName = "freeHomeRun";
    }
    let leaderBoardUrl = this.configService.configData.url.socket + "virtualCatchTheTrendHistory?playerId=" + this.userDetails.customerId + "&gameName=" + gameName + "&startDate=" + startDate + "&endDate=" + endDate
                          + "&sort=createdOn,desc";
    if(this.configService.configData.url.isCertification){
      leaderBoardUrl = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/home-run/bets/history?operator=" + 
      this.configService.configData.operatorId + "&playerId=" + this.userDetails.customerId + "&startDate=" + startDate + "&endDate=" + endDate + "&size=100&sort=createdAt,desc";
    }
    this.http.get(leaderBoardUrl).subscribe(
      (data: any) => {
        if(this.configService.configData.url.isCertification){
          data = data.bets
        }
        // console.warn("history data ", data)
        if (data) {
          data.sort(function (a: any, b: any) {
            return (new Date(b.createdAt).valueOf() - new Date(a.createdAt).valueOf());
          })
          for (let p = 0; p < data.length; p++) {
            let autoCashoutValue = data[p].autoCashOut;
            let points = data[p].points;
            let createdDate = new Date(data[p].createdAt + '.000Z');
            // let dateAndTime = createdDate.split("T");
            let date;
            let time;
            let currentResult = data[p];
            // date = dateAndTime[0];
            date = createdDate.toLocaleDateString();
            time = createdDate.toLocaleTimeString();
            // time = dateAndTime[1].split(".")[0]
            data[p].date = date;
            data[p].time = time;
            if (currentResult) {
              data[p].multiplier = currentResult.finalMultiplier / 100;
              data[p].currentMultiplier = currentResult.cashedOutMultiplier / 100;
              data[p].runs = currentResult.runs;
              data[p].team = currentResult.teamInfo.firstTeam.team;
              data[p].points = points;
              data[p].formattedWinAmount = this.fwpComponent.formatCurrency(data[p].winAmount);
              data[p].formattedWinAmountAfterTax = this.fwpComponent.formatCurrency(data[p].winAmountAfterTax);
              data[p].formattedBetAmount = this.fwpComponent.formatCurrency(data[p].betAmount);
              data[p].formattedBetAmountAfterTax = this.fwpComponent.formatCurrency(data[p].betAmountAfterTax);
              

              if((autoCashoutValue/100) > data[p].currentMultiplier){
                data[p].currentMultiplier = autoCashoutValue / 100;
                data[p].colorFlag = 'bg-red';
              }
            }
          }
        }

        this.resultData = data;
        
        this.resultDataRes = data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }


  onShowResult(i: any) {
    let p;
    let getArrowId = document.getElementById("arrow" + i);
    for (p = 0; p < this.resultDataRes.length; p++) {
      if (p != i) {
        this.showResultData["showResult" + p] = false;
        document.getElementById("arrow" + p)!.style.transform = "rotate(270deg)";
      }
    }
    this.showResultData["showResult" + i] = !this.showResultData["showResult" + i];
    if (this.showResultData["showResult" + i]) {
      getArrowId!.style.transform = "rotate(90deg)";
    }
    else {
      getArrowId!.style.transform = "rotate(270deg)";
    }

  }

}
