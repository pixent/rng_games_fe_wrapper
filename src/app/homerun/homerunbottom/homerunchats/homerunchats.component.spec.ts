import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefenderChatsComponent } from './homerunchats.component';

describe('DefenderChatsComponent', () => {
  let component: DefenderChatsComponent;
  let fixture: ComponentFixture<DefenderChatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefenderChatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefenderChatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
