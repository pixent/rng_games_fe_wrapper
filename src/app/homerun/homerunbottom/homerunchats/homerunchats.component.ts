import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HomeRunBottomComponent } from '../homerunbottom.component';

@Component({
  selector: 'app-homerunchats',
  templateUrl: './homerunchats.component.html',
  styleUrls: ['./homerunchats.component.css']
})
export class HomeRunChatsComponent implements OnInit {

  @Output() checkWordLength: EventEmitter<any> = new EventEmitter();
  @Output() enterMessage: EventEmitter<any> = new EventEmitter();
  @Input() chatData = [];

  message: string = '';
  messageLength: Number = 0;
  isCorrect: boolean = true;
  blockedWords: any;
  matchDetails: any;
  playerIds: any = [];
  displayDropDown: boolean= false;
  chatForm = new FormGroup({
    message: new FormControl('')
  })
  constructor(private http: HttpClient, private fwpBottomComponent: HomeRunBottomComponent) { }

  ngOnInit(): void {
    this.loadConfig();
    this.matchDetails = this.fwpBottomComponent.matchDetails;
  }

  loadConfig() {
    return new Promise((resolve, reject) => {
      this.http.get('./assets/config/config.json')
        .subscribe((data: any) => {
          this.blockedWords = data.config.blockedWords;
          resolve(true);
        }, (err: any) => {
          console.log(err);
        });
    });
  }

  wordLength(event: any) {
    let value = this.chatForm.value.message;
    if(event.key === '@'){
      this.showIDDropDown();
    }
    
    if (value.length <= 0) {
      this.isCorrect = true;
    } else {
      this.isCorrect = false;
    }
    this.checkWordLength.emit(value);
    
  }

  showIDDropDown(){
    this.displayDropDown = true;
    let chats: any = this.chatData;
    for(let p = 0; p < chats.length; p++){
      if(this.playerIds.indexOf(chats[p].playerId) < 0){
        this.playerIds.push(chats[p].playerId)
      }
      
    }
  }

  chatFormSubmit(event: any) {
    let value = this.chatForm.value.message;
    let msgSplit = value.split(" ");
    let flag = 0;
    for (let word = 0; word < msgSplit.length; word++) {
      if (this.blockedWords.indexOf(msgSplit[word]) > -1) {
        flag = 1;
        break;
      } else {
        flag = 0;
      }
    }
    if (flag === 1) {
      document.getElementById('message_popup')!.style.display = "block";
      setTimeout(this.removePopup, 4000);
    }
    else {
      this.enterMessage.emit();
    }
    setTimeout(() => {
      let element = document.getElementById('fwp_chats');
      if (element) {
        element.scrollTop = element.scrollHeight;
      }
    }, 500);
    this.chatForm.patchValue({
      message: ''
    })
  }

  onPlayerIdClicked(playerId: any, isDropDown: boolean) {
    let mentionedMessage;
    this.displayDropDown = false;
    if(isDropDown){
      mentionedMessage = this.chatForm.value.message + playerId + " ";
    }else{
      mentionedMessage = this.chatForm.value.message + "@" + playerId + " ";
    }
    this.chatForm.patchValue({
      message: mentionedMessage
    })
    this.checkWordLength.emit(mentionedMessage);
  }

  removePopup() {
    document.getElementById('message_popup')!.style.display = "none";
  }

  setChatData() {
    setTimeout(() => {
      let element = document.getElementById('fwp_chats');
      if (element) {
        element.scrollTop = element.scrollHeight;
      }
    }, 500);
  }

}
