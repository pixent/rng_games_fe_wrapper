import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRunBottomComponent } from './homerunbottom.component';

describe('HomeRunBottomComponent', () => {
  let component: HomeRunBottomComponent;
  let fixture: ComponentFixture<HomeRunBottomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeRunBottomComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRunBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
