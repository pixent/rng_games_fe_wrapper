import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRunBetPanelComponent } from './homerunbetpanel.component';

describe('HomeRunBetPanelComponent', () => {
  let component: HomeRunBetPanelComponent;
  let fixture: ComponentFixture<HomeRunBetPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRunBetPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRunBetPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
