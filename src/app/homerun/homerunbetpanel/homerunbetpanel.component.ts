import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HomeRunComponent } from '../homerun.component';
import { getCurrencySymbol } from '@angular/common';
import { HomeRunBottomComponent } from '../homerunbottom/homerunbottom.component';
import { Subject } from 'rxjs';
import { ConfigService } from 'src/app/services/config.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-homerunbetpanel',
  templateUrl: './homerunbetpanel.component.html',
  styleUrls: ['./homerunbetpanel.component.css']
})
export class HomeRunBetPanelComponent implements OnInit {

  firstBetAmount: any = 20;
  secondBetAmount: any = 20;
  firstCashOut: any;
  secondCashOut: any;
  showCashOutFirstBetButton: boolean = false;
  showCashOutSecondBetButton: boolean = false;
  disableFirstInput: boolean = false;
  disableSecondInput: boolean = false;
  showBetAmountFirstBetButton: any = true;
  showBetAmountSecondBetButton: any = true;
  disableBet: boolean = true;
  isFirstBetButtonDisabled: boolean = false;
  isSecondBetButtonDisabled: boolean = false;
  inputBet: boolean = false;
  maxBet: Number = 1000;
  showTooltip: boolean = false;
  active: any = 1;
  firstMultiplyBtnType: any = "";
  secondMultiplyBtnType: any = "";
  betBtn: any = "";
  score: any;
  lastBetAmount: any;
  autoBetAmount: any;
  autoCashout: any;
  totalBets: any;
  onWinPercent: any;
  onLossPercent: any;
  stopOnProfit: any;
  stopOnLoss: any;
  autoBetData: any = {};
  betLossBtn: any = "";
  betSessionPlaced: boolean = false;
  firstBetButtonCashedOut: boolean = false;
  secondBetButtonCashedOut: boolean = false;
  autoplayContent: any = 'Start Autoplay';
  isAutoplay: boolean = false;
  newBetPlaced: boolean = true;
  currencyCode: any;
  currencySymbol: any;
  autoBetForm = new FormGroup({
    autoBetAmount: new FormControl(20.00, Validators.required),
    autoCashout: new FormControl('', Validators.required),
    totalBets: new FormControl('5', Validators.required),
    stopOnProfit: new FormControl('', Validators.required),
    stopOnLoss: new FormControl('', Validators.required),
  })

  @Input() multiplierData: any;
  playClicked: boolean = false;
  autoplayBets: any;
  betType: string = '';
  errorText: string = '';
  isPlayabet: boolean = false;
  isBetyetu: boolean = false;
  numberOfPrevMultipliers: number = 20;
  buttonConfig: any = {};
  textConfig: any = {};
  inputConfig: any = {};
  popupText: any;
  nextRoundBetForFirstBetInput: any;
  nextRoundBetForSecondBetInput: any;
  isFirstBetButtonwaitingForNextRound: boolean = false;
  isSecondBetButtonwaitingForNextRound: boolean = false;
  currentCashOutValueObject: { firstCashoutValue: string, secondCashoutValue: string } = { firstCashoutValue: '', secondCashoutValue: '' };
  nextRoundAutoBet: any;
  showBetPlacedTextForFirstButton: boolean = false;
  showBetPlacedTextForSecondButton: boolean = false;
  autoplayContentChange: boolean = false;
  showCashOutContent: boolean = false;
  autoBetPlaced: boolean = false;
  ngbToolTipTextInfo1: string = "Input a multiplier amount if you would prefer to auto cash out rather than doing so manually";
  toolTipTextAuto: string = "Input a profit amount that would stop the autoplay once reached"
  betPlacedFrom: string = 'first';
  betIdObject: { firstBetId: string, secondBetId: string } = { firstBetId: '', secondBetId: '' };
  isCertification: boolean = false;
  disableFirstBet: boolean = true;
  disableSecondBet: boolean = true;
  firstBetCashoutClicked: boolean = false;
  secondBetCashoutClicked: boolean = false;
  showStopAutoPlayButton: boolean = false;
  isStopAutoPlayButtonEnabled: boolean = false;
  sliderValueObject: { sliderValue1: boolean, sliderValue2: boolean } = { sliderValue1: false, sliderValue2: false };
  isMobile: boolean = false;
  updatedBetAmountTypeObject: { firstBetAmountType: any, secondBetAmountType: any } = { firstBetAmountType: null, secondBetAmountType: null };
  updatedCashoutMultiplierObject: { firstMultiplierType: any, secondMultiplierType: any } = { firstMultiplierType: null, secondMultiplierType: null };
  autoActiveClassObject: {
    totalRoundsType: any; updatedBetAmountType: any, updatedCashoutMultiplier: any
  } = { updatedBetAmountType: null, updatedCashoutMultiplier: null, totalRoundsType: null }
  incrementDecrementButton: { disableFirstDecrementButton: boolean, disableFirstIncrementButton: boolean, disableSecondDecrementButton: boolean, disableSecondIncrementButton: boolean, disableAutoDecrementButton: boolean, disableAutoIncrementButton: boolean } = { disableFirstDecrementButton: false, disableFirstIncrementButton: false, disableSecondDecrementButton: false, disableSecondIncrementButton: false, disableAutoDecrementButton: false, disableAutoIncrementButton: false }

  constructor(private fwpComponent: HomeRunComponent, public configService: ConfigService, private http: HttpClient) { }

  @Output() autoplayBet: EventEmitter<any> = new EventEmitter();


  ngOnInit(): void {
    this.onAutoBetInputChanges();

    this.isMobile = this.fwpComponent.isMobile;

    console.warn("defender bets ", this.configService.configData.defenderBets);

    // take values from config
    this.firstBetAmount = this.fwpComponent.defaultBet;

    this.secondBetAmount = this.fwpComponent.defaultBet;

    this.autoBetAmount = this.fwpComponent.defaultBet
    this.autoBetForm.patchValue({
      autoBetAmount: this.fwpComponent.defaultBet
    });

    this.currencySymbol = this.fwpComponent.currencySymbol;

    this.isPlayabet = this.fwpComponent.isPlayabet;
    this.isBetyetu = this.fwpComponent.isBetyetu;

    this.textConfig = this.fwpComponent.textConfig;
    this.buttonConfig = this.fwpComponent.buttonConfig;
    this.inputConfig = this.fwpComponent.inputConfig;

    this.maxBet = this.fwpComponent.maxBet;

    this.ngbToolTipTextInfo1 = this.textConfig.ngbToolTipInfo1;
    this.toolTipTextAuto = this.textConfig.toolTipTextAuto;

    this.isCertification = this.configService.configData.url.isCertification;

    this.cashOutChange("2");

    this.isStopAutoPlayButtonEnabled = (this.configService.configData.platformId == 6);
  }

  setCashOutContent(event: boolean) {
    this.showCashOutContent = event;
  }

  onBetPlaced(isFirstBetInput: boolean) {
    if (this.isCertification) {
      if (isFirstBetInput) {
        if (this.firstBetAmount <= 0 || this.firstBetAmount == "") {
          this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
          return;
        }
        this.isFirstBetButtonDisabled = true;

        this.betPlacedFrom = 'first';

        // // debugger

        if (this.disableFirstBet) {
          this.popupText = this.textConfig.waitingForNextRound || 'Bet placed for the next round';
          // this.fwpComponent.showModal(this.popupText);
          this.nextRoundBetForFirstBetInput = this.firstBetAmount;
          this.showWaitingForNextRoundText(true)
          this.autoBetForm.disable();

          return
        }
        this.isFirstBetButtonwaitingForNextRound = false;
        // console.warn('ON bet placed', this.cashOut);

        let minimumBetAmountText: string = this.textConfig.minimumBetAmount;
        let enterCashoutValueText: string = this.textConfig.enterCashoutValueText;
        this.betType = 'manual';
        if (this.firstBetAmount > 0) {
          if ((this.fwpComponent.isShabiki || this.fwpComponent.isStarbet || this.fwpComponent.isWeAreCasino) && this.firstBetAmount < this.fwpComponent.minBet) {
            this.isFirstBetButtonDisabled = false;
            this.fwpComponent.showModal(`${minimumBetAmountText} ${this.currencyCode} ${this.fwpComponent.minBet}`);
          } else {
            if (this.firstCashOut) {
              if (this.firstCashOut >= 1.01) {
                this.newBetPlaced = true;
                this.disableFirstInput = true;
                this.autoBetForm.disable();
                this.incrementDecrementButton.disableFirstDecrementButton = true;
                this.incrementDecrementButton.disableFirstIncrementButton = true;
                this.fwpComponent.placeBet(this.firstBetAmount, this.firstCashOut, isFirstBetInput);
              } else {
                this.fwpComponent.showModal(`${enterCashoutValueText}`);
              }
            } else if (this.firstCashOut !== 0) {
              this.newBetPlaced = true;
              this.disableFirstInput = true;
              this.autoBetForm.disable();
              this.incrementDecrementButton.disableFirstDecrementButton = true;
              this.incrementDecrementButton.disableFirstIncrementButton = true;
              this.fwpComponent.placeBet(this.firstBetAmount, this.firstCashOut, isFirstBetInput);
            }
          }
        } else {
          this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
        }
      } else {
        if (this.secondBetAmount <= 0 || this.secondBetAmount == "") {
          this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
          return;
        }
        this.isSecondBetButtonDisabled = true;

        this.betPlacedFrom = 'second';
        // console.warn("diable bet ", this.disableSecondBet);
        if (this.disableSecondBet) {
          // // debugger
          this.popupText = this.textConfig.waitingForNextRound || 'Bet placed for the next round';
          // this.fwpComponent.showModal(this.popupText);
          this.nextRoundBetForSecondBetInput = this.secondBetAmount;
          this.showWaitingForNextRoundText(false)
          this.autoBetForm.disable();

          return
        }
        this.isSecondBetButtonwaitingForNextRound = false;
        // console.warn('ON bet placed', this.cashOut);

        let minimumBetAmountText: string = this.textConfig.minimumBetAmount;
        let enterCashoutValueText: string = this.textConfig.enterCashoutValueText;
        this.betType = 'manual';
        if (this.secondBetAmount > 0) {
          if ((this.fwpComponent.isShabiki || this.fwpComponent.isStarbet || this.fwpComponent.isWeAreCasino) && this.secondBetAmount < this.fwpComponent.minBet) {
            this.fwpComponent.showModal(`${minimumBetAmountText} ${this.currencyCode} ${this.fwpComponent.minBet}`);
            this.isSecondBetButtonDisabled = false;
          } else {
            if (this.secondCashOut) {
              if (this.secondCashOut >= 1.01) {
                this.newBetPlaced = true;
                this.disableSecondInput = true;
                this.autoBetForm.disable();
                this.incrementDecrementButton.disableSecondDecrementButton = true;
                this.incrementDecrementButton.disableSecondIncrementButton = true;
                this.fwpComponent.placeBet(this.secondBetAmount, this.secondCashOut, isFirstBetInput);
              } else {
                this.fwpComponent.showModal(`${enterCashoutValueText}`);
              }
            } else if (this.secondCashOut !== 0) {
              this.newBetPlaced = true;
              this.disableSecondInput = true;
              this.autoBetForm.disable();
              this.incrementDecrementButton.disableSecondDecrementButton = true;
              this.incrementDecrementButton.disableSecondIncrementButton = true;
              this.fwpComponent.placeBet(this.secondBetAmount, this.secondCashOut, isFirstBetInput);
            }
          }
        } else {
          this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
        }
      }
    } else {
      if (isFirstBetInput) {
        this.isFirstBetButtonDisabled = true;

        this.betPlacedFrom = 'first';

        if (this.disableFirstBet) {
          this.popupText = this.textConfig.waitingForNextRound || 'Bet placed for the next round';
          // this.fwpComponent.showModal(this.popupText);
          this.nextRoundBetForFirstBetInput = this.firstBetAmount;
          this.showWaitingForNextRoundText(true)
          this.autoBetForm.disable();

          return
        }
        this.isFirstBetButtonwaitingForNextRound = false;
        // console.warn('ON bet placed', this.cashOut);

        let minimumBetAmountText: string = this.textConfig.minimumBetAmount;
        let enterCashoutValueText: string = this.textConfig.enterCashoutValueText;
        this.betType = 'manual';
        if (this.firstBetAmount > 0) {
          if ((this.fwpComponent.isShabiki || this.fwpComponent.isStarbet || this.fwpComponent.isWeAreCasino) && this.firstBetAmount < this.fwpComponent.minBet) {
            this.fwpComponent.showModal(`${minimumBetAmountText} ${this.currencyCode} ${this.fwpComponent.minBet}`);
          } else {
            if (this.firstCashOut) {
              if (this.firstCashOut >= 1.01) {
                this.newBetPlaced = true;
                this.disableFirstInput = true;
                this.autoBetForm.disable();
                this.fwpComponent.placeBet(this.firstBetAmount, this.firstCashOut, isFirstBetInput);
              } else {
                this.fwpComponent.showModal(`${enterCashoutValueText}`);
              }
            } else if (this.firstCashOut !== 0) {
              this.newBetPlaced = true;
              this.disableFirstInput = true;
              this.autoBetForm.disable();
              this.fwpComponent.placeBet(this.firstBetAmount, this.firstCashOut, isFirstBetInput);
            }
          }
        } else {
          this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
        }
      }
    }
  }

  showWaitingForNextRoundText(isFirstBetInput: boolean) {

    if (isFirstBetInput) {
      this.isFirstBetButtonwaitingForNextRound = true;
    } else {
      if (this.isCertification)
        this.isSecondBetButtonwaitingForNextRound = true;
    }
  }

  showBetPlaced(event: boolean, betResponse: any) {


    if (event) {
      const betID = betResponse.betId;
      // if(betResponse)
      if (betID == this.betIdObject.firstBetId) {
        this.showBetPlacedTextForFirstButton = true;
      }
      if (betID == this.betIdObject.secondBetId) {
        this.showBetPlacedTextForSecondButton = true;
      }

      if (this.betType == 'auto' && JSON.stringify(this.autoBetData) !== JSON.stringify({})) {
        this.showBetPlacedTextForSecondButton = false;
      }

    } else {
      this.showBetPlacedTextForFirstButton = false;
      this.showBetPlacedTextForSecondButton = false;
    }
    // if (this.betPlacedFrom == "first")
    //   this.showBetPlacedTextForFirstButton = event;
    // if (this.betPlacedFrom == "second" && this.isCertification)
    //   this.showBetPlacedTextForSecondButton = event;
  }

  setBetResponse(data: any) {
    // console.warn("betIdObject ", this.betIdObject);
    if (this.betIdObject.firstBetId !== "") {
      this.disableFirstBet = true;
    }
    if (this.betIdObject.secondBetId !== "") {
      this.disableSecondBet = true;
    }
    // // // debugger
    // this.disableBet = true;
    // console.warn("Set bet response ", data)
    this.lastBetAmount = data.betRequest ? data.betRequest.betAmount : 0;
    if (data.betRequest && data.betRequest.betAmount) {
      this.betSessionPlaced = true;
    }
  }

  setCashOut(data: any) {

    console.warn("data ", data);
    if (data.autoCashOutMultiplier && data.betId) {
      if (this.betIdObject.firstBetId == data.betId) {
        this.firstBetCashoutClicked = true;
      }
      if (this.betIdObject.secondBetId == data.betId) {
        this.secondBetCashoutClicked = true;
      }
    }

    // debugger
    if (this.firstBetCashoutClicked) {
      this.showBetAmountFirstBetButton = true;
      this.showCashOutFirstBetButton = false;
    }
    if (this.secondBetCashoutClicked) {
      this.showBetAmountSecondBetButton = true;
      this.showCashOutSecondBetButton = false;
    }

    this.betSessionPlaced = false;
  }

  cashOutChange(value: any) {
  }

  resetBetPanel(autoplayBets: any, newMatch?: boolean) {
    this.showStopAutoPlayButton = false;
    this.betIdObject = { firstBetId: '', secondBetId: '' }
    this.isFirstBetButtonDisabled = false;
    this.isSecondBetButtonDisabled = false;
    this.autoplayBets = autoplayBets;
    this.fwpComponent.scoreStarted = false;
    this.autoBetPlaced = false;
    this.incrementDecrementButton.disableFirstDecrementButton = false;
    this.incrementDecrementButton.disableFirstIncrementButton = false;
    this.incrementDecrementButton.disableSecondDecrementButton = false;
    this.incrementDecrementButton.disableSecondIncrementButton = false;
    this.currentCashOutValueObject = {
      firstCashoutValue: '',
      secondCashoutValue: ''
    }
    if (autoplayBets) {
      this.isFirstBetButtonDisabled = true;
      this.isSecondBetButtonDisabled = true;
      this.autoBetPlaced = true;
      this.autoplayContent = this.fwpComponent.getTotalBetsInAuto() + `${this.textConfig.betsRemaining}`;
      this.autoplayContentChange = true;
      this.autoBetForm.disable();
    } else {
      this.autoplayContent = this.buttonConfig.startAutoplay || `${this.buttonConfig.startAutoplay}`;
      this.autoplayContentChange = false;
    }
    this.showCashOutFirstBetButton = false;
    this.showBetAmountFirstBetButton = true;
    if (this.isCertification) {
      this.showCashOutSecondBetButton = false;
      this.showBetAmountSecondBetButton = true;
    }
    if (autoplayBets == 0) {
      // // // debugger
      this.newBetPlaced = false;
      this.autoBetForm.enable();
      this.disableBet = false;
      this.disableFirstInput = false;
      this.disableSecondInput = false;
    }
    if (!this.firstBetAmount) {
      this.disableBet = true
    }
    this.betSessionPlaced = false;
    this.firstBetButtonCashedOut = false;
    this.secondBetButtonCashedOut = false;
    this.score = null;
    if (newMatch) {
      if (this.nextRoundBetForFirstBetInput) {
        this.onBetPlaced(true);
        this.nextRoundBetForFirstBetInput = null;
      }
      if (this.nextRoundBetForSecondBetInput && this.isCertification) {
        this.onBetPlaced(false);
        this.nextRoundBetForSecondBetInput = null;
      }
    }
    if (this.nextRoundAutoBet && newMatch) {
      this.autoBetFormSubmit('play_btn');
      this.nextRoundAutoBet = null;

    }
    this.disableFirstBet = false;
    this.disableSecondBet = false;

    this.firstBetCashoutClicked = false;
    this.secondBetCashoutClicked = false;
  }

  setScore(data: any) {
    this.score = data;

    let stakeAmount = this.firstBetAmount;
    if (this.autoBetPlaced) {
      this.showStopAutoPlayButton = true;
      stakeAmount = this.autoBetAmount
    }

    this.currentCashOutValueObject = {
      firstCashoutValue: this.fwpComponent.formatCurrency((this.score / 100) * stakeAmount),
      secondCashoutValue: this.fwpComponent.formatCurrency((this.score / 100) * this.secondBetAmount)
    }

    !this.firstBetButtonCashedOut && (this.isFirstBetButtonDisabled = false)
    !this.secondBetButtonCashedOut && (this.isSecondBetButtonDisabled = false)

    if (this.nextRoundAutoBet) {
      this.isFirstBetButtonDisabled = true;
    }

    if (this.score && this.playClicked) {
      console.log("-------");
      // this.autoplayContent = (this.autoBetForm.value.totalBets - 1) + ' Bets Remaining';
      this.autoplayContentChange = true;
      this.playClicked = false;
    }
    if (this.betSessionPlaced) {
      if (this.betIdObject.firstBetId !== "") {
        this.showCashOutFirstBetButton = true;
        this.showBetAmountFirstBetButton = false;
        if (this.betIdObject.secondBetId !== "")
          this.showBetAmountSecondBetButton = false;
        else this.showBetAmountSecondBetButton = true;
      }
      if (this.betIdObject.secondBetId !== "") {
        this.showCashOutSecondBetButton = true;
        if (this.betIdObject.firstBetId !== "")
          this.showBetAmountFirstBetButton = false;
        else this.showBetAmountFirstBetButton = true;
        this.showBetAmountSecondBetButton = false;
      }

      this.showBetPlaced(false, false);
    }
    if (this.betType == 'auto' && JSON.stringify(this.autoBetData) !== JSON.stringify({})) {
      this.showBetAmountFirstBetButton = true;
      this.showBetAmountSecondBetButton = true;
      this.showCashOutFirstBetButton = false;
      this.showCashOutSecondBetButton = false;
    }
    if (this.fwpComponent.autoplay || this.showCashOutFirstBetButton) {
      this.isFirstBetButtonDisabled = true;
    }
    if (this.fwpComponent.autoplay || this.showCashOutSecondBetButton) {
      this.isSecondBetButtonDisabled = true;
    }
    this.disableFirstBet = true;
    this.disableSecondBet = true;
  }

  onCashOut(isFirstBetInput: boolean) {
    if (isFirstBetInput) {
      this.fwpComponent.cashOut(this.betIdObject.firstBetId);
      this.firstBetCashoutClicked = true;
      this.disableFirstInput = false;
      this.incrementDecrementButton.disableFirstIncrementButton = false;
      this.incrementDecrementButton.disableFirstDecrementButton = false;
    } else {
      if (this.isCertification) { 
        // debugger
        this.fwpComponent.cashOut(this.betIdObject.secondBetId);
        this.secondBetCashoutClicked = true;
        this.disableSecondInput = false;
        this.incrementDecrementButton.disableSecondIncrementButton = false;
      this.incrementDecrementButton.disableSecondDecrementButton = false;
      }
    }

  }

  onPlayClicked(autoPlayBetData: any) {

    this.isFirstBetButtonDisabled = true;
    if (!autoPlayBetData.autoCashout || !autoPlayBetData.totalBets || autoPlayBetData.totalBets < 1
      || (autoPlayBetData.totalBets % 1 != 0)) {
      if (!autoPlayBetData.autoCashout) {
        this.errorText = `${this.inputConfig.autoCashout}`;
      }
      if (!autoPlayBetData.totalBets) {
        this.errorText = `${this.inputConfig.totalBets}`;
      }
      this.fwpComponent.showModal(`${this.textConfig.pleaseEnter} ${this.errorText} value`);
    } else if (autoPlayBetData.stopOnProfit && autoPlayBetData.stopOnProfit < 0) {
      this.fwpComponent.showModal(`${this.textConfig.pleaseEnter} ${this.textConfig.stopOnProfitPerc}`);
    } else if (autoPlayBetData.stopOnLoss && autoPlayBetData.stopOnLoss < 0) {
      this.fwpComponent.showModal(`${this.textConfig.pleaseEnter} ${this.textConfig.stopOnLossPerc}`);
    } else if (autoPlayBetData.autoBetAmount < this.fwpComponent.minBet) {
      this.fwpComponent.showModal(`${this.textConfig.minimumBetAmount} ${this.currencyCode} ${this.fwpComponent.minBet}`);
      this.isFirstBetButtonDisabled = false;
    }
    else {
      this.autoplayContent = autoPlayBetData.totalBets + `${this.textConfig.betsRemaining}`;
      this.autoplayContentChange = true;

      this.autoBetForm.disable();
      this.newBetPlaced = true;
      this.autoplayBet.emit(autoPlayBetData);
      this.playClicked = true;
    }
  }

  stopAutoplay() {
    this.isAutoplay = false;
  }

  setCurrencyCode(data: any) {
    this.currencySymbol = this.fwpComponent.currencySymbol;
    if (data == "XAF") {
      this.currencyCode = data;
    } else if (data == "BRL" || data == "R$") {
      this.currencyCode = "BRL";
    }
    else {
      this.currencyCode = getCurrencySymbol(data, "narrow");
    }
  }

  activateBetPanel(type: any) {
    (type === "manual") ? this.active = 1 : this.active = 2;
    if (type === "auto") {
      document.getElementById("desktop-component")!.style.visibility = "hidden";
      document.getElementById("desktop-bottom-section")!.style.height = "260px";
      document.getElementById("desktop-component")!.style.marginTop = "unset";

      setTimeout(function () {
        if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
          window.parent.postMessage({
            action: "b2bresize",
            scrollHeight: document.getElementById('game-container')?.clientHeight! + document.getElementById('history-desktop-main-container')?.clientHeight!
          }, (document.referrer || (window.parent && window.parent.location.href)));

        }
      }, 1000);
      // this.fwpComponent.setIframeHeight({"appHeight":this.fwpComponent.lastAppHeight});
    } else {
      document.getElementById("desktop-component")!.style.visibility = "visible";
      document.getElementById("desktop-bottom-section")!.style.height = "unset";
    }


    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: document.getElementById('game-container')?.clientHeight! + document.getElementById('history-desktop-main-container')?.clientHeight!
        }, (document.referrer || (window.parent && window.parent.location.href)));

      }
    }, 1000);

  }

  multiplyBetAmount(type: any, isFirstBetInput: boolean) {    
    if (this.isCertification) {
      const betAmount = isFirstBetInput ? this.firstBetAmount : this.secondBetAmount;
      const betAmountKey = isFirstBetInput ? 'firstBetAmount' : 'secondBetAmount';
      this.autoBetAmount = (this.active === 1 ? betAmount : this.autoBetAmount);

      if (type === "half") {
        this[betAmountKey] = betAmount * 0.5;
      } else if (type === "twice") {
        this[betAmountKey] = betAmount * 2;
      } else {
        this[betAmountKey] = this.maxBet;
      }

      if (this.autoBetAmount && this.active === 2) {
        if (type === "half") {
          this.autoBetAmount = betAmount * 0.5;
        } else if (type === "twice") {
          this.autoBetAmount = betAmount * 2;
        } else {
          this.autoBetAmount = this.maxBet;
        }
      }

      this.betAmountChange(this[betAmountKey], isFirstBetInput);
      this.autoBetForm.patchValue({
        autoBetAmount: this.autoBetAmount || 20.00
      });

      if (isFirstBetInput) {
        this.firstMultiplyBtnType = type;
      } else {
        this.secondMultiplyBtnType = type;
      }
    } else {
      this.firstMultiplyBtnType = type;
      if (type === "half") {
        (this.active == 1) ? this.firstBetAmount = this.firstBetAmount * 0.5 : this.autoBetAmount = this.autoBetAmount * 0.5;
      }
      else if (type === "twice") {
        (this.active == 1) ? this.firstBetAmount = this.firstBetAmount * 2 : this.autoBetAmount = this.autoBetAmount * 2;
      }
      else {
        (this.active == 1) ? this.firstBetAmount = this.maxBet : this.autoBetAmount = this.maxBet;
      }
      this.betAmountChange(this.firstBetAmount, true);
      this.autoBetForm.patchValue({
        autoBetAmount: !this.autoBetAmount ? 20.00 : this.autoBetAmount
      })
    }
  }

  updateBetAmount(value: any, isFirstBetInput: boolean) {
    if (this.isCertification) {
      const betAmount = isFirstBetInput ? this.firstBetAmount : this.secondBetAmount;
      const betAmountKey = isFirstBetInput ? 'firstBetAmount' : 'secondBetAmount';
      this.autoBetAmount = (this.active === 1 ? betAmount : this.autoBetAmount);

      this[betAmountKey] = value;

      if (this.autoBetAmount && this.active === 2) {
        this.autoBetAmount = value;
      }

      this.betAmountChange(this[betAmountKey], isFirstBetInput);
      this.autoBetForm.patchValue({
        autoBetAmount: this.autoBetAmount || 20.00
      });

      if (isFirstBetInput) {
        this.updatedBetAmountTypeObject.firstBetAmountType = value;
        if(value > this.fwpComponent.minBet){
          this.incrementDecrementButton.disableFirstDecrementButton = false;
        }
      } else {
        this.updatedBetAmountTypeObject.secondBetAmountType = value;
        if(value > this.fwpComponent.minBet){
          this.incrementDecrementButton.disableSecondDecrementButton = false;
        }
      }

    } else {
      (this.active == 1) ? this.firstBetAmount = value : this.autoBetAmount = value;
      this.betAmountChange(this.firstBetAmount, true);
      this.autoBetForm.patchValue({
        autoBetAmount: !this.autoBetAmount ? 20.00 : this.autoBetAmount
      })
    }
  }

  updateBetValue(isAutoplay: boolean, isFirstBetInput: boolean, value: any) {
    if (!isAutoplay) {
      if (isFirstBetInput) {
        this.incrementDecrementButton.disableFirstDecrementButton = false;
        this.incrementDecrementButton.disableFirstIncrementButton = false;
        this.firstBetAmount += value;
        if ((this.firstBetAmount - 10) < this.fwpComponent.minBet) {
          this.incrementDecrementButton.disableFirstDecrementButton = true;
        }
        if ((this.firstBetAmount + 10) > this.fwpComponent.maxBet) {
          this.incrementDecrementButton.disableFirstIncrementButton = true;
        }
      }
      else if (!isFirstBetInput) {
        this.incrementDecrementButton.disableSecondDecrementButton = false;
        this.incrementDecrementButton.disableSecondIncrementButton = false;
        this.secondBetAmount += value;
        if ((this.secondBetAmount - 10) < this.fwpComponent.minBet) {
          this.incrementDecrementButton.disableSecondDecrementButton = true;
        }
        if ((this.secondBetAmount + 10) > this.fwpComponent.maxBet) {
          this.incrementDecrementButton.disableSecondIncrementButton = true;
        }
      }
    } else {
      this.autoBetAmount += value;
      this.incrementDecrementButton.disableAutoDecrementButton = false;
      this.incrementDecrementButton.disableAutoIncrementButton = false;
      if ((this.autoBetAmount - 10) < this.fwpComponent.minBet) {
        this.incrementDecrementButton.disableAutoDecrementButton = true;
      }
      if ((this.autoBetAmount + 10) > this.fwpComponent.maxBet) {
        this.incrementDecrementButton.disableAutoIncrementButton = true;
      }
      this.autoBetForm.patchValue({
        autoBetAmount: this.autoBetAmount
      })

    }
  }

  changeCashoutMultiplier(value: any, isFirstBetInput: boolean, isAutoplay: boolean) {
    if (!isAutoplay) {
      if (isFirstBetInput) {
        this.firstCashOut = value;
        this.updatedCashoutMultiplierObject.firstMultiplierType = value;
      } else {
        this.secondCashOut = value;
        this.updatedCashoutMultiplierObject.secondMultiplierType = value;
      }
    } else {
      this.autoCashout = value;
      this.autoBetForm.patchValue({
        autoCashout: this.autoCashout
      })
      this.autoActiveClassObject.updatedCashoutMultiplier = value;
    }
  }

  onCloseBtnClicked(isFirstBetInput: boolean, isAutoPanel: boolean) {
    if(isAutoPanel){
      this.autoCashout = "";
      this.autoBetForm.patchValue({
        autoCashout: this.autoCashout
      })
    } else {
      if(isFirstBetInput){
        this.firstCashOut = "";
      } else {
        this.secondCashOut = "";
      }
    }
  }

  getClassOf(value: any) {
    if (value >= 1.00 && value <= 1.20) {
      return "bg-red";
    }
    else if (value >= 1.21 && value <= 1.99) {
      return "bg-orange";
    }
    else if (value >= 2.00 && value <= 4.99) {
      return "bg-yellow";
    }
    else if (value >= 5.00 && value <= 9.99) {
      return "bg-blue";
    }
    else if (value >= 10.00 && value <= 24.99) {
      return "bg-green";
    }
    else if (value >= 25.00 && value <= 49.99) {
      return "bg-white";
    }
    else if (value >= 50.00 && value <= 99.99) {
      return "bg-cream";
    }
    else {
      return "bg-pink";
    }

  }

  betAmountChange(value: any, isFirstBetInput: boolean) {
    if (isFirstBetInput) {
      if (value > this.maxBet) {
        this.showTooltip = true;
        this.firstBetAmount = this.fwpComponent.defaultBet;
        this.disableFirstBet = true;
        setInterval(() => {
          this.showTooltip = false;
          this.disableFirstBet = false;
        }, 1500)
      }
      else if (!value) {
        this.disableFirstBet = true
      }
      else {
        this.disableFirstBet = false
        this.firstBetAmount = Number(value.toFixed(2))
      }
    } else {
      if (this.isCertification) {
        if (value > this.maxBet) {
          this.showTooltip = true;
          this.secondBetAmount = this.fwpComponent.defaultBet;
          this.disableSecondBet = true;
          setInterval(() => {
            this.showTooltip = false;
            this.disableSecondBet = false;
          }, 1500)
        }
        else if (!value) {
          this.disableSecondBet = true
        }
        else {
          this.disableSecondBet = false
          this.secondBetAmount = Number(value.toFixed(2))
        }
      }
    }
  }

  checkNumber(event: any) {
    return !(event.keyCode === 103 || event.keyCode === 187 || event.keyCode === 189 || event.keyCode === 69 || event.keyCode == 109)
  }

  onAutoBetInputChanges() {
    this.autoBetForm.valueChanges.subscribe(val => {
      let { autoBetAmount, autoCashout, totalBets } = val;

      if (autoBetAmount > this.maxBet) {
        this.showTooltip = true;
        this.newBetPlaced = false;
        this.autoBetForm.patchValue({
          autoBetAmount: 20.00
        })
        setInterval(() => {
          this.showTooltip = false;
        }, 1500)
      }

      if (totalBets > this.maxBet) {
        this.autoBetForm.patchValue({
          totalBets: 5
        })
      }
      if (totalBets < 1 || (totalBets % 1 != 0)) {
        document.getElementById('total_bets_value')!.style.display = "block";
        setTimeout(() => {
          document.getElementById('total_bets_value')!.style.display = "none";
        }, 1500)
        this.playClicked = false;
      }

      // if (onLossPercent < 0) {
      //   this.autoBetForm.patchValue({
      //     onLossPercent: 0
      //   })
      // }

      if (autoCashout > 1000) {
        this.autoBetForm.patchValue({
          autoCashout: 500
        })
      }

      if (!autoBetAmount || this.autoplayBets) {
        this.newBetPlaced = true;
      }
      else if (autoBetAmount && !this.disableFirstInput) {
        this.newBetPlaced = false;
      }
    })
  }

  autoBetFormSubmit(eventId: any) {
    this.betType = 'auto'
    this.autoBetData = this.autoBetForm.value;
    this.autoBetAmount = this.autoBetData.autoBetAmount;
    this.autoCashout = this.autoBetData.autoCashout;
    this.stopOnProfit = this.autoBetData.stopOnProfit;
    this.stopOnLoss = this.autoBetData.stopOnLoss;
    this.disableBet = true;
    this.disableFirstInput = true;
    this.disableSecondInput = true;
    let minimumBetAmountText: string = this.textConfig.minimumBetAmount;

    if (!this.autoBetAmount || this.autoBetAmount < 0 || this.autoBetAmount == "") {
      this.fwpComponent.showModal(`${this.textConfig.enterValidStakeAmountText}`);
      return
    }

    if (eventId === "play_btn") {
      if (this.autoCashout >= 1.01) {
        if (this.fwpComponent.isShabiki && this.autoBetAmount < 10) {
          this.fwpComponent.showModal(`${this.textConfig.minimumBetAmount} ${this.currencyCode} ${this.fwpComponent.minBet}`);
        } else {
          if (this.score != null) {
            this.popupText = this.textConfig.isFirstBetButtonwaitingForNextRound || 'Bet placed for the next round';
            // this.fwpComponent.showModal(this.popupText);

            this.nextRoundAutoBet = this.autoBetData;
            this.isFirstBetButtonDisabled = true;
            return
          }
          this.onPlayClicked(this.autoBetForm.value);
          this.autoBetPlaced = true;
          this.fwpComponent.prevAccountBalance = this.fwpComponent.playerAccountData.balance;
          this.fwpComponent.totalWin = 0;
          this.fwpComponent.totalLoss = 0;
        }
      }
      else {
        this.errorText = 'Auto Cashout';

        this.fwpComponent.showModal(`Please enter a valid ${this.errorText} value`);
      }
    }
    else if (eventId == "close_btn") {
      this.onCloseBtnClicked(true, true);
    }
    else if (eventId == "stop-auto-play-button") {
      this.stopAutoPlay();
    } else if (eventId == "total-bets-2") {
      this.autoActiveClassObject.totalRoundsType = 2;
      this.autoBetForm.patchValue({
        totalBets: 2
      })
    } else if (eventId == "total-bets-5") {
      this.autoActiveClassObject.totalRoundsType = 5;
      this.autoBetForm.patchValue({
        totalBets: 5
      })
    } else if (eventId == "total-bets-10") {
      this.autoActiveClassObject.totalRoundsType = 10;
      this.autoBetForm.patchValue({
        totalBets: 10
      })
    } else if (eventId == "total-bets-15") {
      this.autoActiveClassObject.totalRoundsType = 15;
      this.autoBetForm.patchValue({
        totalBets: 15
      })
    }
    else if(eventId=="decrement"){
      this.updateBetValue(true, true, -10);
    } else if(eventId == "increment"){
      this.updateBetValue(true, true, 10);
    }
    else {

      if(!this.isMobile) {
        this.multiplyBetAmount(eventId, true);
        return
      }
      let value = 0;
      if (eventId == "bet-25") {
        value = 25
      } else if (eventId == "bet-50") {
        value = 50
      } else if (eventId == "bet-100") {
        value = 100
      }
      this.incrementDecrementButton.disableAutoDecrementButton = false;
      this.incrementDecrementButton.disableAutoIncrementButton = false;
      this.autoActiveClassObject.updatedBetAmountType = value;

      this.updateBetAmount(value, eventId);
    }
  }
  stopAutoPlay() {
    this.fwpComponent.stopAutoplayBet();
    this.resetBetPanel(0);
  }

  onInfoClicked() {
    this.fwpComponent.homeRunBottomComponent.showFWPRulesPage();

  }
  showFWPTermsPage() {
    this.fwpComponent.homeRunBottomComponent.showTerms = true;
  }

  onResize() {

    let widthOfMulitplierSection = document.getElementById('previous-multipliers')?.clientWidth;
    if (widthOfMulitplierSection) {

      widthOfMulitplierSection -= 25;
      this.numberOfPrevMultipliers = Math.round(widthOfMulitplierSection / 50);

    }

    if ((this.isPlayabet || this.isBetyetu) && !this.configService.configData.isMobile) {
      this.numberOfPrevMultipliers = 8;
    }

    if (this.configService.configData.isMobile) {
      this.numberOfPrevMultipliers = 6;
    }

  }
}
