import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRunComponent } from './homerun.component';

describe('HomeRunComponent', () => {
  let component: HomeRunComponent;
  let fixture: ComponentFixture<HomeRunComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeRunComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
