import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PenaltyShootoutComponent } from './penaltyshootout.component';

describe('PenaltyShootoutComponent', () => {
  let component: PenaltyShootoutComponent;
  let fixture: ComponentFixture<PenaltyShootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PenaltyShootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PenaltyShootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
