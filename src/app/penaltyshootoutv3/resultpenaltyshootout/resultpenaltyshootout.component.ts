//@ts-nocheck
import { DatePipe, ViewportScroller, Location } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation, ElementRef, } from "@angular/core";
import { ConfigService } from "src/app/services/config.service";
import { NgbDateStruct, NgbDate, NgbCalendar, NgbDateParserFormatter, } from "@ng-bootstrap/ng-bootstrap";

declare let $: any;

import { Injectable } from "@angular/core";
import { PenaltyShootoutComponent } from "../penaltyshootout.component";
import { HostListener } from "@angular/core";

function padNumber(value: any) {
  if (!isNaN(value) && value !== null) {
    return `0${value}`.slice(-2);
  }
  return "";
}
@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct | null {
    if (value) {
      const dateParts = value.trim().split("/");

      let dateObj: NgbDateStruct = {
        day: <any>null,
        month: <any>null,
        year: <any>null,
      };
      const dateLabels = Object.keys(dateObj);

      dateParts.forEach((datePart, idx) => {
        //@ts-ignore
        dateObj[dateLabels[idx]] = parseInt(datePart, 10) || <any>null;
      });
      return dateObj;
    }
    return null;
  }

  static formatDate(date: NgbDateStruct | NgbDate | null): string {
    return date
      ? `${padNumber(date.day)}/${padNumber(date.month)}/${date.year || ""}`
      : "";
  }

  format(date: NgbDateStruct | null): string {
    return NgbDateCustomParserFormatter.formatDate(date);
  }
}

@Component({
  selector: "app-resultpenaltyshootout",
  templateUrl: "./resultpenaltyshootout.component.html",
  styleUrls: ["./resultpenaltyshootout.component.css"],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
})

export class ResultPenaltyShootoutComponent implements OnInit {
  historyData: any;
  historyDataRes: any = [];
  tournamentHistoryData: any;
  expandedHistoryData: any;
  showResult: boolean = true;
  showOptions: boolean = false;
  showOptionsRef: boolean = false;
  startDate: any;
  endDate: any;
  gameMode: any;
  gameName: any;
  gameType: any;
  tourneyGameType: any;
  showResultData: any = {};
  searchInput: any;
  showSearchInput: boolean = false;
  currencyCode: any = "INR";
  currencySymbol: any;
  tourneyName: any;
  isFreePlayNotAvailable: boolean = false;
  jurisdiction: any;
  taxData: any;
  isShabikiPlatform: boolean = false;
  resultsPageConfig: any;
  textConfig: any;
  isStandAlone: boolean = false;
  showCurrencyCode: boolean = true;
  backButtonURL: any = 'assets/images/backBtn.png';
  showResultOperator: boolean = "true";
  constructor(
    public configService: ConfigService,
    private changeDetectorRef: ChangeDetectorRef,
    private http: HttpClient,
    private viewportScroller: ViewportScroller,
    public datePipe: DatePipe,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    public penaltyShootoutComponent: PenaltyShootoutComponent,
    private _location: Location
  ) {
    this.clubs = ["arsenal", "astonvilla", "brentford", "brighton&Hove_Albion", "burnley", "chelsea", "crystalpalace", "everton", "leedsutd", "leicestercity", "liverpool", "manchestercity", "manutd", "newcastleutd", "norwich", "southampton", "tottenhamhotspur", "watford", "westhamutd", "wolverhampton"];
    this.countries = ["algeria", "argentina", "australia", "belgium", "brazil", "cameroon", "chillie", "colombia", "croatia", "czechia", "england", "france", "germany", "ghana", "greece", "ireland", "italy", "jamaica", "japan", "kenya", "mexico", "netherland", "nigeria", "norway", "portugal", "southafrica", "spain", "turky", "uganda", "uruguay", "us"];
  }

  // @HostListener('document:click', ['$event'])
  // onClickedOutside(event) {
  // let $target = $(event.target);
  // console.warn(event)
  // console.warn(event.target);
  // if (!$target.closest('.options-data').length && $('.options-data').is(":visible")) {
  //   if (this.showOptionsRef) {
  //     this.showOptionsRef = false;
  //   } else {
  //     this.showOptions = false;
  //   }
  // }
  // }

  ngOnInit(): void {
    this.getResultData();
    // this.loadConfig();
    this.startDate = this.calendar.getToday();
    this.endDate = this.calendar.getToday();
    this.gameMode = "Cashplay";
    this.currencyCode = this.penaltyShootoutComponent.currencyCode;
    this.isFreePlayNotAvailable = this.configService.configData.psFreePlayNotAvailable;

    this.stakeTax = this.penaltyShootoutComponent.userDetails.stakeTax;
    this.withholdTax = this.penaltyShootoutComponent.userDetails.withholdTax;


    this.resultsPageConfig = this.penaltyShootoutComponent.resultsPageConfig;
    this.textConfig = this.penaltyShootoutComponent.textConfig;

    // console.log("test", this.configService.configData.platformId)
    // this.isShabikiPlatform = this.configService.configData.platformId == 2;

    this.currencySymbol = this.penaltyShootoutComponent.currencySymbol;
    this.isStandAlone = this.penaltyShootoutComponent.isStandAlone;
    this.setPSConfig();
  }

  ngAfterViewInit(): void {
    setTimeout(()=> {
      this.setParentIframeHeight();
    },1500)
  }

  setPSConfig() {
    for (let p in this.configService.configData.psConfig) {
      let operator = this.configService.configData.psConfig[p];
      if (p == this.configService.configData.operatorId) {
        if (operator.text) {
          setTimeout(() => {
            for (let p in operator.text) {
              this.textConfig[p] = operator.text[p];
            }
          },100)
        }
        if (operator.fonts) {
          $("*").css("fontFamily",operator.fonts.main);
        }
        if (operator.icons) {
          if (operator.icons.psBackgroundImage) {
            setTimeout(()=>{
              $("#results").css("background-image","url(assets/images/"+operator.icons.psBackgroundImage2 + ")");
              $("#results").css("background-size","cover");
            },100);
          }
        }
      }
    }
    if(this.configService.configData.operatorId == "playbigapp") {
      this.showResultOperator = false;
      setTimeout(()=> {
        $("#results .header-row img").css("background","none");
        $("#results .table").css("margin-top","-10px");
        $("#results .header-row h1").css("color","#F6BA23");
        $("#results .header-row h1").css("font-size","2rem");
        $("#results .search-box button").css("background","#F6BA23");
        $("#results .search-box").css("margin-top","10px");
        $("#results thead").css("color","#FFFFFF");
        $("#results thead th").css("color","#FFFFFF");
        $("#results .input-group input").css("border-top","1px solid #3D71EB");
        $("#results .input-group input").css("border-left","1px solid #3D71EB");
        $("#results .input-group input").css("border-bottom","1px solid #3D71EB");
        $("#results .input-group input").css("background","#183468 0% 0% no-repeat padding-box");
        $("#results .input-group input").css("box-shadow","inset 0px 0px 2px #00000099");
        $("#results .input-group input").css("padding","5px 5px 5px 10px");
        $("#results .input-group input").css("font-size",".8rem");
        $("#results .input-group input").css("width","92px");
        $("#results .input-group").css("margin","0 5px 0 0");
        $("#results .search-box").css("margin-left","0");
        $("#results .table").css("margin-left","-1.5rem");
        $("#results .expand-icon img").css("width","6px");
        $("#results .expand-icon").css("padding","0 0 0 .2rem");
        $("#results .date-class .col-8").css("padding","0");
        $("#results .form-group").css("display","flex");
        $("#results .form-group .input-group").css("flex-wrap","nowrap");
        // $("#results .date-class > div:first-child").css("margin","10px");
        $("#results .date-class .to-text").css("display","none");
        $("#results .date-class").css("padding","0 5px");
        $("#results .date-picker").css("justify-content","center");
        $("#results .search-box button").css("height","40px");
        $("#results .input-group-append").css("border-right","1px solid #3D71EB");
        $("#results .input-group-append").css("border-top","1px solid #3D71EB");
        $("#results .input-group-append").css("border-bottom","1px solid #3D71EB");
        $("#results .input-group-append").css("background","#183468 0% 0% no-repeat padding-box");
        $("#results .input-group-append").css("padding","5px 10px 5px 5px");
        $("#results .table tr:not(:first-child) th:not(:first-child)").addClass("t-head");
        $("#results .plus").css("border-radius", "50%");
        $("#results .plus").css("background", "#071027");
        $("#results").css("padding", "2%");
        // $("#results .table thead").css("background-color","#183468 0% 0% no-repeat padding-box");
        // $("#results .table thead").css("background-color","#183468 0% 0% no-repeat padding-box");
        $("#results .date-class").css("justify-content","normal");
        $("#results .search-box button").css("width","100%");
        $("#results .result-time").css("padding-left","0px");

        this.backButtonURL = "assets/images/playbigapp/penaltyShootout/backBtn.png";
        this.showCurrencyCode = false;
        if (this.historyData && !this.historyData.length) {
          $("#results .table").css("margin-left","-0.75rem");
        }
        this.setParentIframeHeight();
      },100)
    }
    if (!this.configService.configData.psConfig[this.configService.configData.operatorId]) {
      $("*").css("fontFamily", "Lalezar-Regular");
    }
    this.setParentIframeHeight();
  }

  toggleDate(date: any, dateType: any) {
    let weeks = $(".ngb-dp-weekday");
    for (let i = 0; i < weeks.length; i++) {
      $(".ngb-dp-weekday")[i].innerHTML = $(".ngb-dp-weekday")[
        i
      ].innerHTML.substring(0, 1);
    }
  }

  getResultData() {
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59:59";
    if (this.startDate) {
      startDate = this.startDate.year + "-" + (this.startDate.month < 10 ? "0" + this.startDate.month : this.startDate.month) + "-" + (this.startDate.day < 10 ? "0" + this.startDate.day : this.startDate.day) + " 00:00:00";
      endDate = this.endDate.year + "-" + (this.endDate.month < 10 ? "0" + this.endDate.month : this.endDate.month) + "-" + (this.endDate.day < 10 ? "0" + this.endDate.day : this.endDate.day) + " 23:59:59";
    }

    if (this.gameMode == "Freeplay") {
      this.gameName = "freePenaltyShootout";
      this.tourneyName = "freeTournamentPenaltyShootout"
    }
    else {
      this.gameName = "penaltyShootout";
      this.tourneyName = "tournamentPenaltyShootout"
    }
    const playerId = this.penaltyShootoutComponent.userDetails.customerId;

    let leaderBoardUrl = this.configService.configData.url.socket + "penaltyShootoutHistory?playerId=" + playerId + "&gameName=" + this.gameName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;
    let tournamentPenaltyShootoutUrl = this.configService.configData.url.socket + "tournamentPenaltyShootoutHistory?playerId=" + playerId + "&gameName=" + this.tourneyName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;

    // let leaderBoardUrl = this.configService.configData.url.socket + "/penaltyShootoutHistory?playerId=" + playerId + "&gameName=" + this.gameName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion + "gameType=" + this.gameType;
    // let tournamentPenaltyShootoutUrl = this.configService.configData.url.socket + "/tournamentPenaltyShootoutHistory?playerId=" + playerId + "&gameName=" + this.tourneyName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion + "gameType=" + this.tourneyGameType;

    this.http.get(leaderBoardUrl).subscribe(
      (data: any) => {
        let result = [];
        for (let p in data) {
          data[p].resultData = []
          data[p].resultData[0] = JSON.parse(data[p].result);
          if (data[p].resultData[0]) {
            let localDate: any = new Date(data[p].createdOn + 'Z');
            let localeDate = localDate.toLocaleString().split(", ");
            data[p].date = localeDate[0];
            data[p].time = localeDate[1];
            // let dateTime = data[p].createdOn.split("T");
            // let date = dateTime[0].split("-").reverse().join("/");
            // data[p].date = date;
            // data[p].time = dateTime[1].substring(0, 5);

            let goalScore = data[p].resultData[0].results.filter(
              (data) => data.result == "GOAL"
            );
            data[p].goalScored = goalScore;
            result.push(data[p]);
            data[p].totalGoalsScored = data[p].goalScored.length;
            data[p].resultData[0].stakeAfterTax = data[p].resultData[0].betAmountAfterTax;
            if (this.gameMode == "Freeplay") {
              data[p].gameName = "Free penalties";
            }
            else {
              data[p].gameName = data[p].resultData[0].totalRounds + " Penalties";
            }
            if (this.clubs.indexOf(data[p].resultData[0].results[0].selectedTeam)) {
              data[p].isClub = true;
            }
          }
          data[p].winAmount = data[p].resultData[0] ? data[p].resultData[0].totalWin : 0;

          data[p].formattedWinAmount = this.penaltyShootoutComponent.formatCurrency(data[p].winAmount);

          data[p].formattedBetAmount = this.penaltyShootoutComponent.formatCurrency(data[p].betAmount);
          // data[p].betAmount = this.penaltyShootoutComponent.formatCurrency(data[p].betAmount);
        }

        this.historyData = result;

        // this.historyDataRes = result;
        this.setPSConfig();
        this.http.get(tournamentPenaltyShootoutUrl).subscribe(
          (tourneyData: any) => {
            let result = [];
            for (let p in tourneyData) {
              tourneyData[p].resultData = JSON.parse(tourneyData[p].result);
              if (tourneyData[p].resultData && tourneyData[p].resultData.length) {

                let localDate: any = new Date(tourneyData[p].createdOn + 'Z');
                let localeDate = localDate.toLocaleString().split(", ");
                tourneyData[p].date = localeDate[0];
                tourneyData[p].time = localeDate[1];
                // let dateTime = tourneyData[p].createdOn.split("T");
                // let date = dateTime[0].split("-").reverse().join("/");
                // tourneyData[p].date = date;
                // tourneyData[p].time = dateTime[1].substring(0, 5);
                tourneyData[p].stakeAfterTax = tourneyData[p].resultData[0].betAmountAfterTax;
                result.push(tourneyData[p]);
              }
            }
            this.tournamentHistoryData = result;
            this.historyDataRes = this.historyData;

            if (this.tournamentHistoryData) {
              for (let p in this.tournamentHistoryData) {
                if (this.tournamentHistoryData[p].resultData && this.tournamentHistoryData[p].resultData.length) {
                  this.tournamentHistoryData[p].gameName = "Tournament";
                  this.tournamentHistoryData[p].tournamentName = this.tournamentName;
                  this.tournamentHistoryData[p].teamName = this.tournamentHistoryData[p].resultData[0].results[0].selectedTeam;

                  this.tournamentHistoryData[p].winAmount = this.tournamentHistoryData[p].resultData[0] ? this.tournamentHistoryData[p].resultData[this.tournamentHistoryData[p].resultData.length - 1].totalWin : 0;
                  this.tournamentHistoryData[p].formattedWinAmount = this.penaltyShootoutComponent.formatCurrency(this.tournamentHistoryData[p].winAmount);

                  this.tournamentHistoryData[p].formattedBetAmount = this.penaltyShootoutComponent.formatCurrency(this.tournamentHistoryData[p].betAmount);
                  this.historyDataRes.push(this.tournamentHistoryData[p]);
                }
              }

            }

            if (this.historyDataRes.length > 0) {
              this.historyDataRes.sort(function (a, b) {
                return new Date(b.createdOn) - new Date(a.createdOn);
              })
            }
            this.setPSConfig();

          },
          (err: any) => {
            console.log(err);
          }
        );
      },
      (err: any) => {
        console.log(err);
      }
    );

  }

  onShowOptions() {
    this.showOptions = !this.showOptions;
    this.showOptionsRef = true;
  }

  onHideOptions() {
    if (this.showOptions) {
      this.showOptions = !this.showOptions;
    }
  }

  onShowResult(i: any) {
    for (let p = 0; p < this.historyDataRes.length; p++) {
      if (p != i) {
        this.showResultData["showResult" + p] = false;
      }
    }
    this.showResultData["showResult" + i] = !this.showResultData["showResult" + i];
    if (this.configService.configData.operatorId == "playbigapp") {
      setTimeout(()=>{
        $("#results thead").css("color","#FFFFFF");
        $("#results .minus").css("border-radius", "50%");
        $("#results .minus").css("background", "#071027");
      },0)
    }
  }

  onGameMode(value: any) {
    if (!this.isFreePlayNotAvailable) {
      this.gameMode = value;

      this.getResultData();
    }

  }

  onDateUpdate(date: string): void {
    this.startDate = date;
  }

  onOpen() {
    this.showResult = true;
    this.gameMode = "Cashplay";
    this.getResultData();
  }

  onClose() {
    this.showResult = false;
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      if ((window as any).ReactNativeWebView) {
        (window as any).ReactNativeWebView.postMessage(JSON.stringify(
          {
            msg: "b2bresize",
            data: this.penaltyShootoutComponent.menuHeight + 120,
          }
        ));
        window.parent.postMessage(
          {
            action: "b2bresize",
            scrollHeight: this.penaltyShootoutComponent.menuHeight,
          },
          (document.referrer || (window.parent && window.parent.location.href))
        );
      } else {
        window.parent.postMessage(
          {
            action: "b2bresize",
            scrollHeight: this.penaltyShootoutComponent.menuHeight,
          },
          (document.referrer || (window.parent && window.parent.location.href))
        );
      }
    }
    this._location.back();
    // this.router.navigate(['../playpenaltyshootout'], { relativeTo: this.route });
  }

  searchClicked() {
    this.getResultData();
    this.showOptions = !this.showOptions;
  }

  format(date: NgbDateStruct): string {
    return "" + date.day + "/" + date.month + "/" + date.year;
  }

  setParentIframeHeight() {
    setTimeout(() => {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        if ((window as any).ReactNativeWebView) {
          (window as any).ReactNativeWebView.postMessage(JSON.stringify(
            {
              msg: "b2bresize",
              data: document.getElementById("results")!.clientHeight + 50,
            }
          ));
        }
        window.parent.postMessage(
          {
            action: "b2bresize",
            scrollHeight: document.getElementById("results")!.clientHeight,
          },
          (document.referrer || (window.parent && window.parent.location.href))
        );
      }
    }, 100)
  }

}
