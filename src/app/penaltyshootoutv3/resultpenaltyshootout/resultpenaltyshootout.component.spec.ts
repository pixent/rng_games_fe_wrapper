import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPenaltyShootoutComponent } from './resultpenaltyshootout.component';

describe('ResultPenaltyShootoutComponent', () => {
  let component: ResultPenaltyShootoutComponent;
  let fixture: ComponentFixture<ResultPenaltyShootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultPenaltyShootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPenaltyShootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
