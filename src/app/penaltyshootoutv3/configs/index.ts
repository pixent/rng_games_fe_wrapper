export const PenaltyshootoutConfig = {
  //bet values
  betValues: {
    winMultiplier: 100,
    betsInfo: {
      "3": { minBet: 10, maxBet: 10000, defaultBets: [10, 100, 500] },
      "4": { minBet: 10, maxBet: 10000, defaultBets: [10, 100, 500] },
      "5": { minBet: 10, maxBet: 10000, defaultBets: [10, 100, 500] },
      tourney: { minBet: 10, maxBet: 5000, defaultBets: [10, 100, 500] },
    },
    potentialWinInfo: {
      "3": 6,
      "4": 8,
      "4_1": 1,
      "5": 15,
      "5_1": 2,
      tourney: 200,
    },
    tourneyPrizeInfo: [200, 50, 10, 4, 2],
    tourneyJPPrize: "1 Million",
    allGoalsScoredPrize: 1000000,
  },

  //socket urls
  urls:{}
};
