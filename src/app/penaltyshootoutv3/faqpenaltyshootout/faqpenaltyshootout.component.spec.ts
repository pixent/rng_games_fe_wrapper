import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqPenaltyShootoutComponent } from './faqpenaltyshootout.component';

describe('FaqPenaltyShootoutComponent', () => {
  let component: FaqPenaltyShootoutComponent;
  let fixture: ComponentFixture<FaqPenaltyShootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaqPenaltyShootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqPenaltyShootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
