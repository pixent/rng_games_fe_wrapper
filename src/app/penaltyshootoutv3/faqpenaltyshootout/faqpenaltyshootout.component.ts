import { ViewportScroller } from '@angular/common';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-faqpenaltyshootout',
  templateUrl: './faqpenaltyshootout.component.html',
  styleUrls: ['./faqpenaltyshootout.component.css']
})
export class FaqPenaltyShootoutComponent implements OnInit {

  rules: any;
  termsAndConditions: any;
  showFaq: boolean = true;
  showPopup: boolean = false;
  languageURL: any; 
  oddGamesContent: any = [];
  isShabiki: boolean = false;
  isShabikiPlatform: boolean = false;
  fullTermstitle: any = 'Read Full Terms and Conditions';
  termsMainTitle: any;
  faqsTitle: any;
  operator: any;
  singleGamePaytableData: any;
  tournamentGamePaytableData: any;
  isStandAlone: boolean = false;
  faqHeight: any;
  backButtonURL: any = 'assets/images/backBtn.png';
  termsAndConditionsOperator: any = true;

  constructor(public viewportScroller: ViewportScroller, private http: HttpClient, public configService: ConfigService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.showFaq = true;
    this.isShabiki = this.configService.configData.platformId == 2;
    this.languageURL = this.doesFileExist();
    this.loadFaqPenaltyShootout();
    this.isStandAlone = this.configService.configData.standalone;
    this.setPSConfig();
    // this.rules = this.configService.configData.faqpsrules;
    // this.viewportScroller.setHistoryScrollRestoration('auto');
    // document.getElementById('iframeElement')!.style.display = "none";
    // document.getElementById("terms-modal")!.style.background = this.configService.configData.bgColor[this.configService.configData.operatorId] || this.configService.configData.bgColor.default;
  }

  doesFileExist(): string {
    this.operator = this.configService.configData.operatorId;
    let language = this.configService.configData.languageCode || "en";
    const jurisdiction = this.configService.configData.url.userRegion.toLowerCase();
    const defaultPath = "/assets/config/in/en/psrules.json"
    console.warn("test in ps ", language, jurisdiction)
    let jsonFileName = 'psrules';

    if (language && jurisdiction) {
      if (jurisdiction) {

        if (this.configService.configData.operatorId == "shabiki") {
          jsonFileName = 'psrulesShabiki';
        } else if (this.configService.configData.operatorId == "playabet") {
          jsonFileName = 'psrulesPlayabet';
        } else if (this.configService.configData.operatorId == "betyetu") {
          jsonFileName = 'psrulesBetyetu';
        } else if (this.configService.configData.operatorId == "paribet") {
          if(this.configService.configData.currencyCode == "UGX"){
            jsonFileName = 'psrulesParibetUGX';
          } else {
            jsonFileName = 'psrulesParibet';
          }
        } else if (this.configService.configData.operatorId == "starbet") {
          jsonFileName = 'psrulesStarbet';
        } else if (this.configService.configData.operatorId == "yangasport") {
          jsonFileName = 'psrulesYangaSport';
        } else if (this.configService.configData.operatorId == "playbigapp") {
          jsonFileName = 'psrulesPlaybigapp';
        }
        console.warn("ps rules faqs ", jsonFileName);
      } else
        return defaultPath;
    }

    if (!language)
      language = 'en';

    let path = `./assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/${jsonFileName}.json`;

    // let path = `../../../assets/config/drc/${language.toLowerCase()}/psrules.json`;

    let xhr = new XMLHttpRequest();
    xhr.open("HEAD", path, false);
    xhr.send();

    if (xhr.status === 404) {
      // console.log("doesFileExist", "fail");
      return defaultPath;
    } else {
      // console.log("doesFileExist", "success");
      return path;
    }
  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
    this.setParentIframeHeight(null, document.getElementById('faq')?.clientHeight);
  }

  @HostListener("window:popstate", ["$event"])
  onPopState(event: any) {
    this.closePopup();
  }

  setPSConfig() {

    for (let p in this.configService.configData.psConfig) {
      let operator = this.configService.configData.psConfig[p];
      if (p == this.configService.configData.operatorId) {
        if (operator.fonts) {
          $("*").css("fontFamily",operator.fonts.main);
        }
      }
    }
    if(this.configService.configData.operatorId == "playbigapp") {
      this.termsAndConditionsOperator = false;
      setTimeout(()=> {
        $("#faq").css("background","#071027");
        $("#terms-modal-popup").css("background","#071027");
        $("#terms-modal-popup .heading").css("background","#071027");
        $("#terms-modal-popup .terms").css("background","#071027");
        $("#terms-modal-popup .heading").css("color","#ffffff");
        $("#terms-modal-popup .terms").css("color","#ffffff");
        $("#faq .header-row h1").css("color","#F6BA23");
        $("#faq .header-row h1").css("font-size","2rem");
        $(".card").css("background","#0F2651");
        $(".header-row img").css("background","none");
        $("#faq").css("padding", "2%");        
        $("#faq .rules-text").css("white-space", "inherit");        
        $("#faq .rules-text").css("text-align", "left");        
        this.backButtonURL = "assets/images/playbigapp/penaltyShootout/backBtn.png";
      },100)
    }
    if (!this.configService.configData.psConfig[this.configService.configData.operatorId]) {
      $("*").css("fontFamily", "Lalezar-Regular");
    }
  }

  onClose() {
    this.showFaq = false;
    this.setParentIframeHeight('closefaq', document.getElementById('faq')?.clientHeight);
    this.router.navigate(['../playpenaltyshootout'], { relativeTo: this.route });
  }

  onOpen() {
    this.showFaq = true;
  }

  closePopup() {
    this.showPopup = false;
    this.scrollTop();
    this.setIframeHeight('faq', this.faqHeight);
  }

  showTermsAndConditions() {
    this.showPopup = true;
    this.faqHeight = document.getElementById('faq')?.clientHeight
    this.scrollTop();
    this.setPSConfig();
    this.setIframeHeight('terms-modal-popup', false);
  }

  setIframeHeight(element: any, originalHeight: any) {
    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        if ((window as any).ReactNativeWebView) {
          (window as any).ReactNativeWebView.postMessage(JSON.stringify(
            {
              msg: "b2bresize",
              data: originalHeight ? originalHeight : document.getElementById(element)?.clientHeight! + 220,
            }
          ));
          window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: originalHeight ? originalHeight : document.getElementById(element)?.clientHeight! + 100
        }, (document.referrer || (window.parent && window.parent.location.href)));
        } else {
            window.parent.postMessage({
            action: "b2bresize",
            scrollHeight: originalHeight ? originalHeight : document.getElementById(element)?.clientHeight! + 100
          }, (document.referrer || (window.parent && window.parent.location.href)));
        }
      }
    }, 1000);
  }



  loadFaqPenaltyShootout() {
    return new Promise((resolve, reject) => {
      this.http.get(this.languageURL)
        .subscribe((data: any) => {
          let faqResponse: any = [];
          if (this.configService.configData.psFreePlayNotAvailable) {
            for (let i = 0; i < data['faqpsrules'].length; i++) {
              if (!data['faqpsrules'][i].freePlayFlag) {
                faqResponse.push(data['faqpsrules'][i]);
              } else {
                if (data['faqpsrules'][i].text2) {
                  data['faqpsrules'].text = data['faqpsrules'][i].text2;
                  faqResponse.push(data['faqpsrules'][i]);
                }
              }
            }
            this.rules = faqResponse;
          } else {
            this.rules = data['faqpsrules'];
          }
          let termsResponse: any = [];
          let tncData = data['termsAndConditions'].newTerms;

          if (data.singleGamePaytable && data.tournamentGamePaytable) {
            this.singleGamePaytableData = data.singleGamePaytable;
            this.tournamentGamePaytableData = data.tournamentGamePaytable;
          }

          this.faqsTitle = data.faqsTitle;

          if (data['termsAndConditions'].fullTermstitle) {
            this.fullTermstitle = data['termsAndConditions'].fullTermstitle;
          }
          if (this.configService.configData.psFreePlayNotAvailable && this.configService.configData.operatorId != "playbigapp") {
            for (let i = 0; i < tncData.length; i++) {
              if (!tncData[i].freePlayFlag) {
                if (tncData[i].terms2) {
                  tncData[i].terms = tncData[i].terms2;
                  termsResponse.push(tncData[i]);
                } else {
                  termsResponse.push(tncData[i]);
                }
              }
            }
            this.termsAndConditions = {
              "title": "TERMS and CONDITIONS",
              "newTerms": termsResponse
            }
          } else {
            // console.warn(data);
            this.termsAndConditions = data['termsAndConditions'];
          }
          this.termsMainTitle = this.termsAndConditions.mainTitle;

          if (this.termsAndConditions.newTerms[3] && this.termsAndConditions.newTerms[3].checkAllignment) {
            this.oddGamesContent = this.termsAndConditions.newTerms[3].terms;
          }
          this.setParentIframeHeight(null, document.getElementById('faq')?.clientHeight);
          resolve(true);
        }, (err: any) => {
          this.http.get("assets/config/in/en/psrules.json")
            .subscribe((data: any) => {
              let faqResponse: any = [];
              if (this.configService.configData.psFreePlayNotAvailable) {
                for (let i = 0; i < data['faqpsrules'].length; i++) {
                  if (!data['faqpsrules'][i].freePlayFlag) {
                    faqResponse.push(data['faqpsrules'][i]);
                  } else {
                    if (data['faqpsrules'][i].text2) {
                      data['faqpsrules'].text = data['faqpsrules'][i].text2;
                      faqResponse.push(data['faqpsrules'][i]);
                    }
                  }
                }
                this.rules = faqResponse;
              } else {
                this.rules = data['faqpsrules'];
              }
              let termsResponse: any = [];
              let tncData = data['termsAndConditions'].newTerms;

              if (data.singleGamePaytable && data.tournamentGamePaytable) {
                this.singleGamePaytableData = data.singleGamePaytable;
                this.tournamentGamePaytableData = data.tournamentGamePaytable;
              }

              this.faqsTitle = data.faqsTitle;

              if (data['termsAndConditions'].fullTermstitle) {
                this.fullTermstitle = data['termsAndConditions'].fullTermstitle;
              }
              if (this.configService.configData.psFreePlayNotAvailable) {
                for (let i = 0; i < tncData.length; i++) {
                  if (!tncData[i].freePlayFlag) {
                    if (tncData[i].terms2) {
                      tncData[i].terms = tncData[i].terms2;
                      termsResponse.push(tncData[i]);
                    } else {
                      termsResponse.push(tncData[i]);
                    }
                  }
                }
                this.termsAndConditions = {
                  "title": "TERMS and CONDITIONS",
                  "newTerms": termsResponse
                }
              } else {
                // console.warn(data);
                this.termsAndConditions = data['termsAndConditions'];
              }
              this.termsMainTitle = this.termsAndConditions.mainTitle;

              if (this.termsAndConditions.newTerms[3] && this.termsAndConditions.newTerms[3].checkAllignment) {
                this.oddGamesContent = this.termsAndConditions.newTerms[3].terms;
              }

              resolve(true);
            })
        });
    });
  }

  scrollTop() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({ action: "scrolltotop" }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }

  setParentIframeHeight(type: any, height: any) {
    // let appHeight = Math.max(height,window.outerHeight);
    setTimeout(() => {
      document.getElementById("iframeElement")!.style.height = (window.parent as any).initialWindowHeight + "px";
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "frameresize",
          scrollHeight: height + 100,
          type: type || "faq"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 500);
  }
}
