import { OnDestroy, Renderer2, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalpopupComponent } from '../modalpopup/modalpopup.component';
import { ViewportScroller, Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbCarousel, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from 'src/app/services/config.service';
import { SocketService } from 'src/app/services/socket.service';
import { PenaltyShootoutComponent } from '../penaltyshootout.component';
import { PenaltyshootoutConfig } from '../configs/index';
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { PenaltyShootoutService } from 'src/app/services/penalty-shootout.service';
import { Pipe, PipeTransform } from '@angular/core';
import { getCurrencySymbol, CommonModule } from '@angular/common';

// @Pipe({
//   name: 'currencySymbol'
// })

@Component({
  selector: 'app-cashplayscreenpenaltyshootout',
  templateUrl: './cashplayscreenpenaltyshootout.component.html',
  styleUrls: ['./cashplayscreenpenaltyshootout.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CashplayscreenpenaltyshootoutComponent implements OnInit, OnDestroy, PipeTransform {
  penaltyBtn: any = 3;
  time: any;
  isSinglePlayer: boolean = true;
  images = ["argentinaIcon", "australiaIcon", "belgiumIcon", "brazilIcon", "cameroonIcon", "canadaIcon", "costaricaIcon", "croatiaIcon", "denmarkIcon", "ecuadorIcon", "englandIcon", "franceIcon", "germanyIcon", "ghanaIcon", "iranIcon", "japanIcon", "southkoreaIcon", "mexicoIcon", "moroccoIcon", "netherlandsIcon", "polandIcon", "portugalIcon", "qatarIcon", "saudiarabiaIcon", "senegalIcon", "serbiaIcon", "spainIcon", "switzerlandIcon", "tunisiaIcon", "uruguayIcon", "usaIcon", "walesIcon"].map((n) => `sprite-${n}`);
  clubs = ["argentina", "australia", "belgium", "brazil", "cameroon", "canada", "costarica", "croatia", "denmark", "ecuador", "england", "france", "germany", "ghana", "iran", "japan", "southkorea", "mexico", "morocco", "netherlands", "poland", "portugal", "qatar", "saudiarabia", "senegal", "serbia", "spain", "switzerland", "tunisia", "uruguay", "usa", "wales"];
  countries = ["algeria", "argentina", "australia", "belgium", "brazil", "cameroon", "chillie", "colombia", "croatia", "czechia", "england", "france", "ghana", "greece", "ireland", "italy", "jamaica", "japan", "kenya", "southkorea", "mexico", "netherlands", "nigeria", "norway", "portugal", "southafrica", "spain", "turky", "uganda", "uruguay", "usa"];
  countriesNew = ["argentina", "australia", "belgium", "brazil", "cameroon", "canada", "costarica", "croatia", "denmark", "ecuador", "england", "france", "germany", "ghana", "iran", "japan", "southkorea", "mexico", "morocco", "netherlands", "poland", "portugal", "qatar", "saudiarabia", "senegal", "serbia", "spain", "switzerland", "tunisia", "uruguay", "usa", "wales"];
  updatedClubs: any;
  currencyCode: any;
  playerId: any;
  updatedCountries: any;
  imagesFormatted: any;
  isCashPlay: boolean = false;
  showToolTip: boolean = false;
  showToolTipIcon: boolean = false;
  loadedIframe: any;
  leaderboardData: any;
  leaderboardDataRes: any = [];
  potentialWin: any = 0.00;
  currentUserData: CurrentUserDetails = { teamImg: 'assets/images/countryFlags/argentinaIcon.png', username: '', penaltyScored: '', position: '', playerId: '' };
  stakeAmount: any;
  url: any;
  panelOpenState: boolean = false;
  iframeUrl: any;
  maxBet: any;
  penaltyType: any = "3";
  multiplier: any;
  logoOrIconSelected: any = "argentinaIcon";
  userDetails: any;
  defaultBets: any = [];
  matchDetails: any;
  playerAccountDetails!: PlayerAccDetails;
  messageSubscription: Subscription;
  penaltyShootoutSubscription: Subscription;
  gameInitData: any;
  betSessionResponse: any;
  freePSResponse: any;
  tourneyJoinResponse: any;
  gameExitData: any = {};
  playerAccount: any = {};
  emptyUsername = '';
  currPlayerId: any;
  playButtonDisabled: boolean = true;
  potentialWinInfoText: any;
  tournamentPTInfo: any;
  unfinishedData: any = {};
  isUnfinishedGame: boolean = false;
  @ViewChild('iFrameContainer') iFrameContainer!: ElementRef;
  @ViewChild(MatAccordion) accordion!: MatAccordion;
  showPlayForFreeOption: boolean = true;
  jurisdiction: any;
  taxData: any;
  showTaxData: boolean = false;
  oddsMultiplier: any;
  stakeTax: any;
  withholdTax: any;
  exitGameData: any;
  showInsufficientFunds: boolean = false;
  isShabikiPlatform: boolean = false;
  withHoldingTax: number | undefined;
  jackpotPrize: number = 2000;
  disableButtons: boolean = false;
  freePlayerUserName: any = '';
  freeSingleTrialDone: boolean = false;
  showShabikiTrialAlert: boolean = false;
  freeTourneyTrialDone: boolean = false;
  tournyGameConfigData: any;
  gameConfigData: any;
  tourneyPlayerLogo: any;
  singlePlayerLogo: any;
  genericErrorMessage: any;
  ballKickRequestSent: boolean = false;
  networkTimeout!: NodeJS.Timeout;
  showFreePlayPrizes: boolean = true;
  stakeAfterTax: any;
  psTitleImage: string;
  _isShabikiPlatform: boolean = false;
  _isPlayabetPlatform: boolean = false;
  _isBetyetuPlatform: boolean = false;
  _isStarbetPlatform: boolean = false;
  enableFreePlayCondition: boolean = false;
  showPopup: boolean = false;
  freePlayRequiredBetAmount: any;
  buttonConfig: any = {};
  textConfig: any = {};
  cashTournamentPlay: any;
  prizes: any;
  isTournamentAvailable: boolean = true;
  currencySymbol: any;
  tournamentJPPrize: boolean = true;
  isStandAlone: boolean = false;
  tournamentPlayerBg: any = '';
  singlePlayerBg: any = '';
  singlePlayerActiveBg: any = '';
  tournamentPlayerActiveBg: any = '';
  activeColor: any = '#ffffff';
  showCurrencyCode: boolean = true;
  showAppElements: boolean = true;
  payoutData: any = [];
  totalCoins: any = '';
  appLeaderboardData: any = [];
  backButtonURL: any = 'assets/images/arrow-dropdown.svg';

  constructor(private psService: PenaltyShootoutService, private changeDetectorRef: ChangeDetectorRef, private renderer: Renderer2, public configService: ConfigService, private _location: Location,
    public sanitizer: DomSanitizer, public viewportScroller: ViewportScroller, private http: HttpClient, config: NgbCarouselConfig,
    private route: ActivatedRoute, private router: Router, private penaltyShootoutComponent: PenaltyShootoutComponent,
    public socketService: SocketService, public dialog: MatDialog, private elRef: ElementRef) {
    config.showNavigationIndicators = false;

    this.messageSubscription = this.penaltyShootoutComponent.psServerResponseSubject.subscribe((message) => {
      this.serverResponse(message);
    });
    this.penaltyShootoutSubscription = this.penaltyShootoutComponent.penaltyShootoutSubject.subscribe((response: any) => {
      this.onMessage(response);
    });
    this.psService.playerDetails$.subscribe(res => {
      this.currPlayerId = res.playerId || null
      this.configService.configData.psv2_playerId = res.playerId;
      // localStorage.setItem("psv2_playerId", res.playerId);
    })
    this.isSinglePlayer = this.penaltyShootoutComponent.isSinglePlayer;
    this.currencyCode = this.configService.configData.currencyCode;
    this.showPlayForFreeOption = this.penaltyShootoutComponent.showPlayForFreeOption;
    this._isShabikiPlatform = this.penaltyShootoutComponent._isShabikiPlatform;
    this._isBetyetuPlatform = this.penaltyShootoutComponent._isBetyetuPlatform;
    this._isPlayabetPlatform = this.penaltyShootoutComponent._isPlayabetPlatform;
    this._isStarbetPlatform = this.penaltyShootoutComponent._isStarbetPlatform;
    this.freeSingleTrialDone = this.penaltyShootoutComponent.freeSingleTrialDone;
    this.freeTourneyTrialDone = this.penaltyShootoutComponent.freeTourneyTrialDone;

    if (this._isStarbetPlatform) {
      this.tournamentJPPrize = false;
    }

    this.enableFreePlayCondition = this.penaltyShootoutComponent.enableFreePlayCondition;

    this.psTitleImage = this.penaltyShootoutComponent.psTitleImage;

    if (this.configService.configData.operatorId == "paribet") {
      this.isTournamentAvailable = false;
    }

    // this.loadTextConfig();
    this.loadConfigData();

  }

  transform(
    code: string,
    format: 'wide' | 'narrow' = 'narrow',
    locale?: string
  ): any {
    if(this.configService.configData.currencyCode == "BRL"){
      return "BRL"
    }
    return getCurrencySymbol(code, format, locale);
  }

  @ViewChild('carousel')
  carousel!: NgbCarousel;

  get psConfig() {
    return PenaltyshootoutConfig.betValues
  }

  ngOnInit(): void {

    this.loadedIframe = document.getElementById('iframeElement');
    this.penaltyShootoutComponent.loadTextConfig();
    this.currencySymbol = this.penaltyShootoutComponent.currencySymbol;
    // this.isShabikiPlatform = this.configService.configData.platformId == 2;
    // console.warn("constants are", PenaltyshootoutConfig);
    this.url = this.configService.configData.url.canvas + "psv2/index.html";
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    this.checkRoute();
    this.isCashPlay = this.penaltyShootoutComponent.isCashPlay;
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
    this.getCurrentUser();
    this.loadPSWinInfoText();
    // this.time = setInterval(this.getLeaderBoardData.bind(this), 10000);
    this.addImagesToCarousel();
    this.multiplier = this.psConfig.potentialWinInfo;
    if (this.configService.configData.operatorId == "paribet" || this.configService.configData.operatorId == "starbet") {
      this.multiplier = this.configService.configData.potentialWinInfo[this.configService.configData.operatorId];
    }
    this.playerId = this.penaltyShootoutComponent.playerId;
    this.currencyCode = this.configService.configData.currencyCode;
    // this.potentialWin = this.isSinglePlayer ? this.stakeAmount * this.multiplier[this.penaltyBtn.toString()] : this.stakeAmount * this.multiplier.tourney;
    this.setDefaultBets(this.penaltyBtn.toString());
    this.playerAccountDetails = {
      balance: 0,
      currencyCode: '',
      playerId: '',
      playerName: ''
    }
    this.userDetails = this.penaltyShootoutComponent.userDetails;
    // if (this.penaltyShootoutComponent.userDetails.playerName) {
    //   this.emptyUsername = this.penaltyShootoutComponent.userDetails.playerName;
    // }
    this.showToolTipIcon = !this.configService.configData.psFreePlayNotAvailable;
    this.updateBalance();
    // if (localStorage.getItem('matchDetails')) {
    //   this.matchDetails = JSON.parse(localStorage.getItem('matchDetails')!);
    // }
    this.playButtonDisabled = this.penaltyShootoutComponent.playButtonDisabled;
    document.getElementById('play_section')!.style.visibility = "hidden";

    this.stakeTax = this.userDetails.stakeTax;
    this.withholdTax = this.userDetails.withholdTax;

    if (this.configService.configData.platformId == '1') {
      this.showFreePlayPrizes = false;
    }

    this.freePlayRequiredBetAmount = this.penaltyShootoutComponent.freePlayRequiredBetAmount;

    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight;
      }
      this.setParentIframeHeight(totalHeight);
    }, 500);

    this.textConfig = this.penaltyShootoutComponent.textConfig;

    this.buttonConfig = this.penaltyShootoutComponent.buttonConfig;
    this.cashTournamentPlay = this.penaltyShootoutComponent.cashTournamentPlay;
    // console.warn("standalone ", this.configService.configData.standalone);

    this.isStandAlone = this.configService.configData.standalone;

    const prizesPresent = this.configService.configData.prizes[this.configService.configData.url.userRegion + "_" + this.configService.configData.operatorId.toUpperCase()];

    this.prizes = prizesPresent || this.configService.configData.prizes["default"];
  }

  loadGameConfigData() {
    this.gameConfigData = this.configService.configData.psMatches;
    console.log("gameConfig data ", this.gameConfigData)
    // return new Promise((resolve, reject) => {
    //   this.http
    //     .get(`${this.configService.configData.url.canvas}psv2/gameassets/config/gamesList.json`)
    //     .subscribe(
    //       (data: any) => {
    //         resolve(true);
    //       },
    //       (err: any) => {
    //         console.log(err);
    //       }
    //     );
    // });
  }

  onMoreOrLessClicked() {
    console.log("onMore less clicked ")
    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight;
      }
      this.setParentIframeHeight(totalHeight);
    }, 500);
  }

  // loadTextConfig() {
  //   return new Promise((resolve, reject) => {
  //     const path = this.doesFileExist("mainConfig");

  //     this.http.get(path)
  //       .subscribe((data: any) => {
  //         console.log("data+++++ from cashplay ", data);
  //         this.buttonConfig = data["buttonConfig"];
  //         this.textConfig = data["textConfig"];
  //         console.log("data button config ", this.buttonConfig)
  //         this.cashTournamentPlay = data["cashTournamentPlay"];

  //         resolve(true);
  //       }, (err: any) => {
  //         console.log(err);
  //       });
  //   });
  // }

  ngAfterViewInit() {
    // let iframeElement = document.getElementById('iframeElement');
    (<any>$(".carousel")).carousel({
      interval: 0
    });
    setTimeout(() => {
      if (document.getElementById('play_section')) {
        document.getElementById('play_section')!.style.visibility = "visible";
      }
    }, 0)
    setTimeout(() => {
      if (document.getElementById('play-content')) {
        let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
        if (this.isStandAlone) {
          totalHeight += document.getElementById('main-header-container')!.clientHeight;
        }
        this.setParentIframeHeight(totalHeight);
        // + document.getElementById('main-header-container')!.clientHeight)
      }
    }, 2000);

    if (this.showPlayForFreeOption && this.stakeAmount == "" && document.getElementById("stake-input-field")) {
      (<HTMLInputElement>document.getElementById("stake-input-field"))!.placeholder = "Enter the amount here";
    }

    this.setPSConfig();
  }

  ngOnDestroy() {
    if (this.time)
      clearInterval(this.time);

    this.messageSubscription.unsubscribe();
    this.penaltyShootoutSubscription.unsubscribe();
  }

  setPSConfig() {
    for (let p in this.configService.configData.psConfig) {
      let operator = this.configService.configData.psConfig[p];
      if (p == this.configService.configData.operatorId) {
        if (operator.icons.psHeaderBackground) {
          setTimeout(()=>{
            $(".logo_container").css("background-image","url(assets/images/"+operator.icons.psHeaderBackground + ")");
          },100);
        }
        if (operator.css) {
          this.tournamentPlayerBg = operator.css.tournamentPlayerBg;
          this.singlePlayerBg = operator.css.singlePlayerBg;
          this.singlePlayerActiveBg = operator.css.singlePlayerActiveBg;
          this.tournamentPlayerActiveBg = operator.css.tournamentPlayerActiveBg;
          this.activeColor = operator.css.activeColor;
          // $("#single_player_tab").css("color",operator.css.activeColor);
          if (operator.css.infoTabBg) {
            $("#info_tab").css("background",operator.css.infoTabBg);
          }
        }
        if (operator.fonts) {
          $("*").css("fontFamily", operator.fonts.main);
        }
        if (operator.text) {
          setTimeout(() => {
            for (let p in operator.text) {
              this.buttonConfig[p] = operator.text[p];
              this.textConfig[p] = operator.text[p];
            }
          },100)
        }
        if (operator.payoutData) {
          this.payoutData = operator.payoutData;
        }
        if (operator.appLeaderboardData) {
          this.appLeaderboardData = operator.appLeaderboardData;
        }

        if(operator.coins) {
          this.totalCoins = operator.coins;
        }
        // operator specific
      }
    }
    if (!this.configService.configData.psConfig[this.configService.configData.operatorId]) {
      $("*").css("fontFamily", "Lalezar-Regular");
    }
    if(this.configService.configData.operatorId == "playbigapp") {
      this.isTournamentAvailable = false;
      setTimeout(()=> {
        $(".how_many").css("font-size","1rem");
        $(".current_leaderboard p").css("color","#ffffff");
        $(".choose_teams p").css("color","#ffffff");
        $(".play_btn button").css("font-size","1.8rem");
        $(".potential_win").css("display","none");
        $(".penalties_buttons p").css("color","#ffffff");
        $(".stake p").css("color","#ffffff");
        $(".play_btn button").css("background","#F6BA23");
        $(".confirm_btn").css("background","#F6BA23");
        $("button.btn.back_btn").css("background","#27448E");
        $("#popup_modal .modal-body").css("background","#27448E");
        $("#popup_modal_game_exit .modal-body").css("background","#27448E");
        $("#generic-modal .modal-body").css("background","#27448E");
        $("#generic-modal .modal-footer").css("justify-content","space-between");
        $("#popup_modal .modal-footer").css("justify-content","space-between");
        $("#popup_modal_game_exit .modal-footer").css("justify-content","space-between");
        $("#popup_modal .modal-footer button").css("width","48%");
        $("#popup_modal_game_exit .modal-footer button").css("width","48%");
        $(".back-button img").css("height","30px");
        $(".back-button img").css("width","30px");
        $(".back-button img").css("transform","none");
        $(".back-button").css("border","none");
        this.showCurrencyCode = false;
        this.showAppElements = false;
        this.stakeAmount = 20000;
        this.backButtonURL = "assets/images/playbigapp/penaltyShootout/backBtn.png";

        let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
        if (this.isStandAlone) {
          totalHeight += document.getElementById('main-header-container')!.clientHeight + 50;
        }
        this.setParentIframeHeight(totalHeight);
      },0)
    }
  }

  doesFileExist(configType: string): string {
    let language = this.configService.configData.languageCode || 'en';
    const jurisdiction = this.configService.configData.url.userRegion.toLowerCase();

    let path;
    let defaultPath;
    if (configType === "rules") {

      defaultPath = "assets/config/in/en/psrules.json";

      let jsonFileName = 'psrules';

      if (language && jurisdiction) {
        if (jurisdiction) {
          if (this.configService.configData.operatorId == "shabiki") {
            jsonFileName = 'psrulesShabiki';
          } else if (this.configService.configData.operatorId == "playabet") {
            jsonFileName = 'psrulesPlayabet';
          } else if (this.configService.configData.operatorId == "betyetu") {
            jsonFileName = 'psrulesBetyetu';
          } else if (this.configService.configData.operatorId == "playbigapp") {
            jsonFileName = 'psrulesPlaybigapp';
          }
        } else
          return defaultPath;
      }

      if (!language)
        language = 'en';


      path = `./assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/${jsonFileName}.json`;
      // let path = `../../../assets/config/tz/sh/psrules.json`;


    } else {
      defaultPath = "assets/config/ke/en/betYetuPSConfig.json"

      if (!language || !jurisdiction) return defaultPath;
      path = `assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/betYetuPSConfig.json`;
      // let path = `../../../assets/config/drc/${language.toLowerCase()}/psrules.json`;
    }

    let xhr = new XMLHttpRequest();
    xhr.open("HEAD", path, false);
    xhr.send();

    if (xhr.status === 404) {
      // console.log("test-doesFileExist", "fail");
      return defaultPath;
    } else {
      // console.log("test-doesFileExist", "success");
      return path;
    }
  }

  loadPSWinInfoText() {
    return new Promise((resolve, reject) => {
      const path = this.doesFileExist("rules");

      this.http.get(path)
        .subscribe((data: any) => {
          this.potentialWinInfoText = data["potentialWinInfoText"];
          this.tournamentPTInfo = data["tournamentPayTable"];
          resolve(true);
        }, (err: any) => {
          console.log(err);
        });
    });
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this.onBackButtonClicked();
    document.getElementById("penalty-shootout-menu")!.style.display = "block";
  }

  // @HostListener('window:message', ['$event'])

  onMessage(res: any) {
    // this.loadedIframe = document.getElementById('iframeElement');
    if (res.data) {
      let messageData = res.data;
      switch (messageData.messageName) {
        // case 'init':
        //   this.initializeGame(messageData);
        //   break;
        case 'PS_Ball_Kick':
          this.ballKicked(messageData);
          break;
        case 'PSPlayGameRequest':
          this.sendSocketBetRequest(messageData.data);
          break;
        case 'PSTourneyPlayGameRequest':
          this.callTourneySchedule();
          break;
        case 'PSTournamentStart':
          this.psTournamentStart();
          break;
        case 'PS_GAME_EXIT':
          this.gameExitRequest(messageData);
          // this.confirmgGameExit(messageData);
          break;
        case "TourneySchedule":
          this.tourneyJoined(messageData);
          break;
        case "TourneyListScreen":
          this.tourneyListScreen(messageData);
          break;
        case 'appHeight':
          this.setIframeHeight(messageData);
          break;
        case "CanvasLoadingComplete":
          this.setCanvasLoadingComplete();
          break;
      }
    }

    if (res.data && res.messageName == "MatchScore") {
      // this.scoreBoardService.setScoreBoard(res.data);
      // this.scoreBoardService.emitChange(res.data);
    }

  }

  ballKicked(data: any) {
    // console.log('ball kicked', data);

    if (data.isTourney) {
      let tourneyData = {
        betId: data.betId,
        playerId: this.userDetails.customerId,
        gameName: this.isCashPlay ? "tournamentPenaltyShootout" : "freeTournamentPenaltyShootout",
        goalIndex: data.goalIndex,
        selectedTeam: data.selectedTeam,
        opponentTeam: data.opponentTeam,
        matchId: data.matchId,
        accessToken: this.userDetails.accessToken,
        token: this.userDetails.accessToken,
        jurisdiction: this.userDetails.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": "penaltyShootout",
          "gameType": this.isCashPlay ? "tournamentPenaltyShootout" : "freeTournamentPenaltyShootout",
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      this.socketService.sendTourneyBallKickRequest(tourneyData);
    } else {
      data.playerId = this.userDetails.customerId;
      data.accessToken = this.userDetails.accessToken;
      data.token = this.userDetails.accessToken;
      data.jurisdiction = this.userDetails.userRegion;
      data.matchId = this.getMatchId(this.selectedTeam);
      data.extraData = {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": "penaltyShootout",
        "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
        "playerName": this.configService.configData.playerName,
        "isFreeGame": this.configService.configData.isFreeGame
      }
      this.socketService.sendBallKickRequest(data, data.isCash);
    }
    // this.checkForNetworkError();
  }

  checkForNetworkError() {
    setTimeout(() => {
      // this.genericErrorMessage = "Server error. PLease check network connection";
      this.penaltyShootoutComponent.showModal('Server error');
    }, 1500)
  }

  gameExitRequest(data: any) {
    console.warn('game exit request', data);
    this.exitGameData = data;
    !data.showWarning ? this.exitGame() : this.confirmgGameExit(data);
  }

  confirmgGameExit(data: any) {

    console.warn("confirming exit in game ", data);
    this.updateBalance();
    this.gameExitData = data;
    // $("#iframeElement").removeClass('activate-game');
    if (document.getElementById("popup_modal_game_exit")) {
      document.getElementById("popup_modal_game_exit")!.style.display = 'flex';
    }
    this.penaltyShootoutComponent.isUnfinishedGame = false;
    this.penaltyShootoutComponent.unfinishedData = {};
  }

  exitGame() {

    console.warn("exit game")
    this.updateBalance();
    $("#iframeElement").removeClass('activate-game');
    if (document.getElementById('popup_modal_game_exit')) {
      document.getElementById('popup_modal_game_exit')!.style.display = "none";
    }
    $("#iframeElement").css('display', 'none');
    this._location.back();
    // this.router.navigate(['../playpenaltyshootout'], { relativeTo: this.route });
    // this.penaltyShootoutComponent.showMenu = true;
    if ((this.betSessionResponse || this.freePSResponse || this.tourneyJoinResponse) && this.exitGameData.showWarning) {
      if (!(this.exitGameData && this.exitGameData.potentialWin)) {
        let gameExitData = {
          betId: this.isSinglePlayer ? this.betSessionResponse.betId : this.tourneyJoinResponse.tournamentId,
          playerId: this.userDetails.customerId,
          token: this.userDetails.accessToken,
          jurisdiction: this.userDetails.userRegion,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": "penaltyShootout",
            "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
            "playerName": this.configService.configData.playerName,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        }
        this.isSinglePlayer ? this.socketService.sendGameExit(gameExitData) : this.socketService.sendTourneyGameExit(gameExitData);

      }
    } else {
      if (this.unfinishedData) {
        let gameExitData = {
          betId: this.unfinishedData.betId,
          playerId: this.userDetails.customerId,
          token: this.userDetails.accessToken,
          jurisdiction: this.userDetails.userRegion,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": "penaltyShootout",
            "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
            "playerName": this.configService.configData.playerName,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        }
        if (!(this.exitGameData && this.exitGameData.potentialWin) && this.exitGameData.showWarning) {
          this.isSinglePlayer ? this.socketService.sendGameExit(gameExitData) : this.socketService.sendTourneyGameExit(gameExitData);
          this.penaltyShootoutComponent.isUnfinishedGame = false;
          this.isUnfinishedGame = false;
          this.unfinishedData = {};
        }
      }
    }
  }

  continueGame() {
    document.getElementById("popup_modal_game_exit")!.style.display = 'none';
  }

  closeGenericModal() {
    document.getElementById("generic-modal")!.style.display = 'none';
    this.disableButtons = false;
    if (document.getElementById("popup_modal")) {
      document.getElementById("popup_modal")!.style.display = 'none';
    }
  }

  initializeGame(obj?: any) {
    if (!this.userDetails.customerId) {
      this.penaltyShootoutComponent.initializeGame();
    } else {
      this.playButtonDisabled = false;
    }
  }


  checkRoute() {
    $('#' + this.router.url.split('/')[2].split('?')[0]).addClass('active').siblings('li').removeClass('active');
  }


  addImagesToCarousel() {
    this.imagesFormatted = [];
    this.updatedClubs = [];
    this.updatedCountries = [];
    let j = -1;

    for (var i = 0; i < this.images.length; i++) {
      if (i % 4 == 0) {
        j++;
        this.imagesFormatted[j] = [];
        this.updatedClubs[j] = [];
        this.updatedCountries[j] = [];
        // debugger;
        this.imagesFormatted[j].push(this.images[i]);
        this.updatedClubs[j].push(this.clubs[i]);
        this.updatedCountries[j].push(this.countriesNew[i]);
      }
      else {
        this.imagesFormatted[j].push(this.images[i]);
        this.updatedClubs[j].push(this.clubs[i]);
        this.updatedCountries[j].push(this.countriesNew[i]);
      }
    }
    // setTimeout(()=> {
    //   document.getElementById('play_section')!.style.visibility = "visible";
    // },0)

  }

  getLeaderBoardData() {
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59";
    let leaderBoardUrl;
    if (!this.isSinglePlayer) {
      leaderBoardUrl = this.configService.configData.url.socket + "leaderboard?gameName=freeTournamentPenaltyShootout&startDate=" + startDate + "&endDate=" + endDate;
    }
    else {
      leaderBoardUrl = this.configService.configData.url.socket + "leaderboard?gameName=freePenaltyShootout&startDate=" + startDate + "&endDate=" + endDate;
    }
    this.http.get(leaderBoardUrl).subscribe((data: any) => {


      this.leaderboardData = data;
      this.leaderboardDataRes = [];
      if (this.leaderboardData && this.leaderboardData.length) {
        for (let p = 0; p < 3; p++) {
          if (this.leaderboardData[p]) {
            let icon = this.getSelectedTeamLogo(this.leaderboardData[p].matchId, !this.isSinglePlayer);
            this.leaderboardData[p].img = "assets/images/countryFlags/" + icon + ".png";
            if (!this.isSinglePlayer)
              this.leaderboardData[p].img = "assets/images/countryFlags/" + icon + ".png";
            this.leaderboardDataRes.push(this.leaderboardData[p]);
          }
        }
      }
      setTimeout(() => {
        let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
        if (this.isStandAlone) {
          totalHeight += document.getElementById('main-header-container')!.clientHeight + 50;
        }
        this.setParentIframeHeight(totalHeight);
      }, 500);
      this.sendLeaderBoardData();
    })
  }

  loadConfigData() {

    this.loadTournyGameConfigData();

    this.loadplayerTeamList().then((res) => this.getLeaderBoardData());
    this.loadGameConfigData();
    // this.loadGameConfigData().then((res) => this.loadTournyGameConfigData().then((res) => this.loadplayerTeamList().then((res) => this.getLeaderBoardData())))
  }

  loadTournyGameConfigData() {

    this.tournyGameConfigData = this.configService.configData.psTourneyMatches.tourneyMatches;
  }

  loadplayerTeamList(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get("assets/config/psSinglePlayerTeamList.json")
        .subscribe(
          (data: any) => {
            this.tourneyPlayerLogo = data["TourneyTeamsList"];
            this.singlePlayerLogo = data["SinglePlayerTeamsList"];

            resolve(true);
          },
          (err: any) => {
            console.log(err);
          }
        );
    });
  }

  getSelectedTeamLogo(matchId: number, isTourny: boolean) {

    let arr = isTourny ? this.tournyGameConfigData : this.gameConfigData;

    let len = arr.length;
    let team = "";
    let flag = "argentinaIcon";
    for (let k = 0; k < len; k++) {
      if (arr[k].matchId == matchId) {
        team = arr[k].selectedTeam.toLowerCase();
      }
    }

    let logoArr = isTourny ? this.tourneyPlayerLogo : this.singlePlayerLogo;
    len = logoArr.length;
    for (let k = 0; k < len; k++) {
      if (team == logoArr[k].name.toLowerCase()) {

        flag = logoArr[k].icon;
      }
    }

    return flag;
  }

  sendLeaderBoardData() {
    let leaderboardData: any = {};
    leaderboardData.message = "LEADERBOARD_DATA";
    leaderboardData.data = { isCashGame: this.isCashPlay, isTourney: !this.isSinglePlayer, data: this.leaderboardData };

    if (this.loadedIframe && this.loadedIframe.contentWindow && typeof this.loadedIframe.contentWindow.gameExtension == "function") {
      this.loadedIframe.contentWindow.gameExtension(leaderboardData, "*");
    }
  }

  getLeaderBoardPenaltiesScoreForCurrPlayer(): number {
    let data: any = [];
    this.leaderboardData?.filter((item: any) => {
      if (item.playerId == this.userDetails.customerId) {
        data.push(item);
      }
    })

    if (data[0] && data[0].points > 0) {
      return data[0].points;
    }
    return 0;
  }

  getLeaderBoardPosForCurrPlayer(): boolean {
    let rank;
    rank = this.leaderboardData?.findIndex((item: any) => item.playerId === this.userDetails.customerId);
    return rank < 0 ? this.leaderboardData.length + 1 : rank + 1;
  }

  getRankIcon(index: number) {
    return this.isSinglePlayer ? `assets/icons/single-player/rank_${index + 1}.png` : `assets/icons/tournament/rank_${index + 1}.png`
  }

  penaltyButton(value: any) {
    this.penaltyBtn = value;
    this.penaltyType = value;
    // this.potentialWin = this.stakeAmount * this.multiplier[value.toString()];
    this.setPotentialWin();
    this.setDefaultBets(this.penaltyBtn.toString())
  }

  onSinglePlayerClicked() {
    this.isSinglePlayer = true;
    this.images = ["argentinaIcon", "australiaIcon", "belgiumIcon", "brazilIcon", "cameroonIcon", "canadaIcon", "costaricaIcon", "croatiaIcon", "denmarkIcon", "ecuadorIcon", "englandIcon", "franceIcon", "germanyIcon", "ghanaIcon", "iranIcon", "japanIcon", "southkoreaIcon", "mexicoIcon", "moroccoIcon", "netherlandsIcon", "polandIcon", "portugalIcon", "qatarIcon", "saudiarabiaIcon", "senegalIcon", "serbiaIcon", "spainIcon", "switzerlandIcon", "tunisiaIcon", "uruguayIcon", "usaIcon", "walesIcon"].map((n) => `sprite-${n}`);
    this.addImagesToCarousel();
    this.currentUserData.teamImg = 'assets/images/countryFlags/argentinaIcon.png'
    this.setDefaultBets(this.penaltyBtn.toString())
    // this.potentialWin = this.stakeAmount * this.multiplier[this.penaltyBtn.toString()];
    this.setPotentialWin();
    this.getLeaderBoardData();
    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight + 50;
      }
      this.setParentIframeHeight(totalHeight);
      // + document.getElementById('main-header-container')!.clientHeight
    }, 500);
    this.getParentGameInit();
  }

  onTournamentClicked() {
    this.isSinglePlayer = false;
    this.images = ["argentinaIcon", "australiaIcon", "belgiumIcon", "brazilIcon", "cameroonIcon", "canadaIcon", "costaricaIcon", "croatiaIcon", "denmarkIcon", "ecuadorIcon", "englandIcon", "franceIcon", "germanyIcon", "ghanaIcon", "iranIcon", "japanIcon", "southkoreaIcon", "mexicoIcon", "moroccoIcon", "netherlandsIcon", "polandIcon", "portugalIcon", "qatarIcon", "saudiarabiaIcon", "senegalIcon", "serbiaIcon", "spainIcon", "switzerlandIcon", "tunisiaIcon", "uruguayIcon", "usaIcon", "walesIcon"].map((n) => `sprite-${n}`);
    this.addImagesToCarousel();
    this.logoOrIconSelected = "argentinaIcon";
    this.currentUserData.teamImg = 'assets/images/countryFlags/argentinaIcon.png';

    this.defaultBets = this.configService.configData.bets.betsArray;
    this.setDefaultBets('tourney');
    // this.potentialWin = this.stakeAmount * this.multiplier.tourney;
    this.setPotentialWin();
    this.getLeaderBoardData();
    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight;
      }
      this.setParentIframeHeight(totalHeight);
      // + document.getElementById('main-header-container')!.clientHeight
    }, 500);
    this.getParentGameInit();
    // document.getElementById('play_section')!.style.visibility = "hidden";
  }

  getParentGameInit() {
    let gameName = (this.isSinglePlayer ? "penaltyShootout" : (this.isCashPlay ? "tournamentPenaltyShootout" : "freeTournamentPenaltyShootout"));
    if (this.penaltyShootoutComponent.socketConnected && this.penaltyShootoutComponent.gameInitSent) {
      this.playButtonDisabled = true;
      this.penaltyShootoutComponent.getGameInit({ "gameName": gameName });
    } else {
      setTimeout(() => {
        this.getParentGameInit();
      }, 1000)
    }

  }

  setDefaultBets(value: string) {

    if (this.configService.configData.currencyCode == "undefined") {
      setTimeout(() => this.setDefaultBets(value), 1000);
      return;
    }
    this.currencyCode = this.configService.configData.currencyCode;
    this.currencySymbol = this.penaltyShootoutComponent.getCurrencySymbol();
    if (this.configService.configData.bets) {
      this.defaultBets = this.configService.configData.bets.betsArray;
    } else {
      this.configService.configData.bets = this.configService.configData.betsData[this.configService.configData.url.userRegion + "_" + this.configService.configData.operatorId.toUpperCase() + "_" + this.configService.configData.currencyCode] || this.configService.configData.bets[this.configService.configData.currencyCode] || this.configService.configData.bets["default"];
      this.defaultBets = this.configService.configData.bets.betsArray;
    }
    console.log("default bets ", this.defaultBets);

    if(!this.configService.configData.bets["desertMinBetInBetsArray"]){
      if (value == "tourney") {
        this.defaultBets[0] = this.configService.configData.bets.tourneyMinBet;
      } else {
        this.defaultBets[0] = this.configService.configData.bets.minBet;
      }
    }
    this.stakeAmount = this.defaultBets[0];

    if (this.showPlayForFreeOption && this.configService.configData.platformId == "0") {
      this.stakeAmount = "";
    }
    this.setPotentialWin();
    // this.defaultBets = this.defaultBets[value].defaultBets
  }

  onInfoIconClicked(data: boolean) {
    this.penaltyShootoutComponent.isSinglePlayer = this.isSinglePlayer;
    if (data) {
      this.router.navigate(["../howtoplaypenaltyshootout"], { relativeTo: this.route });
    }
    else {
      this.router.navigate(["./leaderboardpenaltyshootout"], { relativeTo: this.route })
    }
  }

  chooseUserName() {
    this.penaltyShootoutComponent.showPopup = true
  }

  onFlagClicked(path: any, screenIndex: any, index: any) {

    // let logoSelected = path.split("/")[3].split('.')[0];
    let logoSelected = path.split("-")[1];
    this.logoOrIconSelected = logoSelected;
    this.setCurrentUserDetails();
    if (this.isSinglePlayer) {
      $(".flagItems").removeClass("flag-border-active");
      $("#" + screenIndex + "-" + index + "-flagItems").addClass("flag-border-active");
    } else {
      $(".flagItems").removeClass("country-flag-border-active");
      $("#" + screenIndex + "-" + index + "-flagItems").addClass("country-flag-border-active");
    }

  }

  onToolTipClick() {
    this.showToolTip = true;
    setTimeout(() => {
      this.showToolTip = false
    }, 8000);
  }

  setCurrentUserDetails() {
    this.currentUserData.teamImg = `assets/images/${this.isSinglePlayer ? 'countryFlags' : 'countryFlags'}/${this.logoOrIconSelected}.png`;
    // console.log(this.currentUserData)
  }

  onStakeBtnClicked(value: any) {
    this.stakeAmount = value;
    $(".stake_btn").removeClass("active");
    $("#" + "stake_btn_" + value).addClass("active");

    // this.potentialWin = this.isSinglePlayer ? value * this.multiplier[this.penaltyBtn.toString()] : value * this.multiplier.tourney;
    this.setPotentialWin();
  }

  getCurrentUser() {
    const playerId = this.getUUid();
    // localStorage.setItem("psv2_playerId", playerId);
    this.currentUserData.playerId = playerId;
    this.currentUserData.username = this.userName || this.emptyUsername;
  }

  get userName() {
    return (this.configService.configData.currUsername != this.emptyUsername && this.configService.configData.currUsername) ? this.configService.configData.currUsername : this.emptyUsername;
  }

  getUUid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  setIframeHeight(obj: any) {
    console.log("test", obj)
    if (obj.appHeight) {
      document.getElementById('iframeElement')!.style.height = obj.appHeight + "px";
      // document.getElementById('iframeElement')?.style.maxHeight = "max-content";
      if (document.getElementById('play_section')!.style.display == "none") {
        this.setParentIframeHeight(obj.appHeight);
      }
    }
  }

  setCanvasLoadingComplete() {
    if (this.loadedIframe) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }

  setParentIframeHeight(height: any) {
    // let appHeight = Math.max(height,window.outerHeight);
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      if ((window as any).ReactNativeWebView) {
        (window as any).ReactNativeWebView.postMessage(JSON.stringify(
          {
            msg: "b2bresize",
            data: height + 170,
          }
        ));
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: height + 50
        }, (document.referrer || (window.parent && window.parent.location.href)));
      } else {
        window.parent.postMessage({
        action: "b2bresize",
        scrollHeight: height + 50
      }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }
    console.warn("setIframe height in cashplay screen ", height);
  }

  redirectToLogin() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({
        action: "login"
      }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }

  checkNumber(event: any) {
    // console.log("test", event)
    return !(event.keyCode === 103 || event.keyCode === 187 || event.keyCode === 189 || event.keyCode === 69)
  }

  onStakeInputChanged(value: any) {
    let config: any = this.configService.configData.bets;
    debugger;
    let maxBet = this.isSinglePlayer ? config.maxBet : config.tourneyMaxBet;

    if (value > maxBet) {
      let maxPossibleStakeString = this.textConfig.maxPossibleStake || "Max Possible Stake for this selection is ";
      this.openWarningDialog(maxPossibleStakeString + (this.showCurrencyCode ? this.currencyCode: "") + ' ' + maxBet);
      setTimeout(() => {
        this.stakeAmount = maxBet;
        // this.potentialWin = this.isSinglePlayer ? this.stakeAmount * this.multiplier[this.penaltyBtn.toString()] : this.stakeAmount * this.multiplier.tourney;
        this.setPotentialWin();
        this.changeDetectorRef.detectChanges();
      }, 0)

      return;
    }

    this.stakeAmount = value;
    // this.potentialWin = this.isSinglePlayer ? this.stakeAmount * this.multiplier[this.penaltyBtn.toString()] : this.stakeAmount * this.multiplier.tourney;
    this.setPotentialWin();
  }

  openWarningDialog(value: any) {
    this.dialog.open(ModalpopupComponent, {
      data: value,
      height: 'max-content',
      width: '80%'
    });
  }

  verifyStakeInput() {
    let config: any = this.configService.configData.bets;
    let maxBet = this.isSinglePlayer ? config.maxBet : config.tourneyMaxBet;
    let minBet = this.isSinglePlayer ? config.minBet : config.tourneyMinBet;
    if (this.stakeAmount > maxBet) {
      this.openWarningDialog("Max Possible Stake for this selection is " + (this.showCurrencyCode ? this.currencyCode: "") + ' ' + maxBet);
      setTimeout(() => {
        this.stakeAmount = maxBet;
        // this.potentialWin = this.isSinglePlayer ? this.stakeAmount * this.multiplier[this.penaltyBtn.toString()] : this.stakeAmount * this.multiplier.tourney;
        this.setPotentialWin();
        this.changeDetectorRef.detectChanges();
      }, 0)

      return false;
    } else if (this.stakeAmount < minBet) {
      // console.log("test", this.stakeAmount, minBet)
      let minPossibleStakeString = this.textConfig.minPossibleStake || "Min Possible Stake for this selection is ";
      this.openWarningDialog(minPossibleStakeString + (this.showCurrencyCode ? this.currencyCode: "") + ' ' + minBet);
      setTimeout(() => {
        // this.stakeAmount = minBet;
        // this.potentialWin = this.isSinglePlayer ? this.stakeAmount * this.multiplier[this.penaltyBtn.toString()] : this.stakeAmount * this.multiplier.tourney;
        this.setPotentialWin();
        this.changeDetectorRef.detectChanges();
      }, 0)
      return false;
    } else {
      return true
    }
  }

  checkForUserCredentials() {
    let params: any = document.cookie;
    if (params) {
      params = params.split("; ");
      for (let i = 0; i < params.length; i++) {
        let arr = params[i].split("=");
        if (arr[0] == "AppSession") {
          this.userDetails.accessToken = this.configService.configData.token || arr[1];
        } else if (arr[0] == "userId") {
          this.userDetails.customerId = this.configService.configData.playerId || arr[1];
        } else if (arr[0] == "MobileNumber") {
          this.userDetails.playerName = this.configService.configData.playerName || arr[1];
        } else if (arr[0] == "userNumber" && this.configService.configData.platformId == "2") {
          this.userDetails.playerName = this.configService.configData.playerName || arr[1];
        }
      }
    }
    if (this.configService.configData.platformId == "0" && window.location.href.indexOf("10bet") < 0 && (this.userDetails.accessToken == "" || this.userDetails.accessToken == "sga342324")) {
      this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
      this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
      this.userDetails.playerName = this.userDetails.customerId;

    }

    if (!(this.userDetails.accessToken && this.userDetails.customerId) && (window.location.href.indexOf("10bet") >= 0 || this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1")) {
      if (this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1") {
        this.redirectToLogin();
      } else {
        window.parent.location.href = this.configService.configData.url.parentRegion + "/Login";
      }
      return;
    }
    if (this.penaltyShootoutComponent.playerAccountDetails && (this.penaltyShootoutComponent.playerAccountDetails.playerName == this.userDetails.customerId) && (this.penaltyShootoutComponent.playerAccountDetails.balance < this.stakeAmount)) {
      this.showInsufficientFunds = true;
    } else {
      this.showInsufficientFunds = false;
    }
    if (this.verifyStakeInput()) {
      this.isUnfinishedGame = this.penaltyShootoutComponent.isUnfinishedGame;
      if (this.isSinglePlayer) {
        this.unfinishedData = this.penaltyShootoutComponent.unfinishedData.singlePlayer;
      } else {
        this.unfinishedData = this.penaltyShootoutComponent.unfinishedData.tournament;
      }
      if (this.isCashPlay) {
        document.getElementById("popup_modal")!.style.display = 'flex';
      }
      else if (this.userName === this.emptyUsername) {
        this.chooseUserName();
      }
      else {
        if (this.isUnfinishedGame) {
          document.getElementById("popup_modal")!.style.display = 'flex';
        } else {
          // console.log("test", this.isSinglePlayer)
          !this.isSinglePlayer ? this.callTourneySchedule() : this.sendSocketBetRequest("");
          this.disableButtons = true;
          // document.getElementById('play_section')!.style.display = "none";
        }
      }
    }
  }


  onPlayButtonClicked() {

    if (this.enableFreePlayCondition && !this.isCashPlay) {
      if (this.penaltyShootoutComponent.minStakeNotAchieved) {
        this.showPopup = true;
        this.showShabikiTrialAlert = true;
        return;
      }

      this.freeSingleTrialDone = this.penaltyShootoutComponent.freeSingleTrialDone;
      this.freeTourneyTrialDone = this.penaltyShootoutComponent.freeTourneyTrialDone;

      if (this.freeSingleTrialDone && this.isSinglePlayer) {
        this.showShabikiTrialAlert = true;
        document.getElementById("popup_modal")!.style.display = 'flex';
        return;
      }

      if (this.freeTourneyTrialDone && !this.isSinglePlayer) {
        this.showShabikiTrialAlert = true;
        document.getElementById("popup_modal")!.style.display = 'flex';
        return;
      }
    }

    // console.log("test", this.showShabikiTrialAlert, this.showInsufficientFunds)
    if (this.showTaxData) {
      this.displayTaxData();
    }

    this.setCanvasSize();

    this.checkForUserCredentials();
    if (this.configService.configData.operatorId == "playbigapp") {
      if (this.isUnfinishedGame) {
        $("#popup_modal .modal-footer").css("justify-content","center");
      } else {
        $("#popup_modal .modal-footer").css("justify-content","space-between");
      }
    }
  }

  hideFreePlayConditionClick() {
    this.showPopup = false;
  }

  initializeAndSetGame() {
    document.getElementById('play_section')!.style.display = "none";
    this.scrollTop();
    let gameDetailsData: any = {};
    gameDetailsData.message = "PS_PLAY_REQUEST";
    gameDetailsData.data = { ...this.betSessionResponse, isCashGame: this.isCashPlay, isTourney: !this.isSinglePlayer, maxGoals: this.leaderboardDataRes[0] ? this.leaderboardDataRes[0].points : 0, selectedTeam: this.selectedTeam };
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(gameDetailsData, "*");
    // if (this.configService.configData.isMobile && (screen.height) < 850) {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight * .8) + "px";
    // } else {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight) + "px";
    // }
    console.warn("initialize and set game ")
    $("#iframeElement").addClass('activate-game');
    this.setCanvasIframeHeightForMobile();
    if (this.loadedIframe && this.loadedIframe.contentDocument) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }

  get selectedTeam() {
    let clubName = this.logoOrIconSelected.split('I')[0];
    console.log("clubName ", clubName)
    let selectedLogo = this.configService.configData.iconName[clubName];
    return selectedLogo;
  }

  sendSocketBetRequest(data: any) {
    this.userDetails = this.penaltyShootoutComponent.userDetails;
    let request: any = {}
    if (data && data.requestObj) {
      request = data.requestObj;
    } else {
      request = {
        selectedTeam: this.selectedTeam,
        isFreeGame: !this.isCashPlay,
        betAmount: this.stakeAmount,
        token: this.userDetails.accessToken,
        matchId: this.getMatchId(this.selectedTeam),
        playerId: this.userDetails.customerId,
        gameName: !this.isCashPlay ? "freePenaltyShootout" : "penaltyShootout",
        inningNumber: this.penaltyBtn,
        accessToken: this.userDetails.accessToken,
        jurisdiction: this.userDetails.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": "penaltyShootout",
          "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      };
    }
    this.isCashPlay && (request = { ...request, noOfPenalties: 5 })
    if (!this.isCashPlay) {
      request.betAmount = 0;
    }
    // request.jurisdiction = this.userDetails.userRegion;
    request.playerName = this.userDetails.playerName;
    // if (this.isCashPlay) {
    //   request.playerName = this.userDetails.playerName;
    // } else {
    //   request.playerName = this.userDetails.customerId;
    // }
    this.socketService.sendPsoFreeBetRequest(request, this.userDetails.customerId);
  }

  setUsername(data: any) {
    this.playerAccountDetails = { ...data };
    // this.playerAccountDetails.playerName = this.userDetails.customerId;
    // console.log('set username', this.playerAccountDetails);
    if (this.playerAccountDetails.playerName == "null" || this.playerAccountDetails.playerName == null) {
      this.playerAccountDetails.playerName = this.playerAccountDetails.playerId;
    }
    this.configService.configData.currUsername = (this.playerAccountDetails.playerId === this.playerAccountDetails.playerName ? this.emptyUsername : this.playerAccountDetails.playerName);
    // localStorage.setItem('currUsername', this.playerAccountDetails.playerId === this.playerAccountDetails.playerName ? this.emptyUsername : this.playerAccountDetails.playerName)
    // if (this.configService.configData.platformId != 0) {
    //   localStorage.setItem('currUsername', this.userDetails.playerName)
    // } else {
    //   localStorage.setItem('currUsername', this.playerAccountDetails.playerId === this.playerAccountDetails.playerName ? this.emptyUsername : this.playerAccountDetails.playerName)
    // }
  }

  getMatchId(team1: any) {
    console.log("teams ", team1)
    let matches = this.configService.configData.psMatches;
    if (!this.isSinglePlayer) {
      matches = this.configService.configData.psTourneyMatches.tourneyMatches;
    }
    let len = matches.length;
    let matchid;
    for (let k = 0; k < len; k++) {
      if (matches[k].selectedTeam == team1) {
        matchid = matches[k].matchId;
        break;
      }
    }
    return matchid;
  }

  onBackButtonClicked() {
    this.penaltyShootoutComponent.setPSConfig();
    this.router.navigate(["../playpenaltyshootout"], { relativeTo: this.route })
  }

  onCashplayPopupBackClicked() {
    document.getElementById("popup_modal")!.style.display = 'none';
    this.showShabikiTrialAlert = false
  }

  onCashplayConfirmClicked() {
    if (!navigator.onLine) {
      this.genericErrorMessage = this.configService.configData.networkErrorMessage;
      document.getElementById("generic-modal")!.style.display = 'flex';
      return;
    }
    if (this.showShabikiTrialAlert) {
      this.showShabikiTrialAlert = false;
      document.getElementById("popup_modal")!.style.display = "none";
      return
    }
    if (this.configService.configData.platformId == "4") {
      if (!this.socketService.playerBetSubscribed) {
        setTimeout(() => {
          this.onCashplayConfirmClicked();
        }, 1000)
        return;
      }
    }
    if (this.isUnfinishedGame) {
      let unfinishedGameData: any = {};
      unfinishedGameData.message = "PS_UNFINISHED_GAME";
      unfinishedGameData.data = { ...this.unfinishedData, isCashGame: this.isCashPlay, isTourney: !this.isSinglePlayer, maxGoals: this.leaderboardDataRes[0] ? this.leaderboardDataRes[0].points : 0 };
      if (!unfinishedGameData.data.selectedTeam) {
        unfinishedGameData.data.selectedTeam = this.selectedTeam;
      }
      // console.log('send unfinished data to game', unfinishedGameData);
      if (this.loadedIframe && this.loadedIframe.contentWindow)
        this.loadedIframe.contentWindow.gameExtension(unfinishedGameData, "*");
      this.scrollTop();
      $("#iframeElement").addClass('activate-game');
      // document.getElementById('iframeElement')!.style.height = window.innerHeight + "px";
      this.setCanvasIframeHeightForMobile();
      setTimeout(()=> {
        if (this.loadedIframe && this.loadedIframe.contentDocument) {
          this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
        }
      },100);
    } else {
      !this.isSinglePlayer ? this.callTourneySchedule() : this.sendSocketBetRequest("");
      this.disableButtons = true;
      // document.getElementById("popup_modal")!.style.display = 'none';
    }

    // let gameDetailsData: any = {};
    // gameDetailsData.message = "PS_PLAY_REQUEST";
    // gameDetailsData.data = {
    //   "isCashPlay": this.isCashPlay,
    //   "isSinglePlayer": this.isSinglePlayer,
    //   "penaltyType": this.penaltyType,
    //   "stakeAmount": this.stakeAmount,
    //   "flagSelected": this.logoOrIconSelected
    // }
    // gameDetailsData.data = this.betSessionResponse
    // console.warn(this.betSessionResponse);

    //  this.loadedIframe.contentWindow.gameExtension(gameDetailsData, "*");
  }

  callTourneySchedule() {
    let gameDetailsData: any = {};
    gameDetailsData.message = "GET_TOURNEY_SCHEDULE";
    gameDetailsData.data = { team: this.selectedTeam };
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(gameDetailsData, "*");
  }

  tourneyJoined(data: any) {
    // console.log(data);
    let obj: any = {
      token: this.userDetails.accessToken,
      accessToken: this.userDetails.accessToken,
      playerId: this.userDetails.customerId,
      jurisdiction: this.userDetails.userRegion,
      schedule: JSON.stringify(data.tourneySchedule),
      betAmount: this.stakeAmount,
      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": "penaltyShootout",
        "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
        "playerName": this.configService.configData.playerName,
        "isFreeGame": this.configService.configData.isFreeGame
      }
    }
    if (!this.isCashPlay) {
      obj.betAmount = 0;
    }
    obj.playerName = this.userDetails.playerName;
    if (this.isCashPlay) {
      obj.gameName = "tournamentPenaltyShootout";
    } else {
      obj.gameName = "freeTournamentPenaltyShootout";
    }
    let schedule: any;
    if (obj.schedule) {
      schedule = JSON.parse(obj.schedule);
      if (schedule && schedule.finalMatch && !schedule.finalMatch.matchId) {
        schedule.finalMatch.matchId = this.getMatchId(this.selectedTeam);
        schedule = JSON.stringify(schedule);
        obj.schedule = schedule;
      }
    }
    obj.matchId = this.getMatchId(this.selectedTeam);
    obj.selectedTeam = this.selectedTeam;
    this.socketService.sendTourneyJoinRequest(obj)
  }

  setTourneyScreen(data: any) {
    if (data.error || (data.betRequest && data.betRequest.error)) {
      if ((window.location.href.indexOf("10bet") >= 0 || this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1")) {
        if (this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1") {
          this.genericErrorMessage = data.errorMessage;
          document.getElementById("generic-modal")!.style.display = 'flex';
        } else {
          window.parent.location.href = this.configService.configData.url.parentRegion + "/Login";
        }
        return;
      }
    }
    this.tourneyJoinResponse = data;
    let gameDetailsData: any = {};
    gameDetailsData.message = "TOURNEY_JOIN_MSG";
    gameDetailsData.data = data;
    gameDetailsData.data.selectedTeam = this.selectedTeam;
    gameDetailsData.data.isCashPlay = this.isCashPlay;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      // console.log('send tournament data to game', gameDetailsData);
      this.loadedIframe.contentWindow.gameExtension(gameDetailsData, "*");
    }
    // this.scrollTop();
    // if (this.configService.configData.isMobile && (screen.height) < 850) {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight * .8) + "px";
    // } else {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight) + "px";
    // }
    console.warn("tourney screen ")
    $("#iframeElement").addClass('activate-game');
    this.setCanvasIframeHeightForMobile();
    if (this.loadedIframe && this.loadedIframe.contentDocument) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "auto";
    }
    this.setCanvasSize();
    // if (this.loadedIframe) {
    //   this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "auto";
    // }
  }

  setCanvasSize() {
    let gameSizeData: any = {};
    let gameStageSize = {
      height: document.getElementById('play_section')?.clientHeight,
      width: document.getElementById('gameStage')?.clientWidth
    };

    if (gameStageSize.height) {
      if (this.configService.configData.isMobile) {
        gameStageSize.height = gameStageSize.height * 0.8;
      } else {
        gameStageSize.height = gameStageSize.height - 160;
      }
    }

    // gameSizeData.message = "PS_CANVAS_SIZE";
    gameSizeData.message = "SET_SIZE";

    gameSizeData.data = gameStageSize;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(gameSizeData, "*");
    }
  }

  onSocketConnected() {
    // console.log('socket connected');
    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight;
      }
      this.setParentIframeHeight(totalHeight);
      // + document.getElementById('main-header-container')!.clientHeight
    }, 500);
    // this.psService.setPlayerDetails({ playerId: this.userDetails.customerId })
    // let connectionDetails: any = {};
    // connectionDetails.message = "SOCKET_CONNECTED";
    // connectionDetails.data = true;
    // if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //   this.loadedIframe.contentWindow.gameExtension(connectionDetails, "*");
    // }
  }

  setBalanceResponse(balanceReponse: any) {
    // console.log("balance response is", balanceReponse);
    if (this.userDetails.playerId == balanceReponse.playerAccount.playerId) {

    }
    this.playerAccountDetails = balanceReponse.playerAccount;
    // localStorage.setItem('currUsername', this.playerAccountDetails.playerId === this.playerAccountDetails.playerName ? this.emptyUsername : this.playerAccountDetails.playerName)
    this.configService.configData.currUsername =  (this.playerAccountDetails.playerId === this.playerAccountDetails.playerName ? this.emptyUsername : this.playerAccountDetails.playerName);
    if (this.isCashPlay) {
      if (this.userDetails.customerId == balanceReponse.playerAccount.playerName) {
        // this.currencyCode = balanceReponse.playerAccount.currencyCode;
        // this.configService.configData.currencyCode = balanceReponse.playerAccount.currencyCode;
        let currencyDetails: any = {};
        currencyDetails.message = "UPDATE_PLAYER_CURRENCY";
        currencyDetails.data = {
          "currencyCode": this.currencyCode
        };
        if (this.loadedIframe && this.loadedIframe.contentWindow) {
          this.loadedIframe.contentWindow.gameExtension(currencyDetails, "*");
        }
      }
    } else {
      // this.currencyCode = balanceReponse.playerAccount.currencyCode;
      // this.configService.configData.currencyCode = balanceReponse.playerAccount.currencyCode;
      let currencyDetails: any = {};
      currencyDetails.message = "UPDATE_PLAYER_CURRENCY";
      currencyDetails.data = {
        "currencyCode": this.currencyCode
      };
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(currencyDetails, "*");
      }
    }
    this.getGameInit();
  }

  getGameInit() {
    // let token = this.getUUid();
    // this.userDetails.customerId
    let gameInitobj = {
      playerId: this.userDetails.customerId,
      gameName: "freePenaltyShootout",
      token: this.userDetails.accessToken,
      accessToken: this.userDetails.accessToken
    }
    this.socketService.getPsoGameInit(gameInitobj);
    this.configService.configData.playerToken = gameInitobj.token;
    // localStorage.setItem('playerToken', JSON.stringify(gameInitobj.token));
  }

  setGameInit(gameInitRes: any) {
    this.gameInitData = gameInitRes;
    let gameInitDetails: any = {};
    gameInitDetails.message = "GAME_INIT";
    gameInitDetails.data = gameInitRes;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(gameInitDetails, "*");
    }
    this.playButtonDisabled = false;
    if (this.loadedIframe) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
    // } 
    // let apiURLDetails = {
    //   message: "SET_API_URL",
    //   data: {
    //     url: this.configService.configData.apiUrl
    //   }
    // }
    // if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //   this.loadedIframe.contentWindow.gameExtension(apiURLDetails, "*");
    // }
  }

  setBetSessionResponse(data: any) {
    this.penaltyShootoutComponent.balance = data.playerAccount.balance;
    this.betSessionResponse = data;
    if (data.error || (data.betRequest && data.betRequest.error)) {
      if ((window.location.href.indexOf("10bet") >= 0 || this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1" || this.configService.configData.platformId == "10")) {
        if (this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1" || this.configService.configData.platformId == "10") {
          this.genericErrorMessage = data.errorMessage;
          document.getElementById("generic-modal")!.style.display = 'flex';
          if (this.configService.configData.operatorId == "playbigapp") {
            $("#generic-modal .modal-footer").css("justify-content","center");
          }
        } else {
          window.parent.location.href = this.configService.configData.url.parentRegion + "/Login";
        }
        return;
      }
    } 
    // else {
    //   this.betSessionResponse.betRequest.inningNumber = this.betSessionResponse.betRequest.totalRounds;
    // }
    $("#generic-modal .modal-footer").css("justify-content","space-between");
    let betSessionResponse = {
      message: "BET_SESSION_RESPONSE",
      data: data,
    };
    this.updateBalance();
    this.initializeAndSetGame();
    // if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //   this.loadedIframe.contentWindow.gameExtension(betSessionResponse, "*");
    //   $("#iframeElement").addClass('activate-game');
    // }
  }

  setBallKickReponse(data: any) {
    this.penaltyShootoutComponent.balance = data.playerAccount.balance;
    clearTimeout(this.networkTimeout);
    this.ballKickRequestSent = false;
    this.updateBalance();
    let gameDetailsData: any = {};
    gameDetailsData.message = "PS_BALL_KICK_RESPONSE";
    gameDetailsData.data = data;
    // console.warn(gameDetailsData);
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(gameDetailsData, "*");
  }

  setTourneyPSResponse(data: any) {
    this.updateBalance();
    let tourneyResponseData: any = {};
    tourneyResponseData.message = "PS_TOURNEY_RESPONSE";
    tourneyResponseData.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(tourneyResponseData, "*");
  }

  setFreePSResponse(data: any) {
    this.updateBalance();
    let freeResponseData: any = {};
    freeResponseData.message = "PS_FREE_PLAY_RESPONSE";
    freeResponseData.data = data;
    this.freePSResponse = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(freeResponseData, "*");
  }

  psTournamentStart() {
    this.scrollTop();
    // if (this.configService.configData.isMobile && (screen.height) < 850) {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight * .8) + "px";
    // } else {
    //   document.getElementById("iframeElement")!.style.height = (window.innerHeight) + "px";
    // }  
    this.setCanvasIframeHeightForMobile();
    // document.getElementById("iframeElement")!.style.height = (window.innerHeight * .8) + "px";
    if (this.loadedIframe && this.loadedIframe.contentDocument) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }

  tourneyListScreen(data: any) {
    document.getElementById("iframeElement")!.style.height = (window.innerHeight) + "px";
    this.setCanvasIframeHeightForMobileTourney();
    if (data.isTourneyListScreen) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "auto";
    } else {
      this.scrollTop();
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }

  // setTourneyFreePSResponse(data:any) {
  //   let tourneyFreeResponseData: any = {};
  //   tourneyFreeResponseData.message = "PS_TOURNEY_FREE_RESPONSE";
  //   tourneyFreeResponseData.data = data;
  //   if (this.loadedIframe && this.loadedIframe.contentWindow)
  //     this.loadedIframe.contentWindow.gameExtension(tourneyFreeResponseData, "*");
  // }

  setBetResponse(data: any) {
    this.updateBalance();
    let betResponseData: any = {};
    betResponseData.message = "PS_PLAY_RESPONSE";
    betResponseData.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow)
      this.loadedIframe.contentWindow.gameExtension(betResponseData, "*");
  }

  setPotentialWin() {
    let multiplier = this.isSinglePlayer ? this.multiplier[this.penaltyBtn.toString()] : this.multiplier.tourney;
    const userRegion = this.configService.configData.url.userRegion;
    const userRegionWithOperatorId = userRegion + "_" + this.configService.configData.operatorId.toUpperCase();
    let taxData = this.configService.configData.taxData[userRegionWithOperatorId] ? this.configService.configData.taxData[userRegionWithOperatorId] : { 'stakeTax': 0, 'withholdTax': 0 };
    this.oddsMultiplier = multiplier;

    let betAmount = taxData.stakeTax ? this.stakeAmount / taxData.stakeTax : this.stakeAmount;  // STAKE AFTER TAX


    // let grossWinnings = this.stakeAfterTax * multiplier;
    // let stakeAfterExciseTax = this.stakeAmount - (this.stakeAmount * (taxData.stakeTax / 100));
    // this.withHoldingTax = ((stakeAfterExciseTax * this.oddsMultiplier) - this.stakeAmount) * (taxData.withholdTax / 100);
    // let grossReturns = stakeAfterExciseTax * this.oddsMultiplier;

    this.potentialWin = multiplier * betAmount;

    if (this.configService.configData.platformId == 2) {
      this.stakeAfterTax = betAmount;
      let grossWinAmount = betAmount * multiplier;
      // GROSS WIN
      let profit = grossWinAmount - this.stakeAmount;
      let withHoldingTax = profit * (taxData.withholdTax / 100);
      this.withHoldingTax = withHoldingTax;

      // let winAMountAfterWithholdTax = profit - this.withHoldingTax;
      // this.potentialWin = winAMountAfterWithholdTax + this.stakeAfterTax;
      this.potentialWin = grossWinAmount - this.withHoldingTax;
    }

    this.potentialWin = this.penaltyShootoutComponent.formatCurrency(this.potentialWin);
  }

  updateBalance() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      if (this.userDetails.customerId && this.userDetails.accessToken) {
        window.parent.postMessage({ action: "b2bupdateBalance" }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }
  }

  scrollTop() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({ action: "scrolltotop" }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }


  serverResponse(message: any) {
    if (message == "socket connected") {
      this.onSocketConnected();
    } else {
      switch (message.message) {
        case "betSessionResponse":
          this.setBetSessionResponse(message.data);
          break;
        case "ballKickReponse":
          this.setBallKickReponse(message.data);
          break;
        case "tourneyJoinResponse":
          this.setTourneyScreen(message.data);
          break;
        case "tourneyPSResponse":
          this.setTourneyPSResponse(message.data);
          break;
        case "freePSResponse":
          this.setFreePSResponse(message.data);
          break;
        case "closeGame":
          this.confirmgGameExit({});
          break;
        case "playbuttonstate":
          this.playButtonDisabled = message.data;
          break;
        case "setcurrencycode":
          this.currencyCode = this.configService.configData.currencyCode;
          break;
        // case "event":
        //   this.setGameEvent(message.data);
        //   break;
        case "betResponse":
          this.setBetResponse(message.data);
          break;
        case "setUsername":
          this.setUsername(message.data);
          break;

        // case "disconnected":
        //   this.setServerDisconnected(message.data);
        //   break;
        default:
      }
    }
  }

  displayTaxData() {
    this.showTaxData = !this.showTaxData;
    setTimeout(() => {
      let totalHeight = document.getElementById('play-content')!.clientHeight + document.getElementsByClassName('logo_container')[0]?.clientHeight + document.getElementsByClassName('navigation')[0]?.clientHeight + 100;
      if (this.isStandAlone) {
        totalHeight += document.getElementById('main-header-container')!.clientHeight;
      }
      this.setParentIframeHeight(totalHeight);
      // + document.getElementById('main-header-container')!.clientHeight
    }, 500)
  }

  setCanvasIframeHeightForMobile() {
    let height = window.innerHeight;
    if (this.configService.configData.isMobile && (screen.height) < 900) {
      height = window.innerHeight * .8;
    }

    setTimeout(() => {
      document.getElementById("iframeElement")!.style.height = (window.parent as any).initialWindowHeight + "px";
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        if ((window as any).ReactNativeWebView) {
          (window as any).ReactNativeWebView.postMessage(JSON.stringify(
            {
              msg: "b2bresize",
              data: height + 50,
            }
          ));
        }
        window.parent.postMessage({
          action: "frameresize",
          scrollHeight: height,
          type: "pscanvas"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 500);
  }

  setCanvasIframeHeightForMobileTourney() {
    let height = window.innerHeight;
    if (this.configService.configData.isMobile && (screen.height) < 900) {
      height = window.innerHeight * .8;
    }

    setTimeout(() => {
      document.getElementById("iframeElement")!.style.height = (window.parent as any).initialWindowHeight + "px";
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "frameresize",
          scrollHeight: height,
          type: "pscanvastourney"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 500);
  }


}

interface CurrentUserDetails {
  teamImg: string;
  username?: string;
  penaltyScored: string;
  position: any;
  playerId?: string;
}
interface PlayerAccDetails {
  balance: number;
  currencyCode: string;
  playerId: string;
  playerName: string;
}
