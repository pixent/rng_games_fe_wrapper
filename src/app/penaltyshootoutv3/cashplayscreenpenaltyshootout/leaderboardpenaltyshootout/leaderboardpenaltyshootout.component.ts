import { Component, OnInit, ChangeDetectorRef, Input } from "@angular/core";
import { ViewportScroller, DatePipe } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfigService } from "../../../services/config.service";
import { CashplayscreenpenaltyshootoutComponent } from "../cashplayscreenpenaltyshootout.component";
@Component({
  selector: "app-leaderboardpenaltyshootout",
  templateUrl: "./leaderboardpenaltyshootout.component.html",
  styleUrls: ["./leaderboardpenaltyshootout.component.css"],
  providers: [DatePipe],
})
export class LeaderboardpenaltyshootoutComponent implements OnInit {
  leaderBoardData: any = [];
  clubs: any = [];
  countries: any = [];
  gameMode: boolean = true;
  currentPlayer: any = {};
  currencyCode: any = "INR";
  tournamentPrizes: any = [];
  singleGamePrizes: any = [];
  currentUserName: any = "";
  clubImage: any = "assets/images/countryFlags/argentinaIcon.png";
  countryImage: any = "assets/images/countryFlags/brazilIcon.png";
  imagesArray: any = [];
  countryImagesArray: any = [];
  iconName: any;
  currentSunday: any = 0;
  resultLength: Number = 0;
  currentMonth: any = "";
  gameConfigData: any = [];
  tournyGameConfigData: any = [];
  singlePlayerLogo: any = [];
  tourneyPlayerLogo: any = [];
  leaderBoardDataRes: any;
  textConfig: any;
  buttonConfig: any;
  isStandAlone: boolean = false;

  constructor(
    public configService: ConfigService,
    private changeDetectorRef: ChangeDetectorRef,
    private http: HttpClient,
    private viewportScroller: ViewportScroller,
    private datePipe: DatePipe,
    public cashplayscreenpenaltyshootoutComponent: CashplayscreenpenaltyshootoutComponent,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.tournamentPrizes = (this.configService.configData.prizes[this.configService.configData.url.userRegion + "_" + this.configService.configData.operatorId.toUpperCase()]) || this.configService.configData.prizes["default"];
    this.singleGamePrizes = (this.configService.configData.prizes[this.configService.configData.url.userRegion + "_" + this.configService.configData.operatorId.toUpperCase()]) || this.configService.configData.prizes["default"];
    
    this.textConfig = this.cashplayscreenpenaltyshootoutComponent.textConfig;
    this.buttonConfig = this.cashplayscreenpenaltyshootoutComponent.buttonConfig;
    this.isStandAlone = this.cashplayscreenpenaltyshootoutComponent.isStandAlone;
    this.loadConfigData();
    // this.loadplayerTeamList();
    this.loadGameConfigData();
    this.loadTournyGameConfigData();
  }

  ngOnInit(): void {
    this.clubs = [
      "arsenal",
      "astonvilla",
      "brentford",
      "brighton&Hove_Albion",
      "burnley",
      "chelsea",
      "crystalpalace",
      "everton",
      "leedsutd",
      "leicestercity",
      "liverpool",
      "manchestercity",
      "manutd",
      "newcastleutd",
      "norwich",
      "southampton",
      "tottenhamhotspur",
      "watford",
      "westhamutd",
      "wolverhampton",
    ];
    this.countries = [
      "algeria",
      "argentina",
      "australia",
      "belgium",
      "brazil",
      "cameroon",
      "chillie",
      "colombia",
      "croatia",
      "czechia",
      "england",
      "france",
      "germany",
      "ghana",
      "greece",
      "ireland",
      "italy",
      "jamaica",
      "japan",
      "kenya",
      "mexico",
      "netherland",
      "nigeria",
      "norway",
      "portugal",
      "southafrica",
      "spain",
      "turky",
      "uganda",
      "uruguay",
      "us",
    ];

    this.gameMode = this.cashplayscreenpenaltyshootoutComponent.isSinglePlayer;

    let currentDate = new Date();
    let lastday = new Date(
      currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7)
    );
    this.currentSunday = lastday.getDate();
    this.currentMonth = lastday.toLocaleString("default", { month: "long" });
  }

  loadConfigData() {
    // this.loadGameConfigData().then((res) => this.loadTournyGameConfigData().then((res) => this.loadplayerTeamList().then((res) => this.getLeaderBoardData())))
    this.loadTournyGameConfigData();
    this.loadplayerTeamList().then((res) => this.getLeaderBoardData())
  }

  loadTournyGameConfigData() {
    this.tournyGameConfigData = this.cashplayscreenpenaltyshootoutComponent.tournyGameConfigData;
  }

  loadplayerTeamList(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get("assets/config/psSinglePlayerTeamList.json")
        .subscribe(
          (data: any) => {
            this.tourneyPlayerLogo = data["TourneyTeamsList"];
            this.singlePlayerLogo = data["SinglePlayerTeamsList"];

            resolve(true);
          },
          (err: any) => {
            console.log(err);
          }
        );
    });
  }

  loadGameConfigData(){
    this.gameConfigData = this.cashplayscreenpenaltyshootoutComponent.gameConfigData;
  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);

    console.log("test", "ngAfterViewInit");
  }

  getLeaderBoardData() {
    this.currencyCode = this.cashplayscreenpenaltyshootoutComponent.currencyCode;
    if (this.cashplayscreenpenaltyshootoutComponent.leaderboardData) {
      this.leaderBoardData = this.cashplayscreenpenaltyshootoutComponent.leaderboardData.map(
        (info: any) => {
          let icon = this.getSelectedTeamLogo(info.matchId, !this.gameMode);
          console.warn("icons ", icon)
          return this.gameMode
          ? {
            ...info,
            icon,
            img: "assets/images/countryFlags/" + icon + ".png",
          }
          : {
            ...info,
            icon,
            countryImg: "assets/images/countryFlags/" + icon + ".png",
          };
        }
      );
      let rank;
      this.resultLength = this.leaderBoardData.length;
      this.currentPlayer = this.leaderBoardData.find((obj: { playerName: string | null; points: number; playerId: string | null }, index: any) => {
        if ((obj.playerName === this.configService.configData.currUsername) || (obj.playerId === this.configService.configData.playerId)) {
          rank = index;
          if (index == 0 && obj.points > 0) {
            rank = 0;
          }
          return true
        }
        return false;
      });
      if (!this.currentPlayer) {
        this.currentPlayer = {}
      } else {
        this.currentPlayer.rank = rank;
      }
    }
  }

  getSelectedTeamLogo(matchId: number, isTourny: boolean) {
    let arr = isTourny ? this.tournyGameConfigData : this.gameConfigData;

    let len = arr.length;
    let team = "";
    let flag = "argentinaIcon";
    for (let k = 0; k < len; k++) {
      if (arr[k].matchId == matchId) {
        team = arr[k].selectedTeam.toLowerCase();
      }
    }

    let logoArr = isTourny ? this.tourneyPlayerLogo : this.singlePlayerLogo;

    len = logoArr.length;
    for (let k = 0; k < len; k++) {
      if (team == logoArr[k].name.toLowerCase()) {
        flag = logoArr[k].icon;
      }
    }

    return flag;
  }

  onClose() {
    this.router.navigate(["../"], { relativeTo: this.route });
  }
}
