import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardpenaltyshootoutComponent } from './leaderboardpenaltyshootout.component';

describe('LeaderboardpenaltyshootoutComponent', () => {
  let component: LeaderboardpenaltyshootoutComponent;
  let fixture: ComponentFixture<LeaderboardpenaltyshootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderboardpenaltyshootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardpenaltyshootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
