import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashplayscreenpenaltyshootoutComponent } from './cashplayscreenpenaltyshootout.component';

describe('CashplayscreenpenaltyshootoutComponent', () => {
  let component: CashplayscreenpenaltyshootoutComponent;
  let fixture: ComponentFixture<CashplayscreenpenaltyshootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashplayscreenpenaltyshootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CashplayscreenpenaltyshootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
