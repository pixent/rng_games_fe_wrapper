import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-modalpopup',
  templateUrl: './modalpopup.component.html',
  styleUrls: ['./modalpopup.component.css']
})
export class ModalpopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalpopupComponent>,@Inject(MAT_DIALOG_DATA) public inputData: any) { 
  }

  ngOnInit(): void {
    setTimeout(()=>
    {
      this.dialogRef.close();
    },3500)
   
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
