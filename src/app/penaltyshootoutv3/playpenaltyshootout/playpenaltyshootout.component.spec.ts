import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayPenaltyShootoutComponent } from './playpenaltyshootout.component';

describe('PlayPenaltyShootoutComponent', () => {
  let component: PlayPenaltyShootoutComponent;
  let fixture: ComponentFixture<PlayPenaltyShootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayPenaltyShootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayPenaltyShootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
