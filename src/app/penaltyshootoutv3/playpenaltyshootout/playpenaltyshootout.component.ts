import { Component, EventEmitter, HostListener, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../../services/config.service';
import { ScoreBoardService } from '../../services/scoreboard.service';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-playpenaltyshootout',
  templateUrl: './playpenaltyshootout.component.html',
  styleUrls: ['./playpenaltyshootout.component.css']
})
export class PlayPenaltyShootoutComponent implements OnInit {

  url: any;
  iframeUrl: any;

  constructor(public sanitizer: DomSanitizer, public configService: ConfigService, private scoreBoardService: ScoreBoardService, public viewportScroller: ViewportScroller) { }

  ngOnInit(): void {
    this.url = this.configService.configData.url.canvas + "penaltyshootout/index.html";
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    if (document.getElementById('iframeElement')) {
      document.getElementById('iframeElement')!.style.display = "block";
    }
  }


  ngAfterViewInit(): void {
    // this.ref.detach();
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
  }

  onOpen() {

  }
}
