// @ts-nocheck
import { Component, OnInit, HostListener, ViewChild, Renderer2 } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ScoreBoardService } from "../services/scoreboard.service";
import { ConfigService } from "../services/config.service";
import { DomSanitizer } from "@angular/platform-browser";
import * as moment from "moment-timezone";
import _moment from "moment";
import { HowToPlayPenaltyShootoutComponent } from "./howtoplaypenaltyshootout/howtoplaypenaltyshootout.component";
// import { LeaderboardPenaltyShootoutComponent } from "./leaderboardpenaltyshootout/leaderboardpenaltyshootout.component";
import { PlayPenaltyShootoutComponent } from "./playpenaltyshootout/playpenaltyshootout.component";
import { ResultPenaltyShootoutComponent } from "./resultpenaltyshootout/resultpenaltyshootout.component";
import { FaqPenaltyShootoutComponent } from "./faqpenaltyshootout/faqpenaltyshootout.component";
import { HttpClient } from '@angular/common/http';
import { PenaltyShootoutService } from "../services/penalty-shootout.service";
import { SocketService } from 'src/app/services/socket.service';
import { Subject } from "rxjs";

@Component({
  selector: "app-penaltyshootout",
  templateUrl: "./penaltyshootout.component.html",
  styleUrls: ["./penaltyshootout.component.css"],
})
export class PenaltyShootoutComponent implements OnInit {
  matchDetails: any;
  scoreCardDetails: any;
  time: any;
  url: any;
  iframeUrl: any;
  playButtonState: any = false;
  betValues: any;
  selectedBet: any = 100;
  loadedIframe: any;
  inputSelected: boolean = false;
  scrollYPosition: any;
  playerId: any;
  howtoplaycomponent: boolean = false;
  faqcomponent: boolean = false;
  resultcomponent: boolean = false;
  leaderboardcomponent: boolean = false;
  showPopup: boolean = false;
  playcomponent: boolean = true;
  currencyCode: any;
  currencySymbol: any;
  username: any = "";
  isButtonDisabled: boolean = false;
  tourneyLeaderBoardData: any;
  gameCode: string = "Penalty Shootout"
  leaderboardData: any;
  isCashPlay: boolean = true;
  userDetails: any;
  currPlayerId: string;
  showIframe: boolean = false;
  tipsData: any = [];
  tipGeneration: any;
  showMenu: boolean = false;
  preloaderStarted: boolean = false;
  psLoadingComplete: boolean = false;
  showPlayForFreeOption: boolean = false;
  isSinglePlayer: boolean = true;
  playButtonDisabled: boolean = true;
  unfinishedData: any = {};
  isUnfinishedGame: boolean = false;
  userDetailsSent: boolean = false;
  gameInitSent: boolean = false;
  socketConnected: boolean = false;
  playerAccountDetails: any = {};
  isIframeGame: boolean = false;
  freePlayerUserName: any = '';
  freeSingleTrialDone: boolean = false;
  freeTourneyTrialDone: boolean = false;
  minStakeNotAchieved: boolean = true;
  showShabikiTrialAlert: boolean = false;
  showGenericPopup: boolean = false;
  genericErrorMessage: any = '';
  preloaderImg: any = '';
  psTitleImage: any = 'psTitle';
  balance: any;
  _isShabikiPlatform: boolean = false;
  _isPlayabetPlatform: boolean = false;
  _isBetyetuPlatform: boolean = false;
  _isStarbetPlatform: boolean = false;

  penaltyShootoutSubject = new Subject();
  psServerResponseSubject = new Subject();
  messageSubscription: Subscription;
  gameType: string = '';
  showCashOption: boolean = true;
  showPlayOption: boolean = false;
  enableFreePlayCondition: boolean = false;
  freePlayRequiredBetAmount: any;
  buttonConfig: any = {};
  textConfig: any = {};
  cashTournamentPlay: any;
  resultsPageConfig: any = {};
  isStandAlone: boolean = false;
  menuHeight: any;
  hideBackButton: boolean = false;
  playForFreeBgColor: any = '';
  playForCashBgColor: any = '';
  resultBgColor: any = '';
  termsAndConditionsBg: any = '';
  howToPlayBg: any = '';

  @ViewChild(HowToPlayPenaltyShootoutComponent, { static: true })
  howtoplaypenaltyshootout: HowToPlayPenaltyShootoutComponent;
  // @ViewChild(LeaderboardPenaltyShootoutComponent, { static: true })
  // leaderboardpenaltyshootout: LeaderboardPenaltyShootoutComponent;
  @ViewChild(PlayPenaltyShootoutComponent, { static: true })
  playpenaltyshootout: PlayPenaltyShootoutComponent;
  @ViewChild(ResultPenaltyShootoutComponent, { static: true })
  resultpenaltyshootout: ResultPenaltyShootoutComponent;
  @ViewChild(FaqPenaltyShootoutComponent, { static: true })
  faqpenaltyshootout: FaqPenaltyShootoutComponent;

  constructor(
    private psService: PenaltyShootoutService,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private router: Router,
    private http: HttpClient,
    public configService: ConfigService,
    public socketService: SocketService,
    private scoreBoardService: ScoreBoardService,
    private renderer: Renderer2
  ) {
    scoreBoardService.changeEmitted$.subscribe((text) => {
      this.scoreCardDetails = text;
      // this.setScoreCard();
    });

    this.psService.playerDetails$.subscribe((res) => {
      res && (this.currPlayerId = res.playerId);
    });
    this.messageSubscription = this.socketService.messageSubject.subscribe((message) => {
      this.serverResponse(message);
    });
  }

  ngOnInit(): void {
    this.url =
      this.configService.configData.url.canvas + "psv2/index.html";
    this.loadedIframe = document.getElementById('iframeElement');
    this.gameType = this.configService.configData.gameType;
    let preloaderImgPath = "assets/images/pspreloaderimg";
    if (this.configService.configData.preloaderImg.indexOf(this.configService.configData.operatorId) >= 0) {
      this.preloaderImg = preloaderImgPath + "_" + this.configService.configData.operatorId + ".png";
    } else {
      this.preloaderImg = preloaderImgPath + ".png";
    }
    // console.warn("gameType " + this.gameType);

    if (this.configService.configData.psFreePlayNotAvailable || this.gameType == "CASH") {
      this.showPlayForFreeOption = true;
    } else {
      this.showPlayForFreeOption = false;
    }
    if (this.configService.configData.psCashPlayNotAvailable) {
      this.showPlayForFreeOption = false;
      this.showCashOption = false;
      this.showPlayOption = true;
    }
    if (this.gameType == "FREE") {
      this.showCashOption = false;
    }
    this.showPlayForFreeOption = this.configService.configData.psFreePlayNotAvailable;
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    this.checkRoute();
    this.matchDetails = window.history.state.item;
    // if (this.matchDetails) {
    //   localStorage.setItem("matchDetails", JSON.stringify(this.matchDetails));
    // }
    // if (localStorage.getItem("matchDetails")) {
    //   this.matchDetails = JSON.parse(localStorage.getItem("matchDetails")!);
    // }
    this.userDetails = {
      accessToken: "",
      customerId: "",
    };

    this.showMenu = false;
    this.preloaderStarted = true;
    this._isShabikiPlatform = this.configService.configData.operatorId == 'shabiki';
    this._isPlayabetPlatform = this.configService.configData.operatorId == 'playabet';
    this._isBetyetuPlatform = this.configService.configData.operatorId == 'betyetu';
    this._isStarbetPlatform = this.configService.configData.operatorId == 'starbet';

    this.enableFreePlayCondition = this.configService.configData.oneRoundFreePlayCondition[this.configService.configData.operatorId];

    if (this.configService.configData.operatorId !== 'paribet' && this.configService.configData.languageCode !== 'fr') {
      this.loadPSTips();
    } else if (this.configService.configData.operatorId == 'paribet' && this.configService.configData.languageCode == 'fr') {
      this.tipsData = null;
    }
    this.loadTextConfig();
    this.setPSConfig();
    this.loadImages();
    this.loadSecondaryImages();
    this.currencyCode = this.configService.configData.currencyCode;
    this.isUnfinishedGame = false;
    this.unfinishedData = {};
    this.setUserDetails();
    this.initializeGame();
    this.getResultData();
    if (this.currencyCode != "undefined") {
      this.getCurrencySymbol();
    }
    // this.renderer.listen(document.getElementById('iframeElement'),'load',()=>{
    //   this.initializeGame()
    // })
    // console.warn("loading...started canvas loading");
    if(this.configService.configData.bets && this.configService.configData.bets.requiredBetAmountToPlayFree){
      this.freePlayRequiredBetAmount = this.configService.configData.bets.requiredBetAmountToPlayFree;
    }

    if (this.configService.configData.operatorId == "starbet") {
      this.numberOfBetsRequiredToPlayFree = this.configService.configData.bets.requiredNumberOfBetsToPlayFree;
    }



    document.title = "Penalty Shootout";
    this.isStandAlone = this.configService.configData.standalone;
    this.hideBackButton = this.configService.configData.hideBackButtonInHeaderComponent[this.configService.configData.operatorId] ?
      this.configService.configData.hideBackButtonInHeaderComponent[this.configService.configData.operatorId] :
      this.configService.configData.hideBackButtonInHeaderComponent["default"];


  }

  getCalendarWeek() {
    let curr = new Date
    let week = []

    for (let i = 1; i <= 7; i++) {
      let first = curr.getDate() - curr.getDay() + i
      let day = new Date(curr.setDate(first)).toISOString().slice(0, 10)
      week.push(day)
    }
    return {
      startDate: week[0] + " 00:00:00",
      endDate: week[6] + " 23:59:59"
    }
  }

  getResultData(): any {
    // if (!this._isShabikiPlatform) return;

    let playerId = this.userDetails.customerId;
    let _gameName = "freePenaltyShootout";
    let _tourneyName = "freeTournamentPenaltyShootout";
    let { startDate, endDate } = this.getCalendarWeek();

    let _leaderBoardUrl = this.configService.configData.url.socket + "penaltyShootoutHistory?playerId=" + playerId + "&gameName=" + _gameName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;
    let _tournamentPenaltyShootoutUrl = this.configService.configData.url.socket + "tournamentPenaltyShootoutHistory?playerId=" + playerId + "&gameName=" + _tourneyName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;

    this.http.get(_tournamentPenaltyShootoutUrl).subscribe(
      (tourneyData: any) => {
        if (tourneyData.length) {
          this.freeTourneyTrialDone = true;
        }

        this.http.get(_leaderBoardUrl).subscribe(
          (leaderData: any) => {
            if (leaderData.length) {
              this.freeSingleTrialDone = true;
              return
            }
          },
          (err: any) => {
            console.log(err);
          }
        );
      },
      (err: any) => {
        console.log(err);
      }
    );

    let gameName = "penaltyShootout";
    let tourneyName = "tournamentPenaltyShootout";

    let leaderBoardUrl = this.configService.configData.url.socket + "penaltyShootoutHistory?playerId=" + playerId + "&gameName=" + gameName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;
    let tournamentPenaltyShootoutUrl = this.configService.configData.url.socket + "tournamentPenaltyShootoutHistory?playerId=" + playerId + "&gameName=" + tourneyName + "&startDate=" + startDate + "&endDate=" + endDate + "&region=" + this.configService.configData.url.userRegion;

    let totalStake = 0;
    let totalMatchesPlayed = 0;
    this.http.get(tournamentPenaltyShootoutUrl).subscribe(
      (tourneyData: any) => {
        totalMatchesPlayed += tourneyData.length;
        for (const data of tourneyData) {
          totalStake = totalStake + data.betAmount;
        }

        this.http.get(leaderBoardUrl).subscribe(
          (leaderData: any) => {
            totalMatchesPlayed += leaderData.length;
            // setTimeout(this.getResultData.bind(this), 10000)
            for (const data of leaderData) {
              totalStake = totalStake + data.betAmount;
            }

            if (this.enableFreePlayCondition) {
              if (this.configService.configData.operatorId == "starbet" && totalMatchesPlayed >= this.numberOfBetsRequiredToPlayFree) {
                this.minStakeNotAchieved = false;
              } else if (this.configService.configData.operatorId != "starbet" && totalStake >= this.freePlayRequiredBetAmount) {
                this.minStakeNotAchieved = false;
              }
            }
          },
          (err: any) => {
            console.log(err);
          }
        );

      },
      (err: any) => {
        console.log(err);
      }
    );

    return true;
  }

  ngAfterViewInit() {
    let iframeElement = document.getElementById("iframeElement");
    // this.isShabikiPlatform = this.configService.configData.platformId == 2;
    let topNavHeight = 0;
    if (document.getElementById("top-nav")) {
      topNavHeight = document.getElementById("top-nav").clientHeight;
    }
    if (iframeElement) {
      if (window.location.href.indexOf("10bet") >= 0) {
        iframeElement.style.minHeight = window.innerHeight - topNavHeight + "px";
      }
      // iframeElement.style.maxWidth = "767px";
      if ((window.location.href.indexOf("10bet") >= 0) || this.isIframeGame) {
        iframeElement.style.height = screen.height + "px";
      } else {
        iframeElement.style.height = window.innerHeight + "px";
      }
    }
  }

  ngOnDestroy() {
    if (this.time)
      clearInterval(this.time);

    this.messageSubscription.unsubscribe();
    if (this.socketService) {
      this.socketService.disconnectSocket();
    }
  }

  setPSConfig() {
    for (let p in this.configService.configData.psConfig) {
      let operator = this.configService.configData.psConfig[p];
      if (p == this.configService.configData.operatorId) {
        if (operator.icons) {
          this.psTitleImage = operator.icons.psTitleImage;
          if (operator.icons.psBackgroundImage) {
            setTimeout(()=>{
              $("#penalty-shootout-menu").css("background-image","url(assets/images/"+operator.icons.psBackgroundImage + ")");
              $("#preloader").css("background-image","url(assets/images/"+operator.icons.psBackgroundImage + ")");
              $("#tips").css("background",operator.css.resultBgColor);
            },100);
          }
        }
        if (operator.pspreloaderimg) {
          this.preloaderImg = "assets/images/" + operator.pspreloaderimg + ".png";
        }
        if (operator.css) {
          this.playForFreeBgColor = operator.css.playForFreeBgColor;
          this.playForCashBgColor = operator.css.playForCashBgColor;
          this.resultBgColor = operator.css.resultBgColor;
          this.howToPlayBg = operator.css.howToPlayBg;
          this.termsAndConditionsBg = operator.css.termsAndConditionsBg;
        }
        if (operator.fonts) {
          $("*").css("fontFamily",operator.fonts.main);
        }
        if (operator.text) {
          setTimeout(() => {
            for (let p in operator.text) {
              this.buttonConfig[p] = operator.text[p];
              this.textConfig[p] = operator.text[p];
            }
          },100)
        }
      }
    }
    if (!this.configService.configData.psConfig[this.configService.configData.operatorId]) {
      $("*").css("fontFamily", "Lalezar-Regular");
      $("#preloader").css("background-image","url(assets/images/splashScreen_BG.jpg)");
    }
    if(this.configService.configData.operatorId == "playbigapp") {
      if (!this.configService.configData.isMobile) {
        $("body").css("background-image","url(assets/images/playbigapp/penaltyShootout/mainBG.png)");
        $("body").css("background-size","cover");
      }
      setTimeout(()=> {
        $(".game_buttons button").css("font-size","1.5rem");
        $(".game_buttons .results button").css("border","1px solid #ffffff");
      },100)
    }
  }

  @HostListener("window:scroll", ["$event"])
  onScroll(event) {
    this.scrollYPosition = document.documentElement.scrollTop;
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    event.target.innerWidth;
  }

  @HostListener("window:message", ["$event"])
  onMessage(res: any) {
    this.penaltyShootoutSubject.next(res);
    if (res.data) {
      let messageData = res.data;
      switch (messageData.messageName) {
        case "init":
          // console.warn("loading...canvas loaded - received game init");
          this.loadedIframe = document.getElementById("iframeElement");
          if (this.currencyCode != "undefined") {
            this.sendUserDetailsToGame();
          }
          // this.initializeGame(messageData);
          break;
        case "LoadingPercentage":
          this.setPSLoadingPercentage(messageData);
          break;
        case "LoadingComplete":
          this.setPSLoadingComplete();
          break;
        default:
          break;
      }
    }
  }


  serverResponse(message: any) {
    // console.log("server response", message)
    if (message == "socket connected") {
      this.onSocketConnected();
    } else {
      switch (message.message) {
        case "authenticate":
          this.onPlayerAuthenticated(message.data);
          break;
        case "balanceResponse":
          this.setBalanceResponse(message.data);
          break;
        case "gameInitResponse":
          this.setGameInit(message.data);
          break;
        default:
          this.psServerResponseSubject.next(message);
          break;
      }
    }
  }

  onSocketConnected() {
    this.socketConnected = true;
    this.userDetails = this.userDetails;
    if ((window.location.href.indexOf("10bet") >= 0) || this.isIframeGame) {
      document.getElementById("iframeElement")!.style.height = screen.height + "px";
    } else {
      document.getElementById("iframeElement")!.style.height = window.innerHeight + "px";
    }
    if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {

      if ((window.location.href.indexOf("10bet") < 0)) {
        this.socketService.authenticatePlayer({
          token: this.userDetails.accessToken,
          playerId: (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
          gameName: this.isCashPlay ? "penaltyShootout" : "freePenaltyShootout",
          matchId: 100001,
          jurisdiction: this.configService.configData.url.userRegion,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": "penaltyShootout",
            "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
            "playerName": this.configService.configData.playerName,
            "playerId": (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        });
      } else {
        this.socketService.getPsoBalance({
          token: this.userDetails.accessToken,
          playerId: (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
          jurisdiction: this.configService.configData.url.userRegion,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": "penaltyShootout",
            "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
            "playerName": this.configService.configData.playerName,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        });
      }
      // console.warn("loading...socket connected - sending balance call");
      this.psService.setPlayerDetails({ playerId: this.userDetails.customerId })
      let connectionDetails: any = {};
      connectionDetails.message = "SOCKET_CONNECTED";
      connectionDetails.data = true;
      this.psServerResponseSubject.next({
        message: "socket connected",
        data: true
      });
      this.loadedIframe = document.getElementById('iframeElement');
      if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {
        this.loadedIframe.contentWindow.gameExtension(connectionDetails, "*");
      }
    } else {
      setTimeout(() => {
        this.onSocketConnected();
      }, 1000)
    }
  }

  onPlayerAuthenticated(balanceResponse: any) {
    if (balanceResponse.playerAccount) {
      this.balance = balanceResponse.playerAccount.balance;
    }
    if (balanceResponse.playerAccount && (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6")) {
      this.userDetails.customerId = balanceResponse.playerAccount.playerId;
      this.userDetails.playerName = balanceResponse.playerAccount.playerName;
      this.currencyCode = balanceResponse.playerAccount.currencyCode;
      this.configService.configData.currencyCode = balanceResponse.playerAccount.currencyCode;
      this.getCurrencySymbol();
      if (this.configService.configData.platformId == "4") {
        this.sendUserDetailsToGame();
      }
      this.psServerResponseSubject.next({
        "message": "setcurrencycode",
        "data": false
      })
      this.socketService.subscribePlayerBet(balanceResponse.playerAccount.playerId);
      this.psService.setPlayerDetails({ playerId: this.userDetails.customerId })
    }
    if (window.location.href.indexOf("10bet") >= 0) {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerName != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }
    } else {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerId != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }

    }
    if (balanceResponse.error) {
      this.showGenericPopup = true;
      this.showModal(balanceResponse.errorMessage || 'The game is under maintenance. Please check after sometime.');
    }
    this.socketService.getPsoBalance({
      token: this.userDetails.accessToken,
      playerId: this.userDetails.customerId,
      jurisdiction: this.configService.configData.url.userRegion,
      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": "penaltyShootout",
        "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
        "playerName": this.configService.configData.playerName,
        "isFreeGame": this.configService.configData.isFreeGame
      }
    });
  }

  setBalanceResponse(balanceResponse: any) {
    // if (!this.playerAccountDetails) {
    //   this.getGameInit();
    // }
    if (balanceResponse && balanceResponse.playerAccount) {
      this.balance = balanceResponse.playerAccount.balance;
    }

    if (window.location.href.indexOf("10bet") >= 0) {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerName != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }
    } else {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerId != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }
    }
    if (balanceResponse.status == 400 || balanceResponse.error) {
      this.showGenericPopup = true;
      this.showModal(balanceResponse.errorMessage || 'The game is under maintenance. Please check after sometime.');
      this.playButtonDisabled = false;
      this.psServerResponseSubject.next({
        "message": "playbuttonstate",
        "data": false
      })
      return;
    }
    this.playerAccountDetails = { ...balanceResponse.playerAccount };
    this.psServerResponseSubject.next({
      message: "setUsername",
      data: this.playerAccountDetails
    })
    if (balanceResponse.playerAccount.playerId != balanceResponse.playerAccount.playerName) {
      this.playerAccountDetails = balanceResponse.playerAccount;
      this.psServerResponseSubject.next({
        message: "setUsername",
        data: this.playerAccountDetails
      })
    }
    // if (balanceResponse.status != 200) {
    //   this.playButtonDisabled = false;
    //   this.psServerResponseSubject.next({
    //     "message": "playbuttonstate",
    //     "data": false
    //   })
    //   return;
    // } else {
    //   this.playerAccountDetails = balanceResponse.playerAccount;
    //   this.psServerResponseSubject.next({
    //     message: "setUsername",
    //     data: this.playerAccountDetails
    //   })
    // }
    // if (this.isCashPlay) {
    //   if (this.userDetails.customerId == balanceResponse.playerAccount.playerName) {
    //     let currencyDetails: any = {};
    //     currencyDetails.message = "UPDATE_PLAYER_CURRENCY";
    //     currencyDetails.data = {
    //       "currencyCode": this.currencyCode
    //     };
    //     if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //       this.loadedIframe.contentWindow.gameExtension(currencyDetails, "*");
    //     }
    //   }
    // } else {
    //   let currencyDetails: any = {};
    //   currencyDetails.message = "UPDATE_PLAYER_CURRENCY";
    //   currencyDetails.data = {
    //     "currencyCode": this.currencyCode
    //   };
    //   if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //     this.loadedIframe.contentWindow.gameExtension(currencyDetails, "*");
    //   }
    // }
    this.getGameInit({});
  }

  getGameInit(data: any) {
    // console.warn("loading...send game history call");
    if (this.socketConnected) {
      let gameInitobj = {
        playerId: this.userDetails.customerId,
        gameName: this.isCashPlay ? "penaltyShootout" : "freePenaltyShootout",
        token: this.userDetails.accessToken,
        accessToken: this.userDetails.accessToken,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": "penaltyShootout",
          "gameType": (this.isSinglePlayer ? "penaltyShootout" : "tournamentPenaltyShootout"),
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      if (data && data.gameName) {
        gameInitobj.gameName = data.gameName;
      }
      this.socketService.getPsoGameInit(gameInitobj);
      this.configService.configData.playerToken = gameInitobj.token;
      // localStorage.setItem('playerToken', JSON.stringify(gameInitobj.token));
    } else {
      setTimeout(() => {
        this.getGameInit();
      }, 500);
    }
  }

  setGameInit(gameInitRes: any) {
    if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {
      // console.warn("loading...received game history call");
      this.gameInitData = gameInitRes;
      let gameInitDetails: any = {};
      gameInitDetails.message = "GAME_INIT";
      gameInitDetails.data = gameInitRes;
      // console.log('send game init response to game', gameInitDetails);
      if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {
        this.loadedIframe.contentWindow.gameExtension(gameInitDetails, "*");
      }
      this.isUnfinishedGame = false;
      if (this.gameInitData.gameHistory.length) {
        this.unfinishedData.singlePlayer = JSON.parse(this.gameInitData.gameHistory[0].result);
        if (this.unfinishedData.singlePlayer && this.unfinishedData.singlePlayer.betId == "0") {
          this.unfinishedData.singlePlayer.betId = this.gameInitData.gameHistory[0].betId;
        }
        if (!this.unfinishedData.singlePlayer) {
          this.unfinishedData.singlePlayer = this.gameInitData.gameHistory[0];
        } else {
          this.unfinishedData.singlePlayer.inningNumber = this.gameInitData.gameHistory[0].inningNumber;
          if (!this.unfinishedData.singlePlayer.matchId) {
            this.unfinishedData.singlePlayer.matchId = this.gameInitData.gameHistory[0].matchId;
          }
        }
        if (this.unfinishedData.singlePlayer && !this.unfinishedData.singlePlayer.tournamentWin) {
          this.unfinishedData.singlePlayer.tournamentWin = this.gameInitData.tournamentWin;
        }
        if (this.unfinishedData.singlePlayer && !this.unfinishedData.singlePlayer.selectedTeam) {
          this.unfinishedData.singlePlayer.selectedTeam = this.unfinishedData.singlePlayer.results ? this.unfinishedData.singlePlayer.results[0].selectedTeam : "";
          this.unfinishedData.singlePlayer.opponentTeam = this.unfinishedData.singlePlayer.results ? this.unfinishedData.singlePlayer.results[0].opponentTeam : "";
        }
        this.isUnfinishedGame = true;
      }
      if (this.gameInitData.tournamentGameHistory && this.gameInitData.tournamentGameHistory.length) {
        this.unfinishedData.tournament = this.gameInitData.tournamentGameHistory[0];
        if (this.unfinishedData.tournament && !this.unfinishedData.tournament.selectedTeam) {
          if (this.unfinishedData.tournament.result) {
            let resultData = JSON.parse(this.unfinishedData.tournament.result);
            if (resultData && resultData.results && resultData.results.length) {
              this.unfinishedData.tournament.selectedTeam = resultData.results[0].selectedTeam;
              this.unfinishedData.tournament.opponentTeam = resultData.results[0].opponentTeam;
            }
          }
        }
        if (this.unfinishedData.tournament && !this.unfinishedData.tournament.tournamentWin) {
          this.unfinishedData.tournament.tournamentWin = this.gameInitData.tournamentWin;
        }
        this.isUnfinishedGame = true;
      }
      this.gameInitSent = true;
      if (this.userDetailsSent) {
        this.playButtonDisabled = false;
        // console.warn("loading...play button enabled");
        this.psServerResponseSubject.next({
          "message": "playbuttonstate",
          "data": false
        })
      }
    } else {
      setTimeout(() => {
        this.setGameInit(gameInitRes);
      }, 500)
    }
    // } 
    // let apiURLDetails = {
    //   message: "SET_API_URL",
    //   data: {
    //     url: this.configService.configData.apiUrl
    //   }
    // }
    // if (this.loadedIframe && this.loadedIframe.contentWindow) {
    //   this.loadedIframe.contentWindow.gameExtension(apiURLDetails, "*");
    // }
  }



  setScrollable(obj) {
    window.scrollTo(0, 0);
    if (this.loadedIframe) {
      this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }

  navigateToLobby() {
    if (document.getElementById("top-nav")) {
      this.router.navigate([""]);
    }
  }

  navigatePSMenu(data) {
    if (data.buttonId == "howtoplayps") {
      this.clearComponents();
      this.howtoplaycomponent = true;
      this.howtoplaypenaltyshootout.onOpen();
    } else if (data.buttonId == "faqps") {
      this.clearComponents();
      this.faqcomponent = true;
      this.faqpenaltyshootout.onOpen();
    } else if (data.buttonId == "predictover") {
      this.clearComponents();
      this.playcomponent = true;
    } else if (data.buttonId == "resultps") {
      this.clearComponents();
      this.resultcomponent = true;
      this.resultpenaltyshootout.onOpen();
    } else if (data.buttonId.toLowerCase().indexOf("leaderboard") >= 0) {
      this.clearComponents();
      this.leaderboardcomponent = true;
      // this.leaderboardpenaltyshootout.onOpen(
      //   data.isTourney,
      //   data.userName,
      //   data.iconName
      // );
    } else {
      this.clearComponents();
      this.playcomponent = true;
    }
  }

  updatePlayerId(data) {
    this.playerId = data.playerId;
  }

  updateCurrency(data) {
    // this.currencyCode = data.currency;
    // this.resultpenaltyshootout.currencyCode = data.currency + " ";
  }

  clearComponents() {
    this.howtoplaycomponent = false;
    this.playcomponent = false;
    this.leaderboardcomponent = false;
    this.resultcomponent = false;
  }

  checkRoute() {
    $("#" + this.router.url.split("/")[2].split("?")[0])
      .addClass("active")
      .siblings("li")
      .removeClass("active");
  }

  setCountdown() {
    if (this.matchDetails) {
      let serverDate = moment.tz(new Date(), "Asia/Kolkata").format();
      let currentDate = moment(serverDate.split("+")[0]);
      let resultDate = moment(new Date(this.matchDetails.startDate));
      let dateDiff = moment.duration(currentDate.diff(resultDate));
      if (-dateDiff._data.seconds < 0) {
        this.matchDetails.date = "Completed";
      } else {
        this.matchDetails.date =
          -dateDiff._data.hours +
          "hr : " +
          -dateDiff._data.minutes +
          "m : " +
          -dateDiff._data.seconds +
          "s";
      }
    }
  }

  initializeGame(obj?: any) {
    this.playButtonDisabled = true;
    this.gameInitSent = false;
    this.socketConnected = false;
    // console.warn("loading...sent socket connection request");
    if (!navigator.onLine) {
      this.showGenericPopup = true;
      this.showModal(this.configService.configData.networkErrorMessage);
    }
    this.socketService.connectToServer(
      // this.configService.configData.url.socket,
      this.configService.configData.url.socket,
      this.isCashPlay ? "penaltyShootout" : "freePenaltyShootout",
      this.userDetails.customerId,
      this.matchDetails ? this.matchDetails.matchId : 100001
    );
    // this.sendUserDetailsToGame();
  }

  setUserDetails() {
    let params: any = document.cookie;
    if (params) {
      params = params.split("; ");
      for (let i = 0; i < params.length; i++) {
        let arr = params[i].split("=");
        if (arr[0] == "AppSession") {
          this.userDetails.accessToken = this.configService.configData.token || arr[1];
          this.isIframeGame = true;
        } else if (arr[0] == "userId") {
          this.userDetails.customerId = this.configService.configData.playerId || arr[1];
        } else if (arr[0] == "MobileNumber") {
          this.userDetails.playerName = this.configService.configData.playerName || arr[1];
        } else if (arr[0] == "userNumber" && this.configService.configData.platformId == "2") {
          this.userDetails.playerName = this.configService.configData.playerName || arr[1];
        }
      }
      if ((!this.userDetails.accessToken || this.userDetails.accessToken == "undefined") && this.configService.configData.token) {
        this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
        this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
        this.userDetails.playerName = this.userDetails.customerId;
      }
    } else {
      this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
      this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
      this.userDetails.playerName = this.userDetails.customerId;
    }
    // if (this.configService.configData.platformId == "0" && window.location.href.indexOf("10bet") < 0 && this.userDetails.accessToken == "") {
    //   this.userDetails.accessToken = this.getUUid();
    //   this.userDetails.customerId = this.configService.configData.playerId || localStorage.getItem('playerId') || this.getUUid();
    //   this.userDetails.playerName = this.userDetails.customerId;
    // }


    this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
    this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
    this.userDetails.playerName = this.configService.configData.playerName || this.userDetails.customerId;


    this.userDetails.userRegion = this.configService.configData.url.userRegion;
    const userRegion = this.userDetails.userRegion;
    const userRegionWithOperatorId = userRegion + "_" + this.configService.configData.operatorId.toUpperCase();
    this.userDetails.stakeTax = this.configService.configData.taxData[userRegionWithOperatorId] ? this.configService.configData.taxData[userRegionWithOperatorId].stakeTax : 0;
    this.userDetails.withholdTax = this.configService.configData.taxData[userRegionWithOperatorId] ? this.configService.configData.taxData[userRegionWithOperatorId].withholdTax : 0;
    this.userDetails.platformId = this.configService.configData.platformId;
    this.userDetails.operatorId = this.configService.configData.operatorId;
    this.userDetails.brand = this.configService.configData.brand;
    this.userDetails.languageCode = this.configService.configData.languageCode || 'en';
    this.userDetails.autoAdjust = true;
  }


  getUUid() {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (Math.random() * 16) | 0,
          v = c == "x" ? r : (r & 0x3) | 0x8;
        return v.toString(16);
      }
    );
  }

  setBetValues(obj) {
    this.betValues = obj.betsArray;
    this.selectedBet = this.betValues[obj.selectedBetIndex];
    $("#button-" + this.selectedBet).addClass("button-selected");
  }

  loadImages() {
    if (this.preloaderStarted) {
      let img = new Image();
      img.src = 'assets/images/splashScreen_BG.png';
      if (this.configService.configData.operatorId == "playbigapp") {
        img.src = 'assets/images/playbigapp/penaltyShootout/splashScreen_BG.jpg';
      }
      img.decode()
        .then(() => {
          this.sendPreloadingCompleteToWrapper();
          this.preloaderStarted = false;
          this.showMenu = true;
          document.body.style.background = this.configService.configData.bgColor[this.configService.configData.operatorId] || this.configService.configData.bgColor.default;
          $('.menu').css('background-image', "url('../../assets/images/splashScreen_BG.png')");
          clearInterval(this.tipGeneration);
          setTimeout(() => {
            document.getElementById('preloader')!.style.display = "none";
          }, 1000);
          this.setPSConfig();
        })
    } else {
      setTimeout(() => this.loadImages(), 10);
    }
  }

  sendPreloadingCompleteToWrapper() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage(
        {
          action: "preloadingComplete"
        },
        (document.referrer || (window.parent && window.parent.location.href))
      );
    }
  }

  loadSecondaryImages() {
    let psImages = this.configService.configData.psImages;
    for (let p in psImages) {
      for (let i = 0; i < psImages[p].length; i++) {
        let img = new Image();
        img.src = psImages[p][i];
      }
    }
  }

  removePreloader() {
    this.preloaderStarted = false;
    this.showMenu = true;
    clearInterval(this.tipGeneration);
  }

  setPSLoadingPercentage(data: any) {
    // this.preloaderStarted = true;
    // if (document.getElementById('progress-bar')) {
    //   document.getElementById('progress-bar')!.style.width = data.loadedPercentage + '%';
    // }
  }

  setPSLoadingComplete() {
    this.preloaderStarted = false;
    this.psLoadingComplete = true;
  }

  setPlayButtonState(obj) {
    this.playButtonState = obj.enablePlay;
  }

  selectButton(item, event) {
    $(".bet-button").css("background", "white");
    document
      .getElementById("input-bet")
      ?.style.setProperty("background", "white", "important");
    event.target.classList.add("button-selected");
    this.selectedBet = item;
    this.inputSelected = false;
    this.playButtonState = true;
  }

  selectInputButton(event) {
    $(".bet-button").css("background", "white");
    event.target.classList.add("button-selected");
    this.selectedBet = event.target.value;
    this.inputSelected = true;
    this.playButtonState = true;
  }

  changeInputValue(event) {
    this.selectedBet = event.target.value;
    this.playButtonState = true;
  }

  showDisplayNameChooser(msg) {
    this.showPopup = true;
  }

  onDisplayNameChosen() {
    if (this.showShabikiTrialAlert && this.enableFreePlayCondition) {
      this.showPopup = false;
      this.showShabikiTrialAlert = false;
      return;
    }
    let usernameDetails: any = {};
    usernameDetails.message = "USERNAME_ENTRY";
    usernameDetails.data = {
      username: this.username,
    };
    this.freePlayerUserName = this.username;
    if (this.configService.configData.operatorId == "demo") {
      this.userDetails.playerName = this.username;
    }
    this.psService.setUsername(this.username).subscribe((res) => {
      if (res?.playerAccount) {
        this.configService.configData.currUsername = this.username;
        // localStorage.setItem("currUsername", this.username);
        this.loadedIframe = document.getElementById("iframeElement");
        this.loadedIframe.contentWindow.gameExtension(usernameDetails, "*");
        this.showPopup = false;
        this.isButtonDisabled = true;
      }
      else this.showDisplayNameChooserError()
    }, (err) => {
      this.showDisplayNameChooserError()
    });
  }

  showDisplayNameChooserSuccess() {
    this.showPopup = false;
    this.username = "";
  }

  showDisplayNameChooserError() {
    setTimeout(() => {
      alert("Username already exists! Please try another");
      this.username = "";
      this.isButtonDisabled = false;
    }, 1000);
  }

  hidePick10PlayControls(obj) {
    if (obj.enablePlay) {
      document.getElementById("pick-10-bet")?.style.display = "block";
    } else {
      document.getElementById("pick-10-bet")?.style.display = "none";
    }
  }

  closePopup() {
    this.showPopup = false;
  }

  setIframeHeight(obj) {
    if (obj.appHeight) {
      // let appHeight = Math.max(obj.appHeight,window.outerHeight);
      document.getElementById("iframeElement")?.style.height =
        obj.appHeight + "px";
      // document.getElementById('iframeElement')?.style.maxHeight = "max-content";
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        if (window.ReactNativeWebView) {
          window.ReactNativeWebView.postMessage(JSON.stringify(
            {
              msg: "b2bresize",
              data: obj.appHeight + 120,
            }
          ));
          window.parent.postMessage(
            {
              action: "b2bresize",
              scrollHeight: obj.appHeight,
            },
            (document.referrer || (window.parent && window.parent.location.href))
          );
        } else {
          window.parent.postMessage(
            {
              action: "b2bresize",
              scrollHeight: obj.appHeight,
            },
            (document.referrer || (window.parent && window.parent.location.href))
          );
        }
      }
    }
  }

  showTourneyLeaderboard(data: any) {
    this.tourneyLeaderBoardData = data;
  }

  showLeaderboard(data: any) {
    this.leaderboardData = data;
  }

  onHowToPlayClicked() {
    this.router.navigate(["howtoplaypenaltyshootout"], {
      relativeTo: this.route,
    });
  }

  onResultsClicked() {
    this.menuHeight = document.getElementById('penalty-shootout-menu')?.clientHeight;
    console.warn("menu height ", this.menuHeight);

    this.router.navigate(["resultpenaltyshootout"], { relativeTo: this.route });
  }

  onTermsClicked() {
    this.menuHeight = document.getElementById('penalty-shootout-menu')?.clientHeight;
    // console.warn("menu height ", this.menuHeight);

    this.router.navigate(["faqpenaltyshootout"], { relativeTo: this.route });
  }

  onFreePlayClicked() {
    // if (this.enableFreePlayCondition && this.minStakeNotAchieved) {
    //   this.showShabikiTrialAlert = true;
    //   this.showPopup = true;
    //   return;
    // }

    this.isCashPlay = false;
    this.isSinglePlayer = true;
    // this.socketService.disconnectSocket();
    // this.initializeGame();
    if (this.freePlayerUserName) {
      this.playerAccountDetails.playerName = this.freePlayerUserName;
    }
    setTimeout(() => {
      if (this.playerAccountDetails) {
        this.psServerResponseSubject.next({
          message: "setUsername",
          data: this.playerAccountDetails
        })
      }
    }, 100);
    this.playButtonDisabled = true;
    this.gameInitSent = false;
    if (this.socketService) {
      this.getGameInit();
    }
    this.router.navigate(["cashplayscreenpenaltyshootout"], {
      relativeTo: this.route,
    });
  }

  onCashPlayClicked() {
    // this.showMenu = false;
    this.isCashPlay = true;
    this.isSinglePlayer = true;
    this.playButtonDisabled = true;
    this.gameInitSent = false;
    // this.socketService.disconnectSocket();
    // this.initializeGame();
    if (this.socketService) {
      this.getGameInit();
    }
    this.router.navigate(["cashplayscreenpenaltyshootout"], {
      relativeTo: this.route,
    });
  }

  sendUserDetailsToGame() {
    if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {
      let userDetailsData: any = {};
      userDetailsData.message = "USER_DETAILS";
      userDetailsData.data = this.userDetails;
      this.userDetailsSent = true;
      if (this.gameInitSent) {
        this.playButtonDisabled = false;
        // console.warn("loading...play button enabled")
        this.psServerResponseSubject.next({
          "message": "playbuttonstate",
          "data": false
        })
      }
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(userDetailsData, "*");
      }
      let currencyDetails: any = {};
      currencyDetails.message = "UPDATE_PLAYER_CURRENCY";
      currencyDetails.data = {
        "currencyCode": this.currencyCode
      };
      this.loadedIframe.contentWindow.gameExtension(currencyDetails, "*");
    } else {
      setTimeout(() => {
        this.sendUserDetailsToGame();
      }, 1000)
    }
  }

  doesFileExist(configType: string): string {
    const language = this.configService.configData.languageCode || 'en';
    const jurisdiction = this.configService.configData.url.userRegion.toLowerCase();
    if (configType === "rules") {
      let defaultPath = "assets/config/in/en/psrules.json";

      if (!language || !jurisdiction) return defaultPath;

      let path = `assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/psrules.json`;
      // let path = `../../../assets/config/drc/${language.toLowerCase()}/psrules.json`;

      let xhr = new XMLHttpRequest();
      xhr.open("HEAD", path, false);
      xhr.send();

      console.log("Path ", path)

      if (xhr.status === 404) {
        // console.log("doesFileExist", "fail");
        return defaultPath;
      } else {
        // console.log("doesFileExist", "success");
        return path;
      }
    } else if (configType === "mainConfig") {
      let defaultPath = "assets/config/ke/en/PSConfig.json"

      let jsonFileName = "PSConfig";
      if (!language || !jurisdiction) return defaultPath;

      if (this.configService.configData.operatorId == "betyetu" ||
        this.configService.configData.operatorId == "paribet" ||
        this.configService.configData.operatorId == "starbet") {
        jsonFileName = `${this.configService.configData.operatorId}PSConfig`;
      }

      let path = `assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/${jsonFileName}.json`;
      // let path = `../../../assets/config/drc/${language.toLowerCase()}/psrules.json`;

      let xhr = new XMLHttpRequest();
      xhr.open("HEAD", path, false);
      xhr.send();

      if (xhr.status === 404) {
        console.log("doesFileExist+++++++++", "fail");
        return defaultPath;
      } else {
        console.log("doesFileExist--------------", "success");
        return path;
      }
    }
  }

  loadPSTips() {
    return new Promise((resolve, reject) => {
      let path = this.doesFileExist("rules");

      if (this.configService.configData.operatorId == "playbigapp") {
        path = "assets/config/in/en/psrulesPlaybigapp.json";
      }
      this.http.get(path)
        .subscribe((data: any) => {
          this.tipsData = this.configService.configData.psFreePlayNotAvailable
            ? data['psTips'].filter((el, index) => !data["freePlayPsTipsIndex"].includes(index))
            : data['psTips'];
          this.generateRandomTips();
          resolve(true);
        }, (err: any) => {
          this.http.get("assets/config/in/en/psrules.json")
            .subscribe((data: any) => {
              this.tipsData = this.configService.configData.psFreePlayNotAvailable
                ? data['psTips'].filter((el, index) => !data["freePlayPsTipsIndex"].includes(index))
                : data['psTips'];
              this.generateRandomTips();
              resolve(true);
            })
        });
    });
  }

  loadTextConfig() {
    return new Promise((resolve, reject) => {
      const path = this.doesFileExist("mainConfig");
      this.http.get(path)
        .subscribe((data: any) => {
          this.buttonConfig = data["buttonConfig"];
          this.textConfig = data["textConfig"];
          this.cashTournamentPlay = data["cashTournamentPlay"];
          this.resultsPageConfig = data["resultsPageConfig"];

          resolve(true);
        }, (err: any) => {
          this.http.get('assets/config/in/en/PSConfig.json')
            .subscribe((data: any) => {
              this.buttonConfig = data["buttonConfig"];
              this.textConfig = data["textConfig"];
              this.cashTournamentPlay = data["cashTournamentPlay"];
              this.resultsPageConfig = data["resultsPageConfig"];

              resolve(true);
            })
          console.log(err);
        });
    });
  }

  generateRandomTips() {
    let tipElement = document.getElementById("tip");
    if (tipElement) {
      let randomIndex;

      if (this.tipsData) {
        randomIndex = Math.floor(Math.random() * this.tipsData.length)
        tipElement?.innerText = this.tipsData[randomIndex];
      }

      this.tipGeneration = setInterval(() => {
        if (this.tipsData) {
          randomIndex = Math.floor(Math.random() * this.tipsData.length);
          tipElement?.innerText = this.tipsData[randomIndex];
        }
      }, 2500)
    }
  }

  showModal(message: any) {
    this.showGenericPopup = true;
    this.genericErrorMessage = message;
    ($('#generic-popup') as any).modal();
  }

  formatCurrency(amount: number, decimal = 2) {
    const result = amount;
    if (Intl) {
      var str = new Intl.NumberFormat(this.configService.configData.languageCode, {
        currencyDisplay: 'symbol',
        minimumFractionDigits: 0,
        maximumFractionDigits: decimal
      }).format(result);

    } else {
      str = this.configService.configData.currencyCode + result;
    }
    if (this.configService.configData.currencyCode == "XAF") {
      console.log("currency ", this.configService.configData.currencyCode);

      let str1 = str.replace(/FCFA/g, "XAF")
      console.log("currency value == ", str, str1)
      return str1;
    }
    return str;
  }

  getCurrencySymbol() {
    if (this.configService.configData.currencyCode) {
      if(this.configService.configData.excludedCurrencies.indexOf(this.configService.configData.currencyCode)>=0) {
        this.currencySymbol = this.configService.configData.currencyCode;
        return this.configService.configData.currencyCode;
      } else {
        const formatter = new Intl.NumberFormat('locale', {
          style: 'currency',
          currency: this.configService.configData.currencyCode,
        });
    
        let symbol;
        formatter.formatToParts(0).forEach(({ type, value }) => {
          if (type === 'currency') {
            symbol = value;
          }
        });
    
        if (symbol == "FCFA") {
          symbol = "XAF";
        } else if (symbol == "R$") {
          symbol = "BRL";
        }
        this.currencySymbol = symbol;
    
        return symbol;
  
      }
    }
  }

  onBackButtonClicked() {
    if (this.configService.configData.standalone) {
      if (this.configService.configData.platformId == "6") {
        window.parent.postMessage({
          action: "embackbutton"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      } else {
        window.history.go(-1);
      }
    }
  }

}
