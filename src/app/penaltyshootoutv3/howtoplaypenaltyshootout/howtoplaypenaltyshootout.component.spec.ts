import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HowToPlayPenaltyShootoutComponent } from './howtoplaypenaltyshootout.component';

describe('HowToPlayPenaltyShootoutComponent', () => {
  let component: HowToPlayPenaltyShootoutComponent;
  let fixture: ComponentFixture<HowToPlayPenaltyShootoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HowToPlayPenaltyShootoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HowToPlayPenaltyShootoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
