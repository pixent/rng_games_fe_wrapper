import { ViewportScroller, Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-howtoplaypenaltyshootout',
  templateUrl: './howtoplaypenaltyshootout.component.html',
  styleUrls: ['./howtoplaypenaltyshootout.component.css']
})
export class HowToPlayPenaltyShootoutComponent implements OnInit {
  rules: any;
  showHowToPlay: boolean = true;
  configData: any;
  languageURL: any;
  isShabikiPlatform: boolean = false;
  isStandAlone: boolean = false;
  backButtonURL: any = 'assets/images/backBtn.png';

  constructor(public viewportScroller: ViewportScroller, private _location: Location, private http: HttpClient, public configService: ConfigService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit(): void {
    // this.rules = this.configService.configData.howtoplaypsrules;
    // this.isShabikiPlatform = this.configService.configData.platformId == 2;
    this.languageURL = this.doesFileExist();
    this.loadPenaltyShootoutRules();

    this.isStandAlone = this.configService.configData.standalone;
    this.setPSConfig();
    // this.viewportScroller.setHistoryScrollRestoration('auto');
    // document.getElementById('iframeElement')!.style.display = "none";
  }

  doesFileExist(): string {
    let language = this.configService.configData.languageCode || "en";
    const jurisdiction = this.configService.configData.url.userRegion.toLowerCase();
    const defaultPath = "./assets/config/ke/en/psrules.json"
    console.log("test", language, jurisdiction);
    let jsonFileName = 'psrules';

    if (language && jurisdiction) {
      if (jurisdiction) {
        if (this.configService.configData.operatorId == "shabiki") {
          jsonFileName = 'psrulesShabiki';
        } else if (this.configService.configData.operatorId == "playabet") {
          jsonFileName = 'psrulesPlayabet';
        } else if (this.configService.configData.operatorId == "betyetu") {
          jsonFileName = 'psrulesBetyetu';
        } else if (this.configService.configData.operatorId == "paribet") {
          if(this.configService.configData.currencyCode == "UGX"){
            jsonFileName = 'psrulesParibetUGX';
          } else {
            jsonFileName = 'psrulesParibet';
          }
        } else if (this.configService.configData.operatorId == "starbet") {
          jsonFileName = 'psrulesStarbet';
        } else if (this.configService.configData.operatorId == "yangasport") {
          jsonFileName = 'psrulesYangaSport';
        } else if (this.configService.configData.operatorId == "playbigapp") {
          jsonFileName = 'psrulesPlaybigapp';
        } 
      } else
        return defaultPath;
    }

    if (!language)
      language = 'en';

    let path = `./assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/${jsonFileName}.json`;
    // let path = `../../../assets/config/tz/sh/psrules.json`;

    let xhr = new XMLHttpRequest();
    xhr.open("HEAD", path, false);
    xhr.send();

    if (xhr.status === 404) {
      // console.log("test-doesFileExist", "fail");
      return defaultPath;
    } else {
      // console.log("test-doesFileExist", "success");
      return path;
    }

  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
    (<any>$(".carousel")).carousel();

    this.setParentIframeHeight(window.outerHeight);
  }

  setPSConfig() {
    for (let p in this.configService.configData.psConfig) {
      let operator = this.configService.configData.psConfig[p];
      if (p == this.configService.configData.operatorId) {
        if (operator.fonts) {
          $("*").css("fontFamily",operator.fonts.main);
        }
        if (operator.icons) {
          if (operator.icons.psBackgroundImage) {
            setTimeout(()=>{
              $("#howtoplay").css("background-image","url(assets/images/"+operator.icons.psBackgroundImage + ")");
              $("#howtoplay .header-row").css("background-image","url(assets/images/"+operator.icons.psBackgroundImage + ")");
              $("#howtoplay").css("background-size","cover");
            },100);
          }
        }
      }
    }
    if(this.configService.configData.operatorId == "playbigapp") {
      setTimeout(()=> {
        this.backButtonURL = "assets/images/playbigapp/penaltyShootout/backBtn.png";
        $("#howtoplay .search-box button").css("background","#F6BA23");
        $("#howtoplay .carousel-indicators li").css("background-color","#454571");
        // $("#howtoplay .carousel-indicators li.active").css("background-color","#F6BA23");
        $("#howtoplay .header-row").css("color","#F6BA23");
        $("#howtoplay h2").css("color","#F6BA23");
      },100)
    }
    if (!this.configService.configData.psConfig[this.configService.configData.operatorId]) {
      $("*").css("fontFamily", "Lalezar-Regular");
    }
  }

  setParentIframeHeight(height: any) {
    // let appHeight = Math.max(height,window.outerHeight);

    if(this.configService.configData.operatorId == "cbtf"){
      height -= 50;
    }

    setTimeout(() => {
      document.getElementById("iframeElement")!.style.height = (window.parent as any).initialWindowHeight + "px";
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "frameresize",
          scrollHeight: height,
          type: "howtoplay"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 500);
    
    console.warn("setIframe height in cashplay screen ", height);
  }

  loadPenaltyShootoutRules() {
    return new Promise((resolve, reject) => {
      this.http.get(this.languageURL)
        .subscribe((data: any) => {
          let howToPlayResponse: any = [];
          if (this.configService.configData.psFreePlayNotAvailable) {
            for (let i = 0; i < data['howtoplaypsrules'].length; i++) {
              if (!data['howtoplaypsrules'][i].freePlayFlag) {
                howToPlayResponse.push(data['howtoplaypsrules'][i]);
              } else {
                if (data['howtoplaypsrules'][i].text2) {
                  data['howtoplaypsrules'][i].text = data['howtoplaypsrules'][i].text2;
                  howToPlayResponse.push(data['howtoplaypsrules'][i]);
                }
              }
            }
            this.configData = howToPlayResponse;
          } else {
            this.configData = data['howtoplaypsrules'];
          }

          this.configData.howToPlayTitle = data.howToPlay;

          console.warn("configdata htp", this.configData.howToPlayTitle);


          resolve(true);
        }, (err: any) => {
          this.http.get('./assets/config/in/en/psrules.json')
            .subscribe((data: any) => {
              let howToPlayResponse: any = [];
              if (this.configService.configData.psFreePlayNotAvailable) {
                for (let i = 0; i < data['howtoplaypsrules'].length; i++) {
                  if (!data['howtoplaypsrules'][i].freePlayFlag) {
                    howToPlayResponse.push(data['howtoplaypsrules'][i]);
                  } else {
                    if (data['howtoplaypsrules'][i].text2) {
                      data['howtoplaypsrules'][i].text = data['howtoplaypsrules'][i].text2;
                      howToPlayResponse.push(data['howtoplaypsrules'][i]);
                    }
                  }
                }
                this.configData = howToPlayResponse;
              } else {
                this.configData = data['howtoplaypsrules'];
              }

              this.configData.howToPlayTitle = data.howToPlay;

              console.warn("configdata htp", this.configData.howToPlayTitle);


              resolve(true);
            })
        });
    });
  }

  onOpen() {
    this.showHowToPlay = true;
  }

  onClose() {
    this.showHowToPlay = false;
    this._location.back();
    // this.router.navigate(['../playpenaltyshootout'], { relativeTo: this.route });
  }

}
