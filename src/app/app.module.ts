import { DEFAULT_CURRENCY_CODE, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

import { ConfigService, ConfigModule } from './services/config.service';
import { ScoreBoardService } from './services/scoreboard.service';
import { Pick10Component } from './pick10/pick10.component';
import { CatchTrendComponent } from './catchtrendv3/catchtrend.component';
import { HowToPlayPick10Component } from './pick10/howtoplaypick10/howtoplaypick10.component';
import { LeaderboardPick10Component } from './pick10/leaderboardpick10/leaderboardpick10.component';
import { Playpick10Component } from './pick10/playpick10/playpick10.component';
import { ResultPick10Component } from './pick10/resultpick10/resultpick10.component';
import { HowToPlayCatchTrendComponent } from './catchtrendv3/howtoplaycatchtrend/howtoplaycatchtrend.component';
import { LeaderboardCatchTrendComponent } from './catchtrendv3/leaderboardcatchtrend/leaderboardcatchtrend.component';
import { ScorecardCatchTrendComponent } from './catchtrendv3/scorecardcatchtrend/scorecardcatchtrend.component';
import { PredictOverCatchTrendComponent } from './catchtrendv3/predictovercatchtrend/predictovercatchtrend.component';
import { ResultCatchTrendComponent } from './catchtrendv3/resultcatchtrend/resultcatchtrend.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LobbyComponent } from './lobby/lobby.component';
import { SocketService } from './services/socket.service';
import { FormsModule } from '@angular/forms';
import { PenaltyShootoutComponent } from './penaltyshootoutv3/penaltyshootout.component';
import { PlayPenaltyShootoutComponent } from './penaltyshootoutv3/playpenaltyshootout/playpenaltyshootout.component';
import { HowToPlayPenaltyShootoutComponent } from './penaltyshootoutv3/howtoplaypenaltyshootout/howtoplaypenaltyshootout.component';
// import { LeaderboardPenaltyShootoutComponent } from './penaltyshootoutv3/leaderboardpenaltyshootout/leaderboardpenaltyshootout.component';
import { NgbDateCustomParserFormatter, ResultPenaltyShootoutComponent } from './penaltyshootoutv3/resultpenaltyshootout/resultpenaltyshootout.component';
import { FaqPenaltyShootoutComponent } from './penaltyshootoutv3/faqpenaltyshootout/faqpenaltyshootout.component';
import { CatchTrendHistoryComponent } from './catchtrendv3/catchthetrendhistory/catchthetrendhistory.component';
import { OddstableComponent } from './catchtrendv3/oddstable/oddstable.component';
import { FirstwicketpartnershipComponent } from './firstwicketpartnership/firstwicketpartnership.component';
import { FwpbottomComponent } from './firstwicketpartnership/fwpbottom/fwpbottom.component';
import { FwpbetpanelComponent } from './firstwicketpartnership/fwpbetpanel/fwpbetpanel.component';
import { FwpchatsComponent } from './firstwicketpartnership/fwpbottom/fwpchats/fwpchats.component';
import { FwphistoryComponent } from './firstwicketpartnership/fwpbottom/fwphistory/fwphistory.component';
import { LoginComponent } from './login/login.component';
import { FwpService } from './services/fwp.service';
import { CashplayscreenpenaltyshootoutComponent } from './penaltyshootoutv3/cashplayscreenpenaltyshootout/cashplayscreenpenaltyshootout.component';
import { ModalpopupComponent } from './penaltyshootoutv3/modalpopup/modalpopup.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { PenaltyShootoutService } from './services/penalty-shootout.service';
import { LeaderboardpenaltyshootoutComponent } from './penaltyshootoutv3/cashplayscreenpenaltyshootout/leaderboardpenaltyshootout/leaderboardpenaltyshootout.component';
import { DefenderComponent } from './defender/defender.component';
import { DefenderChatsComponent } from './defender/defenderbottom/defenderchats/defenderchats.component';
import { DefenderBottomComponent } from './defender/defenderbottom/defenderbottom.component';
import { DefenderBetPanelComponent } from './defender/defenderbetpanel/defenderbetpanel.component';
import { DefenderHistoryComponent } from './defender/defenderbottom/defenderhistory/defenderhistory.component';
import { DefenderhowtoplayscreensComponent } from './defender/defenderhowtoplayscreens/defenderhowtoplayscreens.component';
import { HomeRunComponent } from './homerun/homerun.component';
import { HomeRunChatsComponent } from './homerun/homerunbottom/homerunchats/homerunchats.component';
import { HomeRunBottomComponent } from './homerun/homerunbottom/homerunbottom.component';
import { HomeRunBetPanelComponent } from './homerun/homerunbetpanel/homerunbetpanel.component';
import { HomeRunHistoryComponent } from './homerun/homerunbottom/homerunhistory/homerunhistory.component';
import { HomeRunhowtoplayscreensComponent } from './homerun/homerunhowtoplayscreens/homerunhowtoplayscreens.component';
import { ErrorPageComponent } from './errorpage/errorpage.component';
import { IframeComponent } from './iframe/iframe.component';
import { FwphowtoplayscreensComponent } from './firstwicketpartnership/fwphowtoplayscreens/fwphowtoplayscreens.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    Pick10Component,
    CatchTrendComponent,
    HowToPlayPick10Component,
    LeaderboardPick10Component,
    Playpick10Component,
    ResultPick10Component,
    HowToPlayCatchTrendComponent,
    LeaderboardCatchTrendComponent,
    ScorecardCatchTrendComponent,
    PredictOverCatchTrendComponent,
    ResultCatchTrendComponent,
    LobbyComponent,
    PenaltyShootoutComponent,
    PlayPenaltyShootoutComponent,
    HowToPlayPenaltyShootoutComponent,
    // LeaderboardPenaltyShootoutComponent,
    ResultPenaltyShootoutComponent,
    FaqPenaltyShootoutComponent,
    CatchTrendHistoryComponent,
    OddstableComponent,
    FirstwicketpartnershipComponent,
    FwpchatsComponent,
    FwpbottomComponent,
    FwpbetpanelComponent,
    FwphistoryComponent,
    CashplayscreenpenaltyshootoutComponent,
    LoginComponent,
    ModalpopupComponent,    
    LeaderboardpenaltyshootoutComponent,
    DefenderComponent,
    DefenderChatsComponent,
    DefenderBottomComponent,
    DefenderBetPanelComponent,
    DefenderHistoryComponent,
    ErrorPageComponent,
    IframeComponent,
    DefenderhowtoplayscreensComponent,
    FwphowtoplayscreensComponent,
    HomeRunComponent,
    HomeRunChatsComponent,
    HomeRunBottomComponent,
    HomeRunBetPanelComponent,
    HomeRunHistoryComponent,
    HomeRunhowtoplayscreensComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatNativeDateModule,
    NgbModule,
    MatDialogModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatExpansionModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    ConfigService,
    ScoreBoardService,
    SocketService,
    PenaltyShootoutService,
    FwpService,
    { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter },
    {provide: DEFAULT_CURRENCY_CODE, useValue: '' },
    ConfigModule.init()
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalpopupComponent
  ]
})
export class AppModule { }
