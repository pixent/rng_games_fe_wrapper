import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatchTrendComponent } from './catchtrend.component';

describe('CatchTrendComponent', () => {
  let component: CatchTrendComponent;
  let fixture: ComponentFixture<CatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
