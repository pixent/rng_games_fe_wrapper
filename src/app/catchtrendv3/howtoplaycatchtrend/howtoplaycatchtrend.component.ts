import { ViewportScroller } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CatchTrendComponent } from "./../catchtrend.component";

@Component({
  selector: "app-howtoplaycatchtrend",
  templateUrl: "./howtoplaycatchtrend.component.html",
  styleUrls: ["./howtoplaycatchtrend.component.css"],
})
export class HowToPlayCatchTrendComponent implements OnInit {

  rules: any;
  heading: any;
  constructor(
    public viewportScroller: ViewportScroller,
    private catchTrendComponent: CatchTrendComponent,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    if (window.history.state.item == "Lobby") {
      this.viewportScroller.setHistoryScrollRestoration("auto");
    }
    document.getElementById("iframeElement")!.style.display = "none";
    this.loadVCTTRules();
  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
  }

  loadVCTTRules(){
    return new Promise((resolve, reject) => {
      this.http.get('assets/config/en/vcttRules.json')
          .subscribe((data: any) => {
            
            this.rules = data['howtoplayvcttrules']; 
              
              resolve(true);
          }, (err: any) => {
              console.log(err);
          });
  });
  }

  onBackButtonClicked() {
    document.getElementById("iframeElement")!.style.display = "block";
    this.catchTrendComponent.onHowToPlayCloseClicked();
    if (history.state.redirect === "main") {
      window.history.back();
    } else {
      this.router.navigate(["../"], {
        relativeTo: this.route,
        state: { redirect: "back" },
      });
    }
  }

  onClose() {
    this.onBackButtonClicked();
    // this.showHowToPlay = false;
    // this.router.navigate(['../playpenaltyshootout'], { relativeTo: this.route });
  }
}
