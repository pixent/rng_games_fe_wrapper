import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HowToPlayCatchTrendComponent } from './howtoplaycatchtrend.component';

describe('HowToPlayCatchTrendComponent', () => {
  let component: HowToPlayCatchTrendComponent;
  let fixture: ComponentFixture<HowToPlayCatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HowToPlayCatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HowToPlayCatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
