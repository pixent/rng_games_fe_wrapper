import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatchTrendHistoryComponent } from './catchthetrendhistory.component';

describe('CatchTrendHistoryComponent', () => {
  let component: CatchTrendHistoryComponent;
  let fixture: ComponentFixture<CatchTrendHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatchTrendHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatchTrendHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
