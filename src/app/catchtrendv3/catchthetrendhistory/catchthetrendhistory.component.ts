import { HttpClient } from "@angular/common/http";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ConfigService } from "src/app/services/config.service";
import { CatchTrendComponent } from "./../catchtrend.component";
import { NgbDate, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "app-history",
  templateUrl: "./catchthetrendhistory.component.html",
  styleUrls: ["./catchthetrendhistory.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class CatchTrendHistoryComponent implements OnInit {

  matchDetails: any = [];
  startDate: any;
  endDate: any;
  gameMode: any = "Cashplay";
  historyData: any = [];
  historyDataRes: any = [];
  dateFromData: any;
  showCalendar: boolean = false;
  fromDate: NgbDate;
  toDate: NgbDate | null = null;
  hoveredDate: NgbDate | null = null;
  freePlayHistoryData: any = [];
  cashPlayHistoryData: any = [];
  gameCode: any;

  constructor(
    private catchTrendComponent: CatchTrendComponent,
    public configService: ConfigService,
    private http: HttpClient,
    calendar: NgbCalendar,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), "d", 10);
  }

  ngOnInit(): void {
    if (
      window.history.state.item == "Lobby" ||
      window.history.state.redirect == "main"
    ) {
      document.getElementById("iframeElement")!.style.display = "none";
    }
    this.matchDetails = this.catchTrendComponent.matchDetails;
    this.getResultData();

    this.sendHeightToIframe();
  }

  // onGameMode(game:any){
  //   this.gameMode = game;
  //   this.getResultData();
  // }

  getResultData() {
    this.gameCode = this.catchTrendComponent.gameCode;
    let date = new Date();
    let currentMonth =
      date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59:59";
    if (this.startDate) {
      startDate = this.startDate.year + "-" + (this.startDate.month < 10 ? "0" + this.startDate.month : this.startDate.month) + "-" + (this.startDate.day < 10 ? "0" + this.startDate.day : this.startDate.day) + " 00:00:00";
      endDate = this.endDate && this.endDate.year + "-" + (this.endDate.month < 10 ? "0" + this.endDate.month : this.endDate.month) + "-" + (this.endDate.day < 10 ? "0" + this.endDate.day : this.endDate.day) + " 23:59:59";
    }

    if (this.gameMode == "Freeplay") {
      if (this.gameCode.indexof('virtual') >= 0) {
        this.gameCode = "freeVirtualCatchTheTrend";
      } else {
        this.gameCode = "freeVirtualCatchTheTrend";
      }
    }
    let leaderBoardUrlForVCTT = this.configService.configData.url.socket + "bet-histories?playerId=" + this.matchDetails.playerId + "&gameName=" + this.gameCode + "&startDate=" + startDate + "&endDate=" + endDate;
    let leaderBoardUrlForCTT = this.configService.configData.url.socket + "bet-histories?playerId=" + this.matchDetails.playerId + "&gameName=catchTheTrend" + "&startDate=" + startDate + "&endDate=" + endDate;

    this.http.get(leaderBoardUrlForVCTT).subscribe(
      (data: any) => {
        let result: any = [];
        console.warn("vctt bets history ", data)
        this.historyData = data;
        this.historyDataRes = data;


        this.http.get(leaderBoardUrlForCTT).subscribe(
          (data: any) => {
            let result: any = [];
            this.historyData = [...this.historyData, ...data];
            this.historyDataRes = [...this.historyDataRes, ...data];

            for (let p in this.historyDataRes) {
              this.historyDataRes[p].resultData = JSON.parse(this.historyDataRes[p].result);
              let overTypes = this.historyDataRes[p].betSession && this.historyDataRes[p].betSession.split("_");
              this.historyDataRes[p].gameType = overTypes && overTypes[0];

              if (this.historyDataRes[p]) {
                let dateTime = this.historyDataRes[p].createdOn.split("T");
                let date = dateTime[0].split("-").reverse().join("/");
                this.historyDataRes[p].date = date;
                this.dateFromData = this.historyDataRes[p].date;
                this.historyDataRes[p].time = dateTime[1].substring(0, 5);
                let weekNumber = this.getWeekNumber(this.dateFromData);
                this.historyDataRes[p].weekNumber = weekNumber;
              }
            }

            for (let p in this.historyDataRes) {
              if (this.historyDataRes[p].betAmount == 0) {
                this.freePlayHistoryData.push(this.historyDataRes[p]);
              } else {
                this.cashPlayHistoryData.push(this.historyDataRes[p]);
              }
            }

          },
          (err: any) => {
            console.log(err);
          }
        );
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getWeekNumber(date: any) {
    let newDateArr = date.split("/");
    let getDayNumber = Number(newDateArr[0]);
    let getMonthNumber = Number(newDateArr[1] - 1);
    let getYear = Number(newDateArr[2]);

    let currentdate: any = new Date(getYear, getMonthNumber, getDayNumber);
    let oneJan: any = new Date(currentdate.getFullYear(), 0, 1);
    let numberOfDays = Math.floor(
      (currentdate - oneJan) / (24 * 60 * 60 * 1000)
    );
    let result = Math.ceil((currentdate.getDay() + 1 + numberOfDays) / 7);

    return result;
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
    this.startDate = this.fromDate;
    this.endDate = this.toDate;

    // this.getResultData();
  }

  onDateRangeSelected() {
    this.getResultData();
    this.showCalendar = !this.showCalendar;
  }

  isHovered(date: NgbDate) {
    return (
      this.fromDate &&
      !this.toDate &&
      this.hoveredDate &&
      date.after(this.fromDate) &&
      date.before(this.hoveredDate)
    );
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      (this.toDate && date.equals(this.toDate)) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  onHistoryBackClicked() {
    if (history.state.redirect === "main") {
      window.history.back();
    } else {
      this.router.navigate(["../"], {
        relativeTo: this.route,
        state: { redirect: "back" },
      });
    }
  }

  onDateIconClicked() {
    this.showCalendar = !this.showCalendar;
    let ngbMonth = document.getElementsByTagName("option");
    for (let opt = 0; opt < 12; opt++) {
      // if (ngbMonth && ngbMonth[opt].ariaLabel) {
      //   ngbMonth[opt].innerText = ngbMonth[opt].ariaLabel;
      // }
    }
  }

  onCashPlayButtonClicked() {
    this.sendHeightToIframe();
  }
  onFreePlayButtonClicked() {
    this.sendHeightToIframe();
  }

  sendHeightToIframe() {
    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: window.outerHeight
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 100);
  }
}
