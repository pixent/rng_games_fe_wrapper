import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OddstableComponent } from './oddstable.component';

describe('OddstableComponent', () => {
  let component: OddstableComponent;
  let fixture: ComponentFixture<OddstableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OddstableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OddstableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
