import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-oddstable',
  templateUrl: './oddstable.component.html',
  styleUrls: ['./oddstable.component.css']
})
export class OddstableComponent implements OnInit {

  constructor(
    public viewportScroller: ViewportScroller) { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
  }

  
}
