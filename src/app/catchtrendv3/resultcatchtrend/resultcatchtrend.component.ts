import { Component, OnInit } from "@angular/core";
import { CatchTrendComponent } from "./../catchtrend.component";
import { Subscription } from "rxjs";
import { runInThisContext } from "vm";
import { ViewportScroller } from "@angular/common";
import { LeaderboardCatchTrendComponent } from "../leaderboardcatchtrend/leaderboardcatchtrend.component";

@Component({
  selector: "app-resultcatchtrend",
  templateUrl: "./resultcatchtrend.component.html",
  styleUrls: ["./resultcatchtrend.component.css"],
  providers: [LeaderboardCatchTrendComponent]
})
export class ResultCatchTrendComponent implements OnInit {
  betSessionResponse: any;
  currentInnings: any;
  scoreCardDetails: any;
  overData: any;
  firstInningData: any;
  secondInningData: any;
  completeScoreDetails: any;
  inningsSelected: boolean = true;
  bothInningsSelected: boolean = false;
  playerBets: any;
  catchSubscription: Subscription;
  playerBetFlag: boolean = true;
  predictedOvers: any;
  predictedValues: any;
  predictedSelections: any;
  betSessionStarted: boolean = false;
  currentOver: any;
  currentPlayerDetails: any;
  isReal: boolean = true;
  currencyCode: any = "INR";
  predictedData: any;
  isLoading: boolean = true;

  constructor(public catchTrendComponent: CatchTrendComponent, public viewportScroller: ViewportScroller, public leaderboardCatchTrendComponent: LeaderboardCatchTrendComponent) {
    this.catchSubscription = this.catchTrendComponent.catchTrendSubject.subscribe((response: any) => {
      console.log("Response in results CT", response);
      if (response.message == "betsessionresponse") {
        this.betSessionResponse = response.data;
        this.updateNotification(this.betSessionResponse);
      } else if (response.message == "gameinit") {
        // game init data
        this.playerBets = response.data;
        this.setPlayerBets(this.playerBets);
      } else {
        this.currentInnings = response.data.innings;
        this.updateScoreCard(response.data, true);
      }
    });
    this.leaderboardCatchTrendComponent.getWeekNumber();
    this.leaderboardCatchTrendComponent.selectedWeek();
    this.leaderboardCatchTrendComponent.getLeaderBoardData();
  }

  ngOnInit(): void {
    this.overData = [
      {
        overNumber: 1,
        overSup: this.getSup(1),
        fours: 0,
        sixes: 0,
        dotBalls: 0,
        wickets: 0,
        tip: 1,
        overArray: [".", ".", ".", ".", ".", "."],
      },
    ];
    this.onResultInit();
    this.isReal = this.catchTrendComponent.matchDetails.isReal;
    setTimeout(() =>
      this.getCurrentPlayerDetails()
      , 1000)
  }

  ngAfterViewInit() {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
  }

  ngOnDestroy() {
    this.catchSubscription.unsubscribe();
  }

  onResultInit() {
    if (this.catchTrendComponent.scoreCardResponse) {
      this.currentInnings = this.catchTrendComponent.scoreCardResponse.runningScore.inning;
      this.scoreCardDetails = this.catchTrendComponent.scoreCardDetails;
      let scoreDetails = {
        scoreDetails: this.scoreCardDetails,
        completeScore: this.catchTrendComponent.scoreCardResponse,
        innings: this.currentInnings,
      };
      this.updateScoreCard(scoreDetails);
      this.catchTrendComponent.getGameInit();
    }
  }

  updateNotification(betSessionResponse: any) {
    this.betSessionStarted = true;
    if (betSessionResponse["inning" + this.currentInnings]) {
      let ballsBowled = Math.round((betSessionResponse["inning" + this.currentInnings].overs - Math.floor(betSessionResponse["inning" + this.currentInnings].overs)) * 10);
      if (ballsBowled == 6) {
        this.betSessionStarted = false;
        this.catchTrendComponent.getGameInit();
      }
    }
  }

  setPlayerBets(playerBets: any) {
    this.playerBetFlag = false;
    let betData = playerBets.gameHistory;
    this.predictedOvers = [[], []];
    this.predictedValues = [[], []];
    this.predictedSelections = [[], []];
    this.predictedData = [[], []];
    for (let i = 0; i < betData.length; i++) {
      if (betData[i]) {
        // debugger;
        let result = JSON.parse(betData[i].result);
        // if (result) {
        // let inningData = result['inning' + betData[i].inningNumber];
        let overElement = document.getElementById(betData[i].inningNumber + "-" + (betData[i].overNumber + 1) + "-predictions");
        if (overElement) {
          overElement.innerHTML = "Predictions";
        }
        this.predictedOvers[betData[i].inningNumber - 1].push(betData[i].overNumber + 1);
        if (result) {
          this.predictedValues[betData[i].inningNumber - 1].push({ winningScores: result["inning" + betData[i].inningNumber].winningScores, nonWinningScores: result["inning" + betData[i].inningNumber].nonWinningScores, });
        } else {
          this.predictedValues[betData[i].inningNumber - 1].push({ winningScores: [], nonWinningScores: [], });
        }
        this.predictedSelections[betData[i].inningNumber - 1].push(betData[i].selections);
        this.predictedData[betData[i].inningNumber - 1].push(betData[i]);
      }
    }
  }

  selectInnings(inning: number) {
    $(".innings-2-value").removeClass("innings-2-active");
    if (!this.completeScoreDetails) {
      if (inning == 1) {
        document.getElementById("firstInnings")?.classList.add("innings-2-active");
      } else {
        document.getElementById("bothInnings")?.classList.add("innings-2-active");
      }
      return;
    }
    if (inning == 1 && this.completeScoreDetails.runningScore.inning == 1) {
      this.inningsSelected = true;
      this.bothInningsSelected = false;
      this.overData = this.getOverData(this.completeScoreDetails.runningScore.firstInning, 1);
      this.currentOver = this.scoreCardDetails && Math.ceil(this.scoreCardDetails.overStat.overCount);
      document.getElementById("firstInnings")?.classList.add("innings-2-active");
    } else if (inning == 1 && this.completeScoreDetails.runningScore.inning == 2) {
      this.inningsSelected = false;
      this.bothInningsSelected = false;
      this.overData = this.getOverData(this.completeScoreDetails.runningScore.firstInning, 1);
      this.currentOver = 0;
      document.getElementById("firstInnings")?.classList.add("innings-2-active");
    } else if (inning == 2 && this.completeScoreDetails.runningScore.inning == 2
    ) {
      this.inningsSelected = true;
      this.bothInningsSelected = false;
      this.overData = this.getOverData(
        this.completeScoreDetails.runningScore.secondInning,
        2
      );
      this.currentOver = Math.ceil(this.scoreCardDetails.overStat.overCount);
      document.getElementById("secondInnings")?.classList.add("innings-2-active");
    } else {
      this.inningsSelected = true;
      this.bothInningsSelected = true;
      let firstInningScore = this.getOverData(this.completeScoreDetails.runningScore.firstInning, 1);

      this.currentOver = 0;
      let secondInningScore = this.getOverData(this.completeScoreDetails.runningScore.secondInning, 2);
      if (this.currentInnings === 1) {
        this.overData = firstInningScore;
      }
      else {
        this.overData = secondInningScore.concat(firstInningScore);
      }

      document.getElementById("bothInnings")?.classList.add("innings-2-active");
    }
  }

  updateScoreCard(data: any, fromResponse?: boolean) {
    this.scoreCardDetails = data.scoreDetails;
    this.completeScoreDetails = data.completeScore;
    // console.log("score card details",this.scoreCardDetails);
    if (this.inningsSelected) {
      if (this.bothInningsSelected) {
        let firstInningScore = this.getOverData(this.completeScoreDetails.runningScore.firstInning, 1);

        if (this.currentInnings === 1) {
          this.overData = firstInningScore;
        }
        else {
          let secondInningScore = this.getOverData(this.completeScoreDetails.runningScore.secondInning, 2);
          this.overData = secondInningScore.concat(firstInningScore);
        }

      } else {
        this.overData = this.getOverData(data.scoreDetails, this.currentInnings);
      }
    }
    if (this.playerBetFlag) {
      this.catchTrendComponent.getGameInit();
    }
    if (fromResponse) {
      this.isLoading = false;
    }
  }

  getOverData(scoreCardDetails: any, inningNumber: any) {
    let overData = [];
    let overs = [];
    this.currentOver = this.scoreCardDetails && Math.ceil(this.scoreCardDetails.overStat.overCount);
    const deliveries = scoreCardDetails && scoreCardDetails.overStat.deliveries;
    for (let d = 0; d < 120; d++) {
      if (deliveries && !deliveries[d]) {
        deliveries.push(".");
      }
    }
    if (deliveries && deliveries.length) {
      while (deliveries.length > 0) {
        overs.push(deliveries.splice(0, 6));
      }
      for (let i = 0; i < overs.length; i++) {
        let fours: any = 0;
        let sixes: any = 0;
        let wickets: any = 0;
        let dotBalls: any = 0;
        let overArray = overs[i];
        // console.log("oversArray",overArray);
        for (let j = 0; j < overArray.length; j++) {
          if (overArray[j] == "4") {
            ++fours;
          } else if (overArray[j] == "6") {
            ++sixes;
          } else if (overArray[j] == "0") {
            ++dotBalls;
          } else if (overArray[j] == "W") {
            ++wickets;
          }
        }

        if (overArray.indexOf(".") == 0) {
          fours = "-";
          sixes = "-";
          wickets = "-";
          dotBalls = "-";
        }
        let obj: any = {
          overNumber: i + 1,
          overSup: this.getSup(i + 1),
          fours: fours,
          sixes: sixes,
          dotBalls: dotBalls,
          wickets: wickets,
          overArray: overArray,
          inning: inningNumber,
        };

        // if (inningNumber == 1 && i == 6) {
        //   debugger;
        // }
        if (this.predictedOvers && this.predictedOvers[this.currentInnings - 1] && this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber) >= 0 && inningNumber == 2) {
          obj.predictions = 1;
          let winningScores = this.predictedValues[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)].winningScores;
          let nonWinningScores = this.predictedValues[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)].nonWinningScores;
          if (winningScores.length || nonWinningScores.length) {
            if (winningScores.indexOf("4") >= 0) {
              obj.fourPrediction = 1;
            } else {
              obj.fourPrediction = 0;
            }
            if (winningScores.indexOf("6") >= 0) {
              obj.sixPrediction = 1;
            } else {
              obj.sixPrediction = 0;
            }
            if (winningScores.indexOf("W") >= 0) {
              obj.wicketPrediction = 1;
            } else {
              obj.wicketPrediction = 0;
            }
            if (winningScores.indexOf("0") >= 0) {
              obj.dotBallPrediction = 1;
            } else {
              obj.dotBallPrediction = 0;
            }
            if (
              obj.fourPrediction == 1 &&
              obj.sixPrediction == 1 &&
              obj.wicketPrediction == 1 &&
              obj.dotBallPrediction == 1
            ) {
              obj.resultPrediction = 1;
            } else {
              obj.resultPrediction = 0;
            }
          } else {
            obj.predictions = 2;
          }
          obj.fours = this.predictedSelections[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)][0];
          obj.sixes = this.predictedSelections[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)][1];
          obj.wickets = this.predictedSelections[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)][2];
          obj.dotBalls = this.predictedSelections[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)][3];
          obj.winAmount = this.predictedData[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)].winAmount;
          obj.points = this.predictedData[this.currentInnings - 1][this.predictedOvers[this.currentInnings - 1].indexOf(obj.overNumber)].points;
        } else {
          if (this.predictedOvers && this.predictedOvers[0].length && this.predictedValues[0][this.predictedOvers[0].indexOf(obj.overNumber)] && inningNumber == 1) {
            obj.predictions = 1;
            let winningScores = this.predictedValues[0][this.predictedOvers[0].indexOf(obj.overNumber)].winningScores;
            let nonWinningScores = this.predictedValues[0][this.predictedOvers[0].indexOf(obj.overNumber)].nonWinningScores;
            if (winningScores.length || nonWinningScores.length) {
              if (winningScores.indexOf("4") >= 0) {
                obj.fourPrediction = 1;
              } else {
                obj.fourPrediction = 0;
              }
              if (winningScores.indexOf("6") >= 0) {
                obj.sixPrediction = 1;
              } else {
                obj.sixPrediction = 0;
              }
              if (winningScores.indexOf("W") >= 0) {
                obj.wicketPrediction = 1;
              } else {
                obj.wicketPrediction = 0;
              }
              if (winningScores.indexOf("0") >= 0) {
                obj.dotBallPrediction = 1;
              } else {
                obj.dotBallPrediction = 0;
              }
              if (obj.fourPrediction == 1 && obj.sixPrediction == 1 && obj.wicketPrediction == 1 && obj.dotBallPrediction == 1) {
                obj.resultPrediction = 1;
              } else {
                obj.resultPrediction = 0;
              }

            } else {
              obj.predictions = 2;
            }
            obj.fours = this.predictedSelections[0][this.predictedOvers[0].indexOf(obj.overNumber)][0];
            obj.sixes = this.predictedSelections[0][this.predictedOvers[0].indexOf(obj.overNumber)][1];
            obj.wickets = this.predictedSelections[0][this.predictedOvers[0].indexOf(obj.overNumber)][2];
            obj.dotBalls = this.predictedSelections[0][this.predictedOvers[0].indexOf(obj.overNumber)][3];
            obj.winAmount = this.predictedData[0][this.predictedOvers[0].indexOf(obj.overNumber)].winAmount;
            obj.points = this.predictedData[0][this.predictedOvers[0].indexOf(obj.overNumber)].points;

          }
        }
        //current over bets
        if (this.betSessionStarted && obj.overNumber == Math.ceil(this.betSessionResponse["inning" + this.currentInnings].overs)) {
          let winningScores = this.betSessionResponse["inning" + this.currentInnings].winningScores;
          obj.predictions = 1;
          for (let i = 0; i < winningScores.length; i++) {
            if (winningScores[i] == "4") {
              obj.fourPrediction = 1;
            } else if (winningScores[i] == "6") {
              obj.sixPrediction = 1;
            } else if (winningScores[i] == "W") {
              obj.wicketPrediction = 1;
            } else {
              obj.dotBallPrediction = 1;
            }
          }
        }
        overData.push(obj);
      }
      if (this.inningsSelected && overData.length <= 19) {
        overData.push({
          overNumber: overData.length + 1,
          overSup: this.getSup(overData.length + 1),
          fours: 0,
          sixes: 0,
          dotBalls: 0,
          wickets: 0,
          tip: 1,
          inning: inningNumber,
          overArray: [".", ".", ".", ".", ".", "."],
        });
      }
      for (let over = 0; over < overData.length; over++) {
        if (this.playerBets && this.playerBets.gameHistory.length > 0) {
          let gameHistory = this.playerBets.gameHistory;
          for (let p = 0; p < gameHistory.length; p++) {
            if (overData[over].overNumber == gameHistory[p].overNumber + 1) {
              overData[over].betAmount = gameHistory[p].betAmount;
            }
          }
        }
      }
      overData = overData.reverse();
      return overData;
    } else {
      return this.overData;
    }
  }

  getCurrentPlayerDetails() {
    if (this.leaderboardCatchTrendComponent.leaderBoardData && this.leaderboardCatchTrendComponent.leaderBoardData[0]) {
      let { playerName, points, rank } = this.leaderboardCatchTrendComponent.leaderBoardData[0];
      this.currentPlayerDetails = {
        playerName: playerName,
        position: rank,
        points: points
      }
    }


  }

  getSup(overNumber: number) {
    if (overNumber == 1) {
      return "st";
    } else if (overNumber == 2) {
      return "nd";
    } else if (overNumber == 3) {
      return "rd";
    } else {
      return "th";
    }
  }
}
