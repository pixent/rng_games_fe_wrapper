import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultCatchTrendComponent } from './resultcatchtrend.component';

describe('ResultCatchTrendComponent', () => {
  let component: ResultCatchTrendComponent;
  let fixture: ComponentFixture<ResultCatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultCatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultCatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
