import { Component, OnInit, HostListener, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfigService } from "../services/config.service";
import { DomSanitizer } from "@angular/platform-browser";
import * as moment from "moment-timezone";
import { SocketService } from "../services/socket.service";
import { Subject } from "rxjs";
import { ViewportScroller } from "@angular/common";

@Component({
  selector: "app-catchtrend",
  templateUrl: "./catchtrend.component.html",
  styleUrls: ["./catchtrend.component.css"],
})
export class CatchTrendComponent implements OnInit, OnDestroy {
  matchDetails: any;
  url: any;
  iframeUrl: any;
  loadedIframe: any;
  gameInit: boolean = true;
  iframeInitialized: boolean = false;
  scoreCardDetails: any;
  showScoreCard: boolean = false;
  showScores: boolean = false;
  lastOverScores: any;
  scoreCardResponse: any;
  scoreCardResponseUpdate: any;
  betResponse: any;
  overBet: any;
  oversBet: any;
  betValues: any;
  playButtonState: boolean = true;
  betData: any;
  betDataSelections: any = [];
  selectedBet: any = 5;
  selectedOver: any = 1;
  gameInitData: any;
  betSessionResponse: any;
  componentInit: boolean = true;
  successBet: boolean = true;
  errorBet: boolean = false;
  errorBetMessage: any = "";
  scrollYPosition: any;
  modalBetTitle: any;
  inputSelected: boolean = false;
  betContents: any = [];
  isReal: any;
  isUpdate: boolean = false;
  betRequest: any;
  bettingOvers: any = [];
  betPlacedSuccessfully: boolean = false;
  betSession: any;
  overNumber: any;
  betAlreadyPlaced: boolean = false;
  newBet: boolean = true;
  maxBet: Number = 1000;
  inputDisabled: boolean = false;
  bar: any = "5";
  correct: any = "5";
  potentialWin: any = "";
  isForcedRequest: boolean = false;
  gameType: any;
  oversWon: any;
  totalOversPlayed: any;
  totalWin: any;
  item: Number = 1;
  gameCode: any;
  runningBetOvers: any;
  isChecked: boolean = false;
  inningsNumber: any;
  game: any;
  overNumbers: any = [];
  selections: any;
  stakeAmount: any;
  potWin: any;
  betSlipWinAmount: any;
  winningScores: any;
  nonWinningScores: any;
  winObj: any = {};
  nonWinObj: any = {};
  isCashGame: any;
  correctSelections: any;
  updatedSelections: any;
  updatedOvers: any = [];
  overSelected: any = 0;
  currentOver: any;
  oversCount: any = 0;
  userDetails: any = {};
  isCatchTheTrendRoute: boolean = false;
  isNotLeaderBoard: boolean = true;

  catchTrendSubject = new Subject();
  messageSubscription: any;
  isViewBetSlipClicked: boolean = false;
  currencyCode: any;
  resultpenaltyshootout: any;
  preloaderStarted: boolean = false;
  genericErrorMessage: string = '';
  showGenericPopup: boolean = false;
  disableCanvas: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public sanitizer: DomSanitizer,
    public configService: ConfigService,
    public socketService: SocketService,
    public viewportScroller: ViewportScroller
  ) {
    this.messageSubscription = this.socketService.messageSubject.subscribe((message) => {
      this.serverResponse(message);
    });
    this.betResponse = {
      overNumber: "",
    };
    this.oversBet = [1, 3, 5];
    this.overBet = 1;
    this.betValues = [5, 10, 25];
    this.betContents = ["All Selections Correct 5.00"];
    this.betData = {};
  }

  ngOnInit(): void {
    this.disableCanvas = this.configService.configData.disableCanvas;
    this.checkRoute();
    // let playerId = "";

    if (this.router.url == '/home/catchthetrend') {
      this.isCatchTheTrendRoute = true
    }

    this.matchDetails = window.history.state.item;
    this.currencyCode = this.configService.configData.currencyCode;
    if (this.matchDetails) {
      localStorage.setItem("matchDetails", JSON.stringify(this.matchDetails));
    }
    if (localStorage.getItem("matchDetails")) {
      this.matchDetails = JSON.parse(localStorage.getItem("matchDetails")!);
    }
    this.url = this.configService.configData.url.canvas + "cricmatch/index.html";
    // if (this.matchDetails.socketFlag) {
    //   this.url = this.configService.configData.url.canvas + "cricmatch/index.html";
    // } else {
    //   this.url = this.configService.configData.url.canvas + "catchthetrend/index.html";
    // }
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    this.isReal = this.matchDetails.isReal;
    if (window.location.href.indexOf("?") >= 0) {
      let urlStr = window.location.href.split("?")[1].split("&");
      let urlParams: any = {};
      for (let i = 0; i < urlStr.length; i++) {
        let params = urlStr[i].split("=");
        if (params[0] == "playerId") {
          urlParams.playerId = params[1];
        }
      }
      this.matchDetails.playerId = urlParams.playerId;
    }
    if (this.matchDetails.gameName == null || this.matchDetails.gameName == "Virtual Catch the Trend") {
      this.gameCode = "virtualCatchTheTrend";
    } else {
      this.gameCode = "catchTheTrend";
    }
    this.gameType = this.matchDetails.gameType;
    this.isForcedRequest = false;


    let params: any = document.cookie;
    if (params) {
      params = params.split("; ");
      for (let i = 0; i < params.length; i++) {
        let arr = params[i].split("=");
        if (arr[0] == "AppSession") {
          this.userDetails.accessToken = this.configService.configData.token || arr[1];

        } else if (arr[0] == "userId") {
          this.userDetails.customerId = this.configService.configData.playerId || arr[1];
        }
      }
    } else {
      this.userDetails.accessToken = this.matchDetails.playerId.toString();
      this.userDetails.customerId = this.matchDetails.playerId;
    }

    this.preloaderStarted = true;
    this.loadImages();

    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: window.outerHeight
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 100);

    document.getElementById("body")!.style.maxWidth = "500px";

    this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
    this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
    this.userDetails.playerName = this.configService.configData.playerName || this.userDetails.customerId;

    this.matchDetails.playerId = this.userDetails.customerId;

    document.title = "Virtual Catch The Trend";
    if (this.disableCanvas) {
      this.initializeGame({});
    }
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    if (this.socketService) {
      this.socketService.disconnectSocket();
    }
    this.messageSubscription.unsubscribe();
  }


  @HostListener("window:message", ["$event"])
  onMessage(res: any) {
    try {
      if (res.data) {
        let messageData = res.data;
        this.loadedIframe = document.getElementById("iframeElement");
        switch (messageData.messageName) {
          case "init":
            this.iframeInitialized = true;
            this.initializeGame(messageData);
            break;
          case "UpdateCTTScore":
            this.updateScores(this.scoreCardResponse);
            break;
          case "OpenVCTTScoreboard":
            this.showCompleteScore(messageData);
            break;
          case "LoadingPercentage":
            this.setVCTTLoadingPercentage(messageData);
            break;
          case "LoadingComplete":
            this.setVCTTLoadingComplete();
            break;
          case "appHeight":
            if (this.iframeInitialized) {
              this.setIframeHeight(messageData);
            }
            break;
          case "PlayButtonClickedCTT":
            setTimeout(() => {
              this.onBetPlaced(messageData.data, messageData.betSession);
            }, 100);
            // this.onBetPlaced(messageData.data);
            break;
          case "GetPlayerGameHistoryCTT":
            this.getGameInit();
            break;
          case "CTTMenuButtonClicked":
            this.navigateCTTMenu(messageData);
            break;
          case "LobbyClicked":
            this.navigateToLobby(messageData);
            break;
          case "setScrollable":
            this.setScrollable(messageData);
            break;
          case "ShowNoSelectionError":
            this.showNoSelectionError(messageData);
            break;
          case "NewBetDetailsPopup":
            this.showNewBetDetailsPopup(messageData);
            break;
          case "HideNoSelectionError":
            this.hideNoSelectionError();
            break;
          case "UpdateCurrency":
            this.updateCurrency(messageData);
            break;
          case "CTTGameEnded":
            this.cttGameEnded(messageData.data);
            break;
          case "CTTUpdateBetOverNumber":
            this.cttUpdateBetOverNumber(messageData.data);
            break;
          case "CTTCloseBetPopup":
            this.cttCloseBetPopup(messageData.data);
            break;
          default:
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  serverResponse(message: any) {
    if (message == "socket connected") {
      this.onSocketConnected();
    } else {
      switch (message.message) {
        case "authenticate":
          this.onPlayerAuthenticated(message.data);
          break;
        case "balance":
          this.setBalance(message.data);
          break;
        case "init":
          this.setGameInit(message.data);
          break;
        case "event":
          this.setGameEvent(message.data);
          break;
        case "betresponse":
          this.setBetResponse(message.data);
          break;
        case "betsessionresponse":
          this.setBetSessionResponse(message.data);
          break;
        case "disconnected":
          this.setServerDisconnected(message.data);
          break;
        default:
      }
    }
  }

  setGameInit(data: any) {
    if (this.gameInitData) {
      this.gameInitData = data;
      let initData = {
        message: "gameinit",
        data: this.gameInitData,
      };
      this.catchTrendSubject.next(initData);
      let gameHistoryDetails: any = {};
      gameHistoryDetails.message = "PLAYER_GAME_HISTORY";
      gameHistoryDetails.data = data;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(gameHistoryDetails, "*");
      }
    } else {

      this.gameInitData = data;
      let gameInitDetails: any = {};
      gameInitDetails.message = "GAME_INIT";
      gameInitDetails.data = data;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(gameInitDetails, "*");
      }
    }

    let apiURLDetails = {
      message: "SET_API_URL",
      data: {
        url: this.configService.configData.url.apiUrl
      }
    }
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(apiURLDetails, "*");
    }
  }

  setVCTTLoadingPercentage(data: any) {
    document.getElementById('progress-bar')!.style.width = data.loadedPercentage + '%';
  }

  setVCTTLoadingComplete() {
    setTimeout(function () {
      document.getElementById('preloader')!.style.display = "none";
      document.getElementById('display-game')!.style.visibility = "visible";
      document.body.style.backgroundColor = "initial";
    }, 1500)
  }

  setGameEvent(data: any) {
    this.scoreCardResponse = data;
    if (this.componentInit) {
      this.updateScores(this.scoreCardResponse);
      this.componentInit = false;
    }
    let gameEventDetails: any = {};
    gameEventDetails.message = "GAME_EVENT";
    gameEventDetails.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(gameEventDetails, "*");
    }
  }

  setBalance(data: any) {
    this.updateBalance();
    if (this.gameInit) {
      this.gameInit = false;
      this.getGameInit();
    }
    let balanceDetails: any = {};
    balanceDetails.message = "BALANCE_DETAILS";
    balanceDetails.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(balanceDetails, "*");
    }
  }

  getGameInit() {
    this.socketService.getGameInit({
      matchId: this.matchDetails.matchId,
      playerId: this.userDetails.customerId,
      gameName: this.gameCode,

      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": this.gameCode,
        "gameType": this.gameCode,
        "playerName": this.configService.configData.playerName
      }
    });
  }

  setBetResponse(response: any) {
    console.warn("response in set bet response ", response)
    this.updateBalance();
    let data = { ...response };
    let betResponse: any = {};
    betResponse.message = "BET_RESPONSE";
    betResponse.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      // this.loadedIframe.contentWindow.gameExtension(betResponse, "*");
    }
    if (data.betRequest && (data.betRequest.overNumber || data.betRequest.overNumber == 0)) {
      this.betResponse = { ...data.betRequest };
      if (this.betRequest) {
        this.betResponse.overNumber = this.betRequest.overNumber;
        this.betResponse.selections = this.betRequest.selections;
      }
    }
    if (data.status == 301) {
      this.errorBet = true;
      this.successBet = false;
      if (this.betData.isUpdate) {
        this.errorBetMessage = "Your selection has been updated";
        this.modalBetTitle = "Success";
      } else {
        this.errorBetMessage = "Your balance is 0. Please fund your account and then place the bet.";
        this.modalBetTitle = "Error";
      }
    } else if (data.status == 305) {
      // this.errorBet = true;
      // this.betPlacedSuccessfully = false;
      // this.successBet = false;
      this.betAlreadyPlaced = true;
      setTimeout(() => {
        document.getElementById("betAlreadyPlaced")!.style.display = "flex";
      }, 100);
      setTimeout(() => {
        document.getElementById("betAlreadyPlaced")!.style.display = "none";
      }, 4000)
    } else if (data.status == 200) {
      this.errorBet = false;
      this.betPlacedSuccessfully = true;
      this.newBet = data.newBet;
      this.successBet = true;
      this.modalBetTitle = "Success";
      let playButtonData = {
        message: "betsessionresponse",
        data: data
      };
      if (this.isForcedRequest) {
        if (this.isReal && !this.newBet) {
          document.getElementById("noBetSelection")!.style.display = "flex";
          // document.getElementById("overAlreadyStarted") &&  (document.getElementById("overAlreadyStarted")!.style.display = "flex");
        }
      } else {
        document.getElementById("betModal")!.style.display = "flex";
      }
      this.catchTrendSubject.next(playButtonData);
      setTimeout(() => (
        document.getElementById("betModal")!.style.display = "none",
        document.getElementById("noBetSelection")!.style.display = "none"
      ), 4000)
    } else if (data.status == 304) {
    } else {
      this.errorBet = false;
      this.successBet = true;
      this.modalBetTitle = "Success";
      let playButtonData = {
        message: "playbuttonstate",
        data: true,
      };
      // this.catchTrendSubject.next(playButtonData);
    }
    document.getElementById("overs-bet")!.style.display = "none";
    if (this.loadedIframe && this.loadedIframe.contentDocument) {
      this.loadedIframe.contentDocument.getElementsByTagName("canvas")[0].style.touchAction = "auto";
    }
    document.body.style.overflow = "unset";
    // $("#betModal").modal();
  }

  showNewBetDetailsPopup(data: any) {
    this.isCashGame = data.isCashGame;
    this.currentOver = data.currentOver;
    this.overNumbers = [];
    this.oversCount = data.overNumbers.length;

    this.winObj = {
      winFour: 0,
      winSix: 0,
      winWickets: 0,
      winDotballs: 0
    }
    this.nonWinObj = {
      winFour: 0,
      winSix: 0,
      winWickets: 0,
      winDotballs: 0
    }
    this.game = data.overNumbers.length;
    for (let p = 0; p < data.overNumbers.length; p++) {
      this.overNumbers.push(data.overNumbers[p]);
    }
    this.selections = data.selections;
    this.stakeAmount = data.stakeAmount;

    if (data.winAmount != null) {
      this.betSlipWinAmount = data.winAmount;
    }

    if (data.winningScores != null && data.nonWinningScores != null) {
      this.winningScores = [];
      this.nonWinningScores = [];
      this.winningScores = data.winningScores;
      for (let p = 0; p < this.winningScores.length; p++) {

        if (this.winningScores[p] == '4') {
          this.winObj.winFour = 1;
        }
        else if (this.winningScores[p] == '6') {
          this.winObj.winSix = 1;
        }
        else if (this.winningScores[p] == 'W') {
          this.winObj.winWickets = 1;
        } else {
          this.winObj.winDotballs = 1;
        }
      }
      this.nonWinningScores = data.nonWinningScores;
      for (let p = 0; p < this.nonWinningScores.length; p++) {
        if (this.nonWinningScores[p] == '4') {

          this.nonWinObj.winFour = 1;
        }
        else if (this.nonWinningScores[p] == '6') {
          this.nonWinObj.winSix = 1;
        }
        else if (this.nonWinningScores[p] == 'W') {
          this.nonWinObj.winWickets = 1;
        } else {
          this.nonWinObj.winDotballs = 1;
        }
      }
    }
    else {
      if (this.game === 3) {
        this.potWin = 50 * this.stakeAmount;
      } else if (this.game === 5) {
        this.potWin = 500 * this.stakeAmount;
      } else {
        this.potWin = 5 * this.stakeAmount;
      }
    }
    let selectionsArray = this.selections;

    this.showSelectionsBasedOvers();



    document.getElementById("new-bet-modal")!.style.display = "flex";
  }

  showSelectionsBasedOvers() {
    this.updatedOvers = [];
    for (let over = 0; over < this.selections[this.overSelected].length; over++) {
      this.updatedOvers.push(this.selections[this.overSelected][over]);
    }
  }

  onNavClicked(id: any) {
    if (id == 'resultcatchtrend') {
      this.router.navigate(['/home/catchthetrend/resultcatchtrend']);
    } else if (id == "oddstable") {
      this.router.navigate(['/home/catchthetrend/oddstable']);
    } else if (id == "howtoplaycatchtrend") {
      this.router.navigate(['/home/catchthetrend/howtoplaycatchtrend']);
    } else if (id == "leaderboardcatchtrend") {
      this.isNotLeaderBoard = false;
      this.router.navigate(['/home/catchthetrend/leaderboardcatchtrend'])
    } else {
      this.router.navigate(['/home/catchthetrend'])
    }
  }

  setBetSessionResponse(data: any) {
    this.updateBalance();

    if ((data.error || (data.betRequest && data.betRequest.error)) && (data.playerAccount && data.playerAccount.playerId == this.userDetails.customerId)) {
      if (data.status == 461) {
        this.showModal('Insufficient Funds. Please add funds to your account and retry.')
      } else {
        this.showModal(data.errorMessage)
      }
      return;
    }

    this.betSessionResponse = data;
    let betSessionData = {
      message: "betsessionresponse",
      data: this.betSessionResponse,
    };
    this.catchTrendSubject.next(betSessionData);
    let betSessionResponse = {
      message: "BET_SESSION_RESPONSE",
      data: data,
    };
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(betSessionResponse, "*");
    }
  }

  setServerDisconnected(data: any) {
    let disConnectionDetails: any = {};
    disConnectionDetails.message = "SOCKET_DISCONNECTED";
    disConnectionDetails.data = true;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(disConnectionDetails, "*");
    }
  }

  onPrevClicked() {

    this.overSelected = this.overSelected - 1;
    //call some function
    if (this.overSelected >= 0) {
      this.showSelectionsBasedOvers();
    }

  }

  onNextClicked() {
    this.overSelected = this.overSelected + 1;
    //call some function
    this.showSelectionsBasedOvers();
  }

  showCompleteScore(data: any) {
    this.showScores = true;
    this.showScoreCard = true;
  }

  // onSocketConnected() {
  //   this.socketService.getBalance({
  //     token: this.userDetails.accessToken,
  //     playerId: this.userDetails.customerId,

  //     gameName: this.gameCode,
  //     matchId: this.matchDetails.matchId,
  //     jurisdiction: this.configService.configData.url.userRegion,
  //     extraData: {
  //       "platformId": this.configService.configData.platformId,
  //       "operatorId": this.configService.configData.operatorId,
  //       "brand": this.configService.configData.brand,
  //       "region": this.configService.configData.url.userRegion,
  //       "currency": this.currencyCode,
  //       "gameId": this.gameCode,
  //       "gameType": this.gameCode
  //     }
  //   });
  //   let connectionDetails: any = {};
  //   connectionDetails.message = "SOCKET_CONNECTED";
  //   connectionDetails.data = true;
  //   if (this.loadedIframe && this.loadedIframe.contentWindow) {
  //     this.loadedIframe.contentWindow.gameExtension(connectionDetails, "*");
  //   }
  // }
  onSocketConnected() {
    if ((this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) || this.configService.configData.disableCanvas) {
      let connectionDetails: any = {};
      connectionDetails.message = "SOCKET_CONNECTED";
      connectionDetails.data = true;
      this.loadedIframe = document.getElementById('iframeElement');
      if (this.loadedIframe && this.loadedIframe.contentWindow && this.loadedIframe.contentWindow.gameExtension) {
        this.loadedIframe.contentWindow.gameExtension(connectionDetails, "*");
      }

      this.socketService.authenticatePlayer({
        token: this.userDetails.accessToken,
        playerId: (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
        gameName: this.gameCode,
        matchId: this.matchDetails.matchId || 100001,
        jurisdiction: this.configService.configData.url.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerName": this.configService.configData.playerName
        }
      });
      // else {
      //   this.socketService.getBalance({
      //     token: this.userDetails.accessToken,
      //     playerId: this.userDetails.customerId,

      //     gameName: this.gameCode,
      //     matchId: this.matchDetails.matchId,
      //     jurisdiction: this.configService.configData.url.userRegion,
      //     extraData: {
      //       "platformId": this.configService.configData.platformId,
      //       "operatorId": this.configService.configData.operatorId,
      //       "brand": this.configService.configData.brand,
      //       "region": this.configService.configData.url.userRegion,
      //       "currency": this.currencyCode,
      //       "gameId": this.gameCode,
      //       "gameType": this.gameCode
      //     }
      //   });
      //   this.matchDetails.languageCode = this.configService.configData.languageCode || 'en';
      //   let matchDetails: any = {};
      //   matchDetails.message = "MATCH_DETAILS";
      //   matchDetails.data = this.matchDetails;
      //   if (this.loadedIframe && this.loadedIframe.contentWindow) {
      //     this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
      //   }
      // }
    } else {
      setTimeout(() => {
        this.onSocketConnected();
      }, 1000)
    }
  }


  onPlayerAuthenticated(balanceResponse: any) {
    if (balanceResponse.playerAccount && (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6")) {
      this.userDetails.customerId = balanceResponse.playerAccount.playerId;
      this.userDetails.playerName = balanceResponse.playerAccount.playerName;
      this.socketService.subscribePlayerBet(balanceResponse.playerAccount.playerId);
    }
    if (window.location.href.indexOf("10bet") >= 0) {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerName != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }
    } else {
      if ((balanceResponse.playerAccount && (balanceResponse.playerAccount.playerId != this.userDetails.customerId)) || balanceResponse.status == 400) {
        return;
      }
    }
    // if (balanceResponse.error) {
    //   this.showGenericPopup = true;
    //   this.showModal(balanceResponse.errorMessage || 'The game is under maintenance. Please check after sometime.');
    // }

    let matchDetails: any = {};
    matchDetails.message = "MATCH_DETAILS";
    matchDetails.data = this.matchDetails;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
    }

    this.socketService.getBalance({
      token: this.userDetails.accessToken,
      playerId: this.userDetails.customerId,

      gameName: this.gameCode,
      matchId: this.matchDetails.matchId,
      jurisdiction: this.configService.configData.url.userRegion,
      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": this.gameCode,
        "gameType": this.gameCode,
        "playerName": this.configService.configData.playerName
      }
    });
  }

  initializeGame(obj: any) {
    this.socketService.disconnectSocket();
    if (!this.matchDetails.socketFlag) {
      this.socketService.connectToServer(
        this.configService.configData.url.socket,
        this.gameType,
        this.userDetails.customerId,
        this.matchDetails.matchId
      );
    }

    if (localStorage.getItem("isChecked")) {
      let checked = JSON.parse(localStorage.getItem("isChecked")!);
      if (!checked) {
        document.getElementById("initial_popup")!.style.display = "flex";
      }
    } else {
      document.getElementById("initial_popup")!.style.display = "flex";
    }
  }

  getUUid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  setIframeHeight(obj: any) {
    if (obj.appHeight) {
      // document.getElementById("iframeElement")!.style.height = obj.appHeight + "px";
      // temp fix need to set the exact iframe height
      document.getElementById('iframeElement')!.style.height = "275px";
      if (document.referrer && document.referrer.indexOf("localhost") < 0) {
        window.parent.postMessage(
          {
            action: "b2bresize",
            scrollHeight: document.getElementById('display-game')?.clientHeight! + document.getElementById('score-container-wrapper')?.clientHeight!,
          },
          document.referrer
        );
      }
    }
  }

  @HostListener("window:scroll", ["$event"])
  onScroll(event: any) {
    this.scrollYPosition = document.documentElement.scrollTop;
  }

  navigateCTTMenu(data: any) {
    if (data.buttonId == "howtoplay") {
      this.router.navigate(["howtoplaycatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition },
      });
    } else if (data.buttonId == "predictover" || data.buttonId == "play") {
      this.router.navigate(["predictovercatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition },
      });
    } else if (data.buttonId == "result") {
      this.router.navigate(["resultcatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition, redirect: "main" },
      });
    } else if (data.buttonId == "leaderboard") {
      this.router.navigate(["leaderboardcatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition },
      });
    } else if (data.buttonId == "mainleaderboard") {
      this.router.navigate(["leaderboardcatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition, redirect: "main" },
      });
    } else if (data.buttonId == "CTT_History") {
      this.router.navigate(["historycatchtrend"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition, redirect: "main" },
      });
    } else if (data.buttonId == "oddsTable") {
      this.router.navigate(["oddstable"], {
        relativeTo: this.route,
        state: { item: this.scrollYPosition, redirect: "main" },
      });
    } else {
    }
  }

  setScrollable(obj: any) {
    window.scrollTo(0, 0);
    let iframeElement = this.loadedIframe.contentDocument;
    if (obj.scrollable) {
      iframeElement.getElementsByTagName("canvas")[0].style.touchAction =
        "auto";
    } else {
      iframeElement.getElementsByTagName("canvas")[0].style.touchAction =
        "none";
    }
  }

  navigateToLobby(data: any) {
    this.socketService.disconnectSocket();
    if (this.configService.configData.isRGSSetup) {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({ action: "lobby" }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    } else {
      this.router.navigate([""]);
    }
  }

  checkRoute() {
    $("#" + this.router.url.split("/")[2].split("?")[0])
      .addClass("active")
      .siblings("li")
      .removeClass("active");
  }

  onHowToPlayClicked(event: any) {
    this.addActive(event);
    this.router.navigate(["howtoplaycatchtrend"], { relativeTo: this.route });
  }

  onPredictOverClicked(event: any) {
    this.addActive(event);
    this.router.navigate(["predictovercatchtrend"], { relativeTo: this.route });
  }

  onResultClicked(event: any) {
    this.addActive(event);
    this.router.navigate(["resultcatchtrend"], { relativeTo: this.route });
  }

  onLeaderboardClicked(event: any) {
    this.addActive(event);
    this.router.navigate(["leaderboardcatchtrend"], { relativeTo: this.route });
  }

  addActive(event: any) {
    $(event.target.closest("li"))
      .addClass("active")
      .siblings("li")
      .removeClass("active");
  }

  setCountdown() {
    if (this.matchDetails) {
      let serverDate = moment.tz(new Date(), "Asia/Kolkata").format();
      if (this.configService.configData.isRGSSetup) {
        serverDate = moment.tz(new Date(), "UTC").format();
      }
      let currentDate = moment(serverDate.split("+")[0]);
      let resultDate = moment(new Date(this.matchDetails.startDate));
      let dateDiff: any = moment.duration(currentDate.diff(resultDate));
      if (-dateDiff._data.seconds < 0) {
        this.matchDetails.date = "Live";
      } else {
        this.matchDetails.date =
          -dateDiff._data.hours +
          "hr : " +
          -dateDiff._data.minutes +
          "m : " +
          -dateDiff._data.seconds +
          "s";
      }
      if (this.matchDetails.status == "1") {
        this.matchDetails.date = "Live";
      }
    }
  }

  updateCurrency(data: any) {
    this.currencyCode = data.currency;
    this.resultpenaltyshootout.currencyCode = data.currency + " ";
  }

  cttUpdateBetOverNumber(data: any) {
    this.betData.overNumber = data.nextOver;
    let item = this.item;
    if (this.isReal) {
      let data: any = {};
      data.overs = item;
      let betInformation: any = {};
      betInformation.message = "PLACED_BET_OVERS";
      betInformation.data = data;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(betInformation, "*");
      }
    }

  }

  cttCloseBetPopup(messagedata: any) {
    let data = { isBetPopupClosed: true };
    let betPopupClose: any = {};
    betPopupClose.message = "CLOSED_BET_POPUP";
    betPopupClose.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(betPopupClose, "*");
    }
    document.getElementById("overs-bet")!.style.display = "none";
  }

  onBetPlaced(res: any, betSession: any) {
    let data: any = { ...res };
    this.betSession = betSession;
    this.betData = data;
    this.betDataSelections = data.betSelections;
    this.isReal = this.matchDetails.isReal;
    this.isUpdate = data.isUpdate;
    this.isForcedRequest = data.isForcedRequest;
    this.inningsNumber = data.inningNumber || 1;
    let overNumber = data.overNumber;

    this.overNumber = overNumber || 0;

    if (this.overNumber >= 19) {
      this.selectedOver = 1;
    }
    // if (this.scoreCardDetails && (this.betData.overNumber == Math.ceil(this.scoreCardDetails.overStat.overCount + 1))) {
    //   this.errorBet = true;
    //   this.successBet = false;
    //   this.errorBetMessage = "You cannot place bet on a running over. Please try on upcoming overs.";
    //   $("#betModal").modal();
    //   return;
    // }

    if (data.isUpdate) {
      this.selectedOver = 1;
    } else {
      window.scrollTo(0, 0);
      document.getElementById("overs-bet")!.style.display = "flex";
      // don't remove the below line
      // setTimeout(() => this.cttCloseBetPopup(false),10000)
      if (!this.isReal) {
        document.getElementById("overs-bet-selection")!.style.top = "-85px";
      }
      // document.getElementById("overs-bet")!.style.height =
      //   window.innerHeight - data.boundingBox.y + "px";
      // document.getElementById("overs-bet")!.style.top =
      //   data.boundingBox.y + "px";
      if (this.loadedIframe) {
        this.loadedIframe.contentDocument.getElementsByTagName("canvas")[0].style.touchAction = "none";
      }
      document.body.style.overflow = "hidden";
    }
    if (data.isUpdate && this.runningBetOvers >= 1) {
      this.sendSocketBetRequest();
    }

    if (!data.isReal) {
      if (data.isUpdate && overNumber >= 1) {
        this.sendSocketBetRequest();
      }
    }

    let initialOverData: any = {};
    if (data.isReal) {
      initialOverData.overs = 1;
    } else {
      initialOverData.overs = 1;
    }
    this.item = initialOverData.overs;
    this.betContents = ["All Selections Correct 5.00"];

    let betInformation: any = {};
    betInformation.message = "PLACED_BET_OVERS";
    betInformation.data = initialOverData;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(betInformation, "*");
    }
  }

  updateScores(data: any) {
    this.scoreCardResponseUpdate = this.scoreCardResponse;
    if (!this.showScoreCard) {
      this.showScoreCard = true;
    }
    let scoreData = data.runningScore;
    if (scoreData.inning == 1) {
      this.scoreCardDetails = scoreData.firstInning;
    } else {
      this.scoreCardDetails = scoreData.secondInning;
    }
    if (!this.scoreCardDetails) {
      return;
    }
    this.scoreCardDetails.lastSixBalls = [];
    this.scoreCardDetails.lastSecondOver = [];
    this.scoreCardDetails.lastThirdOver = [];
    let lastSixBallScores = [];
    let lastSecondOver = [];
    let lastThirdOver = [];
    let lastOverScores = [];
    let ballsBowled = this.scoreCardDetails.overStat.deliveries.length % 6;
    for (let i = ballsBowled + 1; i <= ballsBowled + 6; i++) {
      lastSixBallScores.unshift(
        this.scoreCardDetails.overStat.deliveries[
        this.scoreCardDetails.overStat.deliveries.length - i
        ]
      );
    }
    for (let i = ballsBowled + 7; i <= ballsBowled + 12; i++) {
      lastSecondOver.unshift(
        this.scoreCardDetails.overStat.deliveries[
        this.scoreCardDetails.overStat.deliveries.length - i
        ]
      );
    }
    for (let i = ballsBowled + 13; i <= ballsBowled + 18; i++) {
      lastThirdOver.unshift(
        this.scoreCardDetails.overStat.deliveries[
        this.scoreCardDetails.overStat.deliveries.length - i
        ]
      );
    }
    for (let i = 1; i <= 6; i++) {
      if (i <= ballsBowled) {
        lastOverScores.unshift(
          this.scoreCardDetails.overStat.deliveries[
          this.scoreCardDetails.overStat.deliveries.length - i
          ]
        );
      } else {
        if (ballsBowled == 0) {
          lastOverScores.unshift(
            this.scoreCardDetails.overStat.deliveries[
            this.scoreCardDetails.overStat.deliveries.length - i
            ]
          );
        } else {
          lastOverScores.push("\xa0");
        }
      }
    }

    this.scoreCardDetails.lastSixBalls = lastSixBallScores;
    this.scoreCardDetails.lastSecondOver = lastSecondOver;
    this.scoreCardDetails.lastThirdOver = lastThirdOver;
    this.scoreCardDetails.lastOverScores = lastOverScores;
    let overFloor = Math.floor(this.scoreCardDetails.overStat.overCount);
    this.scoreCardDetails.runRate = ((this.scoreCardDetails.overStat.totalScore * 6) / (overFloor * 6 + (this.scoreCardDetails.overStat.overCount - overFloor) * 10)).toFixed(2);
    if (this.scoreCardDetails.runRate == "NaN") {
      this.scoreCardDetails.runRate = 0;
    }
    this.scoreCardDetails.strikerBatsman.economy = ((this.scoreCardDetails.strikerBatsman.run * 100) / this.scoreCardDetails.strikerBatsman.balls).toFixed(2);
    this.scoreCardDetails.strikerBatsman.seconds = Math.ceil(this.scoreCardDetails.strikerBatsman.seconds / 60);
    this.scoreCardDetails.nonStrikerBatsman.seconds = Math.ceil(this.scoreCardDetails.nonStrikerBatsman.seconds / 60);
    this.scoreCardDetails.nonStrikerBatsman.economy = ((this.scoreCardDetails.nonStrikerBatsman.run * 100) / this.scoreCardDetails.nonStrikerBatsman.balls).toFixed(2);
    this.scoreCardDetails.bowlerStat.economy = (this.scoreCardDetails.bowlerStat.runs / (Math.floor(this.scoreCardDetails.bowlerStat.runningOver) + (this.scoreCardDetails.bowlerStat.runningOver - Math.floor(this.scoreCardDetails.bowlerStat.runningOver)) / 0.6)).toFixed(2);
    if (this.scoreCardDetails.strikerBatsman.economy == "NaN") {
      this.scoreCardDetails.strikerBatsman.economy = 0;
    }
    if (this.scoreCardDetails.nonStrikerBatsman.economy == "NaN") {
      this.scoreCardDetails.nonStrikerBatsman.economy = 0;
    }
    if (this.scoreCardDetails.bowlerStat.economy == "NaN") {
      this.scoreCardDetails.bowlerStat.economy = 0;
    }
    if (this.scoreCardDetails.overStat.overCount - Math.floor(this.scoreCardDetails.overStat.overCount) >= 0.55) {
      this.scoreCardDetails.bowlerStat.runningOver = Math.ceil(this.scoreCardDetails.bowlerStat.runningOver);
    }
    if (this.scoreCardResponse.runningScore.firstInning.overStat.overCount - Math.floor(this.scoreCardResponse.runningScore.firstInning.overStat.overCount) >= 0.55) {
      this.scoreCardResponse.runningScore.firstInning.overStat.overCount = Math.ceil(this.scoreCardResponse.runningScore.firstInning.overStat.overCount);
    }
    if (this.scoreCardResponse.runningScore.secondInning && this.scoreCardResponse.runningScore.secondInning.overStat && this.scoreCardResponse.runningScore.secondInning.overStat.overCount - Math.floor(this.scoreCardResponse.runningScore.secondInning.overStat.overCount) >= 0.55) {
      this.scoreCardResponse.runningScore.secondInning.overStat.overCount = Math.ceil(this.scoreCardResponse.runningScore.secondInning.overStat.overCount);
    }
    let sendScoreData = {
      message: "scorecard",
      data: {
        scoreDetails: this.scoreCardDetails,
        completeScore: this.scoreCardResponse,
        innings: scoreData.inning,
      },
    };
    this.catchTrendSubject.next(sendScoreData);
  }

  onShowScores(value: any) {
    if (value) {
      this.showScoreCard = true;
      this.showScores = true;
    } else {
      let showScoreDetails: any = {};
      showScoreDetails.message = "FULL_SCOREBOARD_CLOSED";
      showScoreDetails.data = true;
      this.showScores = false;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(showScoreDetails, "*");
      }
    }
  }

  selectButton(item: any, event: any) {
    this.bar = item;
    $(".bet-button").css("background", "#1B3DB6");
    // document.getElementById("input-bet")!.style.setProperty("background", "white", "important");
    event.target.classList.add("button-selected");
    this.selectedBet = item;
    this.inputSelected = false;
    this.playButtonState = true;
  }

  selectOver(item: any, event: any) {
    if (item === 1) {
      this.betContents = ["All Selections Correct 5.00"];
    } else if (item === 5) {
      this.betContents = [
        "All Selections Correct 500.00",
        "At Least 4 overs Correct 10.00",
        "At Least 2 overs Correct 2.00",
      ];
    } else {
      this.betContents = [
        "All Selections Correct 50.00",
        "At Least 1 over Correct 2.00",
      ];
    }

    $(".over-button").css("background", "#1B3DB6");
    event.target.closest("button").classList.add("button-selected");
    this.selectedOver = item;
    let data: any = {};
    data.overs = item;
    this.item = item;
    let betInformation: any = {};
    betInformation.message = "PLACED_BET_OVERS";
    betInformation.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(betInformation, "*");
    }
  }

  selectInputButton(event: any) {
    this.selectedBet = event.target.value;
    $(".bet-button").css("background", "white");
    event.target.classList.add("button-selected");
    this.inputSelected = true;
    this.playButtonState = true;
  }

  changeInputValue(value: any) {
    if (value > this.maxBet) {
      this.inputDisabled = true;
      this.showTooltip();
      // @ts-ignore
      document.getElementById("bet_amount").value = this.maxBet;
      this.selectedBet = this.maxBet;
    }
    else if (value == "" || !value) {
      this.playButtonState = false;
    }
    else {
      this.bar = value;
      this.selectedBet = value;
      this.playButtonState = true;
    }
  }

  showTooltip() {
    document.getElementById("max-bet-tooltip")!.style.display = 'block';
    setTimeout(() => document.getElementById("max-bet-tooltip")!.style.display = "none", 4000);
  }

  onPlayClicked() {
    if (this.scoreCardDetails && this.overNumber == (Math.floor(this.scoreCardDetails.overStat.overCount) + 1)) {
      document.getElementById("overAlreadyStarted")!.style.display = "flex";
      return;
    }
    this.bettingOvers = [];
    if (this.selectedOver === 3) {
      this.potentialWin = 50 * this.selectedBet;
    } else if (this.selectedOver === 5) {
      this.potentialWin = 500 * this.selectedBet;
    } else {
      this.potentialWin = 5 * this.selectedBet;
    }

    this.runningBetOvers = this.selectedOver;

    this.sendSocketBetRequest();
    // @ts-ignore
    document.getElementById("bet_amount").value = "";
    this.selectedBet = 5;
    for (let p = 0; p < this.runningBetOvers; p++) {
      if (this.betData.overNumber <= 20 && this.bettingOvers.indexOf(p) == -1) {
        this.bettingOvers.push(p);
        // alert(this.bettingOvers)
      }
    }
    this.isForcedRequest = false;
    // this.selectedOver = 1;
  }

  onSubmitClicked() {

    if (this.scoreCardDetails && this.overNumber == (Math.floor(this.scoreCardDetails.overStat.overCount) + 1)) {
      document.getElementById("overAlreadyStarted")!.style.display = "flex";
      return;
    }

    this.selectedBet = this.betDataSelections;
    this.selectedOver = this.betData.overNumber;
    this.sendSocketBetRequest();
  }

  onPlayClickedWhenOverAlreadyStarted() {
    this.betData.overNumber = this.betData.overNumber + 1;
    document.getElementById("overAlreadyStarted")!.style.display = "none";
    this.betPlacedSuccessfully = false;
    this.runningBetOvers = this.selectedOver;
    this.sendSocketBetRequest();
    for (let p = 0; p < this.runningBetOvers; p++) {
      if (this.betData.overNumber <= 20 && this.bettingOvers.indexOf(p) == -1) {
        this.bettingOvers.push(p);
      }
    }
  }

  sendSocketBetRequest() {
    let betSesion;
    if (this.isUpdate) {
      betSesion = this.betSession;
    }
    else {
      betSesion = this.selectedOver + "_" + this.betSession;
    }
    let request = {
      token: this.userDetails.accessToken,
      matchId: this.matchDetails.matchId,
      playerId: this.userDetails.customerId,
      selections: this.betData.betSelections,
      betAmount: this.isReal ? this.selectedBet : 0,
      overNumber: this.betData.overNumber,
      accessToken: this.userDetails.accessToken,
      gameName: this.gameCode,
      betSession: betSesion,
      inningNumber: this.inningsNumber,
      jurisdiction: this.configService.configData.url.userRegion,
      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": this.gameCode,
        "gameType": this.gameCode,
        "playerName": this.configService.configData.playerName
      }
    };
    this.betRequest = { ...request };
    this.betRequest.selectedOvers = this.selectedOver;
    if (this.runningBetOvers == this.selectedOver) {
      this.socketService.sendBetRequest(request, this.selectedOver);
    } else {
      this.socketService.sendBetRequest(request, 1);
    }
    setTimeout(
      () => {
        this.socketService.getGameInit({
          matchId: this.matchDetails.matchId,
          playerId: this.userDetails.customerId,
          gameName: this.gameCode,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": this.gameCode,
            "gameType": this.gameCode,
            "playerName": this.configService.configData.playerName
          }
        })
      },
      2000
    );
  }

  closeBetPanel(isBetPopup: any) {
    if (isBetPopup == "bet-popup") {
      let data = { isBetPopupClosed: true };
      let betPopupClose: any = {};
      betPopupClose.message = "CLOSED_BET_POPUP";
      betPopupClose.data = data;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(betPopupClose, "*");
      }
    }
    document.getElementById("overs-bet") &&
      (document.getElementById("overs-bet")!.style.display = "none");
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentDocument.getElementsByTagName(
          "canvas"
        )[0].style.touchAction = "auto";
      }
    document.body.style.overflow = "unset";
    document.getElementById("noBetSelection") &&
      (document.getElementById("noBetSelection")!.style.display = "none");
    document.getElementById("betModal") &&
      (document.getElementById("betModal")!.style.display = "none");
    document.getElementById("overAlreadyStarted") &&
      (document.getElementById("overAlreadyStarted")!.style.display = "none");
    document.getElementById("betAlreadyPlaced") &&
      (document.getElementById("betAlreadyPlaced")!.style.display = "none");
    document.getElementById("game-end-popup") &&
      (document.getElementById("game-end-popup")!.style.display = "none");
    document.getElementById("new-bet-modal") &&
      (document.getElementById("new-bet-modal")!.style.display = "none");
    this.isViewBetSlipClicked = false;
  }

  cttGameEnded(data: any) {
    this.oversWon = data.oversWon;
    this.totalOversPlayed = data.totalOversPlayed;
    this.totalWin = data.totalWin;
    this.correctSelections = data.correctSelections;
    document.getElementById('game-end-popup')!.style.display = "flex";
    setTimeout(() => document.getElementById("game-end-popup")!.style.display = "none", 4000)
  }

  showNoSelectionError(data: any) {
    // $("#noBetSelection").modal();
    document.getElementById("noBetSelection")!.style.display = "flex";
  }

  hideNoSelectionError() {
    document.getElementById("noBetSelection")!.style.display = "none";
  }

  playNowClicked() {
    document.getElementById('initial_popup')!.style.display = "none";
    if (this.isChecked) {
      localStorage.setItem('isChecked', JSON.stringify(this.isChecked));
    }
  }

  onCheckboxClicked(event: any) {
    this.isChecked = event.target.checked;

  }

  toggleBetDetailsModal() {
    this.isViewBetSlipClicked = true;
    document.getElementById("betModal")!.style.display = "flex";
  }

  onHowToPlayCloseClicked() {
    $("#howtoplaycatchtrend").removeClass("active");
    $("#nav-link-play-btn").addClass("active");
  }

  updateBalance() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({ action: "b2bupdateBalance" }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }

  loadImages() {
    if (this.preloaderStarted) {
      let img = new Image();
      img.src = 'assets/images/preloaderbg.jpg';
      img.decode()
        .then(() => {
          this.preloaderStarted = false;
          document.body.style.backgroundColor = "initial";
          $('.menu').css('background-image', "url('../../assets/images/preloaderbg.jpg')");
          this.sendPreloadingCompleteToWrapper();
          setTimeout(() => {
            document.getElementById('preloader')!.style.display = "none";
          }, 1000);
        })
    } else {
      setTimeout(() => this.loadImages(), 10);
    }
  }
  sendPreloadingCompleteToWrapper() {
    console.log("Preloading is completed ");
    console.log("Preloader ", this.preloaderStarted);
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage(
        {
          action: "preloadingComplete"
        },
        (document.referrer || (window.parent && window.parent.location.href))
      );
    }
  }

  showModal(message: any) {
    this.genericErrorMessage = message;
    this.showGenericPopup = true;
    document.getElementById('generic-popup')!.style.display = "block";
    // ($('#generic-popup') as any).modal();
  }

  closePanel(id: any) {
    this.showGenericPopup = false;
    document.getElementById('generic-popup')!.style.display = "none";
    // ($('#'+id) as any).modal('hide');
  }

}
