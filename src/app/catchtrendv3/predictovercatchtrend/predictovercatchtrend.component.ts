// @ts-nocheck
import { ViewportScroller } from '@angular/common';
import { Component, OnInit, ChangeDetectorRef, Directive, Input, ElementRef, Renderer2, HostListener, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SocketService } from 'src/app/services/socket.service';
import { ConfigService } from '../../services/config.service';
import { CatchTrendComponent } from '../catchtrend.component';

@Component({
  selector: 'app-predictovercatchtrend',
  templateUrl: './predictovercatchtrend.component.html',
  styleUrls: ['./predictovercatchtrend.component.css']
})
export class PredictOverCatchTrendComponent implements OnInit {
  url: any;
  iframeUrl: any;
  predictions: any;
  currentOver: any;
  runningOver: any;
  overSup: any;
  playButtonState: boolean = false;
  scoreCardDetails: any;
  currentInnings: any;
  betSessionResponse: any;
  dotBallNotification: any;
  sixNotification: any;
  fourNotification: any;
  wicketNotification: any;
  notifyOver: any;
  notifyOverNumber: any;
  overComplete: boolean = false;
  selectedOverScore: any;
  gameInitData: any;
  predictOverInit: boolean = false;
  playButtonText: string = 'Play';
  inningsChange: boolean = false;
  initInnings: any;
  betPlacedOvers: any = {};
  showMyselections: boolean = true;
  mySelections: any;
  correctSelections: any = {};
  mySelectionsUpt: any;
  isReal: boolean = true;
  freePlayBetData: any = {};
  constructor(public sanitizer: DomSanitizer, public configService: ConfigService, private ref: ChangeDetectorRef, private socketService: SocketService, private catchTrendComponent: CatchTrendComponent, public viewportScroller: ViewportScroller) {
    this.predictions = {
      "fours": 0,
      "sixes": 0,
      "wickets": 0,
      "dots": 0
    }

    this.gameInitData = this.catchTrendComponent.gameInitData;
    this.currentOver = 1;
    this.catchTrendComponent.catchTrendSubject.subscribe((response) => {
      console.log("Response***", response);
      if (response.message == "betsessionresponse") {
        this.betSessionResponse = response.data;
        this.notifyOver = true;
      } else if (response.message == "gameinit") {
        // game init data  
        this.betPlacedOvers = {};
        this.gameInitData = response.data;
        this.gameInitData.gameHistory.forEach(element => {
          if (element.inningNumber == this.currentInnings) {
            this.betPlacedOvers[element.overNumber] = element.selections;
            this.freePlayBetData[element.overNumber] = element?.points;
          }
        });
        // this.currentOver = 1;
        this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails);
        this.checkForNotification(this.currentOver);
      } else {
        this.currentInnings = response.data.innings;
        if (!this.initInnings) {
          this.initInnings = response.data.innings;
        }
        if (response.data.innings != this.initInnings) {
          $('.notification').css("display", "none");
          this.initInnings = response.data.innings;
        }
        this.updateScoreCard(response.data.scoreDetails);

        this.currentOver = this.runningOver + 1;
        this.jumpToOver(this.currentOver);
        // revert
        // if (!this.currentOver) {
        //   this.currentOver = this.runningOver < 19 ? this.runningOver + 2 : this.runningOver + 1;
        //   this.jumpToOver(this.currentOver);
        // }
      }
      this.updatePlayButtonText();
    });
  }

  ngOnInit(): void {
    this.url = this.configService.configData.url.canvas + "catchthetrend/index.html";
    if (this.iframeUrl) {

    } else {
      this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    }
    // this.currentOver = 1;
    // this.catchTrendComponent.getGameInit();
    this.overSup = 'st';
    setTimeout(() => this.onPredictOversInit(), 1000);
    document.getElementById('iframeElement')?.style.display = "block";
    this.isReal = this.catchTrendComponent.matchDetails?.isReal;
  }

  ngAfterViewInit() {
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  onPredictOversInit() {
    this.predictOverInit = true;
  }

  updateScoreCard(scoreCardDetails) {
    this.scoreCardDetails = scoreCardDetails;
    this.runningOver = Math.floor(this.scoreCardDetails.overStat.overCount);
    this.checkForOverComplete();

    if (this.runningOver > 9) {
      // quick fix scroll issue
      document.getElementById('overs-btns')?.scrollLeft += 400;
    }

    if ((this.currentOver <= (this.runningOver + 1)) && (this.overComplete || this.predictOverInit)) {
      this.predictOverInit = false;
      this.changeOver(1);
      $('.notification').css("display", "none");
      this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails);
      this.checkForNotification(this.currentOver);
      // revert
      // if (this.runningOver >= 19) {
      //   this.currentOver = 20;
      // }
    }
    if (this.currentOver - 1 in this.betPlacedOvers) {
      this.checkForNotification(this.currentOver);
    }
    this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails);
    if (this.notifyOver && this.betSessionResponse && this.betSessionResponse["inning" + this.currentInnings] && ((Math.ceil(this.betSessionResponse["inning" + this.currentInnings].overs) == this.currentOver))) {
      this.notifyOverNumber = this.currentOver;
      // this.updateNotification(this.betSessionResponse);
    }
    this.updateSup();
    this.updateMySelections();
  }

  updateMySelections() {
    this.mySelections = []
    let selectionHistory = this.gameInitData?.gameHistory
    selectionHistory = selectionHistory?.filter(obj => {
      return obj?.betSession !== "1_" && obj.inningNumber === this.currentInnings
    })

    selectionHistory = selectionHistory?.sort((betObj1, betObj2) => (
      betObj1?.overNumber < betObj2?.overNumber ? -1 : 1
    ))


    for (var index = 0; index <= selectionHistory?.length;) {
      if (selectionHistory[index]?.betSession === '3_') {
        if (index + 2 < selectionHistory?.length && selectionHistory[index + 2].overNumber >= this.runningOver) {
          for (var inner_index = index; inner_index <= index + 2; inner_index++) {
            var betData = selectionHistory[inner_index]
            this.mySelections.push({
              overNumber: betData.overNumber + 1,
              selections: betData.selections,
              status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
            })
          }
          break;
        } else {
          index = index + 3;
          continue;
        }
      } else if (selectionHistory[index]?.betSession === '5_') {
        if (index + 4 < selectionHistory?.length && selectionHistory[index + 4].overNumber >= this.runningOver) {
          if (selectionHistory[index + 2]?.overNumber >= this.runningOver) {
            for (inner_index = index; inner_index <= index + 2; inner_index++) {

              if (selectionHistory[inner_index].overNumber >= this.runningOver) {

                var betData = selectionHistory[inner_index]
                this.mySelections.push({
                  overNumber: betData.overNumber + 1,
                  selections: betData.selections,
                  status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
                })

                betData = selectionHistory[inner_index + 1]
                this.mySelections.push({
                  overNumber: betData.overNumber + 1,
                  selections: betData.selections,
                  status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
                })

                betData = selectionHistory[inner_index + 2]
                this.mySelections.push({
                  overNumber: betData.overNumber + 1,
                  selections: betData.selections,
                  status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
                })
                break;
              }
            }
            break;
          } else if (selectionHistory[index + 4]?.overNumber >= this.runningOver) {
            var betData = selectionHistory[index + 2]
            this.mySelections.push({
              overNumber: betData.overNumber + 1,
              selections: betData.selections,
              status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
            })

            betData = selectionHistory[index + 3]
            this.mySelections.push({
              overNumber: betData.overNumber + 1,
              selections: betData.selections,
              status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
            })

            betData = selectionHistory[index + 4]
            this.mySelections.push({
              overNumber: betData.overNumber + 1,
              selections: betData.selections,
              status: this.runningOver >= betData.overNumber ? 'locked' : 'pending'
            })
            break;
          }
        } else {
          index = index + 5;
          continue;
        }
      } else {
        index = index + 1;
      }
    }

    // this.mySelections = []
    // for (const [overNum, overSelections] of Object.entries(this.betPlacedOvers)) {
    //   if(this.mySelections.length < 3 && overNum >= this.runningOver) {
    //     this.mySelections.push({overNumber: parseInt(overNum)+1, selections: overSelections, status: this.runningOver >= parseInt(overNum)+1 ? 'locked' : 'pending'})
    //   }
    // }

    if (document.referrer && document.referrer.indexOf("localhost") < 0) {
      window.parent.postMessage(
        {
          action: "b2bresize",
          scrollHeight: document.getElementById('display-game')?.clientHeight! + document.getElementById('score-container-wrapper')?.clientHeight!,
        },
        document.referrer
      );
    }

    
  }

  updateNotification(betSessionResponse) {
    if (this.notifyOverNumber == null || (!(this.notifyOverNumber >= 0))) {
      this.notifyOverNumber = this.currentOver;
    }
    this.notifyOver = true;
    $('.notification').css("display", "inline-block");
    let lastSixBalls = [];
    let deliveries = betSessionResponse["inning" + this.currentInnings].deliveries;
    let ballsBowled = deliveries.length % 6;
    for (let i = 1; i <= 6; i++) {
      if (i <= ballsBowled) {
        lastSixBalls.unshift(deliveries[deliveries.length - i]);
      }
      if (ballsBowled == 0) {
        lastSixBalls.unshift(deliveries[deliveries.length - i]);
      }
    }
    $('.fours .notification-circle').removeClass('notification-circle-green');
    $('.sixes .notification-circle').removeClass('notification-circle-green');
    $('.wickets .notification-circle').removeClass('notification-circle-green');
    $('.dots .notification-circle').removeClass('notification-circle-green');
    let fours = 0;
    let sixes = 0;
    let wickets = 0;
    let dotBalls = 0;
    for (let j = 0; j < this.selectedOverScore.length; j++) {
      if (this.selectedOverScore[j] == "4") {
        ++fours;
      } else if (this.selectedOverScore[j] == "6") {
        ++sixes;
      } else if (this.selectedOverScore[j] == "W") {
        ++wickets;
      } else if (this.selectedOverScore[j] == "0") {
        ++dotBalls;
      }
    }
    if (fours == this.fourNotification) {
      $('.fours .notification-circle').addClass('notification-circle-green');
    } else {
      $('.fours .notification-circle').removeClass('notification-circle-green');
    }
    if (sixes == this.sixNotification) {
      $('.sixes .notification-circle').addClass('notification-circle-green');
    } else {
      $('.sixes .notification-circle').removeClass('notification-circle-green');
    }
    if (wickets == this.wicketNotification) {
      $('.wickets .notification-circle').addClass('notification-circle-green');
    } else {
      $('.wickets .notification-circle').removeClass('notification-circle-green');
    }
    if (dotBalls == this.dotBallNotification) {
      $('.dots .notification-circle').addClass('notification-circle-green');
    } else {
      $('.dots .notification-circle').removeClass('notification-circle-green');
    }

    if (this.currentOver != this.notifyOverNumber) {
      $('.notification').css("display", "none");
    } else {
      $('.notification').css("display", "inline-block");
    }
    $('.fours .notification-circle').removeClass('notification-circle-grey');
    $('.sixes .notification-circle').removeClass('notification-circle-grey');
    $('.wickets .notification-circle').removeClass('notification-circle-grey');
    $('.dots .notification-circle').removeClass('notification-circle-grey');
    if (ballsBowled == 0) {
      setTimeout(() => {
        $('.notification').css("display", "none");
        this.notifyOverNumber = null;
        this.notifyOver = false;
      }, 2000)
    }

  }
  resetPredictions = (over) => {
    if (over - 1 in this.betPlacedOvers) {
      this.predictions = {
        "fours": parseInt(this.betPlacedOvers[over - 1][0]),
        "sixes": parseInt(this.betPlacedOvers[over - 1][1]),
        "wickets": parseInt(this.betPlacedOvers[over - 1][2]),
        "dots": parseInt(this.betPlacedOvers[over - 1][3])
      }
      if (over - 1 <= this.runningOver) {
        $('.bet-left').css("visibility", "hidden");
        $('.bet-right').css("visibility", "hidden");
      } else {
        $('.bet-left').css("visibility", "visible");
        $('.bet-right').css("visibility", "visible");
      }
    } else {
      this.predictions = {
        "fours": 0,
        "sixes": 0,
        "wickets": 0,
        "dots": 0
      }
      $('.bet-left').css("visibility", "visible");
      $('.bet-right').css("visibility", "visible");
    }
  }

  jumpToOver(value: any, isOverComplete?: boolean) {
    
    this.resetPredictions(value);
    $('#carouselExampleControls').carousel(isOverComplete ? value + 1 : value - 1);
    this.currentOver = value
    if (this.currentOver != this.notifyOverNumber) {
      $('.notification').css("display", "none");
    } else {
      if (this.notifyOver) {
        $('.notification').css("display", "inline-block");
      }
    }
    this.updatePlayButtonText();
    this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails);
    this.checkForNotification(this.currentOver);
    this.updateSup();
  }

  changeOver(value: any) {
    // debugger;
    this.resetPredictions(this.currentOver + value);
    if (value > 0) {
      this.currentOver += value;
      //revert
      if (this.currentOver < 20) {
        this.currentOver += value;
      }
    } else {
      if (this.currentOver > 1) {
        this.currentOver += value;
      }
    }
    if (this.currentOver != this.notifyOverNumber) {
      $('.notification').css("display", "none");
    } else {
      if (this.notifyOver) {
        $('.notification').css("display", "inline-block");
      }
    }
    this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails);
    this.checkForNotification(this.currentOver);
    this.updateSup();
    this.updatePlayButtonText();
  }

  updateSup() {
    let currentOverSplit = this.currentOver.toString().split('');
    let currentOverNumber = currentOverSplit[currentOverSplit.length-1];
    if (currentOverNumber == 1) {
      this.overSup = 'st';
    } else if (currentOverNumber == 2) {
      this.overSup = 'nd';
    } else if (currentOverNumber == 3) {
      this.overSup = "rd";
    } else {
      this.overSup = 'th';
    }
  }

  scrollTowards(direction: string) {
    if (direction == 'left') {
      document.getElementById('overs-btns')?.scrollLeft -= 100;
    } else {
      document.getElementById('overs-btns')?.scrollLeft += 100;
    }
  }

  changePredictions(type: any, value: any) {
    if (value > 0) {
      document.getElementById('bet-left-' + type + '-' + (this.currentOver - 1))?.style.opacity = 1;
      if (this.predictions[type] == 6) {
        return;
      } else if (this.predictions[type] == 5) {
        this.predictions[type] += value;
        document.getElementById('bet-right-' + type + '-' + (this.currentOver - 1))?.style.opacity = 0.3;
      } else {
        this.predictions[type] += value;
      }
    } else {
      document.getElementById('bet-right-' + type + '-' + (this.currentOver - 1))?.style.opacity = 1;
      if (this.predictions[type] == 0) {
        document.getElementById('bet-left-' + type + '-' + (this.currentOver - 1))?.style.opacity = 0.3;
      } else if (this.predictions[type] == 1) {
        this.predictions[type] += value;
        document.getElementById('bet-left-' + type + '-' + (this.currentOver - 1))?.style.opacity = 0.3;
      } else {
        this.predictions[type] += value;
      }
    }
  }
  onPlayClicked() {
    // By default the selected over value should be 1
    this.catchTrendComponent.selectedOver = 1;
    if (this.playButtonText === 'View Betslip') {
      this.catchTrendComponent.toggleBetDetailsModal();
    } else {
      //play or update case
      let data = {
        betSelections: [this.predictions.fours, this.predictions.sixes, this.predictions.wickets, this.predictions.dots],
        overNumber: this.currentOver,
        inningNumber: this.currentInnings
      }
      if (this.playButtonText === 'Update') {
        data.isUpdate = true
      }
      // data.isReal = true
      this.catchTrendComponent.onBetPlaced(data, "");
    }
  }

  checkForOverComplete() {
    // ;
    let deliveries = this.scoreCardDetails.overStat.deliveries;
    let ballsBowled = deliveries.length % 6;
    if (ballsBowled == 0) {
      this.overComplete = true;
      this.selectedOverScore = [];
      this.catchTrendComponent.getGameInit();
      this.jumpToOver(this.currentOver, true);
    } else {
      this.overComplete = false;
    }

    // call if the current over is in bet placed overs
    if ((this.currentOver - 1) in this.betPlacedOvers) {
      this.updateCorrectSelections();
    }
  }

  getOverScore(currentOver, scoreCardDetails) {
    if (scoreCardDetails) {
      let deliveries = [...scoreCardDetails.overStat.deliveries];
      let overs = [];
      while (deliveries.length > 0) {
        overs.push(deliveries.splice(0, 6));
      }
      if (overs[currentOver - 1] && (currentOver - 1) <= this.runningOver) {
        let fours = 0;
        let sixes = 0;
        let wickets = 0;
        let dotBalls = 0;
        $('.bet-left').css("visibility", "hidden");
        $('.bet-right').css("visibility", "hidden");
        for (let j = 0; j < overs[currentOver - 1].length; j++) {
          if (overs[currentOver - 1][j] == "4") {
            ++fours;
          } else if (overs[currentOver - 1][j] == "6") {
            ++sixes;
          } else if (overs[currentOver - 1][j] == "W") {
            ++wickets;
          } else if (overs[currentOver - 1][j] == "0") {
            ++dotBalls;
          }
        }
      } else {
        setTimeout(() => {
          if (this.playButtonText == 'Play') {
            $('.bet-left').css("visibility", "visible");
            $('.bet-right').css("visibility", "visible");
          }
        }, 0);
      }
      return overs[currentOver - 1];
    }
  }

  checkForNotification(overNumber) {
    this.betPlacedOvers = {};
    let playerBets = this.catchTrendComponent.gameInitData;
    if (playerBets?.gameHistory?.length > 0) {
      playerBets.gameHistory.forEach(element => {
        if (element.inningNumber == this.currentInnings) {
          this.betPlacedOvers[element.overNumber] = element.selections;
        }
      });
    }
    let betData = [];
    if (!playerBets) {
      return;
    }
    for (let i = 0; i < playerBets.gameHistory.length; i++) {
      betData.push(playerBets.gameHistory[i]);
    }
    let hasBet = false;
    if (betData.length) {
      for (let i = 0; i < betData.length; i++) {
        if (betData[i] && (betData[i].inningNumber == this.currentInnings)) {
          if (overNumber == (betData[i].overNumber + 1)) {
            hasBet = true
            $('.notification').css("display", "inline-block");
            let fours = betData[i].selections[0];
            let sixes = betData[i].selections[1];
            let wickets = betData[i].selections[2];
            let dotBalls = betData[i].selections[3];
            let overScore = this.getOverScore(overNumber, this.scoreCardDetails);
            this.fourNotification = 0;
            this.sixNotification = 0;
            this.wicketNotification = 0;
            this.dotBallNotification = 0;
            if (overScore) {
              for (let j = 0; j < overScore.length; j++) {
                if (overScore[j] == "4") {
                  ++this.fourNotification;
                } else if (overScore[j] == "6") {
                  ++this.sixNotification;
                } else if (overScore[j] == "W") {
                  ++this.wicketNotification;
                } else if (overScore[j] == "0") {
                  ++this.dotBallNotification;
                }
              }
              if (fours == this.fourNotification) {
                $('.fours .notification-circle').addClass('notification-circle-green');
              } else {
                $('.fours .notification-circle').removeClass('notification-circle-green');
              }
              if (sixes == this.sixNotification) {
                $('.sixes .notification-circle').addClass('notification-circle-green');
              } else {
                $('.sixes .notification-circle').removeClass('notification-circle-green');
              }
              if (wickets == this.wicketNotification) {
                $('.wickets .notification-circle').addClass('notification-circle-green');
              } else {
                $('.wickets .notification-circle').removeClass('notification-circle-green');
              }
              if (dotBalls == this.dotBallNotification) {
                $('.dots .notification-circle').addClass('notification-circle-green');
              } else {
                $('.dots .notification-circle').removeClass('notification-circle-green');
              }
              $('.fours .notification-circle').removeClass('notification-circle-grey');
              $('.sixes .notification-circle').removeClass('notification-circle-grey');
              $('.wickets .notification-circle').removeClass('notification-circle-grey');
              $('.dots .notification-circle').removeClass('notification-circle-grey');
            } else {
              $('.fours .notification-circle').addClass('notification-circle-grey');
              $('.sixes .notification-circle').addClass('notification-circle-grey');
              $('.wickets .notification-circle').addClass('notification-circle-grey');
              $('.dots .notification-circle').addClass('notification-circle-grey');
              if (overNumber - 1 <= this.runningOver) {
                $('.bet-left').css("visibility", "hidden");
                $('.bet-right').css("visibility", "hidden");
              }
            }
          }
        }
      }
    } else {
      $('.notification').css("display", "none");
      return;
    }

  }

  isBetPlacedOver(overNumber): boolean {
    var isBetPlacedOverinCurrentInnings = false;
    this.gameInitData?.gameHistory?.forEach((betOverElement) => {
      if (betOverElement['overNumber'] === overNumber && betOverElement['inningNumber'] === this.currentInnings) {
        isBetPlacedOverinCurrentInnings = true;
      }
    })
    return overNumber in this.betPlacedOvers && isBetPlacedOverinCurrentInnings;
  }

  ngAfterViewInit(): void {

  }

  getWhatToDisplayInOver(overNumber): number {
    if (this.isReal || this.runningOver < overNumber) return overNumber;
    return (overNumber - 1) in this.freePlayBetData ? this.freePlayBetData[overNumber - 1] : 0;
  }

  // Need to optimize the below method
  getWhichColorToDisplay(overNumber): string {
    if (overNumber === this.currentOver) return 'orange';

    if (this.isReal) return 'white';

    if (overNumber <= this.runningOver) {
      if (!((overNumber - 1) in this.freePlayBetData)) return '#DE494C';
      else {
        return this.freePlayBetData[overNumber - 1] === 0 ? '#DE494C' : '#438F5E';
      }
    }
    return 'white';
  }
  // Need to optimize the below method
  getWhichBGToDisplay(overNumber): string {
    let textColor = this.getWhichColorToDisplay(overNumber);
    // red background
    if (textColor === '#DE494C') return '#F6CBCC';
    // green background
    if (textColor === '#438F5E') return '#C0E2CC';
    // fade out 
    if ((overNumber - 1 in this.betPlacedOvers) && (overNumber - 1 > this.runningOver)) {
      return '#2c4bb1';
    }
    return '#142C80';
  }

  getWhichBorderToDisplay(overNumber): string {
    if (this.isReal && (overNumber - 1) in this.correctSelections) {
      let isWon = this.correctSelections[overNumber - 1] === 4;
      return `2px solid ${isWon ? ' #438F5E' : '#DE494C'}`;
    }
  }

  updatePlayButtonText(): string {
    
    if ((this.currentOver - 1) in this.betPlacedOvers) {
      // update or result or view bet slip
      if (this.currentOver < (this.runningOver + 1) && (this.currentOver - 1) in this.correctSelections) {
        this.playButtonText = `Correct Selections = ${this.correctSelections[this.currentOver - 1]}`
      } else if (this.currentOver === (this.runningOver + 1)) {
        this.playButtonText = 'View Betslip'
      } else {
        this.playButtonText = 'Update'
      }
    } else {
      // play or not played or current over
      if (this.runningOver) {
        if (this.currentOver < (this.runningOver + 1)) {
          this.playButtonText = 'Not Played';
        } else if (this.currentOver > (this.runningOver + 1)) {
          this.playButtonText = 'Play';
        } else {
          this.playButtonText = 'Current Over';
        }
      } else {
        this.playButtonText = 'Play';
      }
    }
  }

  updateCorrectSelections(): number {
    this.selectedOverScore = this.getOverScore(this.currentOver, this.scoreCardDetails)
    let betSelections = this.betPlacedOvers[this.currentOver - 1];
    let currentOverPoints = 0
    let fours = 0;
    let sixes = 0;
    let wickets = 0;
    let dotBalls = 0;
    if (this.selectedOverScore && betSelections) {
      for (let j = 0; j < this.selectedOverScore?.length; j++) {
        if (this.selectedOverScore[j] == "4") {
          ++fours;
        } else if (this.selectedOverScore[j] == "6") {
          ++sixes;
        } else if (this.selectedOverScore[j] == "W") {
          ++wickets;
        } else if (this.selectedOverScore[j] == "0") {
          ++dotBalls;
        }
      }
      if (betSelections[0] == fours) ++currentOverPoints;
      if (betSelections[1] == sixes) ++currentOverPoints;
      if (betSelections[2] == wickets) ++currentOverPoints;
      if (betSelections[3] == dotBalls) ++currentOverPoints;
      this.correctSelections[this.currentOver - 1] = currentOverPoints;
    }
  }

  isPlayButtonDisabled(): boolean {
    return this.playButtonText.startsWith('Correct Selections');
  }
}
