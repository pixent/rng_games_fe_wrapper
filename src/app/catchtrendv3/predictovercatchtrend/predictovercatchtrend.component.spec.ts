import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictOverCatchTrendComponent } from './predictovercatchtrend.component';

describe('PredictOverCatchTrendComponent', () => {
  let component: PredictOverCatchTrendComponent;
  let fixture: ComponentFixture<PredictOverCatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredictOverCatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictOverCatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
