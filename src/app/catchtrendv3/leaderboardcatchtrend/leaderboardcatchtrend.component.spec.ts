import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardCatchTrendComponent } from './leaderboardcatchtrend.component';

describe('LeaderboardCatchTrendComponent', () => {
  let component: LeaderboardCatchTrendComponent;
  let fixture: ComponentFixture<LeaderboardCatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderboardCatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardCatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
