import { ViewportScroller } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as moment from "moment";
import { Config } from "protractor";
import { Subscription } from "rxjs/internal/Subscription";
import { ConfigService } from "../../services/config.service";
import { CatchTrendComponent } from "./../catchtrend.component";
@Component({
  selector: "app-leaderboardcatchtrend",
  templateUrl: "./leaderboardcatchtrend.component.html",
  styleUrls: ["./leaderboardcatchtrend.component.css"],
})
export class LeaderboardCatchTrendComponent implements OnInit {
  leaderBoardData: any = [];
  matchDetails: any = [];
  currentWeekNumber: any = 1;
  weekNumbers: Number[] = [];
  currentWeek: any;
  value: any;
  date: any = new Date();
  currentMonth: any = '';
  startDate: any;
  endDate: any;
  prize: Number[] = [50000, 10000, 5000];
  leaderboardType: any = "match";
  matchId: any;
  active: Number = 1;
  playerId: Number = 0;
  currentSunday: any = 0;
  gameCode: any;
  catchSubscription: Subscription;
  betSessionResponse: any;
  playerBets: any;
  currentInnings: any;

  constructor(
    public configService: ConfigService,
    private changeDetectorRef: ChangeDetectorRef,
    private http: HttpClient,
    public viewportScroller: ViewportScroller,
    private catchTrendComponent: CatchTrendComponent,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.catchSubscription = this.catchTrendComponent.catchTrendSubject.subscribe((response: any) => {

      if (response.message == "betsessionresponse") {
        this.getWeekNumber();
        this.selectedWeek();
      } else if (response.message == "gameinit") {
        this.getWeekNumber();
        this.selectedWeek();
      } else {
        this.getWeekNumber();
        this.selectedWeek();
      }
    });
   }

  ngOnInit(): void {
    if (
      window.history.state.item == "Lobby" ||
      window.history.state.redirect == "main"
    ) {
      this.leaderboardType = "date";
    } else {
      this.leaderboardType = "match";
      this.active = 2;
    }
    document.getElementById("iframeElement")!.style.display = "none";
    this.getWeekNumber();
    this.selectedWeek();

    this.playerId = JSON.parse(localStorage.matchDetails).playerId;
  }

  ngAfterContentInit(): void {
    // console.log('After Content Init')
    this.leaderBoardData = this.configService.configData.leaderboard;
    this.matchDetails = this.catchTrendComponent.matchDetails;

    for (let p in this.leaderBoardData) {
      this.leaderBoardData[p].random = Math.ceil(Math.random() * 4);
    }
    this.changeDetectorRef.detectChanges();
    this.getLeaderBoardData();
    this.viewportScroller.scrollToPosition([0, window.history.state.item]);

    let currentDate = new Date();
    let lastday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay() + 7));
    this.currentSunday = lastday.getDate();
    this.currentMonth = lastday.toLocaleString('default', { month: 'long' });

  }

  getLeaderBoardData() {
    this.matchId = this.catchTrendComponent.matchDetails.matchId;
    this.gameCode = this.catchTrendComponent.gameCode;
    let startDate = this.startDate.split("-");
    let endDate = this.endDate.split("-");
    
    let getStartMonth = startDate[1];
    let getEndMonth = endDate[1];

    let getStartDay = startDate[2].split(" ")[0] < 10 ? "0" + startDate[2].split(" ")[0] : startDate[2].split(" ")[0];
    let getEndDay = endDate[2].split(" ")[0] < 10 ? "0" + endDate[2].split(" ")[0] : endDate[2].split(" ")[0];
    let leaderBoardUrl = this.configService.configData.url.socket + "leaderboard?gameName=" + this.gameCode + "&startDate=" + startDate[0] + "-" + getStartMonth + "-" + getStartDay + " 00:00" +
      "&endDate=" + endDate[0] + "-" + getEndMonth + "-" + getEndDay + " 00:00";
    if (this.active == 2) {
      leaderBoardUrl = leaderBoardUrl + "&matchId=" + this.catchTrendComponent.matchDetails.matchId;
    }
    this.http.get(leaderBoardUrl).subscribe(
      (data: any) => {
        this.leaderBoardData = data;
        if (this.leaderBoardData) {
          for (let p = 0; p < this.leaderBoardData.length; p++) {
            this.leaderBoardData[p].rank = p + 1;
            if (p === 0 || p === 1 || p === 2) {
              this.leaderBoardData[p].prize = this.prize[p];
            } else {
              this.leaderBoardData[p].prize = 0;
            }
          }
        }

        for (let p in this.leaderBoardData) {
          if (this.leaderBoardData[p].playerId == this.matchDetails.playerId) {
            let temp = this.leaderBoardData.splice(p, 1);
            this.leaderBoardData.unshift(temp[0]);
          }
        }
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getWeekNumber() {
    let currentdate: any = new Date();
    let weeknumber = moment(currentdate).isoWeek();
    this.currentWeekNumber = weeknumber;

    this.currentWeek = weeknumber;
    this.weekNumbers = new Array(weeknumber);
  }

  selectedWeek(value: any = this.currentWeekNumber) {
    this.currentWeekNumber = value;

    let startDateFromWeek = this.getStartDate(this.currentWeekNumber, new Date().getFullYear());
    let getMonthFromWeek =
      startDateFromWeek.getMonth() < 9
        ? "0" + (startDateFromWeek.getMonth() + 1)
        : startDateFromWeek.getMonth() + 1;
    let startDate =
      startDateFromWeek.getFullYear() +
      "-" +
      getMonthFromWeek +
      "-" +
      startDateFromWeek.getDate() +
      " 00:00";

    let startDayNumber = startDateFromWeek.getDay();
    let sundayNumber = 7 - startDayNumber;

    let endDateFromWeek = new Date(
      startDateFromWeek.getFullYear(),
      startDateFromWeek.getMonth(),
      startDateFromWeek.getDate() + sundayNumber
    );
    let getMonthFromEndDate = endDateFromWeek.getMonth() < 9 ? "0" + (endDateFromWeek.getMonth() + 1) : endDateFromWeek.getMonth() + 1;
    let endDay = endDateFromWeek.getFullYear() + "-" + getMonthFromEndDate + "-" + endDateFromWeek.getDate() + " 00:00";

    this.startDate = startDate;
    this.endDate = endDay;
    this.getLeaderBoardData();
  }
  getStartDate(w: any, y: any) {
    let date = new Date(y, 0, 1 + (w - 1) * 7);
    let dow = date.getDay();
    let ISOweekStart = date;
    if (dow <= 4) ISOweekStart.setDate(date.getDate() - date.getDay() + 1);
    else ISOweekStart.setDate(date.getDate() + 8 - date.getDay());
    return ISOweekStart;
  }

  incrementOrDecrementWeek(val: any) {
    let weekNumber = this.currentWeekNumber;
    if (val === "next") {
      weekNumber = weekNumber + 1;
      if (weekNumber > this.currentWeek) {
        weekNumber = 1;
      }
      this.currentWeekNumber = weekNumber;
    }
    if (val === "prev") {
      weekNumber = weekNumber - 1;
      if (weekNumber <= 0) {
        weekNumber = this.currentWeek;
      }
      this.currentWeekNumber = weekNumber;
    }
    this.selectedWeek(this.currentWeekNumber);
  }

  onNavChange(type: any) {
    if (type === "matchwise") {
      this.active = 2;
    }
    this.getLeaderBoardData();
  }

  backButtonClicked() {
    this.catchTrendComponent.isNotLeaderBoard = true;
    if (history.state.redirect === "main") {
      window.history.back();
    } else {
      this.router.navigate(["../"], {
        relativeTo: this.route,
        state: { redirect: "back" },
      });
    }
  }
}
