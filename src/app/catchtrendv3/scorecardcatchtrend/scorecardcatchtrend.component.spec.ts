import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorecardCatchTrendComponent } from './scorecardcatchtrend.component';

describe('ScorecardCatchTrendComponent', () => {
  let component: ScorecardCatchTrendComponent;
  let fixture: ComponentFixture<ScorecardCatchTrendComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScorecardCatchTrendComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorecardCatchTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
