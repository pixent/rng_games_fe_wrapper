import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../../services/config.service';

@Component({
  selector: 'app-scorecardcatchtrend',
  templateUrl: './scorecardcatchtrend.component.html',
  styleUrls: ['./scorecardcatchtrend.component.css']
})
export class ScorecardCatchTrendComponent implements OnInit {

  url: any;
  iframeUrl: any;

  constructor(public sanitizer: DomSanitizer, public configService: ConfigService ) { }

  ngOnInit(): void {
    this.url = this.configService.configData.url.canvas + "catchthetrend/index.html";
    if(this.iframeUrl){

    } else{
      this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    }
    document.getElementById('iframeElement')!.style.display = "none";
  }

}


