import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from './services/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'cricket-app';
  gameName = '';

  constructor(private route: ActivatedRoute, private router: Router, private configService: ConfigService) {

  }

  ngOnInit() {
    if (this.mobileAndTabletCheck()) {
      document.body.style.maxWidth = "100%";
      document.body.setAttribute("base","mobile");
      this.configService.configData.isMobile = true;
    } else {
      document.body.setAttribute("base","desktop");
      this.configService.configData.isMobile = false;
    }
    if (this.checkIOSDevice()) {
      $('head meta[name=viewport]').remove();
      $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />');
    }
    this.checkPassword();
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({
        action: "b2bresize",
        scrollHeight: (this.configService.configData.operatorId == "fortune") ? window.innerHeight : window.outerHeight
      }, (document.referrer || (window.parent && window.parent.location.href)));
      document.body.style.overflow = "auto";
    }
    if (window.location.href.indexOf("dev-games.sportsit-tech.net/lobby") >= 0 && document.referrer) {
      this.router.navigate(['errorpage']);
    }
    // if (window.location.href.indexOf("dev-games.sportsit-tech.net/lobby") >= 0 || window.location.href.indexOf("localhost") >= 0) {
    //   this.router.navigate(["login"]);
    // }
    // this.route.queryParams.subscribe(
    //   params => {
    //     if (params['gamecode']) {
    //       let gamecode = params['gamecode'];
    //       if (params['gamecode'] == "football_penalty_shootout") {
    //         gamecode = 'penaltyshootout';
    //       }
    //       this.router.navigate(['home/' + gamecode], { queryParams: { gameName: gamecode } });
    //     }
    //   });
    document.body.setAttribute("operator",this.configService.configData.operatorId);
  }

  ngOnDestroy() {
    this.configService.configData.currUsername = '';
    // localStorage.removeItem('currUsername')
  }

  checkPassword() {
    if (window.location.href.indexOf("pixentstudio") >= 0) {
      if (localStorage.getItem('authenticatedUser') && (localStorage.getItem('authenticatedUser') == 'pixent1234')) {
        this.navigateToGame();
      } else {
        let enteredPassword = prompt("Please enter a password to continue to the site", "Password");
        if (enteredPassword == "pixent1234") {
          localStorage.setItem('authenticatedUser', "pixent1234");
        } else {
          this.checkPassword();
        }
      }
    } else if (window.location.href.indexOf("https://dev-games-rgs.sportsit-tech.net/lobby") >= 0) {
      // if (localStorage.getItem('authenticatedUser') && (localStorage.getItem('authenticatedUser') == 'truewave4321')) {
      //   this.navigateToGame();
      // } else {
      //   let enteredPassword = prompt("Please enter a password to continue to the site", "Password");
      //   if (enteredPassword == "truewave4321") {
      //     localStorage.setItem('authenticatedUser', "truewave4321");
      //   } else {
      //     this.checkPassword();
      //   }
      // }
      this.navigateToGame();
    } {
      this.navigateToGame();
    }
  }

  navigateToGame() {
    let urlStr = window.location.href.split('?')[1];
    if (urlStr) {
      let urlParams: any = {};
      let params = urlStr.split("&");
      for (let p in params) {
        let param = params[p].split('=');
        urlParams[param[0]] = param[1];
      }
      let gameCode = "";
      if (urlParams['gameName']) {
        urlParams['gameCode'] = urlParams['gameName'];
      }
      this.configService.configData.urlParams = urlParams;
      if (urlParams['gamecode']) {
        if (urlParams['gamecode'].toLowerCase() == "football_penalty_shootout") {
          gameCode = 'penaltyshootout';
        }
      } else {
        if (urlParams['gameCode']) {
          if (urlParams['gameCode'].toLowerCase().indexOf("penaltyshootout") >= 0) {
            gameCode = 'penaltyshootout';
          } else if (urlParams['gameCode'].toLowerCase().indexOf("catchthetrend") >= 0 || urlParams['gameCode'].toLowerCase().indexOf("virtualcatchthetrend") >= 0) {
            gameCode = 'catchthetrend';
          } else if (urlParams['gameCode'].toLowerCase().indexOf("firstwicketpartnership") >= 0) {
            gameCode = 'firstwicketpartnership';
          } else if (urlParams['gameCode'].toLowerCase().indexOf("defender") >= 0) {
            gameCode = 'defender';
          } else if (urlParams['gameCode'].toLowerCase().indexOf("homerun") >= 0) {
            gameCode = 'homerun';
          } else {

          }
        }
      }
      if (gameCode) {
        if (this.configService.configData.isRGSSetup && (urlParams.gameCode == "virtualCatchTheTrend" || urlParams.gameCode == "catchTheTrend")) {
          this.router.navigate([""], { state: {gameName: this.configService.configData.literals[urlParams.gameCode]}});
        } else {
          this.router.navigate(['home/' + gameCode], { queryParams: { gameName: gameCode } });
        }
      }
      if (urlParams['playerId']) {
        try{
          localStorage.setItem('playerId', urlParams['playerId']);
          this.configService.configData.playerId = urlParams['playerId'];
        }
        catch(err) {
          this.configService.configData.playerId = urlParams['playerId'];
        }
        // if(window.localStorage) {
        //   localStorage.setItem('playerId', urlParams['playerId']);
        // } else {
        //   this.configService.configData.playerId = urlParams['playerId'];
        // }
      }
    } else {
      let playerId = Math.floor(Math.random() * 899999 + 100000);
      try{
        this.configService.configData.playerId = playerId.toString();
        localStorage.setItem('playerId', playerId.toString());
      }
      catch(err) {
        this.configService.configData.playerId = playerId.toString();
      }
    }
  }

  mobileAndTabletCheck() {
    let check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor);
    return check;
  };

  checkIOSDevice() {
    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  }

}
