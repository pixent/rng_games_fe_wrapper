import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FwphowtoplayscreensComponent } from './fwphowtoplayscreens.component';

describe('FwphowtoplayscreensComponent', () => {
  let component: FwphowtoplayscreensComponent;
  let fixture: ComponentFixture<FwphowtoplayscreensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FwphowtoplayscreensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FwphowtoplayscreensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
