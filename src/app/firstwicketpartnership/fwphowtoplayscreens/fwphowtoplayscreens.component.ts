import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { FirstwicketpartnershipComponent } from '../firstwicketpartnership.component';

@Component({
  selector: 'app-fwphowtoplayscreens',
  templateUrl: './fwphowtoplayscreens.component.html',
  styleUrls: ['./fwphowtoplayscreens.component.css']
})
export class FwphowtoplayscreensComponent implements OnInit {
  imagesConfig: any;
  showHTPScreens: boolean = false;
  showHTPScreensInDesktop: boolean = false;

  constructor(private fwpComponent: FirstwicketpartnershipComponent,public configService: ConfigService) { }

  ngOnInit(): void {

    this.imagesConfig = this.configService.textConfigData["htpScreensImagesPath"];
    // console.log("images config ", this.imagesConfig);
    if(!this.configService.configData.isMobile){
      this.showHTPScreensInDesktop = true;
    }

    // (<any>$(".carousel")).carousel();
  }

  onHowToPlaySkipClicked() {
    this.showHTPScreens = false;
    let skipCount = localStorage.getItem("howToPlaySkipCount");
    this.fwpComponent.showHowToPlayFullScreens = false;
    this.fwpComponent.setIframeHeight();
    localStorage.setItem("howToPlaySkipCount", (Number(skipCount)+1).toString());
  }

}
