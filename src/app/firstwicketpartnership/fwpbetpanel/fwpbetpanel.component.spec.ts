import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FwpbetpanelComponent } from './fwpbetpanel.component';

describe('FwpbetpanelComponent', () => {
  let component: FwpbetpanelComponent;
  let fixture: ComponentFixture<FwpbetpanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FwpbetpanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FwpbetpanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
