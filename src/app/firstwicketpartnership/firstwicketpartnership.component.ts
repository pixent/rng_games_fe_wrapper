import { ViewportScroller } from '@angular/common';
import { Component, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../services/config.service';
import { SocketService } from '../services/socket.service';
import { FwpbetpanelComponent } from './fwpbetpanel/fwpbetpanel.component';
import { FwpbottomComponent } from './fwpbottom/fwpbottom.component';
import * as moment from 'moment-timezone';
import { HttpClient } from '@angular/common/http';
import { FwpService } from '../services/fwp.service';
import { FwphistoryComponent } from './fwpbottom/fwphistory/fwphistory.component';

@Component({
  selector: 'app-firstwicketpartnership',
  templateUrl: './firstwicketpartnership.component.html',
  styleUrls: ['./firstwicketpartnership.component.css'],
  providers: [FwphistoryComponent]
})
export class FirstwicketpartnershipComponent implements OnInit {


  matchDetails: any = {};
  url: any;
  iframeUrl: any;
  betAmount: any = 0;
  cashOutAmount: any;
  loadedIframe: any;
  matchData: any;
  scoreData: any;
  resultData: any;
  cashOutMsg: any;
  lastBetAmount: any;
  autoplay: boolean = false;
  autoplayData: any;
  autoplayDataRes: any;
  gameCode: any;
  gameCodeBackend: any;
  matchStarted: boolean = false;
  scoreStarted: boolean = false;
  betSessionResponse: any = {};
  cashOutSessionResponse: any = {};
  cashOutSessionResponse2: any = {};
  showBetContent: boolean = true;
  showCashOutContent: any = false;
  currencyCode: any;
  multiplierData: any;
  autoplayWin: any = 0;
  manualBetAmount: any;
  manualBetAmount2: any;
  cashOutMultiplier: any;
  cashOutMultiplier2: any;
  userDetails: any = {};
  balanceFlag: boolean = true;
  messageSubscription: any;
  chatData: any = [];
  lastConnectionTime: any = 0;
  genericErrorMessage: any = '';
  lastAppHeight: any = 0;
  prevAccountBalance: any;
  totalWin: number = 0;
  totalLoss: number = 0;
  isShabiki: boolean = false;
  cashoutRequestSent: boolean = false;
  showGenericPopup: boolean = false;
  preloaderImg: any = '';
  defenderPreloaderImg: string = '';
  results: any;
  currentBetObj: any;
  newCashoutPopup: boolean = false;
  cashOutMatchId: any;
  cashedOutBets: any = [];
  gameName: any;

  @ViewChild(FwpbetpanelComponent, { static: true }) fwpBetPanelComponent!: FwpbetpanelComponent;
  @ViewChild(FwpbottomComponent, { static: true }) fwpBottomComponent!: FwpbottomComponent;

  showBottomHistory: boolean = true;
  playerAccountData: any;
  gameContainerHeight: any;
  defaultBet: any;
  maxBet: any;
  minBet: any;
  isPlayabet: boolean = false;
  isBetyetu: boolean = false;
  buttonConfig: any = {};
  textConfig: any = {};
  inputConfig: any = {};
  showAnimation: boolean = false;
  showHowToPlayFullScreens: boolean = false;
  currencySymbol: string | undefined;
  isStarbet: boolean = false;
  skipCount: any;
  isWeAreCasino: boolean = false;
  isEveryMatrix: boolean = false;
  preloaderStarted: boolean = false;
  htpInterval: any;
  balance: any;
  isStandAlone: any;
  isMobile: boolean = false;
  hideBackButton: boolean = false;
  setLoadingFromCanvas: boolean = false;
  loadPerc: number = 1;
  loadingInterval: any;
  canvasHeightForDemo: boolean = false;
  canvasDetailsObject: any = null;

  constructor(private route: ActivatedRoute, private router: Router, public sanitizer: DomSanitizer, public configService: ConfigService, public socketService: SocketService, public viewportScroller: ViewportScroller, private http: HttpClient, private fwpService: FwpService) {
    this.buttonConfig = this.configService.textConfigData["buttonConfig"];
    this.textConfig = this.configService.textConfigData["textConfig"];
    this.inputConfig = this.configService.textConfigData["inputConfig"];

    this.messageSubscription = this.fwpService.messageSubject.subscribe((message) => {
      this.serverResponse(message);
    });
  }

  ngOnInit(): void {
    this.gameName = "First Wicket Partnership";
    document.body.style.background = this.configService.configData.bgColor[this.configService.configData.operatorId] || this.configService.configData.bgColor.default;
    document.getElementById('display-game')!.style.visibility = "hidden";
    document.getElementById("body")!.style.background = "black !important";
    this.setParentWindowWidth('500px');
    this.url = this.configService.configData.url.canvas + "fwp/index.html";
    let preloaderImgPath = "assets/images/preloaderimg";
    if (this.configService.configData.preloaderImg.indexOf(this.configService.configData.operatorId) >= 0) {
      this.preloaderImg = preloaderImgPath + "_" + this.configService.configData.operatorId + ".png";
    } else {
      this.preloaderImg = preloaderImgPath + ".png";
    }

    this.defenderPreloaderImg = this.configService.configData.fwpPreloaderImg[this.configService.configData.operatorId] ?
      this.configService.configData.fwpPreloaderImg[this.configService.configData.operatorId] :
      this.configService.configData.fwpPreloaderImg["default"];
    // console.warn("plaptform id ", this.configService.configData.platformId);

    this.isShabiki = this.configService.configData.platformId == "2";
    this.isStarbet = this.configService.configData.platformId == "1";
    this.isWeAreCasino = this.configService.configData.platformId == "4";
    this.isEveryMatrix = this.configService.configData.operatorId == "everymatrix";

    // if (this.isWeAreCasino) {
    //   document.getElementById('game-container')!.style.width = "98%";
    // }

    // if (this.isShabiki || this.isStarbet)
    //   document.getElementById('preloader')!.style.maxHeight = '670px';

    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    let playerId = "";
    this.matchDetails = window.history.state.item || {};
    this.currencyCode = this.configService.configData.currencyCode;
    // console.log(this.matchDetails);
    if (window.location.href.indexOf("?") >= 0) {
      let urlStr = window.location.href.split("?")[1].split("&");
      let urlParams: any = {};
      for (let i = 0; i < urlStr.length; i++) {
        let params = urlStr[i].split("=");
        if (params[0] == "playerId") {
          urlParams.playerId = params[1];
        }
      }
      this.matchDetails.playerId = urlParams.playerId;
    }
    // if (this.matchDetails) {
    //   localStorage.setItem("matchDetails", JSON.stringify(this.matchDetails));
    // }
    // if (localStorage.getItem("matchDetails")) {
    //   this.matchDetails = JSON.parse(localStorage.getItem("matchDetails")!);
    // }
    if (!this.matchDetails.playerId) {
      this.matchDetails.playerId = Math.floor(Math.random() * 899999 + 100000);
    }
    // if (localStorage.getItem('playerId') != this.matchDetails.playerId) {
    //   localStorage.setItem('playerId', this.matchDetails.playerId)
    // }
    this.gameCodeBackend = "FWP";
    this.gameCode = "firstWicketPartnership";
    window.scrollTo(0, 0);

    // $('#preload-img').on('load', function() {
    //   $("#preloader-bar-box").css("visibility","visible");
    // });
    // this.initializeGame({});
    // this.sendLastTwentyGamesRequest();
    let params: any = document.cookie;

    if (params) {
      params = params.split("; ");
      for (let i = 0; i < params.length; i++) {
        let arr = params[i].split("=");
        if (arr[0] == "AppSession") {
          this.userDetails.accessToken = this.configService.configData.token || arr[1];
        } else if (arr[0] == "userId") {
          this.userDetails.customerId = this.configService.configData.playerId || arr[1];
        }
      }
    } else {
      // console.log('match details error', this.matchDetails);
      if ((!this.userDetails.accessToken || this.userDetails.accessToken == "undefined") && this.configService.configData.token) {
        this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
        this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
        this.userDetails.playerName = this.userDetails.customerId;
      }
      if (this.matchDetails.playerId == "undefined") {
        this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
        this.userDetails.customerId = this.getUUid();
      } else {
        this.userDetails.accessToken = this.configService.configData.token || this.matchDetails.playerId.toString();
        this.userDetails.customerId = this.matchDetails.playerId;
      }
    }
    // if (!this.userDetails.customerId && this.configService.configData.playerId) {
    //   this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
    //   this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
    //   this.userDetails.playerName = this.configService.configData.playerName || this.userDetails.customerId;
    // }
    this.userDetails.customerId = this.configService.configData.playerId || this.getUUid();
    this.userDetails.accessToken = this.configService.configData.token || this.getUUid();
    this.userDetails.playerName = this.configService.configData.playerName || this.userDetails.customerId;
    this.matchDetails.playerId = this.userDetails.customerId;
    // if (!this.userDetails.customerId) {
    //   this.userDetails.accessToken = this.matchDetails.playerId.toString();
    //   this.userDetails.customerId = this.matchDetails.playerId;
    // }

    this.balanceFlag = true;
    this.userDetails.userRegion = this.configService.configData.url.userRegion;
    if (this.currencyCode != "undefined") {
      this.getCurrencySymbol();
    }

    if (!(this.userDetails.accessToken && this.userDetails.customerId) && (window.location.href.indexOf("10bet") >= 0 || this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1" || this.configService.configData.platformId == "4")) {
      if (this.configService.configData.platformId == "2" || this.configService.configData.platformId == "1" || this.configService.configData.platformId == "4") {
        this.redirectToLogin();
      } else {
        window.parent.location.href = this.configService.configData.url.parentRegion + "/Login";
      }
      return;
    }
    let preloaderClientHeightStandAlone = this.configService.configData.isStandAlone;

    // console.warn("preloader client height ", document.getElementById("preloader")?.clientHeight)

    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: preloaderClientHeightStandAlone ? window.innerHeight : document.getElementById("preloader")?.clientHeight
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 100);
    if (this.fwpService && this.fwpService.socketService.gameType) {
      this.onSocketConnected();
      setTimeout(() => this.sendLastTwentyGamesRequest(), 100)
    }
    this.preloaderStarted = true;

    this.onResize('true');
    this.loadImages();
    this.getBetsFromConfig();

    this.isStandAlone = this.configService.configData.standalone;

    this.hideBackButton = this.configService.configData.hideBackButtonInHeaderComponent[this.configService.configData.operatorId] ?
      this.configService.configData.hideBackButtonInHeaderComponent[this.configService.configData.operatorId] :
      this.configService.configData.hideBackButtonInHeaderComponent["default"];

    this.isPlayabet = this.configService.configData.operatorId == 'playabet';
    this.isBetyetu = this.configService.configData.operatorId == 'betyetu';

    document.title = this.gameName;


    // setTimeout(() => this.closeHowToPlay("false"), 5000)

    this.showLoadingProgress();
    this.newCashoutPopup = this.configService.configData.useNewCashoutPopup || false;
    
    this.isMobile = this.configService.configData.isMobile;
  }

  // showHowToPlayScreenBasedOnSkipCount() {

  //   if (!localStorage) {

  //     console.warn("loacalStorage epmpty")
  //     this.fwpHowToPlayScreens.showHTPScreens = true;
  //     this.adjustHowToPlayHeight();

  //     return;
  //   }

  //   let skipCount = localStorage.getItem("howToPlaySkipCount");
  //   console.log("how to play screens ", this.fwpHowToPlayScreens)
  //   if (!skipCount && !this.preloaderStarted) {
  //     localStorage.setItem("howToPlaySkipCount", "1");
  //     this.fwpHowToPlayScreens.showHTPScreens = true;
  //   } else {
  //     this.fwpHowToPlayScreens.showHTPScreens = !(Number(skipCount) > 3)
  //   }

  //   this.skipCount = localStorage.getItem("howToPlaySkipCount");
  //   this.adjustHowToPlayHeight();

  // }

  // adjustHowToPlayHeight() {
  //   let resolutionAdjustmentHeight = this.configService.configData.defenderHowToPlayResolutionAdjustment;
  //   let operatorId = this.configService.configData.operatorId;
  //   let heightDeductionValue = resolutionAdjustmentHeight[operatorId] ? resolutionAdjustmentHeight[operatorId].headerFooterSpaceOfPlatform : resolutionAdjustmentHeight["default"].headerFooterSpaceOfPlatform
  //   let useExcessHeight = this.configService.configData.useExcessHeightProperty[operatorId] ? this.configService.configData.useExcessHeightProperty[operatorId] : this.configService.configData.useExcessHeightProperty["demo"];
  //   let excessHeight: any;
  //   if (useExcessHeight) {
  //     excessHeight = (window.parent as any).excessHeight
  //   } else {
  //     excessHeight = 0;
  //   }

  //   // console.warn("excess height ", excessHeight)

  //   // console.warn("height deduction va;ue ", heightDeductionValue)
  //   // if (this.fwpHowToPlayScreens.showHTPScreens) {
  //   //   setTimeout(function () {
  //   //     console.warn("window outer screen height of htp screens ", screen.height);
  //   //     if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
  //   //       window.parent.postMessage({
  //   //         action: "b2bresize",
  //   //         scrollHeight: screen.height - (0.85 * excessHeight) - heightDeductionValue
  //   //       }, (document.referrer || (window.parent && window.parent.location.href)));

  //   //     }
  //   //   }, 1000);
  //   // } else {
  //     this.setIframeHeight();
  //   // }
  // }

  getBetsFromConfig() {
    let regionKeyIndex = this.userDetails.userRegion;
    let regionOperatorKeyIndex = regionKeyIndex + "_" + this.configService.configData.operatorId.toUpperCase();
    let regionOperatorCurrencyKeyIndex = regionOperatorKeyIndex + "_" + this.configService.configData.currencyCode;

    if (this.configService.configData.operatorId == "paribet") {
      this.defaultBet = this.configService.configData.defenderBets[this.configService.configData.currencyCode].defaultBet;
      this.maxBet = this.configService.configData.defenderBets[this.configService.configData.currencyCode].maxBet;
      this.minBet = this.configService.configData.defenderBets[this.configService.configData.currencyCode].minBet;
    } else if (regionOperatorCurrencyKeyIndex in this.configService.configData.defenderBets) {
      this.defaultBet = this.configService.configData.defenderBets[regionOperatorCurrencyKeyIndex].defaultBet;
      this.maxBet = this.configService.configData.defenderBets[regionOperatorCurrencyKeyIndex].maxBet;
      this.minBet = this.configService.configData.defenderBets[regionOperatorCurrencyKeyIndex].minBet;
    }
    else if (regionOperatorKeyIndex in this.configService.configData.defenderBets) {

      this.defaultBet = this.configService.configData.defenderBets[regionOperatorKeyIndex].defaultBet;
      this.maxBet = this.configService.configData.defenderBets[regionOperatorKeyIndex].maxBet;
      this.minBet = this.configService.configData.defenderBets[regionOperatorKeyIndex].minBet;

    } else if (regionKeyIndex in this.configService.configData.defenderBets) {

      this.defaultBet = this.configService.configData.defenderBets[this.userDetails.userRegion].defaultBet;
      this.maxBet = this.configService.configData.defenderBets[this.userDetails.userRegion].maxBet;
      this.minBet = this.configService.configData.defenderBets[this.userDetails.userRegion].minBet;

    } else {

      this.defaultBet = this.configService.configData.defenderBets["IN"].defaultBet;
      this.maxBet = this.configService.configData.defenderBets["IN"].maxBet;
      this.minBet = this.configService.configData.defenderBets["IN"].minBet;

    }

  }

  ngOnDestroy() {
    this.messageSubscription.unsubscribe();
  }


  @HostListener('window:message', ['$event'])

  onMessage(res: any) {
    // console.log('received message from game', res.data);
    if (res.data) {
      let messageData = res.data;
      switch (messageData.messageName) {
        case 'init':
          this.loadedIframe = document.getElementById('iframeElement');
          // console.warn('check-received init from canvas');
          this.initializeGame(messageData);
          break;
        case 'appHeight':
          this.setIframeHeight();
          break;
        case "FWPLoadingPercentage":
          this.setFWPLoadingPercentage(messageData);
          break;
        case "FWPLoadingComplete":
          this.setFWPLoadingComplete();
          break;
        case "LobbyClicked":
          this.navigateToLobby(messageData);
          break;
        case "UpdateCurrency":
          this.updateCurrency(messageData);
          break;
        case 'setScrollable':
          this.setScrollable(messageData);
          break;
        default:
        // code block
      }
    }
  }

  onBackButtonClicked() {
    if (this.configService.configData.standalone) {
      if (this.configService.configData.platformId == "6") {
        window.parent.postMessage({
          action: "embackbutton"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      } else {
        window.history.go(-1);
      }
    }
  }

  initializeGame(data: any) {
    // this.socketService.disconnectSocket();
    // if (!this.socketService.gameType) {
    // if(this.skipCount <= 3){
    //   this.showHowToPlayOnCanvas();
    // }
    if (!navigator.onLine) {
      this.showGenericPopup = true;
      this.showModal(this.configService.configData.networkErrorMessage);
      return;
    }
    this.sendLanguageDetails();
    let url = this.configService.configData.url.socket + "lobbyV2?gameName=" + this.gameCode + "&operatorId=" + ((this.configService.configData.defender.operatorSeparation.indexOf(this.configService.configData.operatorId) >= 0) ? this.configService.configData.operatorId : "default");
    // console.warn('check-send lobby request for matchId');
    if (this.configService.configData.url.isCertification) {
      url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games?gameName=" + this.gameCodeBackend + "&operatorId=" + this.configService.configData.operatorId;
    }
    this.http.get(url).subscribe((res) => {
      let result: any = res;
      let results: any = [];
      if (result.games) {
        results = [{
          "firstTeam": result.games[0].teamInfo.firstTeam.team,
          "firstTeamId": result.games[0].teamInfo.firstTeam.id,
          "secondTeam": result.games[0].teamInfo.secondTeam.team,
          "secondTeamId": result.games[0].teamInfo.secondTeam.id,
          "seriesName": result.games[0].teamInfo.tournamentId,
          "matchId": result.games[0].id,
          "status": (result.games[0].status == "RUNNING") ? 1 : 0,
          "startDate": result.games[0].startedAt,
          "isReal": true,
          "gameName": this.gameCode
        }]
      } else {
        results = result.filter((data: any) => (
          data.gameName && data.gameName.includes("FWP"))
        );
      }
      this.results = results;
      if (results.length) {
        // console.warn('check-received lobby response for matchId');
        this.sendUserDetailsToGame();
        let matchDetails: any = { ...results[0] };
        matchDetails.status = 1;
        matchDetails.demoPlay = false;
        matchDetails.gameStatus = matchDetails.status;
        let playerId = this.configService.configData.playerId;
        if (playerId) {
          matchDetails.playerId = playerId;
        } else {
          matchDetails.playerId = Math.floor(Math.random() * 899999 + 100000);
        }
        this.matchDetails = matchDetails;
        // this.configService.configData.url.socket = "https://dev-beapi-games-rgs.sportsit-tech.net/truewave/truewave/"
        let socketURL = this.configService.configData.url.socket;
        if (this.configService.configData.url.isCertification) {
          socketURL = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/fwp";
        }
        this.socketService.connectToServer(
          socketURL,
          "firstWicketPartnership",
          this.userDetails.customerId,
          this.matchDetails.matchId
        );
        this.onResize('true', true);
      } else {
        // console.warn('check-received lobby response without fwp matchId');
        // setTimeout(() => this.initializeGame({}), 5000);
      }

    })
    // }
    // this.onResize('true');
  }

  sendLanguageDetails() {
    let languageDetails = {
      languageCode: this.configService.configData.languageCode || 'en'
    }
    let languageDetailsData: any = {};
    languageDetailsData.message = "SET_LANGUAGE";
    languageDetailsData.data = languageDetails;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(languageDetailsData, "*");
    }
  }

  sendUserDetailsToGame() {
    const userRegion = this.userDetails.userRegion;
    const userRegionWithOperatorId = userRegion + "_" + this.configService.configData.operatorId.toUpperCase();
    this.userDetails.stakeTax = this.configService.configData.taxData[userRegionWithOperatorId] ? this.configService.configData.taxData[userRegionWithOperatorId].stakeTax : 0;
    this.userDetails.withholdTax = this.configService.configData.taxData[userRegionWithOperatorId] ? this.configService.configData.taxData[userRegionWithOperatorId].withholdTax : 0;
    this.userDetails.platformId = this.configService.configData.platformId;
    this.userDetails.operatorId = this.configService.configData.operatorId;
    this.userDetails.brand = this.configService.configData.brand;
    this.userDetails.animation = this.showAnimation;
    this.userDetails.hideExitButtonInCanvas = this.configService.configData.defenderHideExitButton[this.userDetails.operatorId] || this.configService.configData.defenderHideExitButton["default"];
    this.userDetails.languageCode = this.configService.configData.languageCode || 'en';
    let userDetailsData: any = {};
    userDetailsData.message = "USER_DETAILS";
    userDetailsData.data = this.userDetails;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(userDetailsData, "*");
    }
  }

  getUUid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  showLoadingProgress() {
    if (!this.setLoadingFromCanvas) {
      this.loadingInterval = setInterval(() => this.progressBarLoadingFromWrapper(this.loadPerc), 100);
    }
  }

  progressBarLoadingFromWrapper(count: number) {

    if (count) {
      count = count + 1;
    }
    this.loadPerc = count;
    if (count < 45) {
      document.getElementById('progress-bar')!.style.width = count + '%';
    } else {
      // clear the interval
      clearInterval(this.loadingInterval)
    }
  }

  setFWPLoadingPercentage(data: any) {
    this.setLoadingFromCanvas = true;
    clearInterval(this.loadingInterval)

    document.getElementById('progress-bar')!.style.width = this.loadPerc + data.loadedPercentage + '%';
  }

  setFWPLoadingComplete() {
    if (this.results && this.results.length) {
      setTimeout(() => {
        this.setParentWindowWidth('auto');
        document.getElementById('preloader')!.style.display = "none";
        document.getElementById('display-game')!.style.visibility = "visible";
        document.getElementById('body')!.style.maxWidth = "100%";
        if (this.configService.configData.operatorId != "demo") {
          this.setIframeHeight();
        }
        if (this.loadedIframe && this.loadedIframe.contentDocument && this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0]) {
          this.loadedIframe.contentDocument.getElementsByTagName('canvas')[0].style.touchAction = "auto";
        }
        // this.showHowToPlayScreenBasedOnSkipCount();
        // document.body.style.backgroundColor = "initial";
      }, 1500);

    } else {
      setTimeout(() => this.initializeGame({}), 2000);
    }

  }


  setIframeHeight() {
    // if (obj.appHeight) {
    //   this.lastAppHeight = obj.appHeight;
    // console.log('received height from game--',obj.appHeight);
    // document.getElementById("iframeElement")!.style.height = obj.appHeight + "px";
    // console.warn("scroll height ", document.getElementById("htp-screen-main-container")?.clientHeight);

    // if(this.fwpHowToPlayScreens.showHTPScreens){
    //   setTimeout(function () {
    //     if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
    //       window.parent.postMessage({
    //         action: "b2bresize",
    //         scrollHeight: document.getElementById('htp-screen-main-container')?.clientHeight
    //       }, (document.referrer || (window.parent && window.parent.location.href)));

    //     }
    //   }, 1000);

    //   return;
    // }
    let desktopBottomContainerHeight = document.getElementById('history-desktop-main-container')?.clientHeight;
    let gameContainerHeight = document.getElementById('game-container')?.clientHeight;

    if (gameContainerHeight) {

      this.gameContainerHeight = gameContainerHeight;

    }

    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: gameContainerHeight! + desktopBottomContainerHeight!
        }, (document.referrer || (window.parent && window.parent.location.href)));

      }
    }, 1000);
    // console.warn("gamecontainerheight " + this.gameContainerHeight);

    // document.getElementById("canvas")!.style.maxHeight = this.gameContainerHeight * 0.20 + "px";

    // if((this.gameContainerHeight * 0.20) < 200){
    //   document.getElementById("canvas")!.style.maxHeight = "545px";
    // }
    // }
    if (!this.configService.configData.isMobile) {
      document.getElementById("iframeElement")!.style.maxHeight = "545px";
      // document.getElementById("iframeElement")!.style.maxWidth = "524px";
    }
  }

  setScrollable(obj: any) {
    window.scrollTo(0, 0);
    let iframeElement = this.loadedIframe.contentDocument;
    if (obj.scrollable) {
      iframeElement.getElementsByTagName('canvas')[0].style.touchAction = "auto";
    } else {
      iframeElement.getElementsByTagName('canvas')[0].style.touchAction = "none";
    }
  }


  serverResponse(message: any) {
    if (message == "socket connected") {
      // this.onSocketConnected();
      // setTimeout(() => this.sendLastTwentyGamesRequest(), 100)
      this.onSocketConnected();
      setTimeout(() => this.sendLastTwentyGamesRequest(), 100)
    } else {
      switch (message.message) {
        case "authenticate":
          this.onPlayerAuthenticated(message.data);
          break;
        case "init":
          this.setPlayerBets(message.data);
          break;
        case "balance":
          this.setBalance(message.data);
          break;
        case "betsessionresponse":
          this.setBetSessionResponse(message.data);
          break;
        case "cashout":
          this.setCashOut(message.data);
          break;
        case "newmatch":
          this.setNewMatch(message.data);
          break;
        case "score":
          this.setScore(message.data);
          break;
        case "result":
          this.setResult(message.data);
          break;
        case "last20":
          this.setLastTwentyGames(message.data);
          break;
        case "fwpdata":
          this.setFwpDataResponse(message.data);
          break;
        case "fwpbet":
          this.setFwpBetResponse(message.data);
          break;
        case "fwpcashout":
          this.setFwpCashOutResponse(message.data);
          break;
        case "chat":
          this.setChatResponse(message.data);
          break;
        case "lasttenchat":
          this.setLastTenChatResponse(message.data);
          break;
        case "gameevent":
          this.setGameEventResponse(message.data);
          break;
        default:
      }
    }
  }

  onSocketConnected() {
    // implement init
    if (this.configService.configData.url.isCertification) {
      let initObj = {
        "token": this.userDetails.accessToken,
        "playerId": (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
        "gameType": this.gameCodeBackend,
        "matchId": this.matchDetails.matchId,
        "extraData": {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerId": (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games/init";
      this.http.post(url, initObj).subscribe((res) => {
        this.onPlayerAuthenticated(res);
      })
    } else {
      this.socketService.authenticatePlayer({
        token: this.userDetails.accessToken,
        playerId: (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
        gameName: this.gameCode,
        matchId: this.matchDetails.matchId,
        jurisdiction: this.configService.configData.url.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerName": this.configService.configData.playerName,
          "playerId": (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      });
    }
  }

  onPlayerAuthenticated(data: any) {
    this.balance = data.playerAccount ? data.playerAccount.balance : 0;
    if (document.referrer && document.referrer.indexOf('localhost') < 0) {
      this.playerAccountData = data.playerAccount;
    }

    this.sendLastTenChatRequest();
    if (data && data.playerAccount && (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6")) {
      this.userDetails.customerId = data.playerAccount.playerId;
      this.matchDetails.playerId = this.userDetails.customerId;
      this.currencyCode = data.playerAccount.currencyCode;
      this.configService.configData.currencyCode = data.playerAccount.currencyCode;
      this.getCurrencySymbol();
      this.configService.configData.playerId = this.userDetails.customerId;
    }
    if (this.balanceFlag) {
      if (this.configService.configData.currencyCode) {
        this.currencyCode = this.configService.configData.currencyCode;
      } else {
        this.currencyCode = data.playerAccount ? data.playerAccount.currencyCode : "INR";
      }
      this.fwpBetPanelComponent.setCurrencyCode(this.currencyCode);
      this.fwpBottomComponent.setCurrencyCode(this.currencyCode);
      // send initial match details to game
      let matchDetails: any = {};
      matchDetails.message = "MATCH_DETAILS";
      matchDetails.data = this.matchDetails;
      matchDetails.data.currencyCode = this.currencyCode;
      matchDetails.data.windowHeight = window.innerHeight;
      matchDetails.data.windowWidth = window.innerWidth;
      matchDetails.data.timezone = "Asia/Kolkata";
      if (this.configService.configData.isRGSSetup) {
        matchDetails.data.timezone = "UTC";
      }
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
        let newMatchData = { ...matchDetails.data };
        if (!newMatchData.id) {
          newMatchData.id = newMatchData.matchId;
        }
        this.setNewMatch(newMatchData);
        // console.log('sent match details', matchDetails);
        this.balanceFlag = false;
      } else {
        setTimeout(() => {
          this.setBalance(data);
        }, 1000);
      }
    }
  }

  setBalance(data: any) {
    this.balance = data.playerAccount ? data.playerAccount.balance : 0;
    if (this.balanceFlag) {
      this.currencyCode = data.playerAccount.currencyCode;
      this.fwpBetPanelComponent.setCurrencyCode(this.currencyCode);
      this.fwpBottomComponent.setCurrencyCode(this.currencyCode);
      // send initial match details to game
      let matchDetails: any = {};
      matchDetails.message = "MATCH_DETAILS";
      matchDetails.data = this.matchDetails;
      matchDetails.data.currencyCode = this.currencyCode;
      matchDetails.data.windowHeight = window.innerHeight;
      matchDetails.data.windowWidth = window.innerWidth;
      matchDetails.data.timezone = "Asia/Kolkata";
      if (this.configService.configData.isRGSSetup) {
        matchDetails.data.timezone = "UTC";
      }
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
        let newMatchData = { ...matchDetails.data };
        if (!newMatchData.id) {
          newMatchData.id = newMatchData.matchId;
        }
        this.setNewMatch(newMatchData);
        // console.log('sent match details', matchDetails);
        this.balanceFlag = false;
      } else {
        setTimeout(() => {
          this.setBalance(data);
        }, 1000);
      }
    }

    this.balance = data.playerAccount ? data.playerAccount.balance : 0;
  }

  onResize(event: any, storeCanvasHeightForDemo?: boolean) {
    console.log("on resize function is called");
    let canvasDetails = document.getElementById("canvas");
    let data = {
      "width": (window.innerWidth >= 800) ? 524 : window.innerWidth,
      "height": Math.min(window.innerHeight * .5, 545)
    }
    // if(event == "true"){
    // console.warn(data);
    if (window.parent && (window.parent as any).excessHeight) {
      data.height = Math.min(screen.height - (window.parent as any).excessHeight - 220, 545);
    }
    if (window.innerWidth >= 800) {
      data.height = 545;
    }

    if ((this.isPlayabet || this.isBetyetu) && !this.configService.configData.isMobile)
      data.width = 500;
    console.warn(" resizedata ", data);

    let heightAlreadySetForDemo = false;

    if (this.canvasDetailsObject && this.configService.configData.operatorId == "demo") {
      document.getElementById("iframeElement")!.style.height = this.canvasDetailsObject.height + "px";
      document.getElementById("iframeElement")!.style.width = this.canvasDetailsObject.width + "px";
      data = this.canvasDetailsObject;
      heightAlreadySetForDemo = true;
    }

    if (!heightAlreadySetForDemo) {
      if (!storeCanvasHeightForDemo) {
        document.getElementById("iframeElement")!.style.height = data.height + "px";
        document.getElementById("iframeElement")!.style.width = data.width + "px";
      } else {
        document.getElementById("iframeElement")!.style.height = data.height + "px";
        document.getElementById("iframeElement")!.style.width = data.width + "px";
        this.canvasDetailsObject = data
      }
    }


    let canvasSize: any = {};
    canvasSize.message = "FWP_CANVAS_SIZE";
    canvasSize.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(canvasSize, "*");
    }

    this.fwpBetPanelComponent.onResize();

  }

  setBetSessionResponse(data: any) {
    this.updateBalance();
    // if (data.betRequest && (data.betRequest.matchId != this.matchDetails.matchId)) {
    //   return;
    // }
    if ((data.error || (data.betRequest && data.betRequest.error)) && (data.playerAccount && data.playerAccount.playerId == this.userDetails.customerId)) {
      if (data.status == 461) {
        this.showModal('Insufficient Funds. Please add funds to your account and retry.')
      } else {
        this.showModal(data.errorMessage)
      }
      this.stopAutoplayBet();
      this.fwpBetPanelComponent.resetBetPanel(0);
      return;
    }
    this.betSessionResponse = data;
    this.lastBetAmount = data.betRequest ? data.betRequest.betAmount : this.betAmount;
    this.lastBetAmount = this.formatCurrency(this.lastBetAmount);
    this.fwpBetPanelComponent.setBetResponse(data);
    this.balance = data.playerAccount.balance;
    // ($('#bet-placed-popup') as any).modal();
    // setTimeout(() => ($('#bet-placed-popup') as any).modal('hide')
    //   , 3000);
    this.fwpBetPanelComponent.showBetPlaced(true, this.betSessionResponse);
    // setTimeout(() => this.fwpBetPanelComponent.showBetPlaced(false), 3000)
    this.showBetContent = true;
    this.showCashOutContent = false;
    this.fwpBetPanelComponent.setCashOutContent(this.showCashOutContent);
    if (this.autoplayData) {
      this.autoplayData.totalBets = this.autoplayData && this.autoplayData.totalBets - 1;
    }
    let betResponse: any = {};
    betResponse.message = "BET_RESPONSE";
    betResponse.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(betResponse, "*");
    }
    this.getPlayerBets();
  }

  getShowBetContentValue() {
    return this.showBetContent;
  }

  getTotalBetsInAuto() {
    return this.autoplayData.totalBets;
  }

  setCashOut(data: any) {
    this.getBalance();
    this.updateBalance();
    this.fwpBetPanelComponent.setCashOut(data);
    if (this.configService.configData.url.isCertification && (this.fwpBetPanelComponent.betIdObject.secondBetId && data.betId == this.fwpBetPanelComponent.betIdObject.secondBetId) ) {
      this.cashOutSessionResponse2 = data;
  
      let result = JSON.parse(this.cashOutSessionResponse2.result);
      if (!result) {
        return;
      }
      this.cashOutMultiplier2 = result.multiplier / 100;
  
      this.cashOutSessionResponse2.winAmountAfterTax = this.formatCurrency(result.winAmountAfterTax);

    } else {
      this.cashOutSessionResponse = data;
  
      let result = JSON.parse(this.cashOutSessionResponse.result);
      if (!result) {
        return;
      }
      this.cashOutMultiplier = result.multiplier / 100;
  
      this.cashOutSessionResponse.winAmountAfterTax = this.formatCurrency(result.winAmountAfterTax);

    }
    // ($('#bet-placed-popup') as any).modal();
    // setTimeout(() => ($('#bet-placed-popup') as any).modal('hide')
    //   , 3000);
    this.showBetContent = false;
    this.showCashOutContent = true;
    this.fwpBetPanelComponent.setCashOutContent(this.showCashOutContent);
    let cashOutResponse: any = {};
    cashOutResponse.message = "MY_CASHOUT";
    cashOutResponse.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(cashOutResponse, "*");
    }
    if (this.autoplay) {
      // this.autoplayWin += data.winAmount;
      this.autoplayWin = data.winAmount;

      // console.warn("set cashout autoPlay win ", this.autoplayWin);
    }
  }

  setNewMatch(data: any) {
    this.updateBalance();
    // console.log('send new match to game', data);
    data.currencyCode = this.currencyCode;
    this.cashoutRequestSent = false;
    this.matchStarted = true;
    this.scoreStarted = false;
    this.cashOutAmount = 0;
    this.matchDetails.matchId = data.id;
    this.matchData = data.id;
    let matchTime: any = new Date(data.startDate);
    let newMatch: boolean = true;
    matchTime.setSeconds(matchTime.getSeconds() + 7);
    // if (this.configService.configData.isRGSSetup) {
    //   matchTime.setSeconds(matchTime.getSeconds() + 7);
    // } else {
    //   matchTime.setSeconds(matchTime.getSeconds() + 15);
    // }
    matchTime = moment(matchTime).format('YYYY-MM-DDTHH:mm:ss');
    data.startDate = matchTime;
    data.timezone = "Asia/Kolkata";
    if (this.configService.configData.isRGSSetup) {
      data.timezone = "UTC";
    }
    if (this.autoplay) {
      this.fwpBetPanelComponent.autoBetForm.enable();
      this.checkAutoplayConditions();
    } else {
      this.fwpBetPanelComponent.resetBetPanel(0, newMatch);
      // this.fwpBottomComponent.generatePlayerBetsData('bet');
    }
    let matchDetails: any = {};
    matchDetails.message = "NEW_MATCH";
    matchDetails.data = data;
    console.log("send new match to game", data);
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(matchDetails, "*");
    }
  }

  setScore(data: any) {
    //check if match details sent to game
    this.lastConnectionTime = Date.now();
    setTimeout(() => {
      this.checkConnectionStatus();
    }, 20000);
    if (!this.balanceFlag) {
      this.fwpBetPanelComponent.setScore(data);
      if (!this.scoreStarted) {
        // this.fwpBottomComponent.generatePlayerBetsData('win');
        this.getPlayerBets();
      }
      this.scoreData = data;
      this.scoreStarted = true;
      if (this.betAmount && this.cashOutAmount) {
        // if ((this.lastBetAmount * data / 10000).toFixed(2) >= this.cashOutAmount) {
        //   console.log('yes', (this.lastBetAmount * data / 10000).toFixed(2));
        //   this.cashOut();
        // }
        // Cashout if score is reached
        // if ((this.scoreData / 100) == this.cashOutAmount) {
        //   this.cashOut();
        // }
      }
      let scoreDetails: any = {};
      scoreDetails.message = "SET_SCORE";
      scoreDetails.data = data;
      if (this.loadedIframe && this.loadedIframe.contentWindow) {
        this.loadedIframe.contentWindow.gameExtension(scoreDetails, "*");
      }
    }
  }

  checkConnectionStatus() {
    if (this.lastConnectionTime < (Date.now() - 30000)) {
      location.reload();
    }
  }

  setResult(data: any) {
    if (this.betAmount || this.autoplay) {
      // this.cashOut();
    }
    if (!this.autoplay) {
      this.fwpBetPanelComponent.resetBetPanel(0);
    }
    this.resultData = data;
    let resultDetails: any = {};
    resultDetails.message = "SET_RESULT";
    resultDetails.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(resultDetails, "*");
    }
    this.sendLastTwentyGamesRequest();
    // this.getPlayerBets();
  }

  setLastTwentyGames(data: any) {
    for (let p in data) {
      let score = JSON.parse(data[p].score);
      data[p].multiplier = score.multiplier / 100;
    }
    this.multiplierData = data;
    this.isMobile = this.configService.configData.isMobile;
    this.fwpBetPanelComponent.onResize()
  }

  setFwpDataResponse(data: any) {
    // console.log(data);
  }

  setPlayerBets(data: any) {
    if (this.fwpBottomComponent) {
      this.fwpBottomComponent.playerBetsUpdate(data);
    }

  }

  setFwpBetResponse(data: any) {
    // console.log("bet response match", data);
    // if ((data.error || (data.betRequest && data.betRequest.error)) && (data.playerAccount && data.playerAccount.playerId == this.userDetails.customerId)) {
    //   if (data.status == 461) {
    //     this.showModal('Insufficient Funds. Please add funds to your account and retry.')
    //   } else {
    //     this.showModal(data.errorMessage)
    //   }
    //   return;
    // }
    this.updateBalance();
    let fwpBetDetails: any = {};
    fwpBetDetails.message = "CURRENT_MATCH_BETS";
    fwpBetDetails.data = data;

    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(fwpBetDetails, "*");
    }
    this.getPlayerBets();
  }

  setFwpCashOutResponse(data: any) {
    if (data.playerBalance) {
      this.balance = data.playerBalance;
    }
    this.updateBalance();
    let fwpCashOutDetails: any = {};
    fwpCashOutDetails.message = "CURRENT_MATCH_CASHOUTS";
    fwpCashOutDetails.data = data;
    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(fwpCashOutDetails, "*");
    }
    if (data.playerId == this.userDetails.customerId) {
      this.getBalance();
      if (this.fwpBetPanelComponent.betIdObject.secondBetId && data.betId == this.fwpBetPanelComponent.betIdObject.secondBetId) {
        this.cashOutSessionResponse2 = data;
        this.cashOutSessionResponse2.winAmountAfterTax = this.formatCurrency(data.winAmountAfterTax);
        this.cashOutMultiplier2 = data.multiplier / 100;
        this.manualBetAmount2 = data.betAmount;
      } else {
        this.cashOutSessionResponse = data;
        this.cashOutMultiplier = data.multiplier / 100;
        this.manualBetAmount = data.betAmount;
        this.cashOutSessionResponse.winAmountAfterTax = this.formatCurrency(data.winAmountAfterTax);
      }
      if (this.cashedOutBets.indexOf(data.betId)<0) {
        this.cashedOutBets.push(data.betId);
      }
      if (data.winAmount != 0) {
        // ($('#bet-placed-popup') as any).modal();
        // setTimeout(() => ($('#bet-placed-popup') as any).modal('hide')
        //   , 3000);
        this.showBetContent = false;
        this.showCashOutContent = true;
        this.fwpBetPanelComponent.setCashOutContent(this.showCashOutContent);
        this.fwpBetPanelComponent.setCashOut(data);
        setTimeout(()=> {
          if (this.fwpBetPanelComponent.betIdObject.secondBetId && data.betId == this.fwpBetPanelComponent.betIdObject.secondBetId) {
            if (!this.fwpBetPanelComponent.betIdObject.firstBetId || this.cashedOutBets.indexOf(this.fwpBetPanelComponent.betIdObject.firstBetId) < 0) {
              document.getElementById('firstCashoutContent') && (document.getElementById('firstCashoutContent')!.style.display = "none");
            }
            document.getElementById('secondCashoutContent')!.style.display = "block";
            // document.getElementById('firstCashoutContent')!.style.display = "none";
          } else {
            if (!this.fwpBetPanelComponent.betIdObject.secondBetId || this.cashedOutBets.indexOf(this.fwpBetPanelComponent.betIdObject.secondBetId) < 0) {
              document.getElementById('secondCashoutContent') && (document.getElementById('secondCashoutContent')!.style.display = "none");
            }
            document.getElementById('firstCashoutContent')!.style.display = "block";
          }
        }, 10);
      }
      if (this.autoplay) {
        // this.autoplayWin += data.winAmount;
        this.autoplayWin = data.winAmount;
      }
    }
    this.getPlayerBets();
  }

  setChatResponse(data: any) {
    this.fwpBottomComponent.onChatResponse(data);
  }
  setLastTenChatResponse(data: any) {
    this.fwpBottomComponent.onLastTenChatResponse(data);
  }

  setGameEventResponse(data: any) {
    switch (data.eventType) {
      case "NEW_GAME":
        console.log('fwp NEW_GAME---', data);
        this.newGameEvent(data);
        break;
      case "BET_PLACED":
        // console.log('fwp BET_PLACED---', data);
        this.setGameBetPlaced(data);
        break;
      case "GAME_STARTING":
        // console.log('fwp GAME_STARTING---', data);
        this.setGameStarted(data);
        break;
      case "GAME_UPDATE":
        this.setScore(data.content.currentMultiplier);
        // console.log('fwp GAME_UPDATE---', data);
        break;
      case "CASH_OUT":
        // console.log('fwp CASH_OUT---', data);
        this.setGameCashOut(data);
        break;
      case "GAME_END":
        // console.log('fwp GAME_END---', data.content);
        this.setGameEnded(data);
        break;
      default:
        break;
    }

  }

  newGameEvent(data: any) {
    let content = data.content;
    let score = {
      "firstTeam": content.teamInfo.firstTeam.team,
      "firstTeamId": content.teamInfo.firstTeam.id,
      "secondTeam": content.teamInfo.secondTeam.team,
      "secondTeamId": content.teamInfo.secondTeam.id,
    }
    let newGameData: any = {};
    let matchTime: any = new Date(content.gameCreatedAt);
    matchTime.setSeconds(matchTime.getSeconds() + 3);
    matchTime = moment(matchTime).format('YYYY-MM-DDTHH:mm:ss');
    let timezone = "Asia/Kolkata";
    if (this.configService.configData.isRGSSetup) {
      timezone = "UTC";
    }
    newGameData = {
      "id": content.id,
      "gameName": this.gameCode,
      "liveId": "virtual",
      "operatorId": this.configService.configData.operatorId,
      "seriesName": content.teamInfo.tournamentId,
      "score": JSON.stringify(score),
      "startDate": matchTime,
      "timezone": timezone
    }
    this.cashOutSessionResponse2 = null;
    this.cashOutMultiplier2 = null;
    this.manualBetAmount2 = null;
    this.cashedOutBets = [];
    this.setNewMatch(newGameData);
  }

  setGameEnded(data: any) {
    // let resultObj = {
    //   ""
    // }
    let score = {
      "multiplier": data.content.finalMultiplier,
      "currentMultiplier": data.content.currentMultiplier,
      "firstTeam": this.matchDetails.firstTeam,
      "firstTeamId": this.matchDetails.firstTeamId,
      "secondTeam": this.matchDetails.secondTeam,
      "secondTeamId": this.matchDetails.secondTeamId
    };
    data.content.score = JSON.stringify(score);
    this.setResult(data.content);
  }

  setGameStarted(data: any) {

  }

  setGameBetPlaced(data: any) {
    let betPlacedResponse: any = {
      "betAmount": data.content.betAmount,
      "autoCashOut": data.content.autoCashOut,
      "matchId": data.content.gameId,
      "playerId": data.content.playerId
    }
    this.setFwpBetResponse(betPlacedResponse);
  }

  setGameCashOut(data: any) {
    this.setFwpCashOutResponse({...data.content});
  }

  placeBet(betAmount: any, cashOut: any, isFirstBetInput: boolean) {
    this.cashOutAmount = cashOut;
    this.betAmount = betAmount;
    this.manualBetAmount = this.formatCurrency(this.betAmount);
    this.sendBetRequest(isFirstBetInput);
  }

  checkAutoplayConditions() {
    if (this.autoplayData.totalBets > 0) {
      // let betPlaced = (this.autoplayDataRes.totalBets - this.autoplayData.totalBets) * this.autoplayDataRes.autoBetAmount;
      let betPlaced = this.autoplayDataRes.autoBetAmount;
      let flag = false;
      let actualWin = this.autoplayWin - betPlaced;

      if (actualWin <= 0) {
        this.totalLoss += betPlaced;
      }
      else {
        this.totalWin += actualWin;
      }

      if (this.autoplayData.stopOnProfit) {
        // console.warn("actualWIn " + actualWin);
        // console.warn("totalWin " + this.totalWin);
        let win = this.totalWin - this.totalLoss;
        if (win > 0 && ((win) >= this.autoplayData.stopOnProfit)) {
          flag = true;
        }
      }
      if (this.autoplayData.stopOnLoss) {
        let actualLoss = this.totalLoss - this.totalWin;

        if ((actualLoss > 0) && (actualLoss) >= this.autoplayData.stopOnLoss) {
          flag = true;
        }
      }
      // }
      if (flag) {
        this.stopAutoplayBet();
        this.fwpBetPanelComponent.resetBetPanel(0);
        // this.fwpBottomComponent.generatePlayerBetsData('bet');
      } else {
        this.onAutoplay();
        this.fwpBetPanelComponent.resetBetPanel(this.autoplayData.totalBets);
        // this.fwpBottomComponent.generatePlayerBetsData('bet');
      }
    } else {
      this.stopAutoplayBet();
      this.fwpBetPanelComponent.resetBetPanel(0);
      // this.fwpBottomComponent.generatePlayerBetsData('bet');





    }
  }
  getBalance() {
    if (this.configService.configData.url.isCertification) {
      let balanceObj = {
        "token": this.userDetails.accessToken,
        "playerId": (this.configService.configData.platformId == "4" || this.configService.configData.platformId == "6") ? null : this.userDetails.customerId,
        "jurisdiction": this.configService.configData.url.userRegion,
        "extraData": {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/balance";
      this.http.post(url, balanceObj).subscribe((res) => {
        this.setBalance(res);
      })
    } else {
      let request = {
        token: this.userDetails.accessToken,
        matchId: this.matchDetails.matchId,
        playerId: this.userDetails.customerId,
        gameName: this.gameCode,
        jurisdiction: this.configService.configData.url.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      this.socketService.getBalance(request);
    }
  }

  autoplayBet(data: any) {
    // debugger;
    this.autoplay = true;
    this.betAmount = data.autoBetAmount;
    this.autoplayData = { ...data };
    this.autoplayDataRes = { ...data };
    if (this.matchStarted && !this.scoreStarted) {
      this.placeBetAuto(this.autoplayData.autoBetAmount, this.autoplayData.autoCashout);
    }
  }
  placeBetAuto(autoBetAmount: any, autoCashout: any) {
    this.cashOutAmount = autoCashout;
    this.betAmount = autoBetAmount;
    this.sendBetRequest(false);
  }

  onAutoplay() {
    this.betAmount = this.autoplayData.autoBetAmount;
    this.cashOutAmount = this.autoplayData.autoCashout;
    this.sendBetRequest(false);
  }

  stopAutoplayBet() {
    this.autoplay = false;
    this.autoplayWin = 0;
    this.fwpBetPanelComponent.stopAutoplay();
  }


  cashOut(betId: string) {
    if (!navigator.onLine) {
      this.showGenericPopup = true;
      this.showModal(this.configService.configData.networkErrorMessage);
      return;
    }
    // this.socketService.sendResultRequest();
    if (this.configService.configData.url.isCertification) {
      this.currentBetObj.betId = betId;
      let cashOutObj = {
        "betId": this.currentBetObj.betId,
        "operatorId": this.configService.configData.operatorId,
        "gameName": this.gameCodeBackend
      }
      let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games/" + this.matchDetails.matchId + "/bet/" + this.currentBetObj.betId + "/cash-out";
      if (!this.cashoutRequestSent) {
        this.http.post(url, cashOutObj).subscribe((res: any) => {
          let cashOutData = {}
          let result = {
            "betAmount": res.betAmount,
            "betAmountAfterTax": res.betAmountAfterTax,
            "winAmount": res.winAmount,
            "winAmountAfterTax": res.winAmountAfterTax,
            "multiplier": res.cashedOutMultiplier,
            "playerId": this.userDetails.customerId
          }
          cashOutData = {
            "result": JSON.stringify(result),
            "betId": res.betId
          }
          this.setCashOut(cashOutData);
        })
        let betIdObject = this.fwpBetPanelComponent.betIdObject;
        const hasFirstBetId = 'firstBetId' in betIdObject;
        const hasSecondBetId = 'secondBetId' in betIdObject;

        if ((hasFirstBetId && !hasSecondBetId) || (!hasFirstBetId && hasSecondBetId)) {
          this.cashoutRequestSent = true;
        } else if (hasFirstBetId && hasSecondBetId) {
          this.cashoutRequestSent = false;
        }
      }
      this.betAmount = 0;
      this.cashOutAmount = 0;
    }
    let request = {
      token: this.userDetails.accessToken,
      matchId: this.matchDetails.matchId,
      playerId: this.userDetails.customerId,
      gameName: this.gameCode,
      jurisdiction: this.configService.configData.url.userRegion,
      extraData: {
        "platformId": this.configService.configData.platformId,
        "operatorId": this.configService.configData.operatorId,
        "brand": this.configService.configData.brand,
        "region": this.configService.configData.url.userRegion,
        "currency": this.currencyCode,
        "gameId": this.gameCode,
        "gameType": this.gameCode,
        "playerName": this.configService.configData.playerName,
        "isFreeGame": this.configService.configData.isFreeGame
      }
    }
    if (!this.cashoutRequestSent) {
      this.socketService.sendFwpCashOutRequest(request);
      let betIdObject = this.fwpBetPanelComponent.betIdObject;
      const hasFirstBetId = 'firstBetId' in betIdObject;
      const hasSecondBetId = 'secondBetId' in betIdObject;

      if ((hasFirstBetId && !hasSecondBetId) || (!hasFirstBetId && hasSecondBetId)) {
        this.cashoutRequestSent = true;
      } else if (hasFirstBetId && hasSecondBetId) {
        this.cashoutRequestSent = false;
      }
    }
    this.betAmount = 0;
    this.cashOutAmount = 0;
  }


  sendBetRequest(isFirstBetInput: boolean) {
    if (!navigator.onLine) {
      this.showGenericPopup = true;
      this.showModal(this.configService.configData.networkErrorMessage);
      return;
    }
    if (this.matchStarted && !this.scoreStarted) {
      let request = {
        token: this.userDetails.accessToken,
        matchId: this.matchDetails.matchId,
        playerId: this.userDetails.customerId,
        betAmount: this.betAmount,
        autoCashOut: Math.round(this.cashOutAmount * 100),
        betSession: Math.floor(Math.random() * 899999 + 100000),
        gameName: this.gameCode,
        jurisdiction: this.configService.configData.url.userRegion,
        extraData: {
          "platformId": this.configService.configData.platformId,
          "operatorId": this.configService.configData.operatorId,
          "brand": this.configService.configData.brand,
          "region": this.configService.configData.url.userRegion,
          "currency": this.currencyCode,
          "gameId": this.gameCode,
          "gameType": this.gameCode,
          "playerName": this.configService.configData.playerName,
          "isFreeGame": this.configService.configData.isFreeGame
        }
      }
      if (this.configService.configData.url.isCertification) {
        let betObj = {
          "token": this.userDetails.accessToken,
          "playerId": this.userDetails.customerId,
          "playerName": this.configService.configData.playerName,
          "operatorId": this.configService.configData.operatorId,
          "isAutoCashOut": this.cashOutAmount ? true : false,
          "autoCashOutMultiplier": Math.round(this.cashOutAmount * 100),
          "gameName": this.gameCodeBackend,
          "betAmount": this.betAmount,
          "jurisdiction": this.configService.configData.url.userRegion,
          "extraData": {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": this.gameCode,
            "gameType": this.gameCode,
            "playerName": this.configService.configData.playerName,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        }
        let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games/" + this.matchDetails.matchId + "/bet";
        this.http.post(url, betObj).subscribe((res: any) => {

          if (isFirstBetInput) {
            this.fwpBetPanelComponent.betIdObject.firstBetId = res.id;
          } else {
            this.fwpBetPanelComponent.betIdObject.secondBetId = res.id;
          }

          let betPlacedObj = {
            "betId": res.id,
            "betRequest": {
              "betAmount": res.betAmount,
              "autoCashOut": res.autoCashOut,
              "matchId": res.gameId
            },
            "playerAccount": {
              "balance": res.playerAccount.balance,
              "playerId": res.playerAccount.playerId,
              "playerName": this.userDetails.playerName,
              "currencyCode": this.currencyCode
            }
          }
          this.currentBetObj = betPlacedObj;
          this.setBetSessionResponse(betPlacedObj);
        }, (err) => {
          console.log(err);
          if (err.error && err.error.message.indexOf("already started")>=0) {
            this.showModal(this.configService.configData.placeBetErrorMessage);
          } else {
            this.showModal(err.error.message);
          }
        })
      } else {
        this.socketService.sendFwpBetRequest(request);
      }
    }
  }

  getPlayerBets() {
    if (this.matchStarted) {
      if (this.configService.configData.url.isCertification) {
        let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games/" + this.matchDetails.matchId + "/bets?operatorId=" + this.configService.configData.operatorId + "&gameName=" + this.gameCodeBackend;
        this.http.get(url).subscribe((res: any) => {
          let playerBetsData = {
            "gameHistory": res.bets
          }
          for (let p in playerBetsData.gameHistory) {
            playerBetsData.gameHistory[p].result = null;
          }
          this.setPlayerBets(playerBetsData);
        })
      } else {
        this.socketService.getGameInit({
          matchId: this.matchDetails.matchId,
          playerId: null,
          gameName: this.gameCode,
          jurisdiction: this.configService.configData.url.userRegion,
          extraData: {
            "platformId": this.configService.configData.platformId,
            "operatorId": this.configService.configData.operatorId,
            "brand": this.configService.configData.brand,
            "region": this.configService.configData.url.userRegion,
            "currency": this.currencyCode,
            "gameId": this.gameCode,
            "gameType": this.gameCode,
            "playerName": this.configService.configData.playerName,
            "isFreeGame": this.configService.configData.isFreeGame
          }
        });
      }
    }
  }

  sendFwpDataRequest() {
    if (this.matchStarted) {
      this.socketService.sendFwpDataRequest({
        matchId: this.matchDetails.matchId
      });
    }
  }

  navigateToLobby(data: any) {
    this.onBackButtonClicked();
    if (this.configService.configData.isRGSSetup) {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({ action: "lobby" }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    } else {
      this.router.navigate([""]);
    }
  }

  sendLastTwentyGamesRequest() {
    if (this.configService.configData.url.isCertification) {
      let url = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/games/last20?gameName=" + this.gameCodeBackend;
      this.http.get(url).subscribe((res: any) => {
        let lastTwentyArr: any = [];
        for (let i = 0; i < res.games.length; i++) {
          let score = {
            "firstTeam": res.games[i].teamInfo.firstTeam.team,
            "firstTeamId": res.games[i].teamInfo.firstTeam.id,
            "secondTeam": res.games[i].teamInfo.secondTeam.team,
            "secondTeamId": res.games[i].teamInfo.secondTeam.id,
            "multiplier": res.games[i].crashMultiplier
          }
          res.games[i].score = JSON.stringify(score);
          lastTwentyArr.push(res.games[i]);
        }
        this.setLastTwentyGames(lastTwentyArr);
      })
    } else {
      this.socketService.sendLastTwentyGamesRequest({
        gameName: this.gameCode,
        extraData: {
          "operatorId": this.configService.configData.operatorId
        }
      });
    }
  }

  sendLastTenChatRequest() {
    this.socketService.sendLastTenChatRequest(this.configService.configData.operatorId);
  }

  updateCurrency(data: any) {
    this.currencyCode = data.currency;
    this.fwpBottomComponent.currencyCode = data.currency + " ";
  }

  sendChatRequest(data: any) {
    this.socketService.sendFwpChatRequest(data);
  }


  closeBetPanel(id: any) {
    this.showGenericPopup = false;
    document.getElementById('generic-popup')!.style.display = "none";
    // ($('#'+id) as any).modal('hide');
  }

  getClassOf(value: any) {
    if (value >= 1.00 && value <= 1.20) {
      return "bg-red";
    }
    else if (value >= 1.21 && value <= 1.99) {
      return "bg-orange";
    }
    else if (value >= 2.00 && value <= 4.99) {
      return "bg-yellow";
    }
    else if (value >= 5.00 && value <= 9.99) {
      return "bg-blue";
    }
    else if (value >= 10.00 && value <= 24.99) {
      return "bg-green";
    }
    else if (value >= 25.00 && value <= 49.99) {
      return "bg-white";
    }
    else if (value >= 50.00 && value <= 99.99) {
      return "bg-cream";
    }
    else {
      return "bg-pink";
    }

  }

  itemChange(event: any) {
    this.showBottomHistory = true;
    if (event === 'auto')
      this.showBottomHistory = false;
  }

  onInfoClicked() {
    this.fwpBottomComponent.showRules = true;
    this.sendIframeHeightToParentIframe('fwp-rules-page');
  }

  showFWPTermsPage() {
    this.fwpBottomComponent.showTerms = true;
    setTimeout(() => {
      this.setParentIframeHeight(document.getElementById('fwpTerms')?.clientHeight);
    }, 1000);
  }

  sendIframeHeightToParentIframe(selectElement: string) {
    setTimeout(() => {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "frameresize",
          scrollHeight: document.getElementById(selectElement)?.clientHeight,
          type: "howtoplay"
        }, (document.referrer || (window.parent && window.parent.location.href)));
      }
    }, 500);
  }

  setParentIframeHeight(height: any) {
    setTimeout(function () {
      if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
        window.parent.postMessage({
          action: "b2bresize",
          scrollHeight: height
        }, (document.referrer || (window.parent && window.parent.location.href)));

      }
    }, 1000);
  }

  updateBalance() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({ action: "b2bupdateBalance" }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }

  redirectToLogin() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage({
        action: "login"
      }, (document.referrer || (window.parent && window.parent.location.href)));
    }
  }

  onChatsClicked() {
    // console.warn("chats clicked");

    this.fwpBottomComponent.onChatsClicked('desktop');
  }

  showModal(message: any) {
    this.genericErrorMessage = message;
    this.showGenericPopup = true;
    document.getElementById('generic-popup')!.style.display = "block";
    // ($('#generic-popup') as any).modal();
  }

  setParentWindowWidth(width: any) {
    window.parent.postMessage({
      action: "setiframewidth",
      scrollWidth: width
    }, (document.referrer || (window.parent && window.parent.location.href)));
  }

  showAnimationOnCanvas() {
    this.showAnimation = !this.showAnimation;
    let showAnimationDetails = {
      message: "SHOW_ANIMATION",
      data: this.showAnimation
    };

    if (this.loadedIframe && this.loadedIframe.contentWindow) {
      this.loadedIframe.contentWindow.gameExtension(showAnimationDetails, "*");
    }

  }

  formatCurrency(amount: number, decimal = 2) {
    const result = amount;
    if (Intl) {
      var str = new Intl.NumberFormat(this.configService.configData.languageCode, {
        currencyDisplay: 'symbol',
        minimumFractionDigits: decimal,
        maximumFractionDigits: decimal
      }).format(result);

    } else {
      str = this.configService.configData.currencyCode + result;
    }
    if (this.configService.configData.currencyCode == "XAF") {

      let str1 = str.replace(/FCFA/g, "XAF")
      return str1;
    } else if (this.configService.configData.currencyCode == "BRL") {

      let str1 = str.replace("R$", "BRL")
      return str1;
    }
    return str;
  }

  getCurrencySymbol() {
    if(this.configService.configData.excludedCurrencies.indexOf(this.configService.configData.currencyCode)>=0) {
      this.currencySymbol = this.configService.configData.currencyCode;
      return this.configService.configData.currencyCode;
    } else {
      const formatter = new Intl.NumberFormat('locale', {
        style: 'currency',
        currency: this.configService.configData.currencyCode,
      });
  
      let symbol;
      formatter.formatToParts(0).forEach(({ type, value }) => {
        if (type === 'currency') {
          symbol = value;
        }
      });
  
      if (symbol == "FCFA") {
        symbol = "XAF";
      } else if (symbol == "R$") {
        symbol = "BRL";
      }
      this.currencySymbol = symbol;
  
      return symbol;

    }
  }

  loadImages() {
    if (this.preloaderStarted) {
      let img = new Image();
      img.src = 'assets/images/preloaderbg.jpg';
      img.decode()
        .then(() => {
          this.preloaderStarted = false;
          // document.body.style.backgroundColor = "initial";
          $('.menu').css('background-image', "url('../../assets/images/preloaderbg.jpg')");
          this.sendPreloadingCompleteToWrapper();
          // setTimeout(() => {
          //   document.getElementById('preloader')!.style.display = "none";
          // }, 1000);
        })
    } else {
      setTimeout(() => this.loadImages(), 10);
    }
  }
  sendPreloadingCompleteToWrapper() {
    if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
      window.parent.postMessage(
        {
          action: "preloadingComplete"
        },
        (document.referrer || (window.parent && window.parent.location.href))
      );
    }
  }
}
