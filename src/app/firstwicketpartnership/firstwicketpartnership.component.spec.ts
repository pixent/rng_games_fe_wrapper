import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstwicketpartnershipComponent } from './firstwicketpartnership.component';

describe('FirstwicketpartnershipComponent', () => {
  let component: FirstwicketpartnershipComponent;
  let fixture: ComponentFixture<FirstwicketpartnershipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirstwicketpartnershipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstwicketpartnershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
