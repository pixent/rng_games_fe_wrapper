import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FwphistoryComponent } from './fwphistory.component';

describe('FwphistoryComponent', () => {
  let component: FwphistoryComponent;
  let fixture: ComponentFixture<FwphistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FwphistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FwphistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
