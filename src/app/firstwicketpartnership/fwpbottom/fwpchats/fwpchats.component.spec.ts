import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FwpchatsComponent } from './fwpchats.component';

describe('FwpchatsComponent', () => {
  let component: FwpchatsComponent;
  let fixture: ComponentFixture<FwpchatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FwpchatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FwpchatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
