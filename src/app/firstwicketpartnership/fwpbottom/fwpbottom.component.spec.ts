import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FwpbottomComponent } from './fwpbottom.component';

describe('FwpbottomComponent', () => {
  let component: FwpbottomComponent;
  let fixture: ComponentFixture<FwpbottomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FwpbottomComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FwpbottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
