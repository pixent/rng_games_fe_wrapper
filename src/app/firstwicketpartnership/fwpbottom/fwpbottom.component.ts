import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { NgbDate, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";
import { FirstwicketpartnershipComponent } from '../firstwicketpartnership.component';
import { Subject, Subscription } from 'rxjs';
import { getCurrencySymbol } from '@angular/common';
import { FwpchatsComponent } from './fwpchats/fwpchats.component';
import { FwphistoryComponent } from './fwphistory/fwphistory.component';

@Component({
  selector: 'app-fwpbottom',
  templateUrl: './fwpbottom.component.html',
  styleUrls: ['./fwpbottom.component.css']
})
export class FwpbottomComponent implements OnInit {

  isSettingsTabOpen: boolean = false;
  resultData: any;
  resultDataRes: any;
  leaderboardData: any = [];
  bigWinsData: any = [];
  startDate: any;
  endDate: any;
  gameMode: any = 'Cashplay';
  matchDetails: any;
  showResultData: any = {};
  openChats: boolean = false;
  messageLength: Number = 0;
  maxWin: Number = 10;
  playerData: any = {};
  playerBets: any = [];
  leaderboardDataRes: any;
  chatData: any = [];
  message: any;
  playerBetsRes: any = [];
  currencyCode: any = 'USD';
  lastTenChats: any = [];
  showUnreadChats: boolean = true;
  chatsCount: any = 0;
  playerId: any;
  isMyId: boolean = false;
  showChatsOption: boolean = true;

  resetHistory: Subject<any> = new Subject<any>();

  @Input() updatePanel: Subject<any> = new Subject<any>();

  @ViewChild(FwpchatsComponent, { static: true }) fwpChatComponent!: FwpchatsComponent;
  @ViewChild(FwphistoryComponent, { static: true }) fwpHistoryComponent!: FwphistoryComponent;
  showRules: boolean = false;
  rules: any;
  showTerms: boolean = false;
  terms: any;
  openDesktopViewChats: boolean = false;
  languageURL: string = '';
  isPlayabet: boolean = false;
  isBetyetu: boolean = false;
  headings: any = {};
  buttonConfig: any = {};
  textConfig: any = {};
  inputConfig: any = {};
  isDemo: boolean = false;
  setAnimationOption: any;
  showAnimationTrue: boolean = false;
  currencySymbol: string | undefined;

  constructor(public configService: ConfigService, private http: HttpClient, calendar: NgbCalendar, public fwpComponent: FirstwicketpartnershipComponent) { }

  ngOnInit(): void {
    this.matchDetails = this.fwpComponent.matchDetails;
    this.getLeaderBoardData(false);
    // this.generatePlayerBetsData('bet');
    this.playerId = this.configService.configData.playerId;
    this.chatData = [];
    this.currencyCode = this.fwpComponent.currencyCode;

    this.currencySymbol = this.fwpComponent.currencySymbol;

    this.languageURL = this.doesFileExist();
    this.loadFWPRules();
    this.onChatsClicked('mobile');

    this.updatePanel.subscribe((response) => {
      // console.warn(" panel type " + response);

    })
    this.openChats = false;

    if (this.fwpComponent.configService.configData.platformId == '2') {
      this.showChatsOption = false;
    }

    if(this.fwpComponent.configService.configData.operatorId == "shabiki"){
      this.showChatsOption = true;
    }

    this.isPlayabet = this.fwpComponent.isPlayabet;
    this.isBetyetu = this.fwpComponent.isBetyetu;

    this.textConfig = this.fwpComponent.textConfig;
    this.buttonConfig = this.fwpComponent.buttonConfig;
    this.inputConfig = this.fwpComponent.inputConfig;
    this.setAnimationOption = this.configService.configData.defenderCashoutAnimationSelector[this.configService.configData.operatorId] ||
      this.configService.configData.defenderCashoutAnimationSelector["default"];
  }

  resetBetHistory() {
    this.openChats = false;
    this.fwpHistoryComponent.setCurrency();
    this.resetHistory.next(true);
  }

  settingsClicked() {
    this.isSettingsTabOpen = !this.isSettingsTabOpen;
    let setting = document.getElementById("openSetting");

    // if (this.isSettingsTabOpen) {
    //   if (setting!.style.display == 'none')
    //     setting!.style.display = 'block';
    //   document.onclick = function (e: any) {

    //     if (e.target.id !== 'openSetting' && e.target.id !== 'settingIcon') {
    //       setting!.style.display = 'none';
    //     }
    //   }
    // }
  }

  getBigWinsData() {
    this.openChats = false;
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59:59";
    if (this.startDate) {
      startDate = this.startDate.year + "-" + (this.startDate.month < 10 ? "0" + this.startDate.month : this.startDate.month) + "-" + (this.startDate.day < 10 ? "0" + this.startDate.day : this.startDate.day) + " 00:00:00";
      endDate = this.endDate.year + "-" + (this.endDate.month < 10 ? "0" + this.endDate.month : this.endDate.month) + "-" + (this.endDate.day < 10 ? "0" + this.endDate.day : this.endDate.day) + " 00:00:00";
    }
    let gameName = "FWP";
    if (this.gameMode == "Freeplay") {
      gameName = "freeFWP";
    }
    let bigWinsUrl = this.configService.configData.url.socket + "bigWinBets?gameName=" + gameName + "&operatorId=" + this.configService.configData.operatorId;

    if(this.configService.configData.url.isCertification){
      bigWinsUrl = this.configService.configData.url.apiUrl.split('/truewave')[0] + "/fwp/bets/big-win?operator=" + 
      this.configService.configData.operatorId;
    }

    this.http.get(bigWinsUrl).subscribe(
      (data: any) => {
        if(this.configService.configData.url.isCertification){
          data = data.bets;
        }  
        const bigWinsDataSorted = data.sort(function (a: any, b: any) {
          return b.winAmount - a.winAmount;
        })
        this.bigWinsData = bigWinsDataSorted;

        for (let data = 0; data < this.bigWinsData.length; data++) {
          this.bigWinsData[data].formattedBetAmount = this.fwpComponent.formatCurrency(this.bigWinsData[data].betAmount);
          this.bigWinsData[data].formattedWinAmount = this.fwpComponent.formatCurrency(this.bigWinsData[data].winAmount);
        }
      },
      (err: any) => {
        console.log(err);
      }
    )

  }

  getResultData(playerId: any) {
    this.isSettingsTabOpen = false;
    this.openChats = false;
    let date = new Date();
    let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00:00";
    let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59:59";
    if (this.startDate) {
      startDate = this.startDate.year + "-" + (this.startDate.month < 10 ? "0" + this.startDate.month : this.startDate.month) + "-" + (this.startDate.day < 10 ? "0" + this.startDate.day : this.startDate.day) + " 00:00:00";
      endDate = this.endDate.year + "-" + (this.endDate.month < 10 ? "0" + this.endDate.month : this.endDate.month) + "-" + (this.endDate.day < 10 ? "0" + this.endDate.day : this.endDate.day) + " 00:00:00";
    }
    let gameName = "FWP";
    if (this.gameMode == "Freeplay") {
      gameName = "freeFWP";
    }
    let leaderBoardUrl = this.configService.configData.url.socket + "virtualCatchTheTrendHistory?playerId=" + playerId + "&gameName=" + gameName + "&startDate=" + startDate + "&endDate=" + endDate;
    this.http.get(leaderBoardUrl).subscribe(
      (data: any) => {
        if (data) {
          data.sort(function (a: any, b: any) {
            return (new Date(b.createdOn).valueOf() - new Date(a.createdOn).valueOf());
          })
          for (let p = 0; p < data.length; p++) {
            let dateAndTime = data[p].createdOn.split("T");
            let date;
            let time;
            date = dateAndTime[0];
            time = dateAndTime[1].split(".")[0];

            data[p].date = date;
            data[p].time = time;
            let result = JSON.parse(data[p].result);
            data[p].multiplier = result.currentMultiplier;
          }
        }

        this.resultData = data;
        this.resultDataRes = data;
      },
      (err: any) => {
        console.log(err);
      }
    );
  }

  getLeaderBoardData(data: any) {
    this.openChats = false;
    let playerId = this.fwpComponent.matchDetails.playerId;
    this.isSettingsTabOpen = false;
    this.openChats = false;
    if (data) {
      this.leaderboardData.unshift(data);
    } else {
      let date = new Date();
      let currentMonth = date.getMonth() < 9 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
      let startDate = date.getFullYear() + "-" + currentMonth + "-01 00:00";
      let endDate = date.getFullYear() + "-" + currentMonth + "-" + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate() + " 23:59";
      let gameName = "FWP";

      if (this.gameMode == "Freeplay") {
        gameName = "freeFWP";
      }
      let leaderBoardUrl = this.configService.configData.url.socket + "leaderboard?gameName=" + gameName + "&startDate=" + startDate + "&endDate=" + endDate;

      this.http.get(leaderBoardUrl).subscribe(
        (data: any) => {
          this.leaderboardData = data;
          for (let p = 0; p < this.leaderboardData.length; p++) {
            let multiplier = this.leaderboardData[p].points * 0.16;
            let playerId = this.leaderboardData[p].playerId;
            if (multiplier > 10) {
              while (multiplier > 10) {
                multiplier = multiplier / 2;
              }
              this.leaderboardData[p].multiplier = multiplier;
            } else {
              this.leaderboardData[p].multiplier = multiplier;
            }
            this.leaderboardData[p].betAmount = Math.round(this.leaderboardData[p].points * 1.25);
          }
          for (let player = 0; player < this.playerBets.length; player++) {

            this.playerBets[player].formattedBetAmount = this.fwpComponent.formatCurrency(this.playerBets[player].betAmount);
            this.playerBets[player].formattedwinAmount = this.fwpComponent.formatCurrency(this.playerBets[player].winAmount);
            this.leaderboardData.unshift(this.playerBets[player]);
          }
        }
      )

    }
  }

  // generatePlayerBetsData(flag: any) {
  //   this.playerBets = [];
  //   for (let player = 0; player < 10; player++) {
  //     let playerObj = {};
  //     let playerId = Math.floor(100000 + Math.random() * 900000);
  //     let playerName = playerId;
  //     let betAmount = (Math.floor(Math.random() * 11) * 10) || 10;
  //     let winAmount: any = '-';
  //     let points: any = '-';
  //     let multiplier: any = 0;
  //     if (flag == 'win') {
  //       winAmount = Math.floor(Math.random() * 200);
  //       points = Math.floor(Math.random() * 100) + 100;
  //       multiplier = points * 0.16;
  //       if (multiplier > 10) {
  //         multiplier = multiplier / 2;
  //       }
  //     }
  //     playerObj = {
  //       playerId,
  //       playerName,
  //       betAmount,
  //       winAmount,
  //       multiplier: multiplier,
  //       points
  //     }
  //     this.playerBets.push(playerObj);
  //   }
  //   this.playerBetsRes = [...this.playerBets];
  // }

  playerBetsUpdate(data: any) {
    this.playerBets = [...this.playerBetsRes];
    let playerData = data.gameHistory;
    for (let p in playerData) {
      let playerResult = playerData[p];
      // if(playerResult) {
      //   debugger;
      // }
      let obj = {
        playerId: playerData[p].playerId,
        playerName: playerData[p].playerName,
        betAmount: playerData[p].betAmount,
        formattedBetAmount: this.fwpComponent.formatCurrency(playerData[p].betAmount),
        winAmount: playerData[p].winAmount,
        formattedWinAmount: this.fwpComponent.formatCurrency(playerData[p].winAmount),
        multiplier: playerResult ? playerResult.cashedOutMultiplier / 100 : 0,
        points: playerData[p].winAmountAfterTax ? playerData[p].winAmountAfterTax.toFixed(2) : '-',
        formattedPoints: playerData[p].winAmountAfterTax ? this.fwpComponent.formatCurrency(playerData[p].winAmountAfterTax) : '-'
      };
      this.playerBets.unshift(obj);

      this.playerBets = this.playerBets.sort(function (a: any, b: any) {
        if (a.winAmount == 0 && b.winAmount == 0) {
          return (b.betAmount - a.betAmount);
        } else if (a.winAmount > 0 || b.winAmount > 0) {
          return (b.winAmount - a.winAmount);
        } else {
          return (b.betAmount - a.betAmount);
        }
      })

      let myPlayerIndex;

      let myPlayer = this.playerBets.find((item: any, index: number) => {
        if (item.playerId === this.fwpComponent.matchDetails.playerId) {
          myPlayerIndex = index;
          return item;
        }
      })
      if (myPlayer) {
        this.playerBets.splice(myPlayerIndex, 1);
        this.playerBets.unshift(myPlayer);
      }
    }
  }

  onChatResponse(data: any) {
    this.isMyId = false;
    if (data.message.split("@")) {
      let mentionedId = data.message.split("@")[1];
      if (mentionedId && mentionedId.split(" ")[0] == this.fwpComponent.matchDetails.playerId) {
        this.isMyId = true;
      }
    }


    if (!this.openChats) {
      this.showUnreadChats = true;
      this.chatsCount++;
    }
    this.chatData.push(data);
  }

  onChatRequest(message: any) {
    let data = {
      "playerId": this.fwpComponent.matchDetails.playerId,
      "message": message,
      "operatorId": this.configService.configData.operatorId,
      "gameName": "FWP"
    }
    this.fwpComponent.sendChatRequest(data);
  }

  onChatsClicked(type: string) {
    this.openChats = true;
    this.showUnreadChats = false;
    this.chatsCount = 0;
    this.isMyId = false;

    if (type == 'desktop') {
      this.openDesktopViewChats = true;
    }
  }

  onLastTenChatResponse(data: any) {
    this.chatData = [];
    for (let msg of data) {
      this.chatData.unshift(msg);
    }
    if (this.fwpChatComponent) {
      this.fwpChatComponent.setChatData();
    }
  }

  checkWordLength(value: string) {
    this.message = value;
    this.messageLength = value.length;
  }

  enterMessage() {
    this.onChatRequest(this.message);
  }

  showFWPRulesPage() {

    this.showRules = true;

    if (this.fwpComponent.isPlayabet && this.fwpComponent.isBetyetu) {

      document.getElementById('fwp-rules-page')!.style.maxWidth = "500px";
    }

    this.fwpComponent.sendIframeHeightToParentIframe('fwp-rules-page');

    this.isSettingsTabOpen = false;
  }

  showFWPTermsPage() {
    this.showTerms = true;
    this.isSettingsTabOpen = false;

    // document.getElementById('fwp-bottom-component-container')!.style.display = "none";

    if (this.configService.configData.operatorId === "demo") {
      this.isDemo = true;
    }

    this.fwpComponent.setParentIframeHeight(document.getElementById('fwpTerms')?.clientHeight);
  }

  doesFileExist(): string {

    let language = this.configService.configData.languageCode || "en";
    const jurisdiction = this.configService.configData.url.userRegion.toLowerCase();
    const defaultPath = "./assets/config/in/en/fwpRules.json";

    // console.log("test", language, jurisdiction);
    let jsonFileName = 'fwpRules';

    if (!language || jurisdiction) {
      if (jurisdiction) {
        if (this.configService.configData.operatorId == "paribet") {
          if (this.currencyCode == "UGX") {
            jsonFileName = 'defenderRulesParibetUGX';
          } else {
            jsonFileName = 'defenderRulesParibet';
          }
        } else {
          const fileName = this.getUppercaseOperatorName(this.configService.configData.operatorId);
          jsonFileName = `defenderRules${fileName}`;
        }
      } else
        return defaultPath;
    }

    let path = `./assets/config/${jurisdiction.toLowerCase()}/${language.toLowerCase()}/${jsonFileName}.json`;
    // let path = `../../../assets/config/tz/sh/psrules.json`;
    // console.warn("path ", path);

    let xhr = new XMLHttpRequest();
    xhr.open("HEAD", path, false);
    xhr.send();

    if (xhr.status === 404) {
      // console.log("test-doesFileExist", "fail");
      return defaultPath;
    } else {
      // console.log("test-doesFileExist", "success");
      return path;
    }
  }
  getUppercaseOperatorName(operatorId: string) {
    const str = operatorId;
    const str2 = str.charAt(0).toUpperCase() + str.slice(1);
    return str2;
  }

  loadFWPRules() {
    return new Promise((resolve, reject) => {
      this.http.get('assets/config/in/en/fwpRules.json')
        .subscribe((data: any) => {

          this.rules = data['howtoplayfwprules'];
          this.terms = data['fwpTerms'];
          this.headings = data['headings'];
          this.rules = this.changeCurrencySymbolsUsingRegex(this.rules);
          resolve(true);
        }, (err: any) => {
          this.http.get('assets/config/in/en/fwpRules.json')
            .subscribe((data: any) => {

              this.rules = data['howtoplayfwprules'];
              this.terms = data['fwpTerms'];
              this.headings = data['headings'];
              this.rules = this.changeCurrencySymbolsUsingRegex(this.rules);
              resolve(true);
            })
        });
    });
  }

  changeCurrencySymbolsUsingRegex(rules: any): any {
    
    for(let rule of rules) {
      const originalText = rule.text;
      const newText = originalText.replace(/Rs.|Rs/g, this.currencySymbol);
      rule.text = newText;
    }

    return rules;
  }

  showFWPTerms() {
    this.showTerms = true;
  }

  onClose() {
    this.showRules = false;
    this.showTerms = false;
    // document.getElementById('fwp-bottom-component-container')!.style.display = "flex";
    this.fwpComponent.setIframeHeight();
  }

  removeChatSection() {
    this.openDesktopViewChats = false;
  }

  showAnimation() {
    this.showAnimationTrue = !this.showAnimationTrue;
    // event.preventDefault();
    this.fwpComponent.showAnimationOnCanvas();
  }

  setCurrencyCode(data: any) {
    this.currencySymbol = this.fwpComponent.currencySymbol;
    if (data == "XAF") {
      this.currencyCode = data;
    } else {
      this.currencyCode = getCurrencySymbol(data, "narrow");
    }
    this.loadFWPRules();
  }

}
