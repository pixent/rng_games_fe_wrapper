// @ts-nocheck
import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../services/config.service';
import * as moment from 'moment-timezone';
import { SocketService } from '../services/socket.service';
import { FwpService } from '../services/fwp.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {

  lobbyDetails: any;
  catchTheTrendDetails: any;
  pick10Details: any;
  penaltyShootoutDetails: any;
  fwpDetails: any;
  defenderDetails: any;
  homeRunDetails: any;
  timeoutVal: any = [];
  stopInterval: boolean = true;
  loginState: boolean = false;
  socketFlag: boolean = true;
  fwpMatchDetails: any;
  messageSubscription: any;

  constructor(private http: HttpClient, public configService: ConfigService, private route: ActivatedRoute, private router: Router, private socketService: SocketService, private fwpService: FwpService) {
    this.messageSubscription = this.fwpService.messageSubject.subscribe((message) => {
      this.fwpServerResponse(message);
    });
  }

  ngOnInit(): void {
    if (window.location.href.indexOf('pixent') < 0 && window.location.href.indexOf('localhost') < 0 && window.location.href.indexOf('/lobby') < 0) {
      if (this.configService.configData.isRGSSetup && (window.history.state.gameName != "Virtual Catch the Trend")) {
        if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
          window.parent.postMessage({ action: "lobby" }, (document.referrer || (window.parent && window.parent.location.href)));
        }
      }
      if (!window.history.state.gameName || window.history.state.gameName == "Penalty Shootout") {
        this.router.navigate(['home/penaltyshootout']);
      }
    }
    if (window.location.href.indexOf("https://dev-games-rgs.sportsit-tech.net/lobby") >= 0) {
      if (sessionStorage.getItem('gameName') != "Virtual Catch the Trend" || !window.history.state.gameName) {
        if (!(window.history.state.gameName && window.history.state.gameName == "All")) {
          this.router.navigate(["login"]);
          return;
        }
      }
    }
    this.setGameDetails();
    if (this.socketService) {
      this.socketService.onDisconnected();
    }
    document.body.style.setProperty("background-color", "#fff", "important");
  }

  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    if (window.location.href.indexOf("localhost") >= 0) {
      if (window.history.state.navigationId != 2) {
        this.router.navigate(["login"]);
      }
    }
  }

  ngAfterViewInit(): void {
    this.checkForRoutes();
  }

  ngOnDestroy() {
    // if(this.socketService) {
    //   this.socketService.disconnectSocket();
    // }
    // this.messageSubscription.unsubscribe();
  }

  onPlayClicked(item: any, flag) {
    item.isReal = flag;
    if (item.gameName == "Virtual Catch the Trend") {
      item.gameType = "virtualEventScore";
      this.router.navigate(['home/catchthetrend'], {
        state: { item: item }
      });
    } else if (item.gameName == "Catch the Trend") {
      item.gameType = "liveEventScore";
      this.router.navigate(['home/catchthetrend'], {
        state: { item: item }
      });
    } else if (item.gameName == "Penalty Shootout") {
      this.router.navigate(['home/penaltyshootout'], {
        state: { item: item }
      });
    } else if (item.gameName == "First Wicket Partnership") {
      // this.socketService.onDisconnected();
      this.router.navigate(['home/firstwicketpartnership'], {
        state: { item: item }
      });
      // this.checkForLatestMatch();
    } else if (item.gameName == "Defender") {
      // this.socketService.onDisconnected();
      this.router.navigate(['home/defender'], {
        state: { item: item }
      });
    } else if (item.gameName == "Home Run") {
      // this.socketService.onDisconnected();
      this.router.navigate(['home/homerun'], {
        state: { item: item }
      });
    } else if (item.gameName == "Match Predictor") {
      // this.socketService.onDisconnected();
      this.router.navigate(['home/iframe'], {
        state: { item: item }
      });
    } else {
      this.router.navigate(['home'], {
        state: { item: item }
      });
    }
  }

  fwpServerResponse(message: any) {
    if (message == "socket connected") {

    } else {
      switch (message.message) {
        case "newmatch":
          this.fwpMatchDetails = { ...message.data };
          break;
        default:
      }
    }
  }

  // connectFWPSocket(matchDetails) {
  //   if (this.socketFlag) {
  //     this.socketService.connectToServer(
  //       this.configService.configData.url.socket,
  //       "firstWicketPartnership",
  //       matchDetails.playerId,
  //       matchDetails.matchId
  //     );
  //     this.socketFlag = false;
  //   }
  // }

  // checkForLatestMatch() {
  //   let url = this.configService.configData.url.socket + "lobby";
  //   this.http.get(url).subscribe((res) => {
  //     let result: any = res;
  //     let results: any = [];
  //     results = result.filter((data: any) => (
  //       data.gameName && data.gameName.includes("firstWicketPartnership"))
  //     );
  //     let matchDetails: any = { ...results[0] };
  //     matchDetails.status = 1;
  //     matchDetails.demoPlay = false;
  //     matchDetails.gameStatus = matchDetails.status;
  //     let playerId = localStorage.getItem('playerId');
  //     if (playerId) {
  //       matchDetails.playerId = playerId;
  //     } else {
  //       matchDetails.playerId = Math.floor(Math.random() * 899999 + 100000);
  //     }
  //     this.router.navigate(['home/firstwicketpartnership'], {
  //       state: { item: matchDetails }
  //     });
  //   })
  // }

  setGameDetails() {
    let url = this.configService.configData.url.socket + "lobby" + "?operatorId=" + ((this.configService.configData.defender.operatorSeparation.indexOf(this.configService.configData.operatorId) >= 0) ? this.configService.configData.operatorId : "default");
    this.http.get(url).subscribe((res) => {
      let result: any = res;
      this.lobbyDetails = [];
      this.stopInterval = true;
      let serverDate = moment.tz(new Date(), "Asia/Kolkata").format();
      if (this.configService.configData.isRGSSetup) {
        serverDate = moment.tz(new Date(), "UTC").format();
      }
      let platformPlayerId: any = "";
      let params: any = document.cookie;
      if (params) {
        params = params.split("; ");
        for (let i = 0; i < params.length; i++) {
          let arr = params[i].split("=");
          if (arr[0] == "userId") {
            platformPlayerId = arr[1];
          }
        }
      }
      for (let p in result) {
        let currentDate = moment(serverDate.split('+')[0]);
        let resultDate = moment(result[p].startDate + 'Z');
        // if (result[p].matchId == "6499") {
        //   debugger;
        // }
        result[p].gameName = this.configService.configData.literals[result[p].gameName];
        if (!result[p].firstTeamId && !result[p].secondTeamId) {
          result[p].firstTeamId = this.configService.configData.literals[result[p].firstTeam];
          result[p].secondTeamId = this.configService.configData.literals[result[p].secondTeam];
        }
        if (this.configService.configData.literals[result[p].firstTeamId]) {
          result[p].firstTeamId = this.configService.configData.literals[result[p].firstTeamId]
        }
        if (this.configService.configData.literals[result[p].secondTeamId]) {
          result[p].secondTeamId = this.configService.configData.literals[result[p].secondTeamId]
        }
        result[p].jackpot = "10,00,000";
        result[p].demoPlay = true;
        result[p].league = this.configService.configData.literals[result[p].seriesName] ? this.configService.configData.literals[result[p].seriesName] : result[p].seriesName;
        let playerId = this.configService.configData.playerId;
        if (playerId) {
          result[p].playerId = playerId;
        } else {
          result[p].playerId = Math.floor(Math.random() * 899999 + 100000);
        }
        if (platformPlayerId) {
          result[p].playerId = platformPlayerId;
        }
        if (result[p].gameName == "First Wicket Partnership") {
          result[p].status = 1;
          result[p].demoPlay = false;
          result[p].gameStatus = result[p].status;
          this.fwpMatchDetails = { ...result[p] };
          // this.connectFWPSocket(result[p]);
        }
        if (result[p].gameName == "Defender") {
          result[p].status = 1;
          result[p].demoPlay = false;
          result[p].gameStatus = result[p].status;
          this.fwpMatchDetails = { ...result[p] };
        }
        if (result[p].gameName == "Home Run") {
          result[p].status = 1;
          result[p].demoPlay = false;
          result[p].gameStatus = result[p].status;
          this.fwpMatchDetails = { ...result[p] };
        }
        /* enable for socket in the game */
        if (result[p].gameName == "Virtual Catch the Trend" || result[p].gameName == "Catch the Trend") {
          result[p].socketFlag = false;
        } else {
          result[p].socketFlag = true;
        }
        if (moment(currentDate).isBefore(resultDate)) {
          let dateDiff = moment.duration(currentDate.diff(resultDate));
          result[p].date = (-dateDiff._data.hours) + "hr : " + (-dateDiff._data.minutes) + "m : " + (-dateDiff._data.seconds) + "s";
          this.lobbyDetails.push(result[p]);
        } else {
          if (result[p].status == 1) {
            result[p].date = "LIVE";
            this.lobbyDetails.push(result[p]);
          }
        }
      }
      if (this.lobbyDetails.length == 1) {
        this.lobbyDetails.push({
          "matchId": 1200031,
          "gameName": "Virtual Catch the Trend",
          "firstTeam": "CSK",
          "secondTeam": "KKR",
          "league": "Indian Premier League",
          "jackpot": 200000,
          "date": "Completed"
        });
      }
      this.lobbyDetails.push({
        "date": "8hr : 50m : 48s",
        "firstTeam": "Manchester United",
        "firstTeamId": "MUN",
        "gameName": "Penalty Shootout",
        "isReal": false,
        "jackpot": "10,00,000",
        "league": "English Premier League",
        "matchId": 411377,
        "playerId": 646339,
        "secondTeam": "Liverpool",
        "secondTeamId": "LIV",
        "startDate": "2021-05-25T23:47:00",
        "status": 1,
        "demoPlay": false
      })

      this.lobbyDetails.push({
        "date": "8hr : 50m : 48s",
        "gameName": "Defender",
        "isReal": false,
        "jackpot": "10,00,000",
        "league": "English Premier League",
        "matchId": 411377,
        "playerId": this.configService.configData.playerId || 623535,
        "gameCode": "defender",
        "startDate": "2021-05-25T23:47:00",
        "status": 1,
        "demoPlay": false
      })
      this.lobbyDetails.push({
        "date": "8hr : 50m : 48s",
        "gameName": "Home Run",
        "isReal": false,
        "jackpot": "10,00,000",
        "league": "English Premier League",
        "matchId": 411377,
        "playerId": this.configService.configData.playerId || 623535,
        "gameCode": "homerun",
        "startDate": "2021-05-25T23:47:00",
        "status": 1,
        "demoPlay": false
      })
      this.lobbyDetails.push({
        "date": "8hr : 50m : 48s",
        "firstTeam": "Manchester United",
        "firstTeamId": "MUN",
        "gameName": "Match Predictor",
        "isReal": false,
        "jackpot": "10,00,000",
        "league": "English Premier League",
        "matchId": 411377,
        "playerId": this.configService.configData.playerId || 623535,
        "gameCode": "matchpredictor",
        "secondTeam": "Liverpool",
        "secondTeamId": "LIV",
        "startDate": "2021-05-25T23:47:00",
        "status": 1,
        "demoPlay": false
      })
      // this.lobbyDetails.push({
      //   "date": "8hr : 50m : 48s",
      //   "firstTeam": "Mumbai Indians",
      //   "firstTeamId": "MI",
      //   "gameName": "First Wicket Partnership",
      //   "isReal": false,
      //   "jackpot": "10,00,000",
      //   "league": "Indian Premier League",
      //   "matchId": 411377,
      //   "playerId": localStorage.getItem('playerId') ? localStorage.getItem('playerId') : Math.floor(Math.random() * 899999 + 100000),
      //   "secondTeam": "Chennai Super Kings",
      //   "secondTeamId": "CSK",
      //   "startDate": "2021-05-25T23:47:00",
      //   "status": 1,
      //   "demoPlay": false
      // })
      // debugger;
      this.lobbyDetails.sort(function (a, b) {
        return b.status - a.status;
      });
      // this.lobbyDetails.sort(function (a, b) {
      //   return a.startDate - b.startDate
      // });
      // this.lobbyDetails = this.lobbyDetails.filter((data: any) => (
      //   data.gameName.includes("Virtual Catch the Trend"))
      // );
      this.lobbyDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && !data.gameName.includes("Pick 10") && !data.league.includes("T20 World Cup")
      ))
      this.pick10Details = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("Pick 10"))
      );
      this.pick10Details.sort(function (a, b) {
        return (new Date(a.startDate)) - (new Date(b.startDate))
      });
      this.catchTheTrendDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("Catch the Trend"))
      );
      this.penaltyShootoutDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("Penalty Shootout"))
      );
      this.fwpDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("First Wicket Partnership"))
      );
      this.defenderDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("Defender"))
      );
      this.homeRunDetails = this.lobbyDetails.filter((data: any) => (
        data.gameName && data.gameName.includes("Home Run"))
      );

      if (window.history.state.gameName) {
        let game = window.history.state.gameName;
        if (game != "All") {
          if (window.history.state.gameName.includes('Virtual Catch the Trend') || window.history.state.gameName.includes('Catch the Trend')) {
            this.lobbyDetails = this.lobbyDetails.filter((data: any) => (
              data.gameName && data.gameName.includes('Catch the Trend'))
            );
          } else {
            this.lobbyDetails = this.lobbyDetails.filter((data: any) => (
              data.gameName && data.gameName.includes(window.history.state.gameName))
            );
          }
        }
      }

      let resultData = this.lobbyDetails;
      for (let p in this.timeoutVal) {
        if (this.timeoutVal[p]) {
          clearInterval(this.timeoutVal[p].interval);
        }
      }
      for (let p in resultData) {
        let interval = setInterval(() => this.setCountdown(resultData, resultData[p], p), 1000);
        this.timeoutVal.push({
          interval: interval,
          result: resultData
        });
      }
      setTimeout(function () {
        if ((document.referrer && document.referrer.indexOf('localhost') < 0) || (window.parent && window.parent.location.origin)) {
          if ((document.referrer && document.referrer.indexOf('localhost') < 0)) {
            window.parent.postMessage({
              action: "b2bresize",
              scrollHeight: document.getElementById('all').clientHeight + 60
            }, (document.referrer || (window.parent && window.parent.location.href)));
            document.body.style.overflow = "auto";
          }
        }
      }, 1000);
    });

  }

  setCountdown(result, resultValue, p) {
    let serverDate = moment.tz(new Date(), "Asia/Kolkata").format();
    let currentDate = moment(serverDate.split('+')[0]);
    let resultDate: any = '';
    if (this.configService.configData.isRGSSetup) {
      serverDate = moment.tz(new Date(), "UTC").format();
      resultDate = moment(resultValue.startDate + 'Z');
    } else {
      resultDate = moment(resultValue.startDate);
    }
    if (resultValue.status == 1) {
      this.lobbyDetails[p].date = "LIVE";
    } else {
      let dateDiff = moment.duration(currentDate.diff(resultDate));
      if ((-dateDiff._data.seconds) < 0) {
        if (!(document.getElementById('iframeElement'))) {
          for (let p in this.timeoutVal) {
            if (this.timeoutVal[p] && this.timeoutVal[p].result.date == "0hr : 0m : 0s") {
              clearInterval(this.timeoutVal[p].interval);
            }
          }
          if (this.stopInterval) {
            this.setGameDetails();
            this.stopInterval = false;
          }
        }
      } else {
        this.lobbyDetails[p].date = (-dateDiff._data.hours) + "hr : " + (-dateDiff._data.minutes) + "m : " + (-dateDiff._data.seconds) + "s";
      }
    }
  }

  checkForRoutes() {
    setTimeout(function () {
      if (window.history.state.game) {
        let game = window.history.state.game;
        if (game == "Pick 10") {
          $('#pick10element').click();
        } else if (game == "Penalty Shootout") {
          $('#penaltyshootoutelement').click();
        } else {

        }
      }
    }, 10);
  }

  onNavClicked(id: any) {
    let matchDetails = {
      "playerId": this.configService.configData.playerId
    }
    if (id == 'bethistoryvctt') {
      // $('#bethistoryvctt').click();
      this.router.navigate(['/home/catchthetrend/historycatchtrend'], { state: { item: matchDetails, redirect: "main" } });
    } else if (id == "leaderboardvctt") {
      this.router.navigate(['/home/catchthetrend/leaderboardcatchtrend'], { state: { item: matchDetails, redirect: "main" } });
    } else if (id == "rulesvctt") {
      this.router.navigate(['/home/catchthetrend/howtoplaycatchtrend'], { state: { item: matchDetails, redirect: "main" } });
    } else {

    }
  }

}
