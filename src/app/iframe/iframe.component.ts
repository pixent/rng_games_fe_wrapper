import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from '../services/config.service';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.css']
})
export class IframeComponent implements OnInit {

  url: any;
  iframeUrl: any;
  matchDetails: any;

  constructor(private route: ActivatedRoute, private router: Router, private configService: ConfigService, public sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.matchDetails = window.history.state.item || {};
    // console.log(this.matchDetails);
    if (window.location.href.indexOf("?") >= 0) {
      let urlStr = window.location.href.split("?")[1].split("&");
      let urlParams: any = {};
      for (let i = 0; i < urlStr.length; i++) {
        let params = urlStr[i].split("=");
        if (params[0] == "playerId") {
          urlParams.playerId = params[1];
        }
      }
      this.matchDetails.playerId = urlParams.playerId;
    }
    // if (this.matchDetails) {
    //   localStorage.setItem("matchDetails", JSON.stringify(this.matchDetails));
    // }
    // if (localStorage.getItem("matchDetails")) {
    //   this.matchDetails = JSON.parse(localStorage.getItem("matchDetails")!);
    // }
    if (!this.matchDetails.playerId) {
      this.matchDetails.playerId = Math.floor(Math.random() * 899999 + 100000);
    }
    // if (localStorage.getItem('playerId') != this.matchDetails.playerId) {
    //   localStorage.setItem('playerId', this.matchDetails.playerId)
    // }
    this.url = this.configService.configData.url.iframe + "?gameCode=" + (this.matchDetails.gameCode || 'matchpredictor') + "&playerId=" + this.matchDetails.playerId + "&token=" + this.getUUid() + "&languageCode=en-US&currencyCode=INR&platformId=0&operatorId=demo&brand=demo&gameType=CASH/FREE"
    this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  getUUid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
