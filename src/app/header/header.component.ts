import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.checkRoute();
  }

  checkRoute() {
    $('#top-nav').css('display', 'flex');
    if (this.router.url.indexOf('catchthetrend') >= 0) {
      $('#allelement').removeClass('active');
      if (this.router.url.indexOf('history') >= 0) {
        $('#bethistoryvcttelement').addClass('active');
      } else if (this.router.url.indexOf('leaderboard') >= 0) {
        $('#leaderboardvcttelement').addClass('active');
      } else if (this.router.url.indexOf('howtoplay') >= 0) {
        $('#rulesvcttelement').addClass('active');
      } else {
        $('#allelement').addClass('active');
      }
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('penaltyshootout') >= 0) {
      $('#allelement').removeClass('active');
      $('#penaltyshootoutelement').addClass('active');
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('firstwicketpartnership') >= 0) {
      $('#allelement').removeClass('active');
      $('#firstwicketpartnershipelement').addClass('active');
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('defender') >= 0) {
      $('#allelement').removeClass('active');
      $('#defenderelement').addClass('active');
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('homerun') >= 0) {
      $('#allelement').removeClass('active');
      $('#homerunelement').addClass('active');
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('iframe') >= 0) {
      $('#allelement').removeClass('active');
      $('#iframeelement').addClass('active');
      $('#top-nav').css('display', 'none');
    } else if (this.router.url.indexOf('home') >= 0) {
      $('#allelement').addClass('active');
    }
  }

  onBetHistoryClicked(event: any) {
    this.router.navigate(['/home/catchthetrend/historycatchtrend'], { state: { item: "Lobby" } });
  }

  onAllClicked(event: any) {
    this.router.navigate(['/'], { state: { game: "All" } });
  }

  onLeaderboardClicked(event: any) {
    this.router.navigate(['/home/catchthetrend/leaderboardcatchtrend'], { state: { item: "Lobby" } });
  }

  onRulesClicked(event: any) {
    this.router.navigate(['/home/catchthetrend/howtoplaycatchtrend'], { state: { item: "Lobby" } });
  }

  addActive(event: any) {
    $(event.target.closest('li')).addClass('active').siblings('li').removeClass('active');
  }

}
