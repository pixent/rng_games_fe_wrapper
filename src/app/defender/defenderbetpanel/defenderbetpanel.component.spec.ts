import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefenderBetPanelComponent } from './defenderbetpanel.component';

describe('DefenderBetPanelComponent', () => {
  let component: DefenderBetPanelComponent;
  let fixture: ComponentFixture<DefenderBetPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefenderBetPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefenderBetPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
