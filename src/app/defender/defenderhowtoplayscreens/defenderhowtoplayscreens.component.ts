import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { DefenderComponent } from '../defender.component';

@Component({
  selector: 'app-defenderhowtoplayscreens',
  templateUrl: './defenderhowtoplayscreens.component.html',
  styleUrls: ['./defenderhowtoplayscreens.component.css']
})
export class DefenderhowtoplayscreensComponent implements OnInit {
  imagesConfig: any;
  showHTPScreens: boolean = false;
  showHTPScreensInDesktop: boolean = false;

  constructor(private defenderComponent: DefenderComponent,public configService: ConfigService) { }

  ngOnInit(): void {

    this.imagesConfig = this.configService.textConfigData["htpScreensImagesPath"];
    // console.log("images config ", this.imagesConfig);
    if(!this.configService.configData.isMobile){
      this.showHTPScreensInDesktop = true;
    }

    // (<any>$(".carousel")).carousel();
  }

  onHowToPlaySkipClicked() {
    this.showHTPScreens = false;
    let skipCount = localStorage.getItem("howToPlaySkipCount");
    this.defenderComponent.showHowToPlayFullScreens = false;
    this.defenderComponent.setIframeHeight();
    localStorage.setItem("howToPlaySkipCount", (Number(skipCount)+1).toString());
  }

}
