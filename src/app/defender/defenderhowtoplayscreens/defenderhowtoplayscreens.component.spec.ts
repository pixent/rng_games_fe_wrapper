import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefenderhowtoplayscreensComponent } from './defenderhowtoplayscreens.component';

describe('DefenderhowtoplayscreensComponent', () => {
  let component: DefenderhowtoplayscreensComponent;
  let fixture: ComponentFixture<DefenderhowtoplayscreensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefenderhowtoplayscreensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefenderhowtoplayscreensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
