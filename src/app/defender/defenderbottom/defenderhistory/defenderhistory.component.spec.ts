import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefenderHistoryComponent } from './defenderhistory.component';

describe('DefenderHistoryComponent', () => {
  let component: DefenderHistoryComponent;
  let fixture: ComponentFixture<DefenderHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefenderHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefenderHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
