import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefenderBottomComponent } from './defenderbottom.component';

describe('DefenderBottomComponent', () => {
  let component: DefenderBottomComponent;
  let fixture: ComponentFixture<DefenderBottomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DefenderBottomComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefenderBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
